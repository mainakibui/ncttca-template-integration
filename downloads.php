<?php
require_once("includes/config.php");
require_once("includes/analyticstracking.php");
$mtitle = "Document Center";

$docid = clean($_GET["docid"]);
$docname = clean($_GET["document_name"]);
$docfrom = $_GET["from"]!="" ? clean($_GET["from"])." 00:00:00" : "";
$docto = $_GET["to"]!="" ? clean($_GET["to"])." 00:00:00" : "";
$doctype = clean($_GET["document_type"]);

if (!is_numeric($docid)) {
	$docid = 0;
}
else {
	$docid = $docid + 0;
}

	$mystr = $mystr."<div class='tab-content'>";
	$mystr = $mystr."<div class='row panel4 clearfix'>";
	    $mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12 nopadding'>";
	    
	    		$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12 nopadding bg-lightgrey'>";
	    		
				$mystr = $mystr."<div class='data-filter'><!--data filters-->";
				
					$mystr = $mystr."<form action='downloads.php' method='get'>";
						$mystr = $mystr."<div class='input-group indicator-1 pull-right'>";
							$mystr = $mystr."<button class='btn btn-primary btn-sm mainnav-form-btn'>Search</i></button>";
						$mystr = $mystr."</div>";
						
						$mystr = $mystr."<div class='input-group indicator-1 pull-right'>";
			
							$mystr = $mystr."<div class='btn-group'>";
								$mystr = $mystr."<select id='document_type' name='document_type' class='form-control'>";
									$mystr = $mystr."<option value='all-reports'>All reports</option>";
									$mystr = $mystr."<option value='top-reports'>TOP reports</option>";
									$mystr = $mystr."<option value='other-reports'>Other reports</option>";
								$mystr = $mystr."</select>";
							$mystr = $mystr."</div>";											
						
						$mystr = $mystr."</div>";
						
						$mystr = $mystr."<div class='input-group indicator-1 pull-right'>";
							$mystr = $mystr."<input readonly id='to' name='to' type='text' placeholder='To' class='date-picker form-control input-indicator-1'  value='".str_replace(" 00:00:00","",$docto)."' >";
						$mystr = $mystr."</div>";
						
						$mystr = $mystr."<div class='input-group indicator-1 pull-right'>";
							$mystr = $mystr."<input readonly id='from' name='from' type='text' placeholder='From' class='date-picker form-control input-indicator-1'  value='".str_replace(" 00:00:00","",$docfrom)."' >";
						$mystr = $mystr."</div>";
						
						$mystr = $mystr."<div class='input-group indicator-1 pull-right'>";
							$mystr = $mystr."<input id='document_name' name='document_name' type='text' class='form-control input-indicator-1' placeholder='Document name' value='".$docname."'>";
						$mystr = $mystr."</div>";
					$mystr = $mystr."</form>";
				
				$mystr = $mystr."</div><!--/data filters-->";
				
	    		$mystr = $mystr."</div>";
	    		
			$mystr = $mystr."<!--data table-->";
			$mystr = $mystr."<div class='plot-data documents col-sm-12 col-md-12 col-lg-12'>";
				$mystr = $mystr."<div id='table'>";
				    $mystr = $mystr."<table data-toggle='table' data-search='true' data-pagination='true' data-show-columns='true' data-query-params='queryParams' data-pagination='true'>";
				    $mystr = $mystr."<thead>";
				    $mystr = $mystr."<tr>";
					$mystr = $mystr."<th>Documents</th>";
					$mystr = $mystr."<th>Document Type</th>";
					$mystr = $mystr."<th data-field='date' data-sortable='true'>Date <span class='order'> <span class='caret-filter' style='margin: 10px 5px;'> <i class='fa fa-sort'></i> </span> </span></th>";
					$mystr = $mystr."<th>Hits</th>";
					$mystr = $mystr."<th>&nbsp;</th>";
					
				    $mystr = $mystr."</tr>";
				    $mystr = $mystr."</thead>";
				    $mystr = $mystr."<tbody>";
				    
				    	$mystr = $mystr.list_documents($docname,$docfrom,$docto,$doctype);
				    
				    $mystr = $mystr."</tbody>";
				    $mystr = $mystr."</table>";    				
				$mystr = $mystr."</div>";
			$mystr = $mystr."</div>";
			$mystr = $mystr."<!--/data table-->";
	    $mystr = $mystr."</div><!--/column-->";
	$mystr = $mystr."</div><!--/row-->";
	$mystr = $mystr."</div><!--/tab-content-->";	


if ($docid > 0) {
	$mystr = $mystr."<div class='row'>";
		$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12'>";
			$mystr = $mystr."<p class='bg-info'>".download_file($docid)."</p>";
		$mystr = $mystr."</div>";
	$mystr = $mystr."</div>";
}

display($mystr);


function list_documents($docname=NULL,$docfrom=NULL,$docto=NULL,$doctype=NULL) {
	global $dba;
	
	if ( $docname!="" || ($docfrom!="" || $docto!="") )
	{
		$querystring = " WHERE ";
		$queryjoin = "";
		
		if( (isset($docfrom) && isset($docto)))
		{
			$querystring = $querystring.'date BETWEEN "'.$docfrom.'" AND "'.$docto.'" ';
			$queryjoin = " AND ";
		}
		
		if( isset($docname) && $docname!="" )
		{
			$querystring = $querystring.$queryjoin.'title LIKE "%'.$docname.'%" ';
			$queryjoin = " AND ";
		}
		
		if(isset($doctype) && $doctype=='top-reports')
		{
			$querystring = $querystring.$queryjoin.'documenttypeid = "135499005479979499" ';
			$queryjoin = " AND ";
		}
		
		if(isset($doctype) && $doctype=='other-reports')
		{
			$querystring = $querystring.$queryjoin.'NOT (documenttypeid = "135499005479979499") ';
		}
	}
	
	$sql = "select id, title, documenttypeid, summary, documentfile, date, hits from `document` $querystring order by date desc;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		while(!$rs->eof()) {
			
			$str = $str."<tr>";
			
				$str = $str."<td>";
				$str = $str."<div>";
				$str = $str."<div><a href='downloads.php?docid=".$rs->row("id")."'>".titlecase($rs->row("title"))."</a></div>";
				$str = $str."<small>".$rs->row("summary")."</small>";
				$str = $str."</div>";
				$str = $str."</td>";
				
				$str = $str."<td>".putspace(getmyfield("documenttype", $rs->row("documenttypeid")))."</td>";
				
				$str = $str."<td>".formatmydate($rs->row("date"))."</td>";
				
				$str = $str."<td>".$rs->row("hits")."</td>";
				
				$str = $str."<td class='download_td_btn'>"; 
				$str = $str."<a class='btn btn-default bg-lightgrey btn-primary' href='downloads.php?docid=".$rs->row("id")."' target='_blank'> Download <i class='fa fa-download'></i> </a>";	
				$str = $str."</td>";
			
			$str = $str."</tr>";
			$rs->movenext();
		}
	}
	else {
		$str = $str."<br>".messagebox("There are no document shared at the moment, please check again later.", false);
	}
	return $str;
}

function download_file($docid) {
	global $dba;
	$path = "documents";
	$sql = "select id, title, documentfile, hits from `document` where id = $docid;";
	
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$hitscount = $rs->row("hits") + 1;
		
		//increment hits
		$sql = "update document set hits = $hitscount where id = $docid;";
		$rs = $dba->execute($sql);
		
		$file = "$path/".$rs->row("documentfile");
		if(is_file($file)) {
			if(ini_get('zlib.output_compression')) { 
				ini_set('zlib.output_compression', 'Off');
			}
			
			switch(strtolower(substr(strrchr($file,'.'),1))) {
				case 'pdf':
					$mime = 'application/pdf'; 
				break;
				case 'jpeg':
				case 'jpg': 
					$mime = 'image/jpg';
				break;
				case 'png': 
					$mime = 'image/png';
				break;
				case 'gif': 
					$mime = 'image/gif';
				break;
				default: 
					$mime = 'application/force-download';
			}
			
			header('Pragma: public');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Cache-Control: private',false);
			header('Content-Type: '.$mime);
			header('Content-Disposition: attachment; filename='.$rs->row("title"));
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: '.filesize($file));
			readfile($file);
			exit();
		}
		else {
			$str = messagebox("Requested document was not found", false);
		}
	}
	else {
		$str = messagebox("Requested document was not found", false);
	}
	return $str;
}

?>
