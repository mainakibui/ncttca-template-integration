<?php
require_once("includes/config.php");
$mtitle = "Account Login";

$username = clean(strtolower($_POST['username']));
$password = clean(strtolower($_POST['password']));

if (loggedin()) {
	header("Location: index.php");
}
else {
	if (strlen(trim($username)) > 0 || strlen(trim($password)) > 0) {
		if (!getuserprefs($username, $password, "account", "username", "password", "statusid=1 and accounttypeid > 2"))	{
			$mystr = $mystr.messagebox("Authentication failure. Cannot login due to invalid username and/or password", false);
			$mystr = $mystr.loginadmin($username, false, "", false, false, "login.php", "Login to your Account");
		}
		else {
			header("Location: "."index.php");
		}
	}
	else {
		if (strlen(trim($username)) > 0 || strlen(trim($password)) > 0) {
			$mystr = $mystr.messagebox("Authentication failure. Cannot login due to invalid username and/or password", false);
		}
		$mystr = $mystr.loginadmin($username, false, "", false, false, "login.php", "Login to your Account");
	}
}
print simple_top().$mystr.simple_bottom();
?>
