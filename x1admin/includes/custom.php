<?php

keepalive();

if (strlen(trim($_SESSION["languageid"])) == 0) {
	$_SESSION["languageid"] = $application["languageid"];
}


function top() {
	if (loggedin()) {
		global $application, $mtitle, $page;
		$hdr = $hdr."<link href=\"includes/templates/".$application['template']."/stylesheet.css\" rel=stylesheet>";
		$str = $str.headers($hdr);
		$str = $str."<body topmargin=0 bottommargin=0 leftmargin=0 rightmargin=0 marginheight=0 marginwidth=0 onload=\"CoolClock.findAndCreateClocks()\">";
		$str = $str."<table border=\"0\" cellpadding=0 cellspacing=0 width='100%' bgcolor=#ffffff>";
		$str = $str."<tr valign=top><td align=center>";
		
		$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
		$str = $str."<tr><td align=center><table border=\"0\" width=\"90%\"><tr>";
		if (strlen(trim($application['logo'])) > 0) {
			$str = $str."<td width=\"120\" align=center><img src=\"images/margin.gif\" border=0 width=\"100\" height=1><img src=\"images/".$application['logo']."\" border=0></td>";
		}
		$str = $str."<td width='100%'><h3><font color=#606060>".titlecase(translate($application['title']))."<br>TOP Management Information System</font></h3></td>";
		$str = $str."<td>".clock()."</td>";
		//$country = get_country($application['countryid']);
		if (strlen(trim($country)) > 0) {
			$str = $str."<td align=center>".$country."</td>";
		}
		$str = $str."</tr></table></td></tr>";
		$str = $str."<tr><td align=center>";
		
		$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='90%'>";
		$str = $str."<tr>";
		$str = $str."<td valign=bottom align=\"left\" width=\"100%\">".menu(2, 0)."</td>";
		
		$str = $str."<td width=\"200\" valign=bottom>";
		$str = $str."<img src=\"images/margin.gif\" width=\"200\">";
		$str = $str."<table border=0 cellpadding=0 cellspacing=0 width=\"100%\">";
		$str = $str."<tr align=center><td class=\"tab pad\">";
			$str = $str."<table border=0 cellpadding=2><tr><td><img src=images/captain.png border=0 width=20></td><td><font color=#ffffff><b>".user("name")."</b>&nbsp;|&nbsp;".getmyfield("accounttype", user("accounttypeid"))."<br><a href=account_edit.php><font color=yellow>".putspace(titlecase(translate("Edit My Account")))."</font></a>&nbsp;|&nbsp;<a href=logout.php><font color=yellow>".putspace(titlecase(translate("Log out")))."</font></a></font></td></tr></table>";
		$str = $str."</td></tr>";
		$str = $str."</table>";
		
		$str = $str."</td>";
		$str = $str."</table>";
		
		$str = $str."</td></tr>";
		$str = $str."<tr valign=center><td class=\"header\" align=center>";
		$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='90%'><tr valign=center>";
		$str = $str."<td width='100%' height=50>".submenu()."</td>";
		//$str = $str."<td><font color=pink>&nbsp;&nbsp;".$application["language"]."&nbsp;&nbsp;</font></td>";
			
		$str = $str."</tr></table>";
		$str = $str."</td></tr>";
		$str = $str."<tr valign=top><td width='100%' align=center>";

		$str = $str."<table border=\"0\" cellpadding=\"10\" cellspacing=\"0\" width='90%'>";
		$str = $str."<tr valign=top align=\"left\"><td width='100%' style=\"height:400px;\">";
		$parentid = $page[2];
		$parent   = $page[3];
		$str = $str.pagetitle();
		if (strlen(trim($page[4])) > 0) {
			$str = $str."<div align=right><span onclick=\"hiddendiv('help')\"><font color=#0000cc><b>".translate("Help")."</b></font></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><div id=help name=help style=\"display: none\">".messagebox("<h3>How To Use this Feature</h3><br>".$page[4])."</div></div>";
		}
	}
	return $str;
}


function bottom() {
	if (loggedin()) {
		global $page, $application, $sidemenu;
		$str = $str."</td>";
		if(strlen(trim($sidemenu)) > 0) {
			$str = $str."<td bhghggcolor=#FCFDF2>".$sidemenu."<img src=\"images/margin.gif\" height=1 width=\"280\"></td>";
		}
		$str = $str."</tr>";
		$str = $str."</table>";
	
		$str = $str."</td></tr>";
		$str = $str."<tr><td valign=\"top\" align=\"center\" bgcolor=\"#f6f6f6\" style=\"height:30px;background-repeat:repeat-x;\">";
	
		$str = $str."<br><table border=0 cellpadding=0 cellspacing=0 width='90%'>";
		$str = $str."<tr valign=top align=\"left\"><td width='100%' align=\"right\">";
	
		$str = $str."<table border=0 cellpadding=0 cellspacing=10>";
		$str = $str."<tr>";
		$str = $str."<td width='100%' align=right>&copy; ".date("Y")." ".$application['copyright'].". ".titlecase(translate("All rights reserved."))."</td>";
		$str = $str."</tr>";
		$str = $str."</table>";
	
		$str = $str."</td>";
		$str = $str."</table>";

		$str = $str."</td></tr>";
		$str = $str."</table>";

		$str = $str."</td></tr>";
		$str = $str."</table>";
		$str = $str."</body></html>";
    }
    return $str;
}


function userinfo() {
	$str = $str."<table border=0 cellpadding=0 cellspacing=0>";
	$str = $str."<tr valign=center align=center>";
	$str = $str."<td>&nbsp;&nbsp;</td>";
	$str = $str."<td><img src=account.png border=0 width=23></td>";
	if (loggedin()) {
		$str = $str."<td>".user("name")."&nbsp;(&nbsp;<b>".user("username")."&nbsp;)</td>";
		if (user("organizationid") > 0) {
			$str = $str."<td><font color=#d0d0d0>&nbsp;&nbsp;|&nbsp;&nbsp;</font></td>";
			$str = $str."<td><font class=textbright>".getmyfield("organization", user("organizationid"))."</font></td>";
		}
		$str = $str."</td>";
		$str = $str."<td><font color=#d0d0d0>&nbsp;&nbsp;|&nbsp;&nbsp;</font></td>";
		$str = $str."<td><a href=account.php>Manage&nbsp;My&nbsp;Account&nbsp;</a></td>";
		$str = $str."<td><font color=#d0d0d0>&nbsp;&nbsp;|&nbsp;&nbsp;</font></td>";
		$str = $str."<td><a href=logout.php><font color=#FF6600>Log&nbsp;Out&nbsp;</font></a>&nbsp;</td>";
	}
	else {
		$str = $str."<td><a href=login.php><b>Login</b>&nbsp;</a><font class=textfaded>to your Account</font>&nbsp;</td>";
	}
	$str = $str."</tr>";
	$str = $str."</table>";
	return $str;
}


function simple_top() {
	global $application, $mtitle;
	$hdr = $hdr."<link href=\"includes/templates/".$application['template']."/stylesheet.css\" rel=stylesheet>";
	$str = $str.headers($hdr);
	$str = $str."<body topmargin=0 bottommargin=0 leftmargin=0 rightmargin=0 marginheight=0 marginwidth=0>";
	$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%' bgcolor=#ffffff>";
	$str = $str."<tr algin=center><td>";
	$str = $str."<br><br><br><br><center><table border=0 cellpadding=0 cellspacing=3 width='600'>";
	$str = $str."<tr align=left><td>";
	if (strlen(trim($application['logo'])) > 0) {
		$str = $str."<img src=\"images/".trim($application['logo'])."\" height=50 border=0>";
	}	
	$str = $str."</td><td width='100%' align=left><h2><font class=textfaded>".$application['title']."</font></h2></td></tr>";
	$str = $str."<tr><td colspan=2>";



	return $str;
}

function simple_bottom() {
	$str = $str."<br></td></tr>";
	$str = $str."</table>";
	$str = $str."</td></tr>";
	$str = $str."</table>";
	$str = $str."</body></html>";
    return $str;
}

function menu() {
	global $dba, $page;
	$pageid = $page[0];
	$parentid = $page[2];
	$sql = "select id, title, url, positionid, accesscontrol from page where menuid=2 and pageid=0 and statusid = 1 order by positionid, id;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%'>";
		$str = $str."<tr>";
		$mcount = 0;
		while (!$rs->eof()) {
			if (accesscontrol($rs->row("accesscontrol"))) {
				$u = "page.php?id=".$rs->row("id");
				if (strlen(trim($rs->row("url"))) > 0) {
					$u = $rs->row("url");
				}
				$c = "taboff";
				if (strtolower(trim($pageid)) == strtolower(trim($rs->row("id"))) || strtolower(trim($parentid)) == strtolower(trim($rs->row("id")))) {
					$c = "tabon";
				}
				$str = $str."<td bgcolor=\"#ffffff\"><img src=\"images/vmargin.gif\" border=\"0\" height=\"6\" width=\"2\"></td>";
				$str = $str."<td class=\"$c\"><a href=\"".$u."\"><font color=#ffffff>&nbsp;&nbsp;<b><font size=-1>".putspace(titlecase(translate($rs->row("title"))))."</font></b>&nbsp;&nbsp;</font></a></td>";
				$str = $str."<td><img src=\"images/margin.gif\" border=0 width=2></td>";
				$mcount++;
			}
			$rs->movenext();
		}
		$str = $str."<td width='100%'>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		$str = $str."</tr>";
		$str = $str."</table>";
	}
	return $str;
}



function submenu() {
	global $dba, $page;
	$pageid   = $page[0];
	$parentid = $page[2];
	if (is_numeric($pageid) && is_numeric($parentid)) {
		$sql = "select id, title, url, positionid, accesscontrol, pageid from page where menuid=2 and (pageid = $pageid or pageid = $parentid) and pageid != 0 and statusid = 1 order by positionid, id;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<table border=0 cellpadding=0 cellspacing=8 width='100%'><tr>";
			$mcount = 0;
			while (!$rs->eof()) {
				if (accesscontrol($rs->row("accesscontrol"))) {
					$u = "page.php?id=".$rs->row("id");
					if (strlen(trim($rs->row("url"))) > 0) {
						$u = $rs->row("url");
					}
					$c = "subtaboff";
					if (strtolower(trim($pageid)) == strtolower(trim($rs->row("id")))) {
						$c = "subtabon";
					}
					$str = $str."<td bgcolor=#606060><img src=images/margin.gif border=0 width=1 height=1></td>";
					$str = $str."<td class=$c style=\"white-space: nowrap;\"><a href=\"".$u."\"><font color=yellow><font size=-1>".putspace(titlecase(translate($rs->row("title"))))."</font></font></a></td>";
					$mcount++;
				}
				$rs->movenext();
			}
			$str = $str."<td bgcolor=#606060><img src=images/margin.gif border=0 width=1 height=1></td>";
			if (trim(user("accounttypeid")) == "4" && trim($pageid) == "2012042617410392036") {
				//$str = $str."<td bgcolor=#606060><img src=images/margin.gif border=0 width=1 height=1></td>";
				//$str = $str."<td>".showpopup("application", 1, "Site Settings", 2, "<font color=yellow><font size=-1>".putspace(titlecase(translate("Application Settings")))."</font></font>")."</td>";
			}
			$str = $str."<td height=30 width='100%'>&nbsp;</td></tr></table>";
		}
	}
	return $str;
}


function clock() {
	$str = $str."<table border=0 cellpadding=2>";
	$str = $str."<tr valign=center>";
	$str = $str."<td><canvas id=\"clk1\" class=\"CoolClock:securephp:40\"></canvas></td>";
	$str = $str."<td><font class=textfaded><font size=+2>".weekdayname(getmydate())."</font><font><br>".monthname(getmydate())."&nbsp;".day(getmydate())."&nbsp;".year(getmydate())."</font></font>";
	$str = $str."</tr>";
	$str = $str."</table>";
	return $str;
}

function get_country($countryid) {
	global $dba, $site_organization;
	$sql = "select country.title, country.code from `country` where country.id='".$countryid."' limit 1;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		if (strlen(trim($rs->row("code"))) > 0) {
			$country = $country."<img src=\"images/".strtolower(trim($rs->row("code"))).".gif\" border=0 height=60><br>";
		}
		else {
			if (strlen(trim($rs->row("title"))) > 0) {
				$country = $country."<font size=+0>".putspace($rs->row("title"))."</font>";
			}
		}
		if (strlen(trim($site_organization)) > 0) {
			$country = $country."<img src=images/margin.gif border=0 width=10 height=4><br><font size=-1>".putspace($site_organization)."</font>";
		}
	}
	return $country;
}	

	
function toolbar($tables, $currenttable) {
	global $id, $organization;
	$excludedtables = array("account", "application", "session", "accounttype", "page", "eventlog", "cargo", "transit", "gpsdata");
	$str = $str."<table border=0 cellpadding=2 cellspacing=0><form class=niceform name=dblist method=get action=\"".get_script_name()."\">";
	$str = $str."<tr>";
	$str = $str."<td><b><font color=#a0a0a0>Select&nbsp;Database</font></b>&nbsp;</td>";
	$str = $str."<td>";
	$str = $str."<select onchange='document.dblist.submit()' name=id id=id>";
	for ($i = 0; $i < sizeof($tables); $i++) {
		if (!in_array($tables[$i], $excludedtables)) {
		//if (strtolower(trim($tables[$i])) != "user" && strtolower(trim($tables[$i])) != "usergroup" && strtolower(trim($tables[$i])) != "application" && strtolower(trim($tables[$i])) != "menu" && strtolower(trim($tables[$i])) != "eventlog" && strtolower(trim($tables[$i])) != "position") {
			$str = $str."<option value=\"".$i."\"";
			if ($i == $id) {
				$str = $str." selected";
			}
			$str = $str.">".ucwords(hashtable($tables[$i]))."</option>";
		}
	}
	$str = $str."</select>";
	$str = $str."</td>";
	$str = $str."<td><input type=submit value='Select' class=button></td>";
	$str = $str."</tr>";
	$str = $str."</form></table>";
	return $str;
}


function filters($tb, $ex = "hsgsh474hdnd8", $csql = null) {
	global $dba, $keywords, $id;
	$sql = "select * from `$tb` limit 1;";
	$rs = $dba->execute($sql);
	if (!$rs->eof() && trim($ex) != "*") {
		$fd = getfields($tb);
		if (sizeof($fd) > 0) {
			$dp = explode(",", $fd[0]);
			$flt = "";
			$ex = strtolower(trim($ex));
			$ex = explode(",", $ex);
			foreach ($dp as $d) {
				if (strlen(trim($d)) > 2 && substr(trim(strtolower($d)), strlen(trim($d)) - 2, 2) == "id" && strtolower(trim($d)) != "positionid") {
					if (in_array(strtolower(trim($d)), $ex) == false) {
						$c = 1;
						if (strtolower(trim($d)) == "manufacturerid" || strtolower(trim($d)) == "modelid") {
							$c = 1;
						}
						$flt = $flt.multiselect($d, $_GET[$d], substr($d, 0, strlen($d) - 2), $c, true, "", false, $tb, $csql);
					}
				}
			}
			$str = $str."<font color=#808080><b>Filter ".ucwords(hashtable($tb))."</b></font>";
			$str = $str."<form class=\"niceform\" method=get action=\"".get_script_name()."\">";
			$str = $str."<table border=0 cellspacing=0 cellpadding=4 width='100%'>";
			$str = $str."<tr><td colspan=3><hr size=1 noshade color=#e0e0e0></td></tr>";			
			$str = $str."<tr><td><font class=texthighlighted><b>Search</b></font></td><td><input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"".$keywords."\" size=\"15\"><input type=hidden name=id id=id value=\"".$id."\"></td><td><input type=submit value=\"Search\"></td></tr>";
			if (strlen(trim($flt)) > 0) {
				//$str = $str."<tr><td colspan=3><hr size=1 noshade color=#e0e0e0></td></tr>";
				$str = $str."<tr valign=top><td colspan=3>".$flt."</td></tr>";
			}
			$str = $str."</table>";
			$str = $str."</form>";
		}
	}
	return $str;
}


function filterimport($fetch) {
	global $table, $column;
	if($fetch=='tables')
	{
		$str = $str."<font color=#808080><b>Filter tables</b></font>";
		$str = $str."<form class=\"niceform\" method=get action=\"".get_script_name()."\">";
		$str = $str."<table border=0 cellspacing=0 cellpadding=4 width='100%'>";
		$str = $str."<tr><td colspan=3><hr size=1 noshade color=#e0e0e0></td></tr>";			
		$str = $str."<tr><td><font class=texthighlighted><b>Search</b></font></td><td><input type=\"text\" name=\"tb\" id=\"tb\" value=\"".$table."\" size=\"15\"></td><td><input type=submit value=\"Search\"></td></tr>";
		$str = $str."</table>";
		$str = $str."</form>";
	}
	
	if($fetch=='table')
	{
		$str = $str."<font color=#808080><b>Filter ".ucwords(hashtable($table))." collumns</b></font>";
		$str = $str."<form class=\"niceform\" method=get action=\"".get_script_name()."\">";
		$str = $str."<table border=0 cellspacing=0 cellpadding=4 width='100%'>";
		$str = $str."<tr><td colspan=3><hr size=1 noshade color=#e0e0e0></td></tr>";			
		$str = $str."<tr><td><font class=texthighlighted><b>Search</b></font></td><td><input type=\"text\" name=\"column\" id=\"column\" value=\"".$column."\" size=\"15\"><input type='text' hidden='hidden' name='tb' id='tb' value=\"".$table."\"></td><td><input type=submit value=\"Search\"></td></tr>";
		$str = $str."</table>";
		$str = $str."</form>";
	}
	
	return $str;
}



function composesql($t) {
	global $keywords;
	$fd = getfields($t);
	$tf = $fd[1].",".$fd[2];
	$tf = explode(",", $tf);
	if (strlen(trim($keywords)) > 0) {
		$s = explode(" ", $keywords);
		foreach ($s as $i) {
			if (strlen(trim($i)) > 0) {
				foreach ($tf as $j) {
					if (strlen(trim($j)) > 0) {
						$sql = $sql." `".$t."`.`$j` like '%$i%' or ";
					}
				}
			}
		}
	}
	if (strlen($sql) > 4) {
		$sql = substr($sql, 0, strlen($sql) - 4);
		$sql = " and (".$sql.") ";
	}
	foreach ($_GET as $i=>$j) {
		$ft = "";
		if (substr(strtolower($i), strlen($i) - 2, 2) == "id" && strlen($i) > 2) {
			$a = $_GET[$i];
			if (!is_array($a)) {
				$a = explode("||||<|", $a);
			}
			foreach ($a as $j) {
				if (strlen(trim($j)) > 0 && is_numeric($j)) {
					$ft = $ft."`".$t."`.`".strtolower(trim($i))."`='".$j."' or ";
				}
			}		
		}
		if (strlen(trim($ft)) > 0) {
			$sql = $sql." and (".substr($ft, 0, strlen($ft) - 4).") ";
		}
	}
	return $sql;
}

function actions($tb, $d, $a) {
	/*
	global $dba;
	if ($a == true) {
		$sql = "select title from `actiontype` where lower(trim(`db`))='".$tb."' order by id;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$pad = "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			while (!$rs->eof()) {
				$str = $str."<td><input type=submit class=action_button value=\"".$rs->row("title")."\"></td>";
				$rs->movenext();
			}
		}
	}	
	if ($d == true) {
		$str = $str.$pad."<td><input type=submit class=delete_button value=Delete></td>";
	}
	if (strlen(trim($str)) > 0) {
		$str = "<table border=0 cellpadding=3 cellspacing=0><tr>".$str."</tr></table>";
	}
	*/
	return $str;
}


function viewer($tb, $cid) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($cid)) > 0) {
		$sql = "select * from `$tb` where id='".$cid."' limit 1;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<font size=+1 color=#000000>".$tb."<tt>-></tt>".$rs->row(1)."</font><br>";
			if (trim($rs->row("statusid")) == "7") {
				$str = $str."<table border=3 cellpadding=5 bordercolor=#cc3300 cellspacing=0 width='100%'><tr><td>";
			}
			$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%'><tr><td bgcolor=#f0f0f0>";
			$str = $str."<table border=0 cellpadding=5 cellspacing=1 width='100%'>";
			$mcount = 0;
			$count = 3; //round((sizeof($rs->fields) + 1) / 2);
			for ($i = 1; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				$v = $rs->row($i);
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
					$v = getmyfield($f, $v);
				}
				$f = ucwords(hashtable($f));
				if (strlen(trim($v)) == 0) {
					$v = "-";
				}
				if ($mcount == 0) {
					$str = $str."<tr bgcolor=#ffffff>";
				}
				$str = $str."<td bgcolor=#fcfcfc><font class=texthighlighted>".$f."</font></td><td>".$v."</td>";
				$mcount++;
				if ($mcount == $count) {
					$mcount = 0;
					$str = $str."</tr>";
				}
			}
			if ($mcount > 0) {
				$str = $str."<td colspan=".(($count - $mcount)*2).">&nbsp;</td>";
				$str = $str."</tr>";
			}
			$str = $str."</table>";
			$str = $str."</td></tr></table><br>";
			$str = $str.eventlog($tb, $cid);
			//print ($tb.$rs->row("statusid"));
			if (strtolower(trim($tb)) == "firearms" && trim($rs->row("statusid")) == "7") {
				$str = $str.getdbdata("destruction", "firearmid=".$cid);
			}
			if (trim($rs->row("statusid")) == "7") {
				$str = $str."</td></tr></table>";
			}
		}
		else {
			$str = $str.messagebox("Sorry, Requested Information could not be displayed", false);
		}
	}
	else {
		$str = $str.messagebox("Sorry, Information could not be displayed", false);
	}
	return $str;
}

	
function eventlog($tb, $cid) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($cid)) > 0) {
		$sql = "select * from `log` where `table`='".strtolower(trim($tb))."' and `record`='".trim($cid)."' order by `date` desc;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<font color=#cc6600>Activity Log</font><br><br>";
			$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%'><tr><td bgcolor=#f0f0f0>";
			$str = $str."<table border=0 cellpadding=2 cellspacing=1 width='100%'>";
			
			$str = $str."<tr bgcolor=#FEF3F3>";
			for ($i = 3; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
				}
				$f = strtoupper(hashtable($f));
				$str = $str."<td><small>".$f."</small></td>";
			}
			$str = $str."</tr>";
			
			while (!$rs->eof()) {
				$str = $str."<tr bgcolor=#ffffff>";
				for ($i = 3; $i < sizeof($rs->fields); $i++) {
					$f = $rs->fields[$i];
					$v = $rs->row($i);
					if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
						$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
						$v = getmyfield($f, $v);
					}
					$f = ucwords(hashtable($f));
					if (strlen(trim($v)) == 0) {
						$v = "-";
					}
					$str = $str."<td><small>".$v."</small></td>";
				}
				$str = $str."</tr>";
				$rs->movenext();
			}
			$str = $str."</table>";
			$str = $str."</td></tr></table>";
		}
	}
	return $str;
}

function getdbdata($tb, $filter) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($filter)) > 0) {
		$sql = "select distinct * from `".$tb."` where ".$filter." order by `date` desc;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<br><font color=#cc6600>".ucwords($tb)." Details</font><br>";
			$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%'><tr><td bgcolor=#f0f0f0>";
			$str = $str."<table border=0 cellpadding=2 cellspacing=1 width='100%'>";
			
			$str = $str."<tr bgcolor=#FEF3F3>";
			for ($i = 1; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
				}
				$f = strtoupper(hashtable($f));
				$str = $str."<td><small>".$f."</small></td>";
			}
			$str = $str."</tr>";
			
			while (!$rs->eof()) {
				$str = $str."<tr bgcolor=#ffffff>";
				for ($i = 1; $i < sizeof($rs->fields); $i++) {
					$f = $rs->fields[$i];
					$v = $rs->row($i);
					if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
						$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
						$v = getmyfield($f, $v);
					}
					$f = ucwords(hashtable($f));
					if (strlen(trim($v)) == 0) {
						$v = "-";
					}
					$str = $str."<td><small>".$v."</small></td>";
				}
				$str = $str."</tr>";
				$rs->movenext();
			}
			$str = $str."</table>";
			$str = $str."</td></tr></table>";
		}
	}
	return $str;
}


function listingdb($fetch, $currenttable=null, $column=null)
{
	global $dba;
	
	if($fetch=='tables')
	{
		if(strlen(trim($currenttable)) > 0)
		{
			$where = " AND tables_in_ncttca = '".$currenttable."'";
		}
		else
		{
			$where = "";
		}
		
		$sql = "show full tables where Table_Type != 'VIEW'".$where;
		$rs = $dba->execute($sql);
		
		$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%'>";
		$str = $str."<tr><td> <br><br><p> <b>Import Admin</b> allows you to manage collumns from different tables to which data can or can not be added to. To manage a tables collumn; </p> <br> <ol><li>Select a table below by clicking on the Go button</li> <li>In the manage table page toggle collumns on and off for import.</li> <li>Go to the dymanic import page to import data.</li></ol> <p>NB: Only tables with columns active for import will be shown in the dynamic import page.</p><br> </td></tr>";
		$str = $str."<tr><td bgcolor=#e0e0e0>";
		$str = $str."<table border=0 cellpadding=3 cellspacing=1 width='100%'>";
		$str = $str."<tr valign=center><th width='100%' bgcolor='#f9f9f9' height='40'>Table</th><th align=right valign=bottom bgcolor='#f9f9f9' height='40'> Launch </th></tr>";
		$bgc = "#ffffff";
		if ($rs->eof()) 
		{
			$str=$str."<tr bgcolor=".$bgc.">";
			$str=$str."<td bgcolor=#FEF1E6 valign='center' colspan='2'><h3 style='text-align: center;'>No items are available.</h3></td>";
			$str=$str."</tr>";
		}
		else 
		{
			while (!$rs->eof()) 
			{
				
				$str=$str."<tr bgcolor=".$bgc.">";
				$str=$str."<td bgcolor=#fffff><font color=#333>".$rs->row("tables_in_ncttca")."</font> </td>";
				$str=$str."<td bgcolor=#fffff style='text-align: center;'> <a href='importadmin_table.php?tb=".$rs->row("tables_in_ncttca")."'><button type='button'><font color=#333>Go</font></button></a> </td>";
				$str=$str."</tr>";
				
				$rs->movenext();
			}
		}
		$str=$str."</table>";
		$str=$str."</td></tr></table>";
		
		return $str;
	}
	
	if($fetch=='table')
	{
		//get import collumns
		$sql = "select ailog_tablecolumn,ailog_columnstatus from import_admin_log where ailog_tablename='".$currenttable."'";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) 
		{
			while (!$rs->eof())
			{
				$importlog[$currenttable][$rs->row('ailog_tablecolumn')]['status']=$rs->row('ailog_columnstatus');
			
				$rs->movenext();
			}
		}
		else
		{
			$importlog=array();
		}
		
		if(strlen(trim($column)) > 0)
		{
			$where = " WHERE field = '".$column."'";
		}
		else
		{
			$where = "";
		}
		
		$sql = "SHOW full columns FROM ".$currenttable.$where;
		$rs = $dba->execute($sql);
		
		$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%'>";
		$str = $str."<tr><td> <br><br><p> NB: Click on the buttons at the end of the rows to toggle columns on or off for import.</p><br> </td></tr>";
		$str = $str."<tr><td bgcolor=#e0e0e0>";
		$str = $str."<table border=0 cellpadding=3 cellspacing=1 width='100%'>";
		$str = $str."<tr valign=center> <th colspan='3' align=center valign=bottom bgcolor='#f9f9f9' height='40'> Table: ".$currenttable." </th> </tr>";
		$str = $str."<tr valign=center> <th align=center valign=bottom bgcolor='#f9f9f9' height='40'>Field name</th> <th align=center valign=bottom bgcolor='#f9f9f9' height='40'> About </th> <th align=center valign=bottom bgcolor='#f9f9f9' height='40'> Import </th> </tr>";
		$bgc = "#ffffff";
		if ($rs->eof()) 
		{
			$str=$str."<tr bgcolor=".$bgc.">";
			$str=$str."<td bgcolor=#FEF1E6 valign='center' colspan='3'><h3 style='text-align: center;'>No items are available.</h3></td>";
			$str=$str."</tr>";
		}
		else 
		{
			while (!$rs->eof()) 
			{
				//Check collumn status
				$column = $importlog[$currenttable][$rs->row("field")]['status'];
				if(!sizeOf($column)>0)
				{
					$column = "off";
				}
				
				$str=$str."<tr id='import-row-".$rs->row("field")."' class='".$column."' bgcolor=".$bgc.">";
				$str=$str."<td bgcolor=#fffff><font color=#333>".$rs->row("field")."</font> </td>";
				$str=$str."<td bgcolor=#fffff> <div>Type: &nbsp;".$rs->row("type")."</div> <div>Key: &nbsp;".$rs->row("key")."</div> <div>Default: &nbsp;".$rs->row("default")."</div> <div>Null: &nbsp;".$rs->row("null")."</div> <div>Comment: &nbsp;".$rs->row("comment")."</div> </td>";
				$str=$str."<td bgcolor=#fffff style='text-align: center;'> <a id='import-set-".$rs->row("field")."' href='javaScript:void(0);' onclick=\"setimportcolumn('".$currenttable."','".$rs->row("field")."','".$rs->row("type")."','".$column."')\"><button type='button'><font color=#333>import&nbsp;".$column."</font></button></a> </td>";
				$str=$str."</tr>";
				
				$rs->movenext();
			}
		}
		$str=$str."</table>";
		$str=$str."</td></tr></table>";
		
		return $str;
	}
}

function listing($currenttable, $fields = "*", $filter = null, $limit = 20, $add = false, $view = false, $edit = false, $delete = false, $dbfilters = false, $dbfilters_excluded = null, $navigation = false, $dbsearch = false, $multiselect = false, $multiselectheader = null, $multiselectscript = null, $multiselectfield = null, $multiselectfieldvalue = null, $orderby = null) {
	global $dba, $id, $searchablefields, $search, $p, $sidemenu, $application;

	$asql = composesql($currenttable);
	if (strlen(trim($filter)) > 0) {
		$asql = $asql." and (".$filter.") ";
	}
	
	if (trim($fields) != "*") {
		$fields = "id,".$fields;
	}
	
	
		
	$sql = "select SQL_NO_CACHE ".$fields." from `$currenttable` ";
	if (strlen(trim($asql)) > 0) {
		$sql = $sql." where ".substr($asql, 5, strlen($asql))." ";
		$pageitems = getcount($currenttable, "id > 0 ".$asql);
	}
	else {
		$pageitems = getcount($currenttable);
	}
	if (!is_numeric($limit)) {
		$limit = 20;
	}
	$page = ($p - 1) * $limit;
	if (strlen(trim($orderby)) == 0) {
		$orderby = "id desc";
	}
	$sql = $sql." order by ".$orderby." limit $page, $limit;";
	//print $sql;
	$rs = $dba->execute($sql);
	
	//SQL_CALC_FOUND_ROWS 
	//$cnsql = "SELECT FOUND_ROWS();";
	//$rscount = $dba->execute($cnsql);
	//$pageitems = $rscount->row(0);
	
	if ($add == true) {
		$addbutton = showpopup($currenttable, 0, "Add New Record to ".ucwords(putspace(hashtable($currenttable)))." Database", 1, "<b><font class=textbright>Add&nbsp;New&nbsp;".putspace(ucwords(hashtable($currenttable)))."</font></b>&nbsp;");
		$addbutton = button($addbutton);
	}
	$refreshbutton = "<a href=\"".get_script_name();
	if (trim($id) != "0") {
		$refreshbutton = $refreshbutton."?id=".$id;
	}	
	$refreshbutton = $refreshbutton."\"><font class=textbright>Clear&nbsp;all&nbsp;Filters</font></a>";
	$refreshbutton = button($refreshbutton);
	if ($dbsearch == true) {
		$searchbutton = searchbox($keywords, get_script_name());
	}
	$buttons = null;
	if (strlen(trim($addbutton)) > 0 || strlen(trim($refreshbutton)) > 0) {
		$buttons = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"5\"><tr valign=center><td>".$addbutton."</td><td>".$refreshbutton."</td></tr></table>";
	}
	$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%'>";
	$str = $str."<tr valign=center><td width='100%'>&nbsp;&nbsp;&nbsp;&nbsp;</td><td align=right valign=bottom>".$buttons."</td></tr>";//<br>".$searchbutton."
	$str = $str."</table><br>";
	if ($rs->eof()) {
		$message = "No items are available. ";
		if (sizeof($asql) == 0) {
			if ($add == true) {
				$message = $message."Click on the link below to add a new item to this database<br><br><table border=0 cellpadding=0 cellspacing=0><tr valign=center><td><img src=images/add_new.gif width=20 border=0></td><td>".$addbutton."</td></tr></table><br>";
			}
		}
		$str = $str.messagebox($message);
	}
	else {
		if ($multiselect == true) {
			//$str = $str."<form method=post action=\"".$multiselectscript."\">";
			//if ($actions == true) {
				//$str = $str.actions($currenttable, $delete, $actions);
			//}
		}	
		$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%'><tr><td bgcolor=#e0e0e0>";
		$str = $str."<table border=0 cellpadding=3 cellspacing=1 width='100%'>";
		$bgc = "#f9f9f9";

		$str = $str."<tr bgcolor=".$bgc.">";
		if ($multiselect == true) {
			$str = $str."<td bgcolor=#FEF1E6><small><font color=#cc6600>".$multiselectheader."</font></small></td>";
		}
		$fc = $rs->fieldcount();
		if (trim($fields) == "*" &&  $rs->fieldcount() > 3) {
			$fc = 5;
		}

		for ($i = 1; $i < $fc; $i++) {
			if (trim(strtolower($rs->types[$i])) != "blob" && trim(strtolower($rs->fields[$i])) != "url" && trim(strtolower($rs->fields[$i])) != "picture") {
				$w = "";
				//if ($mcount == 0) {
				//	$w = " width='50%'";
				//}
				$str = $str."<td background=\"includes/templates/".$application["template"]."/images/barbkg.gif\" height=40><b><font color=#000000>";
				if (substr(strtolower($rs->fields[$i]), strlen($rs->fields[$i])-2, 2) == "id") {
					$str = $str.putspace(ucwords(hashtable(substr($rs->fields[$i], 0, strlen($rs->fields[$i])-2))));
				}
				else {
					$str = $str.putspace(ucwords(hashtable($rs->fields[$i])));
				}
				$str = $str."</td>";
				$mcount++;
			}
		}		
		$manage = 0;
		if ($view == true) {
			$manage++;
		}
		if ($edit == true) {
			$manage++;
		}
		if ($delete == true) {
			$manage++;
		}
		if ($manage > 0) {
			$str = $str."<td align=center background=\"includes/templates/".$application["template"]."/images/barbkg.gif\" width='10%' colspan=".$manage."><b><font color=#000000>Manage</font></b></td>";
		}
		$str = $str."</tr>";
		while (!$rs->eof()) {
			$cl = "rowpending";
			if (trim($rs->row("statusid")) == "") {
				$cl = "rownormal";
			}
			if (trim($rs->row("statusid")) == "1") {
				$cl = "rownormal";
			}

			if (trim($rs->row("statusid")) == "3" || trim($rs->row("statusid")) == "8") {
				$cl = "rowactive";
			}
			if (trim($rs->row("statusid")) == "7") {
				$cl = "rowdisabled";
			}
			if ($multiselect == true) {
				$multiselectstate = false;
				$multiselectbg = "#ffffff";
				if (strtolower(trim($rs->row($multiselectfield))) == trim(strtolower($multiselectfieldvalue))) {
					$multiselectstate = true;
					$multiselectbg = "#FEF4EC";
				}
				$str = $str."<tr valign=top class=\"".$cl."\" bgcolor='".$multiselectbg."' id=\"r".$rs->row("id")."\" name=\"r".$rs->row("id")."\">";
				if ($multiselectstate == false && (trim($rs->row("statusid")) == "1" || trim($rs->row("statusid")) == "3" || trim($rs->row("statusid")) == "8")) {
					$str = $str."<form method=get action=\"".$multiselectscript."\"><td bgcolor=\"".$multiselectbg."\" align=center><input type=checkbox name=id value=\"".$rs->row("id")."\"  onchange=this.form.submit() onclick=this.form.submit()></td></form>";
				}
				else {
					$str = $str."<td>&nbsp;</td>";
				}
			}
			else {
				$str = $str."<tr valign=top class=\"".$cl."\" bgcolor=#ffffff id=\"r".$rs->row("id")."\" name=\"r".$rs->row("id")."\">";
			}			
			$mcount = 0;
			for ($i = 1; $i < $fc; $i++) {
				if (trim(strtolower($rs->types[$i])) != "blob" && trim(strtolower($rs->fields[$i])) != "url" && trim(strtolower($rs->fields[$i])) != "picture") {
					if (substr(strtolower($rs->fields[$i]), (strlen($rs->fields[$i])-2), 2) == "id" && strlen($rs->fields[$i]) > 2) {
						$mtemp = getmyfield(substr($rs->fields[$i], 0, (strlen($rs->fields[$i])-2)), $rs->row($i));
					}
					else {
						if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 4, 4)) == "date") {
							$mtemp = "<div align=right>".formatmydate($rs->row($i))."</div>";
						}
						else {
							if (strtolower($rs->fields[$i]) == "record title" && strtolower($currenttable) == "eventlog") {
								$mtemp = getmyfield($rs->row(3), $rs->row($i));
							}	
							else {	
								if (strtolower($rs->fields[$i]) == "accesscontrol") {
									$mtemp = getmultiselectfield($rs->fields[$i], $rs->row($i));
								}
								else {
									$mtemp = $rs->row($i);
								}
							}
						}
					}
					if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 8, strlen($rs->fields[$i]))) == "password") {
						$mtemp = "******";
					}
					$mtemp = trim($mtemp);
					$mtemp = $mtemp;
					if (substr(strtolower(trim($rs->fields[$i])), 0, 6) == "serial") {
						$mtemp = "<div align=right>".putspace(strtoupper($mtemp))."</div>";
					}
					else {
						$mtemp = ucwords(strtolower($mtemp));
					}
					$str = $str."<td class=$cl>".$mtemp."</td>";
					$mcount++;
				}
			}
			if ($view == true) {
				if (strtolower($currenttable) == "indicatorsubtype") {
					$str = $str."<td><a href=\"indicator_preview.php?id=".$rs->row(0)."\" onclick=\"return parent.GB_show('".removehtml($site_name)."', this.href, 600, 800)\"><font color=\"#0000ff\">Preview</font></a>&nbsp;&nbsp;|&nbsp;&nbsp;".showpopup($currenttable, $rs->row(0), "View Record Information", 4, "<font class=texthighlighted>View</font>")."&nbsp;</td>";
				}
				else {
					$str = $str."<td>&nbsp;".showpopup($currenttable, $rs->row(0), "View Record Information", 4, "<font class=texthighlighted>View</font>")."&nbsp;</td>";
				}
			}
			if ($edit == true) {
				$str = $str."<td>&nbsp;".showpopup($currenttable, $rs->row(0), "Change Record Information", 2, "<font class=texthighlighted>Change</font>")."&nbsp;</td>";
			}
			if ($delete == true) {
				$str = $str."<td>&nbsp;".showpopup($currenttable, $rs->row(0), "Delete Record", 3, "<font class=textalert>Del</font>")."&nbsp;</td>";
			}
			$str = $str."</tr>";
			$rs->movenext();
		}
		$str = $str."</form>";
		$str = $str."</table>";
		$str = $str."</td></tr></table>";
		if ($multiselect == true) {
		//	if ($actions == true) {
		//		$str = $str.actions($currenttable, $delete, $actions);
		//	}
		//	$str = $str."</form>";
		}
		
		if ($navigation == true) {
			$pages = round($pageitems / $limit);
			if (($pages * $limit) < $pageitems) {
				$pages++;
			}
			if ($pages > 0) {

				$g = "";
				foreach ($_GET as $i=>$j) {
					if (strtolower(trim($i)) != "id" && strtolower(trim($i)) != "p" && strtolower(trim($i)) != "search") {
						$a = $j;
						if (is_array($a)) {
							if (sizeof($a) > 0) {
								$a = implode(",", $a);
								$g = $g.$i."[]=".$a."&";
							}
						}
						else {
							$a = trim($a);
							if (strlen(trim($a)) > 0) {
								$g = $g.$i."=".$a."&";
							}
						}
					}
				}			

				$istr = $istr."<div align=right><table border=0 cellpadding=0 cellspacing=0><tr><td bgcolor=#ffffff>";
				$istr = $istr."<table border=0 cellpadding=6 cellspacing=4 width='100%'><tr bgcolor=#ffffff>";
				//$istr = $istr."<td><b>Viewing&nbsp;Page&nbsp;$p&nbsp;of&nbsp;$pages&nbsp;&nbsp;&nbsp;</td>";
				$istr = $istr."<td><a href=\"".get_script_name()."?id=$id&search=$search&p=1&$g\"><font class=texthighlighted>First</font></a></td>";
				$s = $p - 5;
				if ($s < 1) {
					$s = 1;
				}
				$e = $s + 9;
				for ($i = $s; $i <= $pages; $i++) {
					if ($p == $i) {
						$istr = $istr."<td bgcolor=#9FD3FB><b><font class=textbright>$i</font></b></td>";
					}
					else {
						$istr = $istr."<td bgcolor=#ffffff><a href=\"".get_script_name()."?id=$id&search=$search&p=$i&$g\"><font color=#000000>$i</font></a></td>";
					}
					if ($i == $e) {
						break;
					}
				}
				$istr = $istr."<td><a href=\"".get_script_name()."?id=$id&search=$search&p=$pages&$g\"><font class=texthighlighted>Last</font></a></td>";
				$istr = $istr."</tr></table></td></tr></table></div>";
			}
			if (strlen($istr) > 0) {
				$str = $str."".$istr;
			}
		}
	}
	if ($dbfilters == true) {
		$sidemenu = $sidemenu.filters($currenttable, $dbfilters_excluded, $asql); 
	}
	return $str;
}

function view($tb, $cid) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($cid)) > 0) {
		$sql = "select * from `$tb` where id='".$cid."' limit 1;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<font size=+1 color=#000000>".hashtable($tb)."<tt>-></tt>".$rs->row(1)."</font><br>";
			if (trim($rs->row("statusid")) == "7") {
				$str = $str."<table border=3 cellpadding=5 bordercolor=#cc3300 cellspacing=0 width='100%'><tr><td>";
			}
			$str = $str."<table border=0 cellpadding=0 cellspacing=0 width='100%'><tr><td bgcolor=#f0f0f0>";
			$str = $str."<table border=0 cellpadding=5 cellspacing=1 width='100%'>";
			$mcount = 0;
			$count = 3; //round((sizeof($rs->fields) + 1) / 2);
			for ($i = 1; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				$v = $rs->row($i);
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
					$v = getmyfield($f, $v);
				}
				$f = ucwords(hashtable($f));
				if (strlen(trim($v)) == 0) {
					$v = "-";
				}
				if ($mcount == 0) {
					$str = $str."<tr bgcolor=#ffffff>";
				}
				$str = $str."<td bgcolor=#fcfcfc><font class=texthighlighted>".$f."</font></td><td>".$v."</td>";
				$mcount++;
				if ($mcount == $count) {
					$mcount = 0;
					$str = $str."</tr>";
				}
			}
			if ($mcount > 0) {
				$str = $str."<td colspan=".(($count - $mcount)*2).">&nbsp;</td>";
				$str = $str."</tr>";
			}
			$str = $str."</table>";
			$str = $str."</td></tr></table><br>";
		}
		else {
			$str = $str.messagebox("Sorry, Requested Information could not be displayed", false);
		}
	}
	else {
		$str = $str.messagebox("Sorry, Information could not be displayed", false);
	}
	return $str;
}


function view_page($id) {
	global $dba;
	$sql = "select id, title, content from `page` where id=".$id.";";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<center><table border=0 width='98%' cellpadding=0 cellspacing=0><tr valign=top><td><div align=\"justify\">";
		if (strlen(trim($rs->row(2))) > 0) {
			$content = $content.$rs->row(2);
		}
		$str = $str.$content;
		$str = $str."</div></td></tr></table>";
	}
	else {
		$str = $str.messagebox("Sorry, the requested content is not available", false);
	}
	return $str;
} 



function get_prev_status($id, $statusid) {
	global $dba;
	$sql = "select statusid from activity where firearmid = $id and statusid != $statusid order by date desc, id desc;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		return $rs->row(0);
	}
}

function shorten_url($longurl) {
	$apiKey = 'your_api_key_here';
	$postData = array('longUrl' => $longurl, 'key' => $apiKey);
	$jsonData = json_encode($postData);

	$curlObj = curl_init();

	curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
	curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curlObj, CURLOPT_HEADER, 0);
	curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
	curl_setopt($curlObj, CURLOPT_POST, 1);
	curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
	 
	$response = curl_exec($curlObj);
	$json = json_decode($response);
	curl_close($curlObj);
}

function code_popup($t, $title, $w = 600, $h = 400) {
	global $site_name;
	if (strlen(trim($t)) > 0 && strlen(trim($title)) > 0 && strlen(trim($w)) > 0 && strlen(trim($h)) > 0) {
		$str = $str."<a href=\"view.php?t=".$t."\" onclick=\"return parent.GB_show('".removehtml($site_name)."', this.href, ".$h.", ".$w.")\"><font color=\"#0000ff\">".$title."</font></a>";
	}
	return $str;
}

?>
