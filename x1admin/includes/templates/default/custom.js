function setimportcolumn(table,column, type, activity){

	data = new FormData();
	data.append( 'table', table );
	data.append( 'column', column );
	data.append( 'type', type );
	data.append( 'activity', activity );
	
	$.ajax({
			url: 'ajax_import.php',
			type: 'POST',
			data: data,
			async: false,
			cache: false,
			contentType: false,
			processData: false,
			success: function (returndata) {
				rd=JSON.parse(returndata);
				$("#import-set-"+column).attr('onclick',"setimportcolumn('"+table+"','"+column+"','"+type+"','"+rd['activity']+"')");
				$("#import-set-"+column).html("<button type='button'><font color=#333>import&nbsp;"+rd['activity']+"</font></button>");
				$("#import-row-"+column).attr('class',rd['activity']);
			}
	});
	
	return false;
}
