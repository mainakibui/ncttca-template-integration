<?php
set_time_limit(5000);
require_once("includes/config.php");
require_once("../codebase/plugins/excel/reader.php");
$mtitle = "Road Survey Data Import: Excel to DB";
$a = get_default(clean($_POST['a']), "n", 0);
$stakeholderid = get_default(clean($_POST['stakeholderid']), "n", 0);
$monthid = get_default(clean($_POST['monthid']), "n", 0);
$yearid = get_default(clean($_POST['yearid']), "n", 0);

if (loggedin()) {
	if ($a > 0 && is_numeric($a)) {
		$mystr = $mystr.import($stakeholderid, $monthid, $yearid);
	}
	$mystr = $mystr."<table width='100%' border=0 cellspacing=8 cellpadding=0>";
	$mystr = $mystr."<tr valign=top align=left><td width=60%>";
	$mystr = $mystr."<b>How to use this console</b><br><br>To import Road Survey data from excel to DB, browse for the excel file and then click on the import button. The excel file MUST be in the correct format with fields A to U arranged as follows:-<br>";
	$mystr = $mystr."<ul>";
	$mystr = $mystr."<li>A: Form Number</li>";
	$mystr = $mystr."<li>B: ".code_popup("supervisor", "Supervisor Code")."</li>";
	$mystr = $mystr."<li>C: ".code_popup("driver", "Driver Code")."</li>";
	$mystr = $mystr."<li>D: ".code_popup("cargotype", "Cargo Type Code")."</li>";
	$mystr = $mystr."<li>E: ".code_popup("market", "Market Code")."</li>";
	$mystr = $mystr."<li>F: Weight</li>";
	$mystr = $mystr."<li>G: Containter Number</li>";
	$mystr = $mystr."<li>H: Consignment Number</li>";
	$mystr = $mystr."<li>I: ".code_popup("orgcountry", "Country of Origin Code")."</li>";
	$mystr = $mystr."<li>J: ".code_popup("destcountry", "Destination Country Code")."</li>";
	$mystr = $mystr."<li>K: ".code_popup("transportmode", "Transport Mode Code")."</li>";
	$mystr = $mystr."<li>L: ".code_popup("modecarriertype", "Mode Carrier Type Code")."</li>";
	$mystr = $mystr."<li>M: Make</li>";
	$mystr = $mystr."<li>N: Model</li>";
	$mystr = $mystr."<li>O: ".code_popup("regcountry", "Country of Registration Code")."</li>";
	$mystr = $mystr."<li>P: ".code_popup("axle", "Axle Code")."</li>";
	$mystr = $mystr."<li>Q: Arrival Date</li>";
	$mystr = $mystr."<li>R: Departure Date</li>";
	$mystr = $mystr."<li>S: Issue Date</li>";
	$mystr = $mystr."<li>T: Loading Date</li>";
	$mystr = $mystr."<li>U: Permit Date</li>";
	$mystr = $mystr."</ul>";
	$mystr = $mystr."</td><td width=1 bgcolor=#e0e0e0><img src='images/vmargin.gif' heig=10 width=1></td>";
	$mystr = $mystr."<td width=\"40%\" valign=top>".show_form($surveyid)."</td></tr>";
	$mystr = $mystr."</table>";
	print top().$mystr.bottom();
}
else {
	header("location: login.php");
}


function import($stakeholderid, $monthid, $yearid) {
	$db = "roadsurvey";
	$f = $_FILES["filename"]["name"];
	$doc_type = $_FILES["filename"]["type"];
	$doc_allowed = array("application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	
	foreach($_POST as $key=>$value) {
		if (strlen(trim($value)) == 0) {
			$m = $m."<li>".ucwords(hashtable($key))." is empty or invalid.</li>";
		}
	}
	if (!in_array($doc_type, $doc_allowed)) {
		$m = $m."<li>Invalid File selected. Please select a .XLS file</li>";
	} 
	if (strlen($m) > 0) {
		$str = $str.messagebox("<ul>$m</ul>", false);
	}
	else {
		$filename = $_FILES['filename']['tmp_name'];

		$reader = new Spreadsheet_Excel_Reader();
		$reader->setUTFEncoder('mb');
		$reader->setDefaultFormat('%1.0f');
		$reader->setOutputEncoding('UTF-8');
		$reader->read($filename);
		//$reader->setOutputEncoding('CP-1251');
		$reader->setRowColOffset(0);
		$cells = 21;

		foreach($reader->sheets as $k=>$data) {
			if ($cells == $data['numCols']) {
				$mcount = 1;
				foreach($data['cells'] as $row) {
					//$id = getid($db);
					$containerno = mysql_escape_string($row[1]);
					$consignmentno = mysql_escape_string($row[2]);
					$blnumber = mysql_escape_string($row[3]);
					$manifestno = mysql_escape_string($row[4]);
					$shippingvessel = mysql_escape_string($row[5]);
					$shippingline = mysql_escape_string($row[6]);
					$por = mysql_escape_string($row[7]);
					$pol = mysql_escape_string($row[8]);
					$pod = mysql_escape_string($row[9]);
					$orgcountryid = mysql_escape_string($row[10]);
					$destcountryid = mysql_escape_string($row[11]);
					$hscode = mysql_escape_string($row[12]);
					$description = mysql_escape_string($row[13]);
					$volume = (int)$row[14];
					$weight = (int)$row[15];
					$cargotypecategoryid = (int)$row[16];
					$transportmodeid = (int)$row[17];
					$carrierregno = mysql_escape_string($row[18]);
					$containertypeid = (int)$row[19];
					$marketid = (int)$row[20];
					$originalsealno = mysql_escape_string($row[21]);
					$stakeholdersealno = mysql_escape_string($row[22]);
					if($mcount > 1) {
						$v = $v."('$containerno','$consignmentno','$blnumber','$manifestno','$shippingvessel','$shippingline','$por','$pol','$pod','$orgcountryid','$destcountryid','$hscode','$description','$volume','$weight','$cargotypecategoryid','$transportmodeid','$carrierregno','$containertypeid','$marketid','$originalsealno','$stakeholdersealno','$monthid','$yearid','$stakeholderid')".", ";
						//echo $sql."<br><br>";
						//runquery($sql);
					}
					$mcount++;
					if($mcount == 10) {
						//break;
					}
				}
				$sql = "insert into `$db` (`containerno`,`consignmentno`,`blnumber`,`manifestno`,`shippingvessel`,`shippingline`,`por`,`pol`,`pod`,`orgcountryid`,`destcountryid`,`hscode`,`description`,`volume`,`weight`,`cargotypecategoryid`,`transportmodeid`,`carrierregno`,`containertypeid`,`marketid`,`originalsealno`,`stakeholdersealno`,`monthid`,`yearid`,`stakeholderid`)";
				$sql = $sql." values".substr($v, 0, strlen($v) - 2).";";
				//echo $sql;
				runquery($sql);
				$str = $str.messagebox("Successfully Imported $f XLS file", true);
			}
			else {
				$str = $str.messagebox("Invalid number of Columns in your XLS document.", false);
			}
			break;
		}
	}   
	return $str;
}

function formatthisdate($d) {
	if (strlen(trim($d)) > 0) {
		$d = strtotime($d);
		$d = date('Y-m-d H:i:s', $d);
	}
	return $d;
}


function show_form($surveyid) {
	$str = $str."<form method=\"post\" action=\"import_roadsurvey.php\" enctype=\"multipart/form-data\">";
	$str = $str."<table border=0 cellpadding=5 cellspacing=0 width=\"100%\">";
	$str = $str."<tr><td>Survey:</td><td>".dropdown_extended("surveyid", $surveyid, true)."</td></tr>";
	//$str = $str."<tr><td>Month:</td><td>".dropdown_extended("monthid", $monthid, true, "", "", false, "id asc")."</td></tr>";
	//$str = $str."<tr><td>Year:</td><td>".dropdown_extended("yearid", $yearid, true)."</td></tr>";
	$str = $str."<tr><td colspan=2><input type=file id=\"filename\" name=\"filename\" size=30></td></tr>";
	$str = $str."<tr><td align=left colspan=2><input type=submit value=import></td></tr>";
	$str = $str."</table>";
	$str = $str."<input type=\"hidden\" name=\"a\" value=\"1\">";
	$str = $str."</form>";
	return $str;
}


?>
