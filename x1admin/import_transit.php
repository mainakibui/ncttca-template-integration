<?php
set_time_limit(5000);
require_once("includes/config.php");
require_once("../codebase/plugins/excel/reader.php");
//$mtitle = "Transit Data Import: Excel to DB";
$a = get_default(clean($_POST['a']), "n", 0);
$stakeholderid = get_default(clean($_POST['stakeholderid']), "n", 0);
$subnodeid = get_default(clean($_POST['subnodeid']), "n", 0);
$routeid = get_default(clean($_POST['routeid']), "n", 0);
$monthid = get_default(clean($_POST['monthid']), "n", 0);
$yearid = get_default(clean($_POST['yearid']), "n", 0);

if (loggedin()) {
	if ($a > 0 && is_numeric($a)) {
		$mystr = $mystr.import($stakeholderid, $subnodeid, $monthid, $yearid);
	}
	$mystr = $mystr."<table width='100%' border=0 cellspacing=8 cellpadding=0>";
	$mystr = $mystr."<tr valign=top align=left><td width=60%>";
	$mystr = $mystr."<b>How to use this console</b><br><br>To import transit data from excel to DB, browse for the excel file and then click on the import button. The excel file MUST be in the correct format with fields A to K arranged as follows:-<br>";
	$mystr = $mystr."<ul>";
	$mystr = $mystr."<li>A: Container Number</li>";
	$mystr = $mystr."<li>B: Consignment Number</li>";
	$mystr = $mystr."<li>C: Bill of Landing</li>";
	$mystr = $mystr."<li>D: Manifest Number</li>";
	$mystr = $mystr."<li>E: Date IN</li>";
	$mystr = $mystr."<li>F: Date OUT</li>";
	$mystr = $mystr."<li>G: ".code_popup("market", "Market ID")."</li>";
	$mystr = $mystr."<li>H: ".code_popup("modecarriertype", "Mode Carrier Type ID")."</li>";
	$mystr = $mystr."<li>I: Mode Carrier Reg Number</li>";
	$mystr = $mystr."<li>J: Mode Carrier Trailler Number</li>";
	$mystr = $mystr."<li>K: Mode Carrier Mark</li>";
	$mystr = $mystr."</ul>";
	$mystr = $mystr."</td><td width=1 bgcolor=#e0e0e0><img src='images/vmargin.gif' heig=10 width=1></td>";
	$mystr = $mystr."<td width=\"40%\" valign=top>".show_form($stakeholderid, $subnodeid, $yearid, $monthid)."</td></tr>";
	$mystr = $mystr."</table>";
	print top().$mystr.bottom();
}
else {
	header("location: login.php");
}


function import($stakeholderid, $subnodeid, $monthid, $yearid) {
	$db = "transit_copy";
	$f = $_FILES["filename"]["name"];
	$doc_type = $_FILES["filename"]["type"];
	$doc_allowed = array("application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	
	foreach($_POST as $key=>$value) {
		if (strlen(trim($value)) == 0) {
			$m = $m."<li>".ucwords(hashtable($key))." is empty or invalid.</li>";
		}
	}
	if (!in_array($doc_type, $doc_allowed)) {
		$m = $m."<li>Invalid File selected. Please select a .XLS file</li>";
	} 
	if (strlen($m) > 0) {
		$str = $str.messagebox("<ul>$m</ul>", false);
	}
	else {
		$filename = $_FILES['filename']['tmp_name'];

		$reader = new Spreadsheet_Excel_Reader();
		$reader->setUTFEncoder('mb');
		$reader->setDefaultFormat('%1.0f');
		$reader->setOutputEncoding('UTF-8');
		$reader->read($filename);
		//$reader->setOutputEncoding('CP-1251');
		$reader->setRowColOffset(0);
		$cells = 15;

		foreach($reader->sheets as $k=>$data) {
			if ($cells == $data['numCols']) {
				$mcount = 1;
				foreach($data['cells'] as $row) {
					$id = getid($db);
					$containerno = mysql_escape_string($row[1]);
					$consignmentno = mysql_escape_string($row[2]);
					$blnumber = mysql_escape_string($row[3]);
					$manifestno = mysql_escape_string($row[4]);
					//$subnodeid = $row[5];
					//$stakeholderid = $row[6];
					$datein = formatthisdate($row[7]);
					$dateout = formatthisdate($row[8]);
					//$monthid = $row[9];
					//$yearid = $row[10];
					$marketid = $row[11];
					$modecarriertypeid = $row[12];
					$modecarrierregno = mysql_escape_string($row[13]);
					$modecarriertraillerno = mysql_escape_string($row[14]);
					$modecarriermark = mysql_escape_string($row[15]);
					if($mcount > 1) {
						//$v = $v."('$id','$containerno','$consignmentno','$blnumber','$manifestno','$subnodeid','$stakeholderid','$datein','$dateout','$monthid','$yearid','$marketid','$modecarriertypeid','$modecarrierregno','$modecarriertraillerno','$modecarriermark')".", ";
						$v = $v."('$containerno','$consignmentno','$blnumber','$manifestno','$subnodeid','$stakeholderid','$datein','$dateout','$monthid','$yearid','$marketid','$modecarriertypeid','$modecarrierregno','$modecarriertraillerno','$modecarriermark')".", ";
						//echo $sql."<br><br>";
						//runquery($sql);
					}
					$mcount++;
					if($mcount == 10) {
						//break;
					}
				}
				//$sql = "insert into `$db` (`id`,`containerno`,`consignmentno`,`blnumber`,`manifestno`,`subnodeid`,`stakeholderid`,`datein`,`dateout`,`monthid`,`yearid`,`marketid`,`modecarriertypeid`,`modecarrierregno`,`modecarriertraillerno`,`modecarriermark`)";
				$sql = "insert into `$db` (`containerno`,`consignmentno`,`blnumber`,`manifestno`,`subnodeid`,`stakeholderid`,`datein`,`dateout`,`monthid`,`yearid`,`marketid`,`modecarriertypeid`,`modecarrierregno`,`modecarriertraillerno`,`modecarriermark`)";
				$sql = $sql." values".substr($v, 0, strlen($v) - 2).";";
				//echo $sql;
				runquery($sql);
				$str = $str.messagebox("Successfully Imported $f XLS file", true);
			}
			else {
				$str = $str.messagebox("Invalid number of Columns in your XLS document.", false);
			}
			break;
		}
	}   
	return $str;
}

function formatthisdate($d) {
	if (strlen(trim($d)) > 0) {
		$d = strtotime($d);
		$d = date('Y-m-d H:i:s', $d);
	}
	return $d;
}


function show_form($stakeholderid, $subnodeid, $routeid, $yearid, $monthid) {
	$str = $str."<form method=\"post\" action=\"import_transit.php\" enctype=\"multipart/form-data\">";
	$str = $str."<table border=0 cellpadding=5 cellspacing=0 width=\"100%\">";
	$str = $str."<tr><td>Stakeholder:</td><td>".dropdown_extended("stakeholderid", $stakeholderid, true)."</td></tr>";
	$str = $str."<tr><td>Sub Node:</td><td>".dropdown_extended("subnodeid", $subnodeid, true, "", "title <> ''", false, "title asc")."</td></tr>";
	$str = $str."<tr><td>Route:</td><td>".dropdown_extended("routeid", $routeid, true, "", "title <> ''", false, "title asc")."</td></tr>";
	$str = $str."<tr><td>Month:</td><td>".dropdown_extended("monthid", $monthid, true, "", "", false, "id asc")."</td></tr>";
	$str = $str."<tr><td>Year:</td><td>".dropdown_extended("yearid", $yearid, true)."</td></tr>";
	$str = $str."<tr><td colspan=2><input type=file id=\"filename\" name=\"filename\" size=30></td></tr>";
	$str = $str."<tr><td align=left colspan=2><input type=submit value=import></td></tr>";
	$str = $str."</table>";
	$str = $str."<input type=\"hidden\" name=\"a\" value=\"1\">";
	$str = $str."</form>";
	return $str;
}
?>
