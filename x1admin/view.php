<?php
require_once("includes/config.php");
$t = trim($_GET["t"]);

if (loggedin()) {
	if (strlen($t) > 0) {
		$sql = "select id, title from `$t` order by id asc;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$mystr = $mystr."<div id=\"thisdiv\" width='100%'>";
			$mystr = $mystr."&nbsp;&nbsp;<font size=\"+1\">".titlecase(hashtable($t))."&nbsp;Codes</font><br><br>";
			$mystr = $mystr."<center><table border=\"0\" cellpadding=\"5\" cellspacing=\"1\" width='98%' bgcolor=#e0e0e0>";
			$mystr = $mystr."<tr bgcolor=\"#ffffff\" align=\"left\"><td><b>Code</b></td><td><b>Title</b></td></tr>";
			while (!$rs->eof()) {
				$mystr = $mystr."<tr bgcolor=\"#ffffff\" align=\"left\"><td>".$rs->row("id")."</td><td width=\"100%\">".$rs->row("title")."</td></tr>";
				$rs->movenext();
			}
			$mystr = $mystr."</table></center></div>";
		}
	}
	else {
		$mystr = $mystr.messagebox("the requested action could not be validated", false);
	}
}
else {
	$mystr = $mystr.messagebox("you are not properly logged in. Login and then try again", false);
}

errortrap(headerize($mystr));





?>


