<?php
set_time_limit(5000);
require_once("includes/config.php");
require_once("../codebase/plugins/excel/reader.php");
//$mtitle = "Transit Data Import: Excel to DB";
$a = get_default(clean($_POST['a']), "n", 0);
$gpsdeviceid = get_default(clean($_POST['gpsdeviceid']), "n", 0);
$tripid = get_default(clean($_POST['tripid']), "n", 0);


if (loggedin()) {
	if ($a > 0 && is_numeric($a)) {
		$mystr = $mystr.import($gpsdeviceid, $tripid);
	}
	$mystr = $mystr."<table width='100%' border=0 cellspacing=8 cellpadding=0>";
	$mystr = $mystr."<tr valign=top align=left><td width=60%>";
	$mystr = $mystr."<b>How to use this console</b><br><br>To import GPS data from excel to DB, browse for the excel file and then click on the import button. The excel file MUST be in the correct format with fields A to E arranged as follows:-<br>";
	$mystr = $mystr."<ul>";
	$mystr = $mystr."<li>A: Latitude</li>";
	$mystr = $mystr."<li>B: Longitude</li>";
	$mystr = $mystr."<li>C: Altitude</li>";
	$mystr = $mystr."<li>D: Date</li>";
	$mystr = $mystr."<li>E: Odometer Reading</li>";
	$mystr = $mystr."<li>F: Speed</li>";
	$mystr = $mystr."<li>G: Address</li>";
	$mystr = $mystr."<li>H: ".code_popup("gpsstatus", "GPS Status ID")."</li>";
	$mystr = $mystr."</ul>";
	$mystr = $mystr."</td><td width=1 bgcolor=#e0e0e0><img src='images/vmargin.gif' heig=10 width=1></td>";
	$mystr = $mystr."<td width=\"40%\" valign=top>".show_form($gpsdeviceid, $tripid)."</td></tr>";
	$mystr = $mystr."</table>";
	print top().$mystr.bottom();
}
else {
	header("location: login.php");
}


function import($gpsdeviceid, $tripid) {
	$db = "gpsdata";
	$f = $_FILES["filename"]["name"];
	$doc_type = $_FILES["filename"]["type"];
	$doc_allowed = array("application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	foreach($_POST as $key=>$value) {
		if (strlen(trim($value)) == 0) {
			$m = $m."<li>".ucwords(hashtable($key))." is empty or invalid.</li>";
		}
	}
	if (!in_array($doc_type, $doc_allowed)) {
		$m = $m."<li>Invalid File selected. Please select a .XLS file</li>";
	}
	if (strlen($m) > 0) {
		$str = $str.messagebox("<ul>$m</ul>", false);
	}
	else {
		$filename = $_FILES['filename']['tmp_name'];
		$reader = new Spreadsheet_Excel_Reader();
		$reader->setUTFEncoder('mb');
		//$reader->setDefaultFormat('%1.0f');
		$reader->setOutputEncoding('UTF-8');
		$reader->read($filename);
		//$reader->setOutputEncoding('CP-1251');
		$reader->setRowColOffset(0);
		$cells = 8;

		foreach($reader->sheets as $k=>$data) {
			if ($cells == $data['numCols']) {
				$mcount = 1;
				foreach($data['cells'] as $row) {
					$id = getid($db);
					$latitude = mysql_escape_string($row[1]);
					$longitude = mysql_escape_string($row[2]);
					$altitude = mysql_escape_string($row[3]);
					$date = formatthisdate($row[4]);
					$odometer = mysql_escape_string($row[5]);
					$speed = mysql_escape_string($row[6]);
					$address = mysql_escape_string($row[7]);
					$gpsstatusid = (int)$row[8];
					
					if($mcount > 1) {
						$v = $v."('$id','$latitude','$longitude','$date','$odometer','$speed','$gpsdeviceid','$tripid')".", ";
					}
					$mcount++;
				}
				$sql = "insert into `$db` (`id`,`latitude`,`longitude`,`date`,`odometer`,`speed`,`gpsdeviceid`,`tripid`)";
				$sql = $sql." values".substr($v, 0, strlen($v) - 2).";";
				//echo $sql;
				runquery($sql);
				$str = $str.messagebox("Successfully Imported $f XLS file", true);
			}
			else {
				$str = $str.messagebox("Invalid number of Columns in your document.", false);
			}
			break;
		}
	}  
	return $str;
}

function formatthisdate($d) {
	if (strlen(trim($d)) > 0) {
		$d = strtotime($d);
		$d = date('Y-m-d H:i:s', $d);
	}
	return $d;
}


function show_form($gpsdeviceid, $tripid) {
	$str = $str."<form method=post action=import_gps.php enctype=\"multipart/form-data\">";
	$str = $str."<table border=0 cellpadding=5 cellspacing=0 width=\"100%\">";
	$str = $str."<tr><td>GPS Device:</td><td>".dropdown_extended("gpsdeviceid", $gpsdeviceid, true, "", "", false, "id asc")."</td></tr>";
	$str = $str."<tr><td>Trip:</td><td>".dropdown_extended("tripid", $tripid, true, "", "", false, "id asc")."</td></tr>";
	$str = $str."<tr><td colspan=2><input type=file id=\"filename\" name=\"filename\" size=30></td></tr>";
	$str = $str."<tr><td align=left colspan=2><input type=submit value=import></td></tr>";
	$str = $str."</table>";
	$str = $str."<input type=\"hidden\" name=\"a\" value=\"1\">";
	$str = $str."</form>";
	return $str;
}
?>
