<?php
require_once("includes/config.php");

$id = get_default(clean($_GET['id']), "n", 0);
$p = get_default(clean($_GET['p']), "n", 1);
$keywords = clean($_GET['keywords']);
$field  = clean($_GET['field']);
$currenttable = "agent";

if (loggedin() && accesscontrol()) {
	$fields = "*";
	$filter = null;
	$limit = 20;
	$add = true;
	$view = true;
	$edit = true;
	$delete = true;
	$dbsearch = true;
	$dbfilters = true;
	$dbfilters_excluded = "";
	$navigation = true;
	$multiselect = false;
	$multiselectheader = null;
	$multiselectscript = null;
	$mystr = $mystr.listing($currenttable, $fields, $filter, $limit, $add, $view, $edit, $delete, $dbfilters, $dbfilters_excluded, $navigation, $dbsearch, $multiselect, $multiselectheader, $multiselectscript, $multiselectfield, $multiselectfieldvalue);
	print top().$mystr.bottom();
}
else {
   	header("Location: "."login.php");
}

?>

