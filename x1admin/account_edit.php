<?php
require_once("includes/config.php");
$id = "1";
$a = trim($_POST["a"]);
$email = clean(strtolower($_POST['email']));

if (strlen($a) == 0 && !is_numeric($a)) {
    $a = 1;
}
else {
	$a = $a + 0;
}

if (!loggedin() && accesscontrol()) {
	header("Location: "."index.php");
}
else {
	if (strlen($a) > 0 && is_numeric($a)) {
		switch($a) {
			case 1:
				$mtitle = "Edit Account Details";
				$mystr = $mystr.editaccount();
			break;
			case 2:
				$mtitle = "Edit Account Details";
				$mystr = $mystr.updateaccount();
			break;
			default:
				$mystr = $mystr.messagebox("Sorry, the requested action is not available",false);
			break;
		}
	}
	else {
		$mystr = $mystr.messagebox("Sorry, the requested action is not available",false);
	}
	print top().$mystr.bottom();
}


function editaccount() {
	global $dba;
	$sql = "select name, email, username, address, telephone, town, countryid from `account` where id=".user("id").";";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<br><b>*</b> Indicates Required fields.";
		$str = $str."<form method=\"post\" action=\"account_edit.php\" name=\"editaccount\">";
		$str = $str."<table border='0' cellspacing='8' cellpadding='0' align=left>";
		$str = $str."<tr><td align=left>Names:</td><td>".$rs->row("name")."</td></tr>";
		$str = $str."<tr><td align=left>Username:</td><td>".$rs->row("username")."</td></tr>";
		$str = $str."<tr><td align=left>* Email:</td><td>".textfield("email", $rs->row("email"),24)."</td></tr>";
		$str = $str."<tr><td align=left>* Telephone:</td><td>".textfield("telephone", $rs->row("telephone"),24)."</td></tr>";
		$str = $str."<tr valign=top><td align=left>* Address:</td><td>".textarea("address", $rs->row("address"))."</td></tr>";
		$str = $str."<tr><td align=left>* Town:</td><td>".textfield("town", $rs->row("town"),24)."</td></tr>";
		$str = $str."<tr><td align=left>* Country:</td><td>".dropdown("countryid", $rs->row("countryid"))."</td></tr>";
		$str = $str."<tr><td align=right colspan=2><input type=submit value=\"Update My Info\" class=button></td></tr>";
		$str = $str."<input type=hidden name=\"a\" value=2>";
		$str = $str."</table><form>";
	}
	return $str;
}

function updateaccount() {
	global $dba, $email;
	$cols = array();
	$values = array();
	$t = "account";
	foreach ($_POST as $i=>$j) {
		if (strlen($j) == 0 && trim(strtolower($i)) != "a" && trim(strtolower($i)) != "email")  {
			$m = $m."<li>".ucfirst(hashtable($i))." is empty or invalid</li>";
		}
	}
	if (strlen($email) < 6 || (strpos($email,"@") ? strpos($email,"@") + 1 : 0) < 1 || (strpos($email,".") ? strpos($email,".") + 1 : 0) < 1) {
		$m = $m."<li>The email provided is invalid, use a valid email address.</li>";
	}
	if (strlen($m) > 0) {
		$str = $str."Some mandatory fields have not been filled in. Please complete the field(s) listed below:-";
		$str = $str."<ul type=square>".$m."</ul><a href=\"javascript:window.history.go(-1)\">Click here to go back and complete the missing fields</a>";
		$str = messagebox($str,false);
	}
	else {
		foreach ($_POST as $i=>$j) {
			if (trim(strtolower($i)) != "a") {
				array_push($cols, $i);
				array_push($values, $_POST[$i]);
			}
		}
		if (sizeof($cols) > 0) {
			$sql = $sql."update ".$t." set ";
			for ($i = 0; $i < sizeof($cols); $i++) {
				$sql = $sql."`".$cols[$i]."`"."='".mmysql_real_escape_string($values[$i])."'";
				if ($i + 1 < sizeof($cols)) {
					$sql = $sql.",";
				}
			}
			$sql = $sql." where id=".user("id").";";
			$rs = $dba->execute($sql);
			if ($dba->querystatus) {
				$mflag = true;
				$str = messagebox("Your account has been updated successfully",true);				
			}
			else {
				$str = messagebox("An error has occured while updating your account.",false);
			}
			$str = $str.editaccount();
		}
	}
	return $str;
}

?>
