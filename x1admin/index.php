<?php
require_once("includes/config.php");

if (loggedin() && accesscontrol()) {
	
	if (strlen(trim($_SESSION['callback_message'])) > 0) {
		$mystr = $mystr.$_SESSION['callback_message'];
		$_SESSION['callback_message'] = null;
	}
	
	$mystr = $mystr."<center><table width='90%' border=0 cellspacing=8 cellpadding=0>";

	$mystr = $mystr."<tr valign=top><td align=left colspan=3><h3>My Login Account</h3></td></tr>";
	$mystr = $mystr."<tr valign=top><td><img src=images/margin.gif border=0 width=70 height=1></td><td width='100%' align=left>".tb_view("account", "*", "id='".user("id")."'")."</td><td align=right>".button("<a href=\"account_edit.php\"><b><font class=textbright>&nbsp;Change&nbsp;Account&nbsp;Info&nbsp;</font></b></a>")."<br><br>".button("<a href=\"account_password_edit.php\"><b><font class=textbright>Change&nbsp;My&nbsp;Password</font></b></a>")."</td></tr>";
	$mystr = $mystr."<tr valign=top><td align=left colspan=3 bgcolor=#e0e0e0><img src=images/margin.gif border=0></td></tr>";

	$mystr = $mystr."<tr valign=top><td width='100%' colspan=3 align=left><h3>Login History</h3>".list_items("session", "logindate as `login date`,enddate as `last seen`, timediff(enddate,logindate) as `duration (hh:mm:ss)`, concat(host, ' (', ip, ' )') as `ip address`", "username='".user("username")."'", "id desc", "limit 5", false)."<br><br></td></tr>";
	$mystr = $mystr."</table></center>";		
	
	print top().$mystr.bottom();
}
else {
	header("Location: "."login.php");
}



function tb_view($tb, $fields, $filter) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($fields)) && strlen(trim($filter)) > 0) {
		$sql = "select ";
		if (trim($fields) == "*") {
			$sql = $sql.$fields;
		}
		else {
			$sql = $sql."id,".$fields;
		}	
		$sql = $sql." from `$tb` where $filter limit 1;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<table border=0 cellpadding=0 cellspacing=0>";
			for ($i = 1; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				$v = $rs->row($i);
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
					$v = getmyfield($f, $v);
				}
				$f = ucwords(hashtable($f));
				if (strlen(trim($v)) == 0) {
					$v = "<font color=#a0a0a0>(not specified)</font>";
				}
				if (strtolower(trim($f)) == "password") {
					$v = "******".substr($v, strlen($v) - 2, 2);
				}
				if (strtolower(trim($f)) != "password") {
					$str = $str."<tr><td><font class=texthighlighted>".putspace($f)."&nbsp;&nbsp;&nbsp;&nbsp;</font></td><td>".removehtml($v)."</td></tr>";
				}
			}
			$str = $str."</table>";
		}
	}
	return $str;
}


function list_items($tb, $fields, $filter, $o = "id", $limit = null, $manage = true) {
	global $dba, $application;
	$tb = trim(strtolower($tb));
	switch($tb) {
		case "office":
		$t = 1;
		break;

		case "programme":
		$t = 2;
		break;

		case "project":
		$t = 3;
		break;
	}
	if (strlen(trim($tb)) > 0 && strlen(trim($fields)) && strlen(trim($filter)) > 0) {
		$sql = "select ";
		if (trim($fields) == "*") {
			$sql = $sql.$fields;
		}
		else {
			$sql = $sql."id,".$fields;
		}	
		$sql = $sql." from `$tb` where $filter order by $o ";
		if (strlen(trim($limit)) > 0) {
			$sql = $sql.$limit;
		}
		$sql = $sql.";";
		$rs = $dba->execute($sql);
		$str = $str."<br><table width=\"100%\" border=0 cellspacing=1 cellpadding=5 bgcolor=e0e0e0>";
		$str = $str."<tr>";
		for ($i = 1; $i < sizeof($rs->fields); $i++) {
			$f = $rs->fields[$i];
			if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
				$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
			}
			$str = $str."<td background=\"includes/templates/".$application["template"]."/images/barbkg.gif\"><b>".strtoupper($f)."</b></td>";
		}
		if ($manage) {
			$str = $str."<td align=right background=\"includes/templates/".$application["template"]."/images/barbkg.gif\" colspan=2>";
			$str = $str.popup("<b>Add&nbsp;New&nbsp;".titlecase($tb)."</b>", "account_item.php?t=$t&a=1");
			$str = $str."</td>";
		}	
		$str = $str."</tr>";
		if (!$rs->eof()) {
			while(!$rs->eof()) {
				$str = $str."<tr bgcolor=#ffffff>";
				for ($i = 1; $i < sizeof($rs->fields); $i++) {
					$f = $rs->fields[$i];
					$v = $rs->row($i);
					if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
						$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
						$v = getmyfield($f, $v);
					}

					$str = $str."<td>".removehtml($v)."</td>";
				}
				if ($manage) {
					$str = $str."<td width=1>".popup("Edit", "account_item.php?t=$t&a=2&id=".$rs->row("id"))."</td>";
					$str = $str."<td width=1>".popup("Del", "account_item.php?t=$t&a=3&id=".$rs->row("id"))."</td>";
				}
				$str = $str."</tr>";
				$rs->movenext();
			}
		}
		else {
			$str = $str."<tr bgcolor=#ffffff><td colspan=".(sizeof($rs->fields) - 1 + 2)."><font color=#a0a0a0>No $tb items are available</font></td></tr>";
		}
		$str = $str."</table>";
	}
	return $str;
}


?>
