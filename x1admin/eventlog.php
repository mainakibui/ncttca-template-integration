<?php
require_once("includes/config.php");

if (loggedin() && accesscontrol() == true) {
	$id = clean($_GET['id']);
	$keywords = clean($_GET['keywords']);
	$p = clean($_GET['p']);
	$field = clean($_GET['field']);

	if (!is_numeric($id)) {
	    $id = 0;
	}
	else {
		$id = $id + 0;
	}

	if (!is_numeric($p)) {
	    $p = 1;
	}
	else {
		$p = $p + 0;
	}
	$currenttable = "eventlog";
	$fields = "action, record as 'Record Title', `table` as 'database', accountid, date, ip";
	$filter = null;
	$limit = 30;
	$add = false;
	$view = true;
	$edit = false;
	$delete = false;
	$dbsearch = false;
	$dbfilters = true;
	$dbfilters_excluded = "";
	$navigation = true;
	$multiselect = false;
	$multiselectheader = null;
	$multiselectscript = null;

	if (strlen(trim($page[4])) > 0) {
		$mystr = $mystr.messagebox($page[4]);
	}
	if (strlen(trim($currenttable)) > 0) {
		$mystr = $mystr."<table border=0 cellpadding=0 cellspacing=8 width='100%'>";
		$mystr = $mystr."<tr valign=top>";
		$mystr = $mystr."<td width='100%'>".listing($currenttable, $fields, $filter, $limit, $add, $view, $edit, $delete, $dbfilters, $dbfilters_excluded, $navigation, $dbsearch, $multiselect, $multiselectheader, $multiselectscript, $multiselectfield, $multiselectfieldvalue)."</td>";
		$mystr = $mystr."</tr></table>";
	}
	else {
		$mystr = $mystr.messagebox("Cannot determine default database or you have been denied access", false);
	}
	print print top().$mystr.bottom();
}
else {
   	header("Location: "."index.php");
}



?>

