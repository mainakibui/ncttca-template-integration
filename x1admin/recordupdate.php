<?php
require_once("includes/config.php");

$t = trim($_POST["t"]);
$id = trim($_POST["id"]);
$a = trim($_POST["a"]);

if (loggedin() && strlen(trim($t)) > 0 && strlen(trim($a)) > 0 && is_numeric($a)) {
	$mflag = false;
	$tag = "";
	$name = array();
	$type = array();
	
	$rsql = "select * from `$t` limit 1";
	$rscheck = $dba->execute($rsql);
	if (!$rscheck->eof()) {
		for ($i = 1; $i < $rscheck->fieldcount(); $i++) {
			if (strtolower(trim($rscheck->types[$i])) == "newdecimal" || strtolower(trim($rscheck->types[$i])) == "long") {
				array_push($type, strtolower(trim($rscheck->types[$i])));
				array_push($name, strtolower(trim($rscheck->fields[$i])));
			}
		}
	}
	foreach ($_POST as $i=>$j) {
		if (in_array(strtolower($i), $name) && !is_numeric($j) && strlen(trim($j)) > 0) {
			$m = $m."<li>".ucfirst(hashtable($i))." contains an invalid value, numeric value expected.</li>";
		}
	}
	if (strlen($m) > 0) {
		$mystr = $mystr."Error(s) have been detected in the submitted form. Please complete the field(s) listed below:-";
		$mystr = $mystr."<ul type=square>".$m."</ul><a href=\"javascript:window.history.go(-1)\">Click here to go back and complete the missing fields</a>";
		$mystr = messagebox($mystr, false);
	}
	else {
		if (trim($a) == "2") {
			if (strlen(trim($id)) > 0 && is_numeric($id)) {
				$mflag = updaterecord($t, $id, $a);
				$tag = "Updated";
			}
		}
		else {
			if (trim($a) == "1") {
				$mflag = updaterecord($t, 0, $a);
				$tag = "Created";
			}
			else {
				if (trim($a) == "3") {
					if (strlen(trim($id)) > 0 && is_numeric($id)) {
						$mflag = deleterecord($t,  $id, $a);
						$tag = "Deleted";
					}
				}
			}
		}
		if ($mflag == true) {
			//$message = "The <b>".ucwords($t)."</b> Item has been ".$tag;
			//$message = $message." Sucessfully.<br>Click on the \"<b>Close</b>\" button below to close this window.";
			//$message = $message."<br><div align=right>".button("<span onmouseover=\"this.style.cursor='pointer';\" onclick=\"parent.parent.GB_hide();parent.parent.location.href=parent.parent.location.href;\"><font color=#ff6600>Close Window</font></span>")."<br>";
			//$mystr = $mystr.messagebox($message, true);
			$message = $message."\n\n<script language=javascript1.2>\nparent.parent.GB_hide();\nparent.parent.location.href=parent.parent.location.href;\n</script>";
			$mystr = $mystr.$message;
		}
		else {
			$message = "The <b>".ucwords($t)."</b> Item could not be ".$tag;
			$mystr = $mystr.messagebox($message, false);
		}
	}
}
else {
	$message = $message."Sorry your session has expired. Please login and try again";
	$mystr = $mystr.messagebox($message, false);
}
errortrap(headerize($mystr));











?>
