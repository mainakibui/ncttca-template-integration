<?php
//Find temp directory
//$tmp_dir = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();
//die($tmp_dir);

set_time_limit(5000);
require_once("includes/config.php");
require_once("../codebase/plugins/excel/reader.php");
$mtitle = "Dynamic Data Import";
$a = get_default(clean($_POST['a']), "n", 0);
$tablename = clean($_POST['importtable']);

$sql = "select * from import_admin_log where ailog_columnstatus='on';";
$rs = $dba->execute($sql);

if ($rs->eof()) 
{
	$tabledetails = array();
}
else
{
	while (!$rs->eof()) 
	{
		$tabledetails[$rs->row('ailog_tablename')][$rs->row('ailog_tablecolumn')][] = preg_replace("/\([^)]+\)/","",$rs->row('ailog_columntype'));
		$rs->movenext();
	}
}


if (loggedin()) {
	if ($a > 0 && is_numeric($a) && isset($tablename) ) {
		
		$count=1;
		$separator = "";
		$string = "(";
		foreach($tabledetails[$tablename] as $key => $item)
		{
			$fields[$count] = $key;
			$string = $string.$separator.$key;
			$fieldstype[$count] = $item[0];
			$count++;
			$separator = ",";
		};
		$string = $string.")";
		$headerData[] = $string;
		
		$mystr = $mystr.import($tablename, $fields, $fieldstype, $headerData);
	}
	$mystr = $mystr."<table width='100%' border=0 cellspacing=8 cellpadding=0>";
	$mystr = $mystr."<tr valign=top align=left><td width=60%>";
	$mystr = $mystr."<b>How to use this console</b><br><br><p>To import data from excel to DB</p> <br> <ol><li>Select a table below</li> <li>Browse for the excel file and then click on the import button.</li></ol> <p>NB: The excel file MUST be in the correct format with fields A to Z arranged as follows below:</p><br>";
	
	$mystr = $mystr."<select id='change_table'>";
	$mystr = $mystr."<option disabled ".(!isset($tablename) || $tablename=='' ? 'selected' : '')." > -- Select a table to proceed-- </option>";
	if(sizeof($tabledetails) > 0)
	{
		foreach($tabledetails as $table => $olumn){
			$mystr = $mystr."<option ".( isset($tablename) && $tablename!='' && $table==$tablename ? 'selected' : '')." value='".$table."'>$table</option>";
		}
	}
	else
	{
		$mystr = $mystr."<option value=''>no tables has an active import column</option>";
	}
	$mystr = $mystr."</select>";
	
	$mystr = $mystr."<div id='available_columns'>";
	$mystr = $mystr."</div>";
	
	$mystr = $mystr."</td><td width=1 bgcolor=#e0e0e0><img src='images/vmargin.gif' heig=10 width=1></td>";
	$mystr = $mystr."<td width=\"40%\" valign=top>".show_form($tablename)."</td></tr>";
	$mystr = $mystr."</table>";
	
	//Get import select value
	$mystr = $mystr."<script type='text/javascript'>";
	$mystr = $mystr."$(document).ready(function() {
	$('#change_table').change(function(){
	tabledetails = ".json_encode($tabledetails).";
	table = $('#change_table option:selected').val();
	
	var html = '<ul>';
	$.each(tabledetails, function(k, v) {
		if(k==table){
			var counter = 0;
			$.each(v, function(k2, v2) {
				html = html+'<li>'+String.fromCharCode(65 + counter)+'. '+k2+'&nbsp;'+v2+'</li>';
				counter=counter+1;
			});
			
			$('#importtable').val(table);
		}
	});
	html = html+'</ul>';
	
	$('#available_columns').html(html);
	});
	});";
	$mystr = $mystr."</script>";
	
	print top().$mystr.bottom();
}
else {
	header("location: login.php");
}


function import($tablename, $fields, $fieldstype, $headerData) {
	$db = $tablename;
	$f = $_FILES["filename"]["name"];
	$doc_type = $_FILES["filename"]["type"];
	$doc_allowed = array("application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	
	foreach($_POST as $key=>$value) {
		if (strlen(trim($value)) == 0) {
			$m = $m."<li>".ucwords(hashtable($key))." is empty or invalid.</li>";
		}
	}
	
	if (!in_array($doc_type, $doc_allowed)) {
		$m = $m."<li>Invalid File selected. Please select a .XLS file</li>";
	}
	if (strlen($m) > 0) {
		$str = $str.messagebox("<ul>$m</ul>", false);
	}
	else {
		$filename = $_FILES['filename']['tmp_name'];

		$reader = new Spreadsheet_Excel_Reader();
		$reader->setUTFEncoder('mb');
		$reader->setDefaultFormat('%1.0f');
		$reader->setOutputEncoding('UTF-8');
		$reader->read($filename);
		//$reader->setOutputEncoding('CP-1251');
		$reader->setRowColOffset(0);
		$cells = count($fields);

		foreach($reader->sheets as $k=>$data) {
			if ($cells == $data['numCols']) {
				$mcount = 1;
				$ccount = count($data['cells']);
				$rowcount = count($data['cells']) -1;
				
				for ($x = 2; $x <= $ccount; $x++)
				{
					$separator = "";
					$string = "(";
						for ($x2 = 1; $x2 <= count($fields); $x2++)
						{
							if(strToLower($fieldstype[$x2])=='int' || strToLower($fieldstype[$x2])=='bigint')
							{
								$columnData = isset($data['cells'][$x][$x2])?$data['cells'][$x][$x2]:0;
							}
							else
							{
								$columnData = isset($data['cells'][$x][$x2]) ? "'".$data['cells'][$x][$x2]."'" : "'null'";
							}
							
							$string = $string.$separator.$columnData;
							$separator = ",";
						}
					$string = $string.")";
					$insertData[] = $string;
				}
				//echo"<pre>";
				//print_r($insertData);
				//print_r($headerData);
				//echo"</pre>";
				
				foreach($insertData as $row) {
				
				$sql = "insert into $db ".$headerData[0];
				$sql = $sql." values ".$row.";";
				//echo $sql."<br>";
				runquery($sql);
				}
				$str = $str.messagebox("Successfully Imported $rowcount rows from : $f", true);
				
			}
			else {
				$str = $str.messagebox("Invalid number of Columns in your XLS document.", false);
			}
			break;
		}
	}   
	return $str;
}

function formatthisdate($d) {
	if (strlen(trim($d)) > 0) {
		$d = strtotime($d);
		$d = date('Y-m-d H:i:s', $d);
	}
	return $d;
}


function show_form($tablename) {
	$str = $str."<form method=\"post\" action=\"import_dynamic.php\" enctype=\"multipart/form-data\">";
	$str = $str."<table border=0 cellpadding=5 cellspacing=0 width=\"100%\">";
	$str = $str."<tr><td>Table:</td><td> <input readonly='readonly' id='importtable' name='importtable' type='text' value='".$tablename."'> </td></tr>";
	$str = $str."<tr><td colspan=2><input type=file id=\"filename\" name=\"filename\" size=30></td></tr>";
	$str = $str."<tr><td align=left colspan=2><input type=submit value=import></td></tr>";
	$str = $str."</table>";
	$str = $str."<input type=\"hidden\" name=\"a\" value=\"1\">";
	$str = $str."</form>";
	return $str;
}


?>
