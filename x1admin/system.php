<?php
require_once("includes/config.php");
$id = clean($_GET['id']);
$keywords = clean($_GET['keywords']);
$p = clean($_GET['p']);
$field = clean($_GET['field']);

if (!is_numeric($id)) {
    $id = 0;
}
else {
	$id = $id + 0;
}

if (!is_numeric($p)) {
    $p = 1;
}
else {
	$p = $p + 0;
}


if (loggedin() && accesscontrol()) {
	$tables = gettables();
	if (sizeof(tables) == 0) {
		$mystr = $mystr."cannot enumerate databases. exiting";
	}
	else {
		if ($id > 0 && $id < sizeof($tables)) {
			$currenttable = $tables[$id];
		}
		else {
			for($i = 0;$i < sizeof($tables); $i++) {
				$currenttable = $tables[$i];
				break;
			}
		}
		if (strlen(trim($currenttable)) > 0) {
			$mystr = $mystr."<div align=left>".toolbar($tables, $currenttable)."</div><hr noshade color=#e0e0e0 size=1>";
			$mystr = $mystr."<table border=0 cellpadding=0 cellspacing=8 width='100%'>";
			$mystr = $mystr."<tr valign=top>";
			//listing($currenttable, $fields = "*", $filter = null, $limit = 20, $add = false, $view = false, $edit = false, $delete = false, $dbfilters = false, $navigation = false, $actions = false)
			
			$fields = "*";
			$filter = null;
			$limit = 20;
			$add = true;
			$view = true;
			$edit = true;
			$delete = true;
			$dbsearch = false;
			$dbfilters = true;
			$dbfilters_excluded = "";
			$navigation = true;
			$multiselect = false;
			$multiselectheader = null;
			$multiselectscript = null;
	
			$mystr = $mystr."<td width='100%'>".listing($currenttable, $fields, $filter, $limit, $add, $view, $edit, $delete, $dbfilters, $dbfilters_excluded, $navigation, $dbsearch, $multiselect, $multiselectheader, $multiselectscript, $multiselectfield, $multiselectfieldvalue)."</td>";
			$mystr = $mystr."</tr></table>";			
		}
		else {
			$mystr = $mystr.messagebox("Cannot determine default database or you have been denied access", false);
		}
	}
	print top().$mystr.bottom();
}
else {
   	header("Location: "."index.php");
}


?>

