<?php
require_once("includes/config.php");
$id = clean($_GET['id']);
$search = clean($_GET['search']);
$p = clean($_GET['p']);
$field = clean($_GET['field']);
$t = clean(trim($_GET['t']));

if (!is_numeric($id)) {
    $id = 0;
}
else {
	$id = $id + 0;
}

if (!is_numeric($p)) {
    $p = 1;
}
else {
	$p = $p + 0;
}


if (loggedin() && accesscontrol()) {
	$tables = gettables();
	if (sizeof(tables) == 0) {
		$mystr = $mystr."cannot enumerate databases. exiting";
	}
	else {
		$excludedtables = array("account", "application", "session", "accounttype", "page", "eventlog");
		if (in_array($t, $tables) && !in_array($t, $excludedtables)) {
			$currenttable = $t;
		}
		if (strlen(trim($currenttable)) > 0) {
			$mtitle = hashtable($currenttable);
			//$mystr = $mystr."<div align=left>".toolbar($tables, $currenttable)."</div><hr noshade color=#e0e0e0 size=1>";
			$mystr = $mystr."<table border=0 cellpadding=0 cellspacing=8 width='100%'>";
			$mystr = $mystr."<tr valign=top>";
						
			$fields = "*";
			$filter = null;
			$limit = 20;
			$add = true;
			$view = true;
			$edit = true;
			$delete = true;
			$dbsearch = false;
			$dbfilters = true;
			$dbfilters_excluded = "";
			$navigation = true;
			$multiselect = false;
			$multiselectheader = null;
			$multiselectscript = null;
	
			$mystr = $mystr."<td width='100%'>".listing($currenttable, $fields, $filter, $limit, $add, $view, $edit, $delete, $dbfilters, $dbfilters_excluded, $navigation, $dbsearch, $multiselect, $multiselectheader, $multiselectscript, $multiselectfield, $multiselectfieldvalue)."</td>";
			$mystr = $mystr."</tr></table>";			
		}
		else {
			$mystr = $mystr.messagebox("Cannot determine default database or you have been denied access", false);
		}
	}
	print top().$mystr.bottom();
}
else {
   	header("Location: "."index.php");
}


function get_tables() {
	global $dba;
	$tb = array();
	$excludedtables = "*";
	$sql = "select title from `tables` order by title asc;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		while (!$rs->eof()) {
			if (trim($excludedtables) == "*") {
				array_push($tb, $rs->row(0));
			}
			else {
				if (substr_count(",".strtolower($excludedtables).",", ",".strtolower($rs->row(0)).",") > 0) {
					array_push($tb, $rs->row(0));
				}
			}
			$rs->movenext();
		}
		return $tb;
	}
	else {
		return null;
	}
}

?>

