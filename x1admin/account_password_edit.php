<?php
require_once("includes/config.php");

$mtitle = "Change Account Password";

$a = trim($_POST["a"]);
$currentpassword = clean(strtolower($_POST['currentpassword']));
$password = clean(strtolower($_POST['password']));
$password2 = clean(strtolower($_POST['password2']));

if (strlen($a) == 0 && !is_numeric($a)) {
    $a = 1;
}

if (!loggedin()) {
	header("Location: "."login.php");
}
else {
	switch(trim($a)) {
		case "1":
			$mystr = $mystr.editaccount();
			break;
		case "2":
			$mystr = $mystr.updateaccount($currentpassword, $password, $password2);
			break;
		default:
			$_SESSION['callback_message'] = messagebox("Sorry, the requested action is not available",false);
			header("Location: "."account.php");
			break;
	}
	print top().$mystr.bottom();
}





function editaccount() {
	global $dba;
	$sql = "select name, email, username, password from `account` where id=".user("id").";";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str.messagebox("<font color=#cc3300><b>*</b> Indicates Required (Mandatory) Fields", false);
		$str = $str."<br><br><form method=\"post\" action=\"account_password_edit.php\" name=\"editaccount\">";
		$str = $str."<table border='0' cellspacing='8' cellpadding='0' align=left>";
		$str = $str."<tr><td align=left><b>Username</b></td><td><b><font class=texthighlighted>".$rs->row("username")."</font></b></td></tr>";
		$str = $str."<tr><td valign=top align=left><font color=#cc3300>*</font><b>Current Password</b></td><td>".passwordfield("currentpassword", "", 30, false)."</td></tr>";
		$str = $str."<tr><td valign=top align=left><font color=#cc3300>*</font><b>New Password</b></td><td>".passwordfield("password", "", 30, true)."</td></tr>";
		$str = $str."<tr><td align=right colspan=2><input type=submit value=\"Change Password\"></td></tr>";
		$str = $str."<input type=hidden name=\"a\" value=2>";
		$str = $str."</table><form>";
	}
	return $str;
}

function updateaccount($currentpassword, $password, $password2) {
	global $dba;
	$cols = array();
	$values = array();
	$t = "account";
	if ($password == $password2) {
		if (strlen($password) < 6 ) {
			$m = $m."<li>The password entered is too short.</li>";
		}
	}
	else {
		$m = $m."<li>The password entered do not match.</li>";
	}
	if (strlen($m) > 0) {
		$str = $str."Some mandatory fields have not been filled in. Please complete the field(s) listed below:-";
		$str = $str."<ul type=square>".$m."</ul><a href=\"javascript:window.history.go(-1)\">Click here to go back and complete the missing fields</a>";
		$str = messagebox($str, false);
	}
	else {
		if (sha1(strtolower(trim($currentpassword))) != strtolower(trim(getmyfieldid("account", user("id"), "password")))) {
			$str = messagebox("The current password provided is incorrect", false)."<br><br>";
		}
		else {
			$sql = "update `account` set password = '".sha1(strtolower(trim($password)))."' where id = ".user("id")." and username='".user("username")."' and password = '".sha1(strtolower(trim($currentpassword)))."';";
			$rs = $dba->execute($sql);
			if ($dba->querystatus) {
				$mflag = true;
				getuserprefs(user("username"), $password, "account");
				$_SESSION['callback_message'] = messagebox("Your account password has been changed successfully", true);
				header("Location: "."index.php");			
			}
			else {
				$_SESSION['callback_message'] = messagebox("An error has occured while updating your account. Please try again later", false);
				header("Location: "."index.php");
			}
		}		
		$str = $str.editaccount();
		
	}
	return $str;
}

?>
