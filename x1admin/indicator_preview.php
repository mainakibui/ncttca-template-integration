<?php
require_once("includes/config.php");
$id = get_default(clean($_GET['id']), "n", 0);
$p = get_default(clean($_GET['p']), "n", 1);
$export = clean($_GET['export']);

switch(strtolower($export)) {
	case "pdf":
	case "xls":
	case "xml":
	case "json":
		$export = $export;
	break;
	default:
		$export = "";
	break;
}


if (loggedin() && $id > 0) {
	$indicatortypeid = getmyfieldid("indicatorsubtype", $cid, "indicatortypeid");
	$mystr = $mystr."<table width='100%' border='0' cellspacing='5' cellpadding='0'>";
	$mystr = $mystr."<tr><td align=\"left\" valign=top>".show_indicator($id)."</td></tr>";
	$mystr = $mystr."</table>";
}
else {
   	$mystr = messagebox("There are no Indicators at the moment.", false);
}

errortrap(headerize($mystr));


function show_indicator($cid) {
	global $dba, $p, $application, $export;
	$x = $y = $xt = $yt = $tt = "";
	$sql = "select id, indicatorid, db, `fields`, singlefilterfield, multifilterfield, filter, groupby, orderby, `limit`, graphtypeid, plotx, ploty from `indicatorsubtype` where id = $cid limit 1;";
	$rs = $dba->execute($sql);
	if(!$rs->eof()) {
		$t = $rs->row("db");
		$fields = $rs->row("fields");
		$sfilters = $rs->row("singlefilterfield");
		$mfilters = $rs->row("multifilterfield");
		$filters = $sfilters.",".$mfilters;
		$filterby = $rs->row("filter");
		$groupby = $rs->row("groupby");
		$orderby = $rs->row("orderby");
		$limit = 0 + $rs->row("limit");
		$graphtype = $rs->row("graphtypeid");
		$plotx = $rs->row("plotx");
		$ploty = $rs->row("ploty");
		if ($limit == 0) {
			$limit = 20;
		}		
		$page = ($p - 1) * $limit;
		$str = $str."<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";
		if (strtolower(trim($export)) != "pdf" && strtolower(trim($export)) != "xls") {
			if (loggedin()) {
				$str = $str."<tr align=\"left\" valign=\"top\"><td>".show_filters($t, $sfilters, $mfilters, $filterby, $cid, $indicatorviewid, $tfield, $tfieldid)."</td></tr>";
			}
		}
		$str = $str."<tr align=\"left\" valign=\"top\"><td>";
		if (strlen(trim($t)) > 0 && strlen(trim($fields)) > 0) {
			$isql = "select $fields from `$t`";
			if (strlen(trim($filterby)) > 0) {
				$isql = $isql." where $filterby";
			}
			if (strlen(trim($filters)) > 0) {
				$filters = explode(",", $filters);
				foreach ($_GET as $i=>$j) {
					$ft = "";
					if (in_array(trim($i), $filters) || trim($i) == "reportingperiodid") {
						$a = $_GET[$i];
						if (!is_array($a)) {
							$a = explode("||||<|", $a);
						}
						foreach ($a as $j) {
							$j = clean($j);
							//echo $j;
							if (strlen(trim($j)) > 0) {
								if (strtolower(trim($i)) == "reportingperiodid") {
									if ($indicatortypeid == 1) {
										$ft = $ft."`".$t."`.`monthid` in (".get_months($j).") or ";
									}
									else {
										$ft = $ft."`".$t."`.`".strtolower(trim($i))."`='".$j."' or ";
									}
								}
								else {
									$ft = $ft."`".$t."`.`".strtolower(trim($i))."`='".$j."' or ";
								}
							}
						}		
					}
					if (strlen(trim($ft)) > 0) {
						$fsql = $fsql." (".substr($ft, 0, strlen($ft) - 4).") and ";
					}
				}
				if (strlen(trim($fsql)) > 0) {
					$fsql = substr($fsql, 0, strlen($fsql) - 4);
					if (strlen(trim($filterby)) > 0) {
						$isql = $isql." and $fsql";
					}
					else {
						$isql = $isql." where $fsql";
					}
				}
			}
			if (strlen(trim($groupby)) > 0) {
				$isql = $isql." group by $groupby";
			}
			if (strlen(trim($orderby)) > 0) {
				$isql = $isql." order by $orderby";
			}
			$gsql = $isql.";";
			if (strlen(trim($export)) > 0) {
				$isql = $gsql;
			}
			else {
				$isql = $isql." limit $page, $limit;";
			}
			//echo $gsql;
			$rsi = $dba->execute($isql);
			$rsg = $dba->execute($gsql);
			$pageitems = $rsg->recordcount();
			$fcg = $rsg->fieldcount();
			if (!$rsi->eof()) {
				$str = $str."<table border=\"0\" cellspacing=\"3\" cellpadding=\"0\" width=\"100%\">";
				if ($graphtype > 0 && strlen(trim($plotx)) > 0 && strlen(trim($ploty)) > 0) {
					$title = getmyfield("indicator", $rs->row("indicatorid"));
					if ($graphtype == 4) {
						$years = array();
						$months = array();
						$quarters = array();
						while (!$rsg->eof()) {
							for ($i = 0; $i < $fcg; $i++) {
								if (substr($rsg->fields[$i], (strlen($rsg->fields[$i])-2), 2) == "id" && strlen($rsg->fields[$i]) > 2) {
									$json[$rsg->fields[$i]][] = getmyfield(substr($rsg->fields[$i], 0, strlen($rsg->fields[$i])-2), $rsg->row($rsg->fields[$i]));
								}
								else {
									$json[$rsg->fields[$i]][] = $rsg->row($rsg->fields[$i]);
								}
							}
							array_push($years, $rsg->row("yearid"));
							if ($indicatortypeid == 1) {
								array_push($months, $rsg->row("monthid"));
							}
							if ($indicatortypeid == 2) {
								array_push($quarters, $rsg->row("reportingperiod"));
							}
							$rsg->movenext();
						}
						$years = array_unique($years);
						$months = array_unique($months);
						$quarters = array_unique($quarters);
						if (strtolower(trim($export)) == "json") {
							export_file($title.".json", "application/json", json_encode($json));
						}

						if (strtolower(trim($export)) == "xml") {
							export_file($title.".xml", "text/xml", arraytoxml($json));
						}
						$str = $str."<tr align=\"left\" valign=\"top\"><td>".show_graph($title, $graphtype, $years, $months, $quarters, $gsql, $plotx, $ploty)."</td></tr>";
					}
					else {
						$ploty = explode(",", $ploty);
						$xt = "Period";
						$yt = "Weight/Volume";
						$y = array();
						$json = array();
						while (!$rsg->eof()) {
							for ($i = 0; $i < $fcg; $i++) {
								if (substr($rsg->fields[$i], (strlen($rsg->fields[$i])-2), 2) == "id" && strlen($rsg->fields[$i]) > 2) {
									$json[$rsg->fields[$i]][] = getmyfield(substr($rsg->fields[$i], 0, strlen($rsg->fields[$i])-2), $rsg->row($rsg->fields[$i]));
								}
								else {
									$json[$rsg->fields[$i]][] = $rsg->row($rsg->fields[$i]);
								}
							}
							if (substr(strtolower($plotx), (strlen($plotx)-2), 2) == "id" && strlen($plotx) > 2) {
								if (strtolower(trim($plotx)) == "monthid") {
									$x[] = substr(getmyfield(substr($plotx, 0, strlen($plotx) - 2), $rsg->row($plotx)), 0, 3);
								}
								else {
									$x[] = getmyfield(substr($plotx, 0, strlen($plotx) - 2), $rsg->row($plotx));
								}
							}
							else {
								$x[] = $rsg->row($plotx);
							}
							foreach($ploty as $pt) {
								$y[$pt] = $y[$pt].$rsg->row($pt).",";
							}
							$rsg->movenext();
						}
						$x = implode("|", $x);
						$y = json_encode($y);

						$width  = "750";
						$height = "320";
						$graph_src = "jgraph.php?x=".urlencode($x)."&y=".urlencode($y)."&xt=".urlencode($xt)."&yt=".urlencode($yt)."&t=".urlencode($title)."&c=".urlencode(getmyfield("graphtype", $graphtype))."&w=".urlencode($width)."&h=".urlencode($height);
						
						$str = $str."<tr align=\"left\" valign=\"top\"><td><img src=\"$graph_src\" border=\"0\"></td></tr>";
						if (strtolower(trim($export)) == "json") {
							export_file($title.".json", "application/json", json_encode($json));
						}

						if (strtolower(trim($export)) == "xml") {
							export_file($title.".xml", "text/xml", arraytoxml($json));
						}
						
					}
				}
				if (loggedin()) {
					$str = $str."<tr align=\"left\" valign=\"top\"><td>";
					$fc = $rsi->fieldcount();
					$bgc = "#f9f9f9";
					$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='100%'><tr><td bgcolor=\"#e0e0e0\">";
					$str = $str."<table border=\"0\" cellpadding=\"5\" cellspacing=\"1\" width='100%'>";
					$str = $str."<tr bgcolor=".$bgc.">";
					for ($i = 0; $i < $fc; $i++) {
						if (substr(strtolower($rsi->fields[$i]), strlen($rsi->fields[$i])-2, 2) == "id") {
							$f = substr($rsi->fields[$i], 0, strlen($rsi->fields[$i])-2);
						}
						else {
							$f = $rsi->fields[$i];
						}
						$str = $str."<td background=\"includes/templates/".$application["template"]."/images/barbkg.gif\" height=40 align=\"center\"><font color=#000000>".putspace(strtoupper(hashtable($f)))."</td>";
					}
					$str = $str."</tr>";
					$mcount = 0;
					while (!$rsi->eof()) {
						$mtemp = "";
						$str = $str."<tr valign=\"top\" class=\"rownormal\" bgcolor=\"#ffffff\">";
						for ($i = 0; $i < $fc; $i++) {
							if (substr(strtolower($rsi->fields[$i]), (strlen($rsi->fields[$i])-2), 2) == "id" && strlen($rsi->fields[$i]) > 2) {
								$mtemp = getmyfield(substr($rsi->fields[$i], 0, (strlen($rsi->fields[$i])-2)), $rsi->row($i));
							}
							else {
								if (strtolower(substr($rsi->fields[$i], strlen($rsi->fields[$i]) - 4, 4)) == "date") {
									$mtemp = "<div align=right>".formatmydate($rsi->row($i))."</div>";
								}
								else {
									if(is_numeric($rsi->row($i))) {
										$mtemp = number_format($rsi->row($i));
									}
									else {
										$mtemp = $rsi->row($i);
									}
								}
							}
							$mtemp = trim($mtemp);
							$mtemp = ucwords(strtolower($mtemp));
							$str = $str."<td class=\"rownormal\" align=right>".$mtemp."</td>";
							$mcount++;
						}
						$str = $str."</tr>";
						$rsi->movenext();
					}
					$str = $str."</table>";
					$str = $str."</td></tr></table>";
				
					$pages = round($pageitems / $limit);
					if (($pages * $limit) < $pageitems) {
						$pages++;
					}
					if ($pages > 0) {
						$g = "";
						foreach ($_GET as $i=>$j) {
							if (strtolower(trim($i)) != "id" && strtolower(trim($i)) != "p") {
								$a = $j;
								if (is_array($a)) {
									if (sizeof($a) > 0) {
										$a = implode(",", $a);
										$g = $g.$i."=".$a."&";
									}
								}
								else {
									$a = trim($a);
									if (strlen(trim($a)) > 0) {
										$g = $g.$i."=".$a."&";
									}
								}
							}
						}			
						$istr = $istr."<div align=right><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td bgcolor=\"#ffffff\">";
						$istr = $istr."<table border=\"0\" cellpadding=6 cellspacing=4 width='100%'><tr bgcolor=\"#ffffff\">";
						$istr = $istr."<td><b>Viewing&nbsp;Page&nbsp;$p&nbsp;of&nbsp;$pages&nbsp;&nbsp;&nbsp;</td>";
						$istr = $istr."<td><a href=\"".get_script_name()."?id=$cid&indicatorgroupid=$indicatorgroupid&p=1&$g\"><font class=texthighlighted>First</font></a></td>";
						$s = $p - 5;
						if ($s < 1) {
							$s = 1;
						}
						$e = $s + 9;
						for ($i = $s; $i <= $pages; $i++) {
							if ($p == $i) {
								$istr = $istr."<td bgcolor=#9FD3FB><b><font class=textbright>$i</font></b></td>";
							}
							else {
								$istr = $istr."<td bgcolor=\"#ffffff\"><a href=\"".get_script_name()."?id=$cid&indicatorgroupid=$indicatorgroupid&p=$i&$g\"><font color=#000000>$i</font></a></td>";
							}
							if ($i == $e) {
								break;
							}
						}
						$istr = $istr."<td><a href=\"".get_script_name()."?id=$cid&indicatorgroupid=$indicatorgroupid&p=$pages&$g\"><font class=texthighlighted>Last</font></a></td>";
						$istr = $istr."</tr></table></td></tr></table></div>";
					}
					if (strtolower(trim($export)) != "pdf" && strtolower(trim($export)) != "xls") {
						if (strlen($istr) > 0) {
							$str = $str."".$istr;
						}
						if(user("accounttypeid") > 1) {
							if (strlen(trim($_SERVER['QUERY_STRING'])) > 0) {
								$murl = $_SERVER['REQUEST_URI']."&";
							}
							else {
								$murl = $_SERVER['REQUEST_URI']."?";
							}
							$str = $str."<b>Export:</b>&nbsp;&nbsp;<a href=\"".$murl."export=pdf\">PDF</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"".$murl."export=xls\">Excel</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"".$murl."export=json\">JSON</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"".$murl."&export=xml\">XML</a>";
						}
					}
					$str = $str."</td></tr>";
				}
				else {
					$str = $str."<tr><td align=\"left\">".messagebox("<br><a href=\"login.php\">Login</a> is required to view datasets used to generate this indicator.", false)."</td></tr>";
				}
				
				$str = $str."</table>";	
			}
			else {
				$str = $str.messagebox("No records found that matches your selection, <a href=\"".get_script_name()."?id=$cid&indicatorviewid=$indicatorviewid&$tfield=$tfieldid\">click here to reset</a>.", false);
			}
		}
		else {
			$str = messagebox("Requested Indicator not found.", false);
		}
		$str = $str."</td></tr>";
		$str = $str."</table>";	
		
		if (strtolower(trim($export)) == "pdf") {
			print_pdf($str, getmyfield("indicator", $rs->row("indicatorid")));
		}
		if (strtolower(trim($export)) == "xls") {
			export_file(getmyfield("indicator", $rs->row("indicatorid")).".xls", "application/vnd.ms-excel", $str);
		}

	}
	else {
		$str = messagebox("Requested indicator was not found.", false);
	}
	return $str;
}

function show_graph($title, $graphtype, $years, $months, $quarters, $sql, $plotx, $ploty) {
	global $dba, $id;
	$indicatortypeid = getmyfieldid("indicatorsubtype", $id, "indicatortypeid");
	$data = array();
	$plotv = explode(",", $ploty);
	$i = 0;
	if(is_array($years)) {
		foreach($years as $year) {
			if (strpos($sql, "where") != false) {
				$gsql = str_replace("where", "where yearid = $year and ", $sql);
			}
			else {
				$gsql = str_replace("group", "where yearid = $year group ", $sql);
			}
			//echo $gsql;
			$rsy = $dba->execute($gsql);
			if (!$rsy->eof()) {
				foreach($plotv as $y) {
					switch ($indicatortypeid) {
						case 1:
							$data[$y][$year] = array_fill(0, 12, 0);
						break;
						case 2:
							$data[$y][$year] = array_fill(0, 4, 0);
						break;
						case 3:
							$data[$y][$year] = array_fill(0, count($years), 0);
						break;
						default:
						break;
					}
				}
				while (!$rsy->eof()) {
					foreach($plotv as $y) {
						if ($indicatortypeid != 3) {
							$data[$y][$year][$rsy->row($plotx) - 1] = $rsy->row($y);
						}
						else {
							$data[$y][$year][$i] = $rsy->row($y);
						}
						$x[] = $rsy->row($plotx);
					}
					$rsy->movenext();
					$i++;
				}
			}
		}
		//print_r($data);
		switch ($indicatortypeid) {
			case 1:
				$x = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
			break;
			case 2:
				$x = array("Q1", "Q2", "Q3", "Q4");
			break;
			case 3:
				$x = $years;
			break;
			default:
				$x = array_unique($x);
				sort($x);
				for($i = 0; $i < count($x); $i++) {
					$xx[] = getmyfield(substr($plotx, 0, strlen($plotx) - 2), $x[$i]);
				}
				$x = $xx;
			break;
		}
		$x = implode("|", $x);
		//print $x;
		$y = json_encode($data);
		$width  = "750";
		$height = "320";
		$graph_src = "jgraph.php?x=".urlencode($x)."&y=".urlencode($y)."&xt=".urlencode($xt)."&yt=".urlencode($yt)."&t=".urlencode($title)."&c=".urlencode(getmyfieldid("graphtype", $graphtype, "code"))."&w=".urlencode($width)."&h=".urlencode($height);
		$str = $str."<br/><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">";
		$str = $str."<tr><td><img src=\"$graph_src\" border=\"0\"></td></tr>";
		$str = $str."</table>";
		
	}
	return $str;
}


function get_first($t) {
	global $dba;
	$t = substr($t, 0, strlen($t) - 2);
	$sql = "select id, title from `$t` order by id asc limit 1;";
	//echo $sql;
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $rs->row("id");
	}
	return $str;
}



function show_filters($tb, $sfields, $mfields, $filterby, $id, $indicatorviewid, $tfield, $tfieldid) {
	global $dba;
	if (strlen(trim($sfields)) > 0 || strlen(trim($mfields)) > 0) {
		$sfields = explode(",", $sfields);
		$mfields = explode(",", $mfields);
		
		$str = $str."<form class=\"niceform\" method=get action=\"".get_script_name()."\">";
		$str = $str."<table border=\"0\" cellspacing=\"3\" cellpadding=\"0\" width=\"100%\" class=\"box-white\">";
		foreach($sfields as $fld) {
			if (strlen(trim($fld)) > 2) {
				if (strtolower(trim($fld)) == "monthid") {
					$str = $str."<tr align=\"left\" valign=top><td><b>".putspace("Filter By Reporting Period")."</b></td><td width=\"100%\">".quarter_filter()."</td></tr>";
				}
				$str = $str."<tr align=\"left\" valign=top><td><b>".putspace("Filter By ".hashtable($fld)).":</b></td><td>".multi_filter($tb, $fld, $filterby, true)."</td></tr>";
			}
		}

		foreach($mfields as $fld) {
			if (strlen(trim($fld)) > 2) {
				if (strtolower(trim($fld)) == "monthid") {
					$str = $str."<tr align=\"left\" valign=top><td><b>".putspace("Filter By Reporting Period")."</b></td><td width=\"100%\">".quarter_filter()."</td></tr>";
				}
				$str = $str."<tr align=\"left\" valign=top><td><b>".putspace("Filter By ".hashtable($fld)).":</b></td><td>".multi_filter($tb, $fld, $filterby)."</td></tr>";
			}
		}
		$str = $str."<tr align=\"left\" valign=top><td>&nbsp;</td><td align=\"left\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td><input type=\"submit\" value=\"Apply Filter\"></td><td><span class=\"clearbutton\"><a href=\"".get_script_name()."?id=$id&indicatorviewid=$indicatorviewid&$tfield=$tfieldid\"><font color=#ffffff>Clear Filters</font></a></span></td></tr></table></td></tr>";
		$str = $str."</table>";	
		$str = $str."<input type=\"hidden\" name=\"id\" value=\"".$id."\">";	
		$str = $str."<input type=\"hidden\" name=\"indicatorviewid\" value=\"".$indicatorviewid."\">";	
		$str = $str."<input type=\"hidden\" name=\"$tfield\" value=\"".$tfieldid."\">";	
		$str = $str."</form>";			
	}
	return $str;
}

function quarter_filter() {
	global $dba;
	$mcount = 0;
	$sql = "select id, title from `reportingperiod` order by id;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<table border=\"0\" cellspacing=\"3\" cellpadding=\"0\">";
		$str = $str."<tr align=\"left\">";
		while (!$rs->eof()) {
			$checked = "";
			foreach ($_GET as $i=>$j) {
				if (strtolower(trim($i)) == "reportingperiodid") {
					$a = $_GET[$i];
					if (!is_array($a)) {
						$a = explode("||||<|", $a);
					}
					foreach ($a as $j) {
						if (trim($rs->row(0)) == trim($j)) {
							$checked = "checked";
							break;
						}
					}
					break;		
				}
			}
			$str = $str."<td><input type=\"checkbox\" name=\"reportingperiodid[]\" value=\"".$rs->row(0)."\" $checked>&nbsp;".$rs->row(1)."</td>";//onclick=\"this.form.submit();\"
			$rs->movenext();
			$mcount++;
		}
		$str = $str."</tr>";
		$str = $str."</table>";	
	}
	return $str;
}



function multi_filter($t, $field, $filter, $singlefilter = false) {
	global $dba;
	$mcount = 0;
	$field = trim($field);
	switch(strtolower(trim($field))) {
		case "monthid":
			$limit = 12;
		break;
		case "nodeid":
		case "subnodeid":
		case "containertypeid":
			$limit = 3;
		break;
		case "qualitativeid":
		case "countryid":
			$limit = 5;
		break;
		default:
			$limit = 4;
	}
	$sql = "select distinct $field from `$t`";
	if (strlen(trim($filter)) > 0) {
		$sql = $sql." where $filter";
	}
	$sql = $sql." order by $field asc;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<table border=\"0\" cellspacing=\"3\" cellpadding=\"0\">";
		while (!$rs->eof()) {
			$checked = "";
			foreach ($_GET as $i=>$j) {
				if (strtolower(trim($i)) == $field) {
					$a = $_GET[$i];
					if (!is_array($a)) {
						$a = explode("||||<|", $a);
					}
					foreach ($a as $j) {
						if (trim($rs->row(0)) == trim($j)) {
							$checked = "checked";
							break;
						}
					}
					break;		
				}
			}
			if ($mcount == 0) {
				$str = $str."<tr align=\"left\">";
			}
			if ($singlefilter == true) {
				$str = $str."<td><input type=\"radio\" name=\"".$field."\" value=\"".$rs->row(0)."\" $checked>&nbsp;";//onclick=\"this.form.submit();\"
			}
			else {
				$str = $str."<td><input type=\"checkbox\" name=\"".$field."[]\" value=\"".$rs->row(0)."\" $checked>&nbsp;";//onclick=\"this.form.submit();\"
			}
			if (strtolower($field) == "monthid") {
				$str = $str.substr(getmyfield(substr($field, 0, strlen($field) - 2), $rs->row(1)), 0, 3);
			}
			else {
				$str = $str.getmyfield(substr($field, 0, strlen($field) - 2), $rs->row(1));
			}
			$str = $str."</td>";
			$rs->movenext();
			$mcount++;
			if ($mcount == $limit) {
				$mcount = 0;
				$str = $str."</tr>";
			}
		}
		if ($mcount > 0) {
			$str = $str."<td colspan=\"".($limit - $mcount)."\">&nbsp;</td></tr>";
		}
		$str = $str."</table>";	
	}
	return $str;
}


function country_filter($t, $f) {
	global $dba;
	$country_fields = array("countryid", "orgcountryid", "destcountryid", "rcvcountryid");
	$sql = "select id, title from `country` order by regionid,title;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<div style=\"height:250px; overflow-x:hidden; overflow-y:scroll;\">";
		$str = $str."<table border=\"0\" cellspacing=\"3\" cellpadding=\"0\" width=\"100%\">";
		while (!$rs->eof()) {
			$checked = "";
			foreach ($_GET as $i=>$j) {
				if (in_array(trim($i), $country_fields)) {
					$a = $_GET[$i];
					if (!is_array($a)) {
						$a = explode("||||<|", $a);
					}
					foreach ($a as $j) {
						if (trim($rs->row(0)) == trim($j)) {
							$checked = "checked";
							break;
						}
					}		
				}
			}
			$str = $str."<tr align=\"left\"><td><input type=\"checkbox\" name=\"destcountryid[]\" id=\"destcountryid\" value=\"".$rs->row(0)."\" $checked onclick=\"this.form.submit();\"></td><td>".$rs->row(1)."</td></tr>";
			$rs->movenext();			
		}
		$str = $str."</table>";	
		$str = $str."</div>";	
	}
	return $str;
}

function get_months($rpid) {
	global $dba;
	if (is_numeric($rpid)) {
		$sql = "select id, title from `month` where reportingperiodid = $rpid;";
		$rs = $dba->execute($sql);
		if(!$rs->eof()) {
			while(!$rs->eof()) {
				$str = $str.$rs->row(0).",";
				$rs->movenext();
			}
			$str = substr($str, 0, strlen($str) - 1);
		}
	}
	return $str;
}

function print_pdf($document, $filename) {
	global $application;
	include("../codebase/plugins/pdf/mpdf/mpdf.php");
	global $site_logo, $site_organization, $site_contact;
	//$document = strip_tags($document, "<br/>, <pagebreak>, <div>, <span>, <h1>, <h2>, <h3>");
	$document = mb_convert_encoding($document, 'UTF-8', 'HTML-ENTITIES');
	$mpdf = new mPDF('win-1252','A4','','',20,20,20,0,20,20); 
	$mpdf->useOnlyCoreFonts = false;
	//$mpdf->SetProtection(array('print', 'copy', 'modify'));
	$mpdf->SetTitle($filename);
	$mpdf->SetAuthor($application['title']);
	//$mpdf->SetWatermarkText("TTCANC");
	//$mpdf->showWatermarkText = true;
	//$mpdf->watermark_font = 'DejaVuSansCondensed';
	//$mpdf->watermarkTextAlpha = 0.1;
	$mpdf->SetDisplayMode('fullpage');
	$mpdf->SetCompression(true);
	$mpdf->shrink_tables_to_fit = 0;
	$mpdf->keep_table_proportions = true;
	$mpdf->SetHTMLFooter("&copy; 2012 Northern Corridor Transport Observatory Project. All Rights Reserved");
	$mpdf->defaultfooterfontsize = 4;
	$mpdf->defaultfooterfontstyle = 'N';
	$mpdf->defaultfooterline = 0;
	$mpdf->WriteHTML($document);
	$mpdf->Output($filename.".pdf", "D");
	exit;
}

function export_file($filename, $mime, $data) {
	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Cache-Control: private',false);
	header('Content-Type: '.$mime);
	header('Content-Disposition: attachment; filename='.$filename);
	header('Content-Transfer-Encoding: binary');
	print_r($data);
	exit;
}

?>

