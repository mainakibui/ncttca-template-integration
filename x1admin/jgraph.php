<?php   
	require_once("includes/config.php");
	
	require_once("../codebase/plugins/graph/jpgraph/jpgraph.php");
	require_once("../codebase/plugins/graph/jpgraph/jpgraph_line.php");
	require_once("../codebase/plugins/graph/jpgraph/jpgraph_bar.php");
	require_once("../codebase/plugins/graph/jpgraph/jpgraph_pie.php");
	require_once("../codebase/plugins/graph/jpgraph/jpgraph_pie3d.php");
	//require_once ("../codebase/plugins/graph/jpgraph/jpgraph_utils.inc.php");
	
	foreach($_GET as $i=>$j) {
		//echo "$i = $j<br>";
	}
	
	$c = clean(trim($_GET['c']));
	$t = clean(trim($_GET['t']));
	$w = clean(trim($_GET['w']));
	$h = clean(trim($_GET['h']));
	$xt = clean(trim($_GET['xt']));
	$yt = clean(trim($_GET['yt']));
	$x = clean(trim($_GET['x']));
	$y = clean(trim($_GET['y']));
	$margin = clean(trim($_GET['m']));
	
	if (strlen(trim($c)) == 0) {
		$c = "bar";
	}

	if (strlen(trim($x)) > 0 && strlen(trim($y)) > 0) {
		$x = explode("|", $x);
		//$y = stripcslashes($y);
		$y = json_decode($y, true);
		if (!is_numeric($w)) {
			$w = 400;
		}
		if (!is_numeric($h)) {
			$w = 300;
		}

		switch(strtolower($c)) {
			case "line":
				$graph = new Graph($w, $h, 'auto');
				$graph->SetScale('textlin');
				//$graph->title->Set($t);
				$graph->SetMargin(70,10,10,0);
				$graph->xaxis->SetTickLabels($x);
				$graph->yaxis->HideZeroLabel();
				$graph->yaxis->HideLine(false);
				$graph->yaxis->HideTicks(false,false);
				
				foreach($y as $i=>$j) {
					$legend = ucwords(hashtable($i));
					$j = substr($j, 0, strlen($j) - 1);
					$i = new LinePlot(explode(",", $j));
					$i->mark->SetType(MARK_UTRIANGLE);
					$i->SetLegend($legend);
					$graph->Add($i);		
				}
				$graph->Stroke();
			break;
			
			case "bar":
				$graph = new Graph($w, $h, 'auto');
				$graph->SetScale('textlin');
				
				//$graph->title->Set($t);
				$graph->SetMargin(70,10,10,0);
				$graph->xaxis->SetTickLabels($x);
				$graph->yaxis->HideZeroLabel();
				$graph->yaxis->HideLine(false);
				$graph->yaxis->HideTicks(false,false);
				$graph->yaxis->SetLabelFormatCallback('yLabelFormat'); 
				//$graph->xaxis->SetLabelAngle(90);
				$mcount = 0;
				foreach($y as $i=>$j) {
					$legend = ucwords(translate(hashtable($i)));
					$j = substr($j, 0, strlen($j) - 1);
					$i = new BarPlot(explode(",", $j));
					$i->SetLegend($legend);
					$graph->Add($i);	
					$mcount++;	
				}
				$graph->Stroke();
			break;
			
			case "groupbar":
				$graph = new Graph($w, $h, 'auto');
				$graph->SetScale('textint');
				$graph->SetMargin(70,10,10,0);
				$graph->xaxis->SetTickLabels($x);
				$graph->yaxis->HideZeroLabel();
				$graph->yaxis->HideLine(false);
				$graph->yaxis->HideTicks(false,false);
				$graph->yaxis->SetLabelFormatCallback('yLabelFormat'); 
				$m = 0;
				foreach($y as $i=>$j) {
					$legend = ucwords(translate(hashtable($i)));
					foreach($j as $a=>$b) {
						$bplot[$m] = new BarPlot($b);
						$bplot[$m]->SetLegend($legend." - ".$a);
						$m++;
					}
				}
				$gbarplot = new GroupBarPlot($bplot); 
				$graph->Add($gbarplot);
				/*
				$colors = array('#61a9f3','#f381b9','#61e3a9', '#f7b7b7', '#327138', '#327138', '#0000ff', '#ff0000', '#00ff00');
				for($i = 0; $i < $m; $i++) {
					$bplot[$i]->SetColor($colors[$i]);
					$bplot[$i]->SetFillColor($colors[$i]);
				}
				*/ 
				$graph->img->SetExpired(false);
				$graph->Stroke();
			break;
			
			case "pie":
				foreach($y as $i=>$j) {
					$j = substr($j, 0, strlen($j) - 1);
				}
				$graph = new PieGraph($w, $h);
				
				$p1 = new PiePlot3D(explode(",", $j));
				
				$p1->SetLegends($x);
				$graph->legend->SetPos(0.5,0.99,'center','bottom');
				$p1->SetAngle(45);			
				$p1->SetCenter(0.50,0.40);
				$p1->SetSize(0.5);
				
				$graph->Add($p1);
				$graph->img->SetExpired(false);
				$p1->SetSliceColors(array('#61a9f3','#f381b9','#61e3a9', 'blue', 'red', '#f7b7b7')); 
				$graph->Stroke();
			break;
			default:
			
		}
	}
	
	function yLabelFormat($aLabel) { 
    	return number_format($aLabel);
	} 
?>
