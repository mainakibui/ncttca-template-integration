<?php
require_once("includes/config.php");

$t = trim($_GET["t"]);
$a = trim($_GET["a"]);
$cid = trim($_GET["cid"]);

if (!is_numeric($cid) || strlen(trim($cid)) == 0) {
	$cid = 0;
}

if (loggedin()) {
	if (strlen($t) > 0 && strlen($a) > 0 && is_numeric($a)) {
		$mystr = $mystr."<div id=\"thisdiv\" width='100%'>";
		$mystr = $mystr."<center><table border=0 cellpadding=4 width='90%'><tr><td>";
		switch($a) {
			case 1:
			$mystr = $mystr.record($t, $cid, $a, "Add New Record");
			break;

			case 2:
			$mystr = $mystr.record($t, $cid, $a, "Update Record");
			break;

			case 3:
			$mystr = $mystr."<br><table border=0 cellpadding=7 cellspacing=1 width='100%' bgcolor=#cc3300>";
			$mystr = $mystr."<tr><td colspan=3><b><font class=textbright>Confirm Delete for \"".getmyfield($t, $cid)."\"</font></b></td></tr>";
			$mystr = $mystr."<tr valign=center><td height=60><center><font color=#ffffff>Are you sure you want to delete record for <b>".getmyfield($t, $cid)."</b>?<br><small><font class=textbright>(Please note: once deleted, an item cannot be undeleted)</font></font></small></center><br><br></td><form class=\"niceform\" action=\"recordupdate.php\" method=\"post\"><td><input type=hidden name=id value=\"".$cid."\"><input type=hidden name=a value=\"3\"><input type=hidden name=t value=\"".$t."\"><input type=submit value=\"YES, Delete\"></td><td><input type=button onclick=\"parent.parent.GB_hide();\" value=\"Do Not Delete\"></td></form></tr>";
			$mystr = $mystr."</table></td></tr></table></center>";
			break;

			case 4:
			$mystr = $mystr.view($t, $cid);
			break;

			default:
			$mystr = $mystr.messagebox("requested action is not available", false);
			break;
		}
		$mystr = $mystr."</td></tr></table></center></div>";
	}
	else {
		$mystr = $mystr.messagebox("the requested action could not be validated", false);
	}
}
else {
	$mystr = $mystr.messagebox("you are not properly logged in. Login and then try again", false);
}

errortrap(headerize($mystr));





?>


