<?php
require_once("includes/config.php");

if (loggedin() && accesscontrol()) {
	$mystr = $mystr."<center><table width='90%' border=0 cellspacing=8 cellpadding=0>";

	$mystr = $mystr."<tr valign=top><td align=left colspan=3><h3>My Login Account</h3></td></tr>";
	$mystr = $mystr."<tr valign=top><td><img src=images/margin.gif border=0 width=70 height=1></td><td width='100%' align=left>".tb_view("account", "*", "id='".user("id")."'")."</td><td align=right>".button("<a href=\"account_edit.php\"><b><font class=textbright>&nbsp;Change&nbsp;Account&nbsp;Info&nbsp;</font></b></a>")."<br><br>".button("<a href=\"account_password_edit.php\"><b><font class=textbright>Change&nbsp;My&nbsp;Password</font></b></a>")."</td></tr>";
	$mystr = $mystr."<tr valign=top><td align=left colspan=3 bgcolor=#e0e0e0><img src=images/margin.gif border=0></td></tr>";

	$mystr = $mystr."<tr valign=top><td width='100%' colspan=3 align=left><h3>Login History</h3>".list_items("session", "logindate as `login date`,enddate as `last seen`, timediff(enddate,logindate) as `duration (hh:mm:ss)`, concat(host, ' (', ip, ' )') as `ip address`", "username='".user("username")."'", "id desc", "limit 5", false)."<br><br></td></tr>";
	$mystr = $mystr."</table></center>";	
	
	print top().$mystr.bottom();
}
else {
	header("Location: "."login.php");
}


?>
