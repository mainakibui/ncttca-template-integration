<?php
require_once("includes/config.php");
require_once("includes/analyticstracking.php");

global $dba;
$sql = "select id, title, summary, image, url from `mainfeature` where statusid = id order by id desc";
$rs = $dba->execute($sql);

$mtitle = "";

$mystr = $mystr."<!--slider-->";
$mystr = $mystr."<div class='row'>";

$mystr = $mystr."<div class='row' style='display: none;'>";
$mystr = $mystr."<div class='col-md-9 hidden-xs  panel-headers'>";
$mystr = $mystr."<h3>Featured</h3>";
$mystr = $mystr."</div>";
$mystr = $mystr."<div class='col-md-3'>";
$mystr = $mystr."<!-- Controls -->";
$mystr = $mystr."<div class='controls pull-right hidden-xs'>";
$mystr = $mystr."<a class='carouselbtn left fa fa-chevron-left btn btn-primary' href='#carousel-example-generic' data-slide='prev'></a>";
$mystr = $mystr."<a class='carouselbtn right fa fa-chevron-right btn btn-primary' href='#carousel-example-generic' data-slide='next'></a>";
$mystr = $mystr."</div>";
$mystr = $mystr."</div>";
$mystr = $mystr."</div>";

$mystr = $mystr."<div id='carousel-example-generic' class='carousel slide slide-featured hidden-xs' data-ride='carousel'>";

$mystr = $mystr."<div class='carousel-inner'><!-- carousel-inner -->";

$mystr = $mystr."<div class='item active'>";
$mystr = $mystr."<div class='row'>";
$mystr = $mystr."<div class='col-sm-12 col-md-12 nopadding'>";
$mystr = $mystr."<div class='featured'>";
$mystr = $mystr."<div class='col-sm-12 col-md-12 nopadding'>";
$mystr = $mystr."<div class='thumbnail frontpage-featured'>";

$mystr = $mystr.featureimage();

$mystr = $mystr."</div>";
$mystr = $mystr."</div>";

$mystr = $mystr."<div class='col-sm-12 col-md-12 articlecontent'>";
$mystr = $mystr.featuretitle();
$mystr = $mystr."<div class='caption clear'>";
$mystr = $mystr."<hr>";
$mystr = $mystr."<div>".featuretext()."</div>";
$mystr = $mystr."<div class=' pull-right'>";
$mystr = $mystr.featureurl();
$mystr = $mystr."</div>";
$mystr = $mystr."<div style='clear:both;'></div>";
$mystr = $mystr."<br>";
$mystr = $mystr."</div>";
$mystr = $mystr."</div>";
$mystr = $mystr."</div>";
$mystr = $mystr."</div>";

$mystr = $mystr."</div><!--/row-->";
$mystr = $mystr."</div><!--/item-->";

$mystr = $mystr."</div><!-- /carousel-inner -->";
$mystr = $mystr."</div><!-- /carousel-example-generic -->";
$mystr = $mystr."</div><!-- /row -->";
$mystr = $mystr."<!--/slider-->";

displayhome($mystr);

function featureimage()
{
	global $rs;
	if (!$rs->eof())
	{
		if (strlen(trim($rs->row("image"))) > 0) {
			$str = $str."<img class='featured' src=\"images/".$rs->row("image")."\" alt=\"".$rs->row("title")."\" data-src=\"images/".$rs->row("image")."\" />";
		}
	}
	return $str;
}

function featuretitle()
{
	global $rs;
	if (!$rs->eof())
	{
		if (strlen(trim($rs->row("title"))) > 0) {
			$str = $str."<div class='featured-title-text pull-left'>".titlecase(translate($rs->row("title")))."</div>";
		}
	}
	return $str;
}

function featuretext()
{
	global $rs;
	if (!$rs->eof())
	{
		$mtemp = $rs->row("summary");
		$str = $str.substr($mtemp,0,160)."...";
	}
	return $str;
}

function featureurl()
{
	global $rs;
	if (!$rs->eof())
	{
		if (strlen(trim($rs->row("url"))) > 0) {
			$str = $str."<a href=\"".$rs->row("url")."\" class='btn btn-default bg-lightgrey' role='button'>".translate('Read more')."</a>";
		}
	}
	return $str;
}
?>
