<?php
require_once("includes/config.php");
$mtitle = "Manage My Account";

if (loggedin()) {
	if (strlen(trim($_SESSION['callback_message'])) > 0) {
		$mystr = $mystr.$_SESSION['callback_message'];
		$_SESSION['callback_message'] = null;
	}
	$mystr = $mystr."<div class='tab-content'>";
		$mystr = $mystr."<div class='row panel4 clearfix'>";
			$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12 nopadding'>";
			
				$mystr = $mystr."<div class='row clearfix'>";
					$mystr = $mystr."<div class='col-sm-6 col-md-6 col-lg-6'><h3>My Login Account</h3></div>";
					$mystr = $mystr."<div class='col-sm-6 col-md-6 col-lg-6'>&nbsp;</div>";
				$mystr = $mystr."</div>";
				
				$mystr = $mystr."<div class='row clearfix'>";
					$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12'>";
						$mystr = $mystr.tb_view("account", "*", "id='".user("id")."'").button("<a href=\"account_edit.php\" class='btn btn-default'><b><font class=textbright>&nbsp;Change&nbsp;Account&nbsp;Info&nbsp;</font></b></a>")."<br><br>".button("<a href=\"account_password_edit.php\" class='btn btn-default'><b><font class=textbright>Change&nbsp;My&nbsp;Password</font></b></a>");
					$mystr = $mystr."</div>";
				$mystr = $mystr."</div><br>";
				
				$mystr = $mystr."<div class='row clearfix'>";
					$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12'>";
						$mystr = $mystr."<h3>Login History</h3>";
					$mystr = $mystr."</div>";
					$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12'>";
						$mystr = $mystr.list_items("session", "logindate as `login date`,enddate as `last seen`, timediff(enddate,logindate) as `duration (hh:mm:ss)`, concat(host, ' (', ip, ' )') as `ip address`", "username='".user("username")."'", "id desc", "limit 5", false);
					$mystr = $mystr."</div>";
				$mystr = $mystr."</div>";
					
			$mystr = $mystr."</div>";
		$mystr = $mystr."</div>";
	$mystr = $mystr."</div>";
	
	display($mystr);
}
else {
	header("Location: "."login.php");
}



function popup($t, $u, $w = 600, $h = 400) {
	global $site_name;
	if (strlen(trim($t)) > 0 && strlen(trim($u)) > 0 && strlen(trim($w)) > 0 && strlen(trim($h)) > 0) {
		$str = $str."<a href=\"".$u."\" onclick=\"return parent.GB_show('".removehtml($site_name)."', this.href, ".$h.", ".$w.")\">".$t."</a>";
	}
	return $str;
}



function tb_view($tb, $fields, $filter) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($fields)) && strlen(trim($filter)) > 0) {
		$sql = "select ";
		if (trim($fields) == "*") {
			$sql = $sql.$fields;
		}
		else {
			$sql = $sql."id,".$fields;
		}	
		$sql = $sql." from `$tb` where $filter limit 1;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<table border=0 cellpadding=0 cellspacing=0>";
			for ($i = 1; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				$v = $rs->row($i);
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
					$v = getmyfield($f, $v);
				}
				$f = ucwords(hashtable($f));
				if (strlen(trim($v)) == 0) {
					$v = "<font color=#a0a0a0>(not specified)</font>";
				}
				if (strtolower(trim($f)) == "password") {
					$v = "******".substr($v, strlen($v) - 2, 2);
				}
				if (strtolower(trim($f)) != "password") {
					$str = $str."<tr><td><font class=texthighlighted>".putspace($f)."&nbsp;&nbsp;&nbsp;&nbsp;</font></td><td>".removehtml($v)."</td></tr>";
				}
			}
			$str = $str."</table>";
		}
	}
	return $str;
}


function list_items($tb, $fields, $filter, $o = "id", $limit = null, $manage = true) {
	global $dba, $application;
	$tb = trim(strtolower($tb));
	switch($tb) {
		case "office":
		$t = 1;
		break;

		case "programme":
		$t = 2;
		break;

		case "project":
		$t = 3;
		break;
	}
	if (strlen(trim($tb)) > 0 && strlen(trim($fields)) && strlen(trim($filter)) > 0) {
		$sql = "select ";
		if (trim($fields) == "*") {
			$sql = $sql.$fields;
		}
		else {
			$sql = $sql."id,".$fields;
		}	
		$sql = $sql." from `$tb` where $filter order by $o ";
		if (strlen(trim($limit)) > 0) {
			$sql = $sql.$limit;
		}
		$sql = $sql.";";
		$rs = $dba->execute($sql);
		$str = $str."<table class='table table-striped table-bordered table-hover'>";
		$str = $str."<tr>";
		for ($i = 1; $i < sizeof($rs->fields); $i++) {
			$f = $rs->fields[$i];
			if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
				$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
			}
			$str = $str."<th><b>".strtoupper($f)."</b></th>";
		}
		if ($manage) {
			$str = $str."<td align=right colspan=2>";
			$str = $str.popup("<b>Add&nbsp;New&nbsp;".titlecase($tb)."</b>", "account_item.php?t=$t&a=1");
			$str = $str."</td>";
		}	
		$str = $str."</tr>";
		if (!$rs->eof()) {
			while(!$rs->eof()) {
				$str = $str."<tr>";
				for ($i = 1; $i < sizeof($rs->fields); $i++) {
					$f = $rs->fields[$i];
					$v = $rs->row($i);
					if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
						$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
						$v = getmyfield($f, $v);
					}

					$str = $str."<td>".removehtml($v)."</td>";
				}
				if ($manage) {
					$str = $str."<td width=1>".popup("Edit", "account_item.php?t=$t&a=2&id=".$rs->row("id"))."</td>";
					$str = $str."<td width=1>".popup("Del", "account_item.php?t=$t&a=3&id=".$rs->row("id"))."</td>";
				}
				$str = $str."</tr>";
				$rs->movenext();
			}
		}
		else {
			$str = $str."<tr><td colspan=".(sizeof($rs->fields) - 1 + 2)."><font color=#a0a0a0>No $tb items are available</font></td></tr>";
		}
		$str = $str."</table>";
	}
	return $str;
}

?>
