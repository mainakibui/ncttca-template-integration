<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeMQzyx95lS2q0ZHfd-TQ3XRbAENcKdY8&sensor=false">
    </script>
    <script type="text/javascript">
      function initialize() {
		var myLatlng = new google.maps.LatLng(-4.049603,39.691585);
		var contentString = "The Permanent Secretariat of the Transit Transport Co-ordination Authority of the Northern Corridor. <br>Plot 1196, Links Road, Nyali";
        var mapOptions = {
          center: new google.maps.LatLng(-4.049603,39.691585),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title:"TTCA-NC",
			animation: google.maps.Animation.DROP
		});
		
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});

		google.maps.event.addListener(marker, 'click', function() {
		  infowindow.open(map,marker);
		});
      }
    </script>
  </head>
  <body onload="initialize()">
    <div id="map_canvas" style="width:425px; height:350px;"></div>
  </body>
</html>
