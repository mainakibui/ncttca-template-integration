<?php   
	require_once("includes/config.php");
	
	include("../codebase/plugins/graph/pgraph/class/pData.class.php");
	include("../codebase/plugins/graph/pgraph/class/pDraw.class.php");
	include("../codebase/plugins/graph/pgraph/class/pPie.class.php");
	include("../codebase/plugins/graph/pgraph/class/pImage.class.php");
	
	foreach($_GET as $i=>$j) {
		//echo "$i = $j<br>";
	}
	
	$c = clean(trim($_GET['c']));
	$t = clean(trim($_GET['t']));
	$w = clean(trim($_GET['w']));
	$h = clean(trim($_GET['h']));
	$xt = clean(trim($_GET['xt']));
	$yt = clean(trim($_GET['yt']));
	$x = clean(trim($_GET['x']));
	$y = clean(trim($_GET['y']));

	if (strlen(trim($c)) == 0) {
		$c = "pie";
	}

	if (strlen(trim($x)) > 0 && strlen(trim($y)) > 0) {
		$x = explode("|", $x);
		$y = explode("|", $y);
		if (!is_numeric($w)) {
			$w = 400;
		}
		if (!is_numeric($h)) {
			$w = 300;
		}
		
		if (strtolower(trim($c)) == "line") {
			$myData = new pData();
			$myData->addPoints($y, "Serie1");
			$myData->setSerieDescription("Serie1",$xt);
			//$myData->setSerieWeight("Serie1", 2); // thickness of the line
			$myData->setSerieOnAxis("Serie1", 0);

			$myData->addPoints($x, "Absissa");
			$myData->setAbscissa("Absissa");

			$myData->setAxisPosition(0, AXIS_POSITION_LEFT);
			$myData->setAxisName(0, $yt);
			$myData->setAxisUnit(0, "");

			$myPicture = new pImage($w, $h, $myData);
			$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>20));

			$myPicture->setFontProperties(array("FontName"=>"../codebase/plugins/graph/pgraph/fonts/Forgotte.ttf","FontSize"=>14));
			$TextSettings = array("Align"=>TEXT_ALIGN_MIDDLEMIDDLE, "R"=>255, "G"=>255, "B"=>255);
			$myPicture->drawText(350,25,$t,$TextSettings);

			$myPicture->setShadow(false);
			$myPicture->setGraphArea(80,0,$w,$h*0.90);
			$myPicture->setFontProperties(array("R"=>0,"G"=>0,"B"=>0,"FontName"=>"../codebase/plugins/graph/pgraph/fonts/pf_arma_five.ttf","FontSize"=>8));
			
			$Settings = array("XMargin"=>10,"YMargin"=>10,"Floating"=>TRUE,"GridR"=>200,"GridG"=>200,"GridB"=>200,"DrawSubTicks"=>TRUE,"CycleBackground"=>TRUE, "LabelRotation"=>90, "DrawXLines"=>0, "DrawYLines"=>ALL);
			$myPicture->drawScale($Settings);

			$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>10));

			$myPicture->drawLineChart();
			$myPicture->drawPlotChart(array("DisplayValues"=>TRUE,"PlotBorder"=>TRUE,"BorderSize"=>2,"Surrounding"=>-60,"BorderAlpha"=>80));

			$Config = array("FontR"=>0, "FontG"=>0, "FontB"=>0, "FontName"=>"../codebase/plugins/graph/pgraph/fonts/Silkscreen.ttf", "FontSize"=>6, "Margin"=>6, "Alpha"=>30, "BoxSize"=>5, "Style"=>LEGEND_NOBORDER, "Mode"=>LEGEND_HORIZONTAL);
			//$myPicture->drawLegend(($w - 150), 16, $Config);

			$myPicture->stroke();
 
		}
		else {
			if (strtolower(trim($c)) == "bar") {
				$myData = new pData();
				$myData->addPoints($y,"Serie1");
				$myData->setSerieDescription("Serie1",$xt);
				$myData->setSerieOnAxis("Serie1",0);

				$myData->addPoints($x,"Absissa");
				$myData->setAbscissa("Absissa");

				$myData->setAxisPosition(0,AXIS_POSITION_LEFT);
				$myData->setAxisName(0,$yt);
				$myData->setAxisUnit(0,"");

				$myPicture = new pImage($w, $h, $myData);

				$myPicture->setShadow(FALSE);
				$myPicture->setGraphArea($w*0.25,0,$w,$h*0.75);
				$myPicture->setFontProperties(array("R"=>0,"G"=>0,"B"=>0,"FontName"=>"../codebase/plugins/graph/pgraph/fonts/pf_arma_five.ttf","FontSize"=>6));

				$Settings = array("Pos"=>SCALE_POS_LEFTRIGHT, "Mode"=>SCALE_MODE_START0, "LabelingMethod"=>LABELING_ALL, "GridR"=>227, "GridG"=>227, "GridB"=>227, "GridAlpha"=>255, "TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50, "LabelRotation"=>90, "CycleBackground"=>1, "DrawXLines"=>0, "DrawSubTicks"=>1, "SubTickR"=>255, "SubTickG"=>0, "SubTickB"=>0, "SubTickAlpha"=>50, "DrawYLines"=>ALL);
				$myPicture->drawScale($Settings);

				$myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>255,"G"=>255,"B"=>255,"Alpha"=>255));

				$Config = array("DisplayValues"=>1, "Rounded"=>1, "AroundZero"=>1);
				$myPicture->drawBarChart($Config);

				$Config = array("FontR"=>0, "FontG"=>0, "FontB"=>0, "FontName"=>"../codebase/plugins/graph/pgraph/fonts/pf_arma_five.ttf", "FontSize"=>6, "Margin"=>6, "Alpha"=>255, "BoxSize"=>5, "Style"=>LEGEND_ROUND, "Mode"=>LEGEND_VERTICAL);
				$myPicture->drawLegend(335,16,$Config);

				$myPicture->stroke();

			}
			else {//pie chart
				/* Create and populate the pData object */
				$MyData = new pData();   
				$MyData->addPoints($y,"ScoreA");  
				$MyData->setSerieDescription("ScoreA","Application A");

				/* Define the absissa serie */
				$MyData->addPoints($x,"Labels");
				$MyData->setAbscissa("Labels");

				/* Create the pChart object */
				$myPicture = new pImage($w, $h*0.7,$MyData,TRUE); //$w, $h
				$myPicture->setFontProperties(array("FontName"=>"../codebase/plugins/graph/pgraph/fonts/verdana.ttf","FontSize"=>10,"R"=>80,"G"=>80,"B"=>80));

				$PieChart = new pPie($myPicture,$MyData);
				$PieChart->draw3DPie($w*0.35, $h*0.4, array("WriteValues"=>TRUE,"DataGapAngle"=>5,"DataGapRadius"=>6,"Border"=>false, "ValueR"=>0,"ValueG"=>0,"ValueB"=>0,"ValueAlpha"=>255, "Radius"=>($w/3.5)));
				$myPicture->setFontProperties(array("FontName"=>"../codebase/plugins/graph/pgraph/fonts/verdana.ttf","FontSize"=>11));
				$myPicture->setShadow(false,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>255));


				$myPicture->setFontProperties(array("FontName"=>"../codebase/plugins/graph/pgraph/fonts/verdana.ttf","FontSize"=>11,"R"=>255,"G"=>255,"B"=>255));
				$PieChart->drawPieLegend($w*0.75, $h*0.1,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL, "FontR"=>100, "FontG"=>100, "FontB"=>100));

				/* Render the picture (choose the best way) */
				$myPicture->autoOutput($t.".png");
			}
		}
	}
?>
