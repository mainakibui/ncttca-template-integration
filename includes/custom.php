<?php

keepalive();

if (strlen(trim($_SESSION["languageid"])) == 0) {
	$_SESSION["languageid"] = $application["languageid"];
}

function userinfo() {
	$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
	$str = $str."<tr valign=\"middle\" align=\"center\">";
	$str = $str."<td>&nbsp;&nbsp;</td>";
	$str = $str."<td><img src=\"account.png\" border=\"0\" width=\"23\" alt=\"Account Icon\"/></td>";
	if (loggedin()) {
		$str = $str."<td>".user("name")."&nbsp;(&nbsp;<b>".user("username")."&nbsp;)</td>";
		if (user("organizationid") > 0) {
			$str = $str."<td><font color=\"#d0d0d0\">&nbsp;&nbsp;|&nbsp;&nbsp;</font></td>";
			$str = $str."<td><font class=\"textbright\">".getmyfield("organization", user("organizationid"))."</font></td>";
		}
		$str = $str."</td>";
		$str = $str."<td><font color=\"#d0d0d0\">&nbsp;&nbsp;|&nbsp;&nbsp;</font></td>";
		$str = $str."<td><a href=\"account.php\">".putspace(titlecase(translate("Manage My Account")))."</a></td>";
		$str = $str."<td><font color=\"#d0d0d0\">&nbsp;&nbsp;|&nbsp;&nbsp;</font></td>";
		$str = $str."<td><a href=\"logout.php\"><font color=\"#FF6600\">".putspace(titlecase(translate("Log Out")))."</font></a>&nbsp;</td>";
	}
	else {
		$str = $str."<td><a href=\"login.php\"><b>".titlecase(translate("Login"))."</b>&nbsp;</a><font class=\"textfaded\">".translate("to your Account")."</font>&nbsp;</td>";
	}
	$str = $str."</tr>";
	$str = $str."</table>";
	return $str;
}


function simple_top() {
	global $application, $mtitle;
	$hdr = $hdr."<link href=\"includes/themes/".$application['template']."/stylesheet.css\" rel=stylesheet>";
	$str = $str.headers($hdr);
	$str = $str."<body topmargin=0 bottommargin=0 leftmargin=0 rightmargin=0 marginheight=0 marginwidth=0>";
	$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='100%' bgcolor=\"#ffffff\">";
	$str = $str."<tr algin=center><td>";
	$str = $str."<br/><br/><br/><br/><center><table border=\"0\" cellpadding=\"0\" cellspacing=3 width='600'>";
	$str = $str."<tr align=\"left\"><td>";
	if (strlen(trim($application['logo'])) > 0) {
		$str = $str."<img src=\"images/".trim($application['logo'])."\" height=\"50\" border=\"0\">";
	}	
	$str = $str."</td><td width='100%' align=\"right\"><h2><font class=textfaded>".$application['title']."</font></h2></td></tr>";
	$str = $str."<tr><td colspan=2>";



	return $str;
}

function simple_bottom() {
	$str = $str."<br/></td></tr>";
	$str = $str."</table>";
	$str = $str."</td></tr>";
	$str = $str."</table>";
	$str = $str."</body></html>";
    return $str;
}



function menu($menuid = 0, $pageid = 0, $alignment = "h") {
	global $dba, $page;
	$menuicons = array(
		"home"=>"images/home-icon.png",
		"dashboard"=>"images/dashboard_icon.png",
		"indicators"=>"images/indicators-icon.png",
		"gis"=>"images/gis-icon.png",
		"documents"=>"images/documents-icon.png",
		"main site"=>"images/mainsite-icon.png",
		);
	$id = $page[0];
	$parent = getmyfieldid("page", $id, "pageid");
	if (trim($parent) == "0") {
		$parent = $id;
	}
	if ($id == "2012043010334713756") {
		$id = 1;
	}
	if (is_numeric($menuid) && strlen(trim($menuid)) > 0 && is_numeric($pageid) && strlen(trim($pageid)) > 0) {
		if ((int)$menuid > 0 && (int)$menuid < 4) {
			$sql = "select id, title, url, positionid, accesscontrol, image, pagetypeid, (select title from `menu` where id = menuid limit 1) as menu, (select count(*) from `page` as `p` where `p`.pageid=q.id and statusid='1') as total from `page` as q where menuid='".$menuid."' and pageid='".$pageid."' and statusid='1' order by positionid, id;";
			$rs = $dba->execute($sql);
			if (!$rs->eof()) {
				$str = $str."<div class='navbar-ns navbar navbar-default' role='navigation'>";
				$str = $str."<div class='container nopadding ".($id == 1 ? 'top-menu' : '')."'>";
				
				$str = $str."<div class='navbar-header'>";
				$str = $str."<a id='logo-small' style='display:none;' class='navbar-brand' href='#'> <img src='../images/top-small.png' alt='top logo small'> </a>";
				$str = $str."</div>";
				
				$str = $str."<div class='collapse navbar-collapse nopadding'>";
				$str = $str."<ul class='nav navbar-nav'>";
				while (!$rs->eof()) {
					$u = "page.php?id=".$rs->row("id");
					$target = "";
					if (strlen(trim($rs->row("url"))) > 0) {
						$u = $rs->row("url");
					}
					if (is_numeric(strpos(strtolower($u), "http://"))) {
						$target = "target = '_blank'";
					}
					$d = "";
					if (strtolower(trim($id)) == strtolower(trim($rs->row("id"))) || strtolower(trim($parent)) == strtolower(trim($rs->row("id")))) {
						$d = "active";
					}
					if($id == 1)//Show only home menu
					{
						if($rs->row("id")==1)
						{
						$icon = "<i class='icon-image'> <img class='menu_icon_image_small' src='".$menuicons[strtolower($rs->row("title"))]."'> </i>&nbsp;";
						$str = $str."<li class='".$d."'><a href=\"".$u."\" $target>".$icon.putspace(titlecase(translate($rs->row("title"))))."</a></li>";
						}
					}
					else//Show all menus
					{
					$icon = "<i class='icon-image'> <img class='menu_icon_image_small' src='".$menuicons[strtolower($rs->row("title"))]."'> </i>&nbsp;";
					$str = $str."<li class='".$d."'><a href=\"".$u."\" $target>".$icon.putspace(titlecase(translate($rs->row("title"))))."</a></li>";
					}
					$rs->movenext();
				}
				$str = $str."</ul>";
				$str = $str."</div><!--/.nav-collapse /top menu-->";
				
				$str = $str."</div><!--/container-->";
				$str = $str."</div><!--/navbar-->";
			}
		}
	}	
	return $str;
}

function menuold($menuid = 0, $pageid = 0, $alignment = "h") {
	global $dba, $page;
	if (strtolower(trim($alignment)) != "h" && strtolower(trim($alignment)) != "v") {
		$alignment = "h";
	}
	$alignment = trim(strtolower($alignment));
	$filter = "pagetypeid='1'";
	if (loggedin()) {
		$filter = $filter." or pagetypeid='2'";
	}
	if (is_numeric($menuid) && strlen(trim($menuid)) > 0 && is_numeric($pageid) && strlen(trim($pageid)) > 0) {
		if ((int)$menuid > 0 && (int)$menuid < 4) {
			$sql = "select id, title, url, positionid, accesscontrol, image, pagetypeid, (select title from `menu` where id = menuid limit 1) as menu, (select count(*) from `page` as `p` where `p`.pageid=q.id and statusid='1') as total from `page` as q where menuid='".$menuid."' and pageid='".$pageid."' and statusid='1' and (".$filter.") order by positionid, id;";
			$rs = $dba->execute($sql);
			if (!$rs->eof()) {
				$str = $str."<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
				if (strtolower(trim($alignment)) == "h") {
					$str = $str."<tr>";
				}
				while (!$rs->eof()) {
					//if (accesscontrol($rs->row("accesscontrol"))) {
						$u = "page.php?id=".$rs->row("id");
						if (strlen(trim($rs->row("url"))) > 0) {
							$u = $rs->row("url");
						}
						$c = "taboff";
						if (strtolower(trim($pageid)) == strtolower(trim($rs->row("id"))) || strtolower(trim($parentid)) == strtolower(trim($rs->row("id")))) {
							$c = "tabon";
						}
						$d = "menuitem";
						if (trim($rs->row("id")) == "4") {
							$d = "menuitem-featured";
						}
						if (strtolower(trim($alignment)) == "v") {
							$d = "sub".$d;
							$str = $str."<tr>";
						}
						$str = $str."<td class='".$d."'><a href=\"".$u."\"><font class=\"textbright\">".putspace(titlecase(translate($rs->row("title"))))."</font></a></td>";
						if (strtolower(trim($alignment)) == "v") {
							$str = $str."</tr>";
						}
					//}
					$rs->movenext();
				}
				if (strtolower(trim($alignment)) == "h") {
					$str = $str."</tr>";
				}	
				$str = $str."</table>";
			}
		}
	}	
	return $str;
}

function menus($menuid = 0, $pageid = 0) {
	global $dba, $page;
	$filter = "pagetypeid='1'";
	if (loggedin()) {
		$filter = $filter." or pagetypeid='2'";
	}
	if (is_numeric($menuid) && strlen(trim($menuid)) > 0 && is_numeric($pageid) && strlen(trim($pageid)) > 0) {
		if ((int)$menuid > 0 && (int)$menuid < 4) {
			$sql = "select id, title, url, positionid, accesscontrol, featuredid, pagetypeid, (select title from `menu` where id=menuid limit 1) as menu, (select count(*) from `page` as `p` where `p`.pageid=q.id and statusid='1') as total from `page` as q where menuid='".$menuid."' and pageid='".$pageid."' and statusid='1' and (".$filter.") order by positionid, id;";
			$rs = $dba->execute($sql);
			if (!$rs->eof()) {
				$str = $str."<table border='0' cellspacing='3' cellpadding='0'>";
				$str = $str."<tr>";
				while (!$rs->eof()) {
					if (accesscontrol($rs->row("accesscontrol"))) {
						$u = "page.php?id=".$rs->row("id");
						if (strlen(trim($rs->row("url"))) > 0) {
							$u = $rs->row("url");
						}
						$str = $str."<td><a href=\"".$u."\">".putspace(translate($rs->row("title")))."</a></td>";
					}
					$rs->movenext();
					if (!$rs->eof()) {
						$str = $str."<td>&nbsp;&nbsp;|&nbsp;&nbsp;</td>";
					}
				}
				$str = $str."</tr>";
				$str = $str."</table>";
			}
		}
	}	
	return $str;
}

function page_image() {
	global $dba, $mtitle, $page;
	$pageid = $page[0];
	if (strlen(trim($mtitle)) > 0 && is_numeric($pageid)) {
		$sql = "select id, title, image from `page` where image <> '' and id = $pageid limit 1;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			if (file_exists("images/".trim($rs->row("image")))) {
				$str = $str."<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";
				$str = $str."<tr><td valign=\"top\" align=\"left\"><img src=\"".$rs->row("image")."\" alt=\"".$rs->row("title")."\" border=\"0\"/><td></tr>";
				$str = $str."</table>";
			}
		}
	}
	return $str;
}


function submenu() {
	global $dba, $page;
	$pageid   = $page[0];
	$parentid = $page[2];
	if (strlen(trim($parentid)) == 0 || !is_numeric($parentid)) {
		$parentid = $pageid;
	}
	if (is_numeric($parentid) && strlen(trim($parentid)) > 0 && trim($parentid) != "0") {
		$sql = "select id, title, url, positionid, (select title from page where id='".$parentid."' limit 1) as parent from page where menuid=1 and pageid='".$parentid."' and active=1 order by positionid, id;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=8 width='100%'><tr>";
			$mcount = 0;
			while (!$rs->eof()) {
				$u = "page.php?id=".$rs->row("id");
				if (strlen(trim($rs->row("url"))) > 0) {
					$u = $rs->row("url");
				}
				$c = "subtaboff";
				if (strtolower(trim($pageid)) == strtolower(trim($rs->row("id")))) {
					$c = "subtabon";
				}
				$str = $str."<td bgcolor=#606060><img src=images/margin.gif border=\"0\" width=1 height=1></td>";
				$str = $str."<td class=$c><a href=\"".$u."\"><font color=yellow><font size=-1><sb>".putspace(titlecase($rs->row("title")))."</b></font></font></a></td>";
				$mcount++;
				$rs->movenext();
				if (!$rs->eof()) {
				}
			}
			$str = $str."<td bgcolor=#606060><img src=images/margin.gif border=\"0\" width=1 height=1></td>";
			$str = $str."<td height=30 width='100%'>&nbsp;</td></tr></table>";
		}
	}
	return $str;
}




function bottommenus() {
	global $dba, $id;
	$sql = "select id, title, url, positionid from page where menuid = 3 and pageid = 0 and statusid = 1 order by positionid, id;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"3\">";
		$str = $str."<tr><td>&nbsp;</td><td>";
		while(!$rs->eof()) {
			$u = "page.php?id=".$rs->row("id");
			if (strlen(trim($rs->row("url"))) > 0) {
				$u = $rs->row("url");
			}
			$str = $str."<a href=\"$u\"><font class=\"textdull\"><b>".translate($rs->row(1))."</b></font></a>";
			$rs->movenext();
			if(!$rs->eof()) {
				$str = $str."&nbsp;<font class=\"textdull\">|</font>&nbsp;";
			}
		}
		$str = $str."</td></tr>";
		$str = $str."</table>";
	}
	return $str;
}


function clock() {
	$str = $str."<table border=\"0\" cellpadding=2>";
	$str = $str."<tr valign=\"middle\">";
	$str = $str."<td><canvas id=\"clk1\" class=\"CoolClock:securephp:40\"></canvas></td>";
	$str = $str."<td><font class=textfaded><font size=+2>".weekdayname(getmydate())."</font><font><br/>".monthname(getmydate())."&nbsp;".day(getmydate())."&nbsp;".year(getmydate())."</font></font>";
	$str = $str."</tr>";
	$str = $str."</table>";
	return $str;
}

function get_country($countryid) {
	global $dba, $site_organization;
	$sql = "select country.title, country.code from `country` where country.id='".$countryid."' limit 1;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		if (strlen(trim($rs->row("code"))) > 0) {
			$country = $country."<img src=\"images/".strtolower(trim($rs->row("code"))).".gif\" border=\"0\" height=60><br/>";
		}
		else {
			if (strlen(trim($rs->row("title"))) > 0) {
				$country = $country."<font size=+0>".putspace($rs->row("title"))."</font>";
			}
		}
		if (strlen(trim($site_organization)) > 0) {
			$country = $country."<img src=images/margin.gif border=\"0\" width=10 height=4><br/><font size=-1>".putspace($site_organization)."</font>";
		}
	}
	return $country;
}	
	






function toolbar($tables, $currenttable) {
	global $id, $organization;
	$str = $str."<table border=\"0\" cellpadding=2 cellspacing=\"0\"><form class=niceform name=dblist method=get action=\"".get_script_name()."\">";
	$str = $str."<tr>";
	$str = $str."<td><b><font color=#a0a0a0>Select&nbsp;Database</font></b>&nbsp;</td>";
	$str = $str."<td>";
	$str = $str."<select onchange='document.dblist.submit()' name=id id=id>";
	for ($i = 0; $i < sizeof($tables); $i++) {
		if (strtolower(trim($tables[$i])) != "user" && strtolower(trim($tables[$i])) != "usergroup" && strtolower(trim($tables[$i])) != "application" && strtolower(trim($tables[$i])) != "menu" && strtolower(trim($tables[$i])) != "eventlog" && strtolower(trim($tables[$i])) != "position") {
			$str = $str."<option value=\"".$i."\"";
			if ($i == $id) {
				$str = $str." selected";
			}
			$str = $str.">".ucwords(hashtable($tables[$i]))."</option>";
		}
	}
	$str = $str."</select>";
	$str = $str."</td>";
	$str = $str."<td><input type=submit value='Select' class=button></td>";
	$str = $str."</tr>";
	$str = $str."</form></table>";
	return $str;
}


function filters($tb, $ex = "hsgsh474hdnd8", $csql = null) {
	global $dba, $search, $id;
	$sql = "select * from `$tb` limit 1;";
	$rs = $dba->execute($sql);
	if (!$rs->eof() && trim($ex) != "*") {
		$fd = getfields($tb);
		if (sizeof($fd) > 0) {
			$dp = explode(",", $fd[0]);
			$flt = "";
			$ex = strtolower(trim($ex));
			$ex = explode(",", $ex);
			foreach ($dp as $d) {
				if (strlen(trim($d)) > 2 && substr(trim(strtolower($d)), strlen(trim($d)) - 2, 2) == "id" && strtolower(trim($d)) != "positionid") {
					if (in_array(strtolower(trim($d)), $ex) == false) {
						$c = 1;
						if (strtolower(trim($d)) == "manufacturerid" || strtolower(trim($d)) == "modelid") {
							$c = 1;
						}
						$flt = $flt.multiselect($d, $_GET[$d], substr($d, 0, strlen($d) - 2), $c, true, "", false, $tb, $csql);
					}
				}
			}
			$str = $str."<font color=#808080><b>Filter ".ucwords(hashtable($tb))."</b></font>";
			$str = $str."<table border=1 cellspacing=\"0\" cellpadding=4 width='100%'>";
			$str = $str."<tr><form class=\"niceform\" method=get action=\"".get_script_name()."\"><td><font class=texthighlighted><b>Search</b></font></td><td><input type=text name=search id=search value=\"".$search."\" size=\"1\"5><input type=hidden name=id id=id value=\"".$id."\"></td><td><input type=submit value=\"Search\"></td></tr>";
			if (strlen(trim($flt)) > 0) {
				$str = $str."<tr><td colspan=3><hr size=\"1\" noshade color=\"#e0e0e0\"></td></tr>";
				$str = $str."<tr valign=\"top\"><td colspan=3>".$flt."</td></tr>";
			}
			$str = $str."</form></table>";
		}
	}
	return $str;
}




function composesql($t) {
	global $keywords;
	$fd = getfields($t);
	$tf = $fd[1].",".$fd[2];
	$tf = explode(",", $tf);
	if (strlen(trim($keywords)) > 0) {
		$s = explode(" ", $keywords);
		foreach ($s as $i) {
			if (strlen(trim($i)) > 0) {
				foreach ($tf as $j) {
					if (strlen(trim($j)) > 0) {
						$sql = $sql." `".$t."`.`$j` like '%$i%' or ";
					}
				}
			}
		}
	}
	if (strlen($sql) > 4) {
		$sql = substr($sql, 0, strlen($sql) - 4);
		$sql = " and (".$sql.") ";
	}
	foreach ($_GET as $i=>$j) {
		$ft = "";
		if (substr(strtolower($i), strlen($i) - 2, 2) == "id" && strlen($i) > 2) {
			$a = $_GET[$i];
			if (!is_array($a)) {
				$a = explode("||||<|", $a);
			}
			foreach ($a as $j) {
				if (strlen(trim($j)) > 0 && is_numeric($j)) {
					$ft = $ft."`".$t."`.`".strtolower(trim($i))."`='".$j."' or ";
				}
			}		
		}
		if (strlen(trim($ft)) > 0) {
			$sql = $sql." and (".substr($ft, 0, strlen($ft) - 4).") ";
		}
	}
	return $sql;
}

function actions($tb, $d, $a) {
	/*
	global $dba;
	if ($a == true) {
		$sql = "select title from `actiontype` where lower(trim(`db`))='".$tb."' order by id;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$pad = "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
			while (!$rs->eof()) {
				$str = $str."<td><input type=submit class=action_button value=\"".$rs->row("title")."\"></td>";
				$rs->movenext();
			}
		}
	}	
	if ($d == true) {
		$str = $str.$pad."<td><input type=submit class=delete_button value=Delete></td>";
	}
	if (strlen(trim($str)) > 0) {
		$str = "<table border=\"0\" cellpadding=3 cellspacing=\"0\"><tr>".$str."</tr></table>";
	}
	*/
	return $str;
}


function viewer($tb, $cid) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($cid)) > 0) {
		$sql = "select * from `$tb` where id='".$cid."' limit 1;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<font size=+1 color=#000000>".$tb."<tt>-></tt>".$rs->row(1)."</font><br/>";
			if (trim($rs->row("statusid")) == "7") {
				$str = $str."<table border=3 cellpadding=5 bordercolor=#cc3300 cellspacing=\"0\" width='100%'><tr><td>";
			}
			$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='100%'><tr><td bgcolor=#f0f0f0>";
			$str = $str."<table border=\"0\" cellpadding=5 cellspacing=1 width='100%'>";
			$mcount = 0;
			$count = 3; //round((sizeof($rs->fields) + 1) / 2);
			for ($i = 1; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				$v = $rs->row($i);
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
					$v = getmyfield($f, $v);
				}
				$f = ucwords(hashtable($f));
				if (strlen(trim($v)) == 0) {
					$v = "-";
				}
				if ($mcount == 0) {
					$str = $str."<tr bgcolor=\"#ffffff\">";
				}
				$str = $str."<td bgcolor=#fcfcfc><font class=texthighlighted>".$f."</font></td><td>".$v."</td>";
				$mcount++;
				if ($mcount == $count) {
					$mcount = 0;
					$str = $str."</tr>";
				}
			}
			if ($mcount > 0) {
				$str = $str."<td colspan=".(($count - $mcount)*2).">&nbsp;</td>";
				$str = $str."</tr>";
			}
			$str = $str."</table>";
			$str = $str."</td></tr></table><br/>";
			$str = $str.eventlog($tb, $cid);
			//print ($tb.$rs->row("statusid"));
			if (strtolower(trim($tb)) == "firearms" && trim($rs->row("statusid")) == "7") {
				$str = $str.getdbdata("destruction", "firearmid=".$cid);
			}
			if (trim($rs->row("statusid")) == "7") {
				$str = $str."</td></tr></table>";
			}
		}
		else {
			$str = $str.messagebox("Sorry, Requested Information could not be displayed", false);
		}
	}
	else {
		$str = $str.messagebox("Sorry, Information could not be displayed", false);
	}
	return $str;
}

	
function eventlog($tb, $cid) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($cid)) > 0) {
		$sql = "select * from `log` where `table`='".strtolower(trim($tb))."' and `record`='".trim($cid)."' order by `date` desc;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<font color=#cc6600>Activity Log</font><br/><br/>";
			$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='100%'><tr><td bgcolor=#f0f0f0>";
			$str = $str."<table border=\"0\" cellpadding=2 cellspacing=1 width='100%'>";
			
			$str = $str."<tr bgcolor=#FEF3F3>";
			for ($i = 3; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
				}
				$f = strtoupper(hashtable($f));
				$str = $str."<td><small>".$f."</small></td>";
			}
			$str = $str."</tr>";
			
			while (!$rs->eof()) {
				$str = $str."<tr bgcolor=\"#ffffff\">";
				for ($i = 3; $i < sizeof($rs->fields); $i++) {
					$f = $rs->fields[$i];
					$v = $rs->row($i);
					if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
						$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
						$v = getmyfield($f, $v);
					}
					$f = ucwords(hashtable($f));
					if (strlen(trim($v)) == 0) {
						$v = "-";
					}
					$str = $str."<td><small>".$v."</small></td>";
				}
				$str = $str."</tr>";
				$rs->movenext();
			}
			$str = $str."</table>";
			$str = $str."</td></tr></table>";
		}
	}
	return $str;
}

function getdbdata($tb, $filter) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($filter)) > 0) {
		$sql = "select distinct * from `".$tb."` where ".$filter." order by `date` desc;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<br/><font color=#cc6600>".ucwords($tb)." Details</font><br/>";
			$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='100%'><tr><td bgcolor=#f0f0f0>";
			$str = $str."<table border=\"0\" cellpadding=2 cellspacing=1 width='100%'>";
			
			$str = $str."<tr bgcolor=#FEF3F3>";
			for ($i = 1; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
				}
				$f = strtoupper(hashtable($f));
				$str = $str."<td><small>".$f."</small></td>";
			}
			$str = $str."</tr>";
			
			while (!$rs->eof()) {
				$str = $str."<tr bgcolor=\"#ffffff\">";
				for ($i = 1; $i < sizeof($rs->fields); $i++) {
					$f = $rs->fields[$i];
					$v = $rs->row($i);
					if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
						$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
						$v = getmyfield($f, $v);
					}
					$f = ucwords(hashtable($f));
					if (strlen(trim($v)) == 0) {
						$v = "-";
					}
					$str = $str."<td><small>".$v."</small></td>";
				}
				$str = $str."</tr>";
				$rs->movenext();
			}
			$str = $str."</table>";
			$str = $str."</td></tr></table>";
		}
	}
	return $str;
}



function listing($currenttable, $fields = "*", $filter = null, $limit = 20, $add = false, $view = false, $edit = false, $delete = false, $dbfilters = false, $dbfilters_excluded = null, $navigation = false, $dbsearch = false, $multiselect = false, $multiselectheader = null, $multiselectscript = null, $multiselectfield = null, $multiselectfieldvalue = null, $orderby = null) {
	global $dba, $id, $searchablefields, $search, $p, $sidemenu, $application;

	$asql = composesql($currenttable);
	if (strlen(trim($filter)) > 0) {
		$asql = $asql." and (".$filter.") ";
	}
	
	if (trim($fields) != "*") {
		$fields = "id,".$fields;
	}
	
	
		
	$sql = "select SQL_NO_CACHE ".$fields." from `$currenttable` ";
	if (strlen(trim($asql)) > 0) {
		$sql = $sql." where ".substr($asql, 5, strlen($asql))." ";
		$pageitems = getcount($currenttable, "id > 0 ".$asql);
	}
	else {
		$pageitems = getcount($currenttable);
	}
	if (!is_numeric($limit)) {
		$limit = 20;
	}
	$page = ($p - 1) * $limit;
	if (strlen(trim($orderby)) == 0) {
		$orderby = "id desc";
	}
	$sql = $sql." order by ".$orderby." limit $page, $limit;";
	//print $sql;
	$rs = $dba->execute($sql);
	
	//SQL_CALC_FOUND_ROWS 
	//$cnsql = "SELECT FOUND_ROWS();";
	//$rscount = $dba->execute($cnsql);
	//$pageitems = $rscount->row(0);
	
	if ($add == true) {
		$addbutton = showpopup($currenttable, 0, "Add New Record to ".ucwords(hashtable($currenttable))." Database", 1, "<b><font class=textbright>Add&nbsp;New&nbsp;".ucwords(hashtable($currenttable))."</font></b>&nbsp;");
		$addbutton = button($addbutton);
	}
	$refreshbutton = "<a href=\"".get_script_name();
	if (trim($id) != "0") {
		$refreshbutton = $refreshbutton."?id=".$id;
	}	
	$refreshbutton = $refreshbutton."\"><font class=textbright>Clear&nbsp;all&nbsp;Filters</font></a>";
	$refreshbutton = button($refreshbutton);
	if ($dbsearch == true) {
		$searchbutton = searchbox($keywords, get_script_name());
	}
	$buttons = null;
	if (strlen(trim($addbutton)) > 0 || strlen(trim($refreshbutton)) > 0) {
		$buttons = "<table border=\"0\" cellpadding=\"0\" cellspacing=5><tr valign=\"middle\"><td>".$addbutton."</td><td>".$refreshbutton."</td></tr></table>";
	}
	$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='100%'>";
	$str = $str."<tr valign=\"middle\"><td height=40 width='40%'>".gettablestats($currenttable)."</td><td width='60%'>&nbsp;&nbsp;&nbsp;&nbsp;</td><td align=\"right\" valign=bottom>".$buttons."<br/>".$searchbutton."</td></tr>";
	$str = $str."</table><br/>";
	if ($rs->eof()) {
		$message = "No items are available. ";
		if (sizeof($asql) == 0) {
			if ($add == true) {
				$message = $message."Click on the link below to add a new item to this database<br/><br/><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr valign=\"middle\"><td><img src=images/add_new.gif width=20 border=\"0\"></td><td>".$addbutton."</td></tr></table><br/>";
			}
		}
		$str = $str.messagebox($message);
	}
	else {
		if ($multiselect == true) {
			//$str = $str."<form method=post action=\"".$multiselectscript."\">";
			//if ($actions == true) {
				//$str = $str.actions($currenttable, $delete, $actions);
			//}
		}	
		$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='100%'><tr><td bgcolor=\"#e0e0e0\">";
		$str = $str."<table border=\"0\" cellpadding=3 cellspacing=1 width='100%'>";
		$bgc = "#f9f9f9";

		$str = $str."<tr bgcolor=".$bgc.">";
		if ($multiselect == true) {
			$str = $str."<td bgcolor=#FEF1E6><small><font color=#cc6600>".$multiselectheader."</font></small></td>";
		}
		$fc = $rs->fieldcount();
		if (trim($fields) == "*" &&  $rs->fieldcount() > 3) {
			$fc = 5;
		}

		for ($i = 1; $i < $fc; $i++) {
			if (trim(strtolower($rs->types[$i])) != "blob" && trim(strtolower($rs->fields[$i])) != "url" && trim(strtolower($rs->fields[$i])) != "picture") {
				$w = "";
				//if ($mcount == 0) {
				//	$w = " width='50%'";
				//}
				$str = $str."<td background=\"includes/templates/".$application["template"]."/images/barbkg.gif\" height=40><b><font color=#000000>";
				if (substr(strtolower($rs->fields[$i]), strlen($rs->fields[$i])-2, 2) == "id") {
					$str = $str.putspace(ucwords(hashtable(substr($rs->fields[$i], 0, strlen($rs->fields[$i])-2))));
				}
				else {
					$str = $str.putspace(ucwords(hashtable($rs->fields[$i])));
				}
				$str = $str."</td>";
				$mcount++;
			}
		}		
		$manage = 0;
		if ($view == true) {
			$manage++;
		}
		if ($edit == true) {
			$manage++;
		}
		if ($delete == true) {
			$manage++;
		}
		if ($manage > 0) {
			$str = $str."<td align=\"center\" background=\"includes/templates/".$application["template"]."/images/barbkg.gif\" width='10%' colspan=".$manage."><b><font color=#000000>Manage</font></b></td>";
		}
		$str = $str."</tr>";
		while (!$rs->eof()) {
			$cl = "rowpending";
			if (trim($rs->row("statusid")) == "") {
				$cl = "rownormal";
			}
			if (trim($rs->row("statusid")) == "1") {
				$cl = "rownormal";
			}

			if (trim($rs->row("statusid")) == "3" || trim($rs->row("statusid")) == "8") {
				$cl = "rowactive";
			}
			if (trim($rs->row("statusid")) == "7") {
				$cl = "rowdisabled";
			}
			if ($multiselect == true) {
				$multiselectstate = false;
				$multiselectbg = "#ffffff";
				if (strtolower(trim($rs->row($multiselectfield))) == trim(strtolower($multiselectfieldvalue))) {
					$multiselectstate = true;
					$multiselectbg = "#FEF4EC";
				}
				$str = $str."<tr valign=\"top\" class=\"".$cl."\" bgcolor='".$multiselectbg."' id=\"r".$rs->row("id")."\" name=\"r".$rs->row("id")."\">";
				if ($multiselectstate == false && (trim($rs->row("statusid")) == "1" || trim($rs->row("statusid")) == "3" || trim($rs->row("statusid")) == "8")) {
					$str = $str."<form method=get action=\"".$multiselectscript."\"><td bgcolor=\"".$multiselectbg."\" align=\"center\"><input type=checkbox name=id value=\"".$rs->row("id")."\"  onchange=this.form.submit() onclick=this.form.submit()></td></form>";
				}
				else {
					$str = $str."<td>&nbsp;</td>";
				}
			}
			else {
				$str = $str."<tr valign=\"top\" class=\"".$cl."\" bgcolor=\"#ffffff\" id=\"r".$rs->row("id")."\" name=\"r".$rs->row("id")."\">";
			}			
			$mcount = 0;
			for ($i = 1; $i < $fc; $i++) {
				if (trim(strtolower($rs->types[$i])) != "blob" && trim(strtolower($rs->fields[$i])) != "url" && trim(strtolower($rs->fields[$i])) != "picture") {
					if (substr(strtolower($rs->fields[$i]), (strlen($rs->fields[$i])-2), 2) == "id" && strlen($rs->fields[$i]) > 2) {
						$mtemp = getmyfield(substr($rs->fields[$i], 0, (strlen($rs->fields[$i])-2)), $rs->row($i));
					}
					else {
						if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 4, 4)) == "date") {
							$mtemp = "<div align=\"right\">".formatmydate($rs->row($i))."</div>";
						}
						else {
							if (strtolower($rs->fields[$i]) == "record title" && strtolower($currenttable) == "eventlog") {
								$mtemp = getmyfield($rs->row(3), $rs->row($i));
							}	
							else {	
								if (strtolower($rs->fields[$i]) == "accesscontrol") {
									$mtemp = getmultiselectfield($rs->fields[$i], $rs->row($i));
								}
								else {
									$mtemp = $rs->row($i);
								}
							}
						}
					}
					if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 8, strlen($rs->fields[$i]))) == "password") {
						$mtemp = "******";
					}
					$mtemp = trim($mtemp);
					$mtemp = $mtemp;
					if (substr(strtolower(trim($rs->fields[$i])), 0, 6) == "serial") {
						$mtemp = "<div align=\"right\">".putspace(strtoupper($mtemp))."</div>";
					}
					else {
						$mtemp = ucwords(strtolower($mtemp));
					}
					$str = $str."<td class=$cl>".$mtemp."</td>";
					$mcount++;
				}
			}
			if ($view == true) {
				$str = $str."<td>&nbsp;".showpopup($currenttable, $rs->row(0), "View Record Information", 4, "<font class=texthighlighted>View</font>")."&nbsp;</td>";
			}
			if ($edit == true) {
				$str = $str."<td>&nbsp;".showpopup($currenttable, $rs->row(0), "Change Record Information", 2, "<font class=texthighlighted>Change</font>")."&nbsp;</td>";
			}
			if ($delete == true) {
				$str = $str."<td>&nbsp;".showpopup($currenttable, $rs->row(0), "Delete Record", 3, "<font class=textalert>Del</font>")."&nbsp;</td>";
			}
			$str = $str."</tr>";
			$rs->movenext();
		}
		$str = $str."</form>";
		$str = $str."</table>";
		$str = $str."</td></tr></table>";
		if ($multiselect == true) {
		//	if ($actions == true) {
		//		$str = $str.actions($currenttable, $delete, $actions);
		//	}
		//	$str = $str."</form>";
		}
		
		if ($navigation == true) {
			$pages = round($pageitems / $limit);
			if (($pages * $limit) < $pageitems) {
				$pages++;
			}
			if ($pages > 0) {

				$g = "";
				foreach ($_GET as $i=>$j) {
					if (strtolower(trim($i)) != "id" && strtolower(trim($i)) != "p" && strtolower(trim($i)) != "search") {
						$a = $j;
						if (is_array($a)) {
							if (sizeof($a) > 0) {
								$a = implode(",", $a);
								$g = $g.$i."[]=".$a."&";
							}
						}
						else {
							$a = trim($a);
							if (strlen(trim($a)) > 0) {
								$g = $g.$i."=".$a."&";
							}
						}
					}
				}			

				$istr = $istr."<div align=\"right\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td bgcolor=\"#ffffff\">";
				$istr = $istr."<table border=\"0\" cellpadding=6 cellspacing=4 width='100%'><tr bgcolor=\"#ffffff\">";
				//$istr = $istr."<td><b>Viewing&nbsp;Page&nbsp;$p&nbsp;of&nbsp;$pages&nbsp;&nbsp;&nbsp;</td>";
				$istr = $istr."<td><a href=\"".get_script_name()."?id=$id&search=$search&p=1&$g\"><font class=texthighlighted>First</font></a></td>";
				$s = $p - 5;
				if ($s < 1) {
					$s = 1;
				}
				$e = $s + 9;
				for ($i = $s; $i <= $pages; $i++) {
					if ($p == $i) {
						$istr = $istr."<td bgcolor=#9FD3FB><b><font class=textbright>$i</font></b></td>";
					}
					else {
						$istr = $istr."<td bgcolor=\"#ffffff\"><a href=\"".get_script_name()."?id=$id&search=$search&p=$i&$g\"><font color=#000000>$i</font></a></td>";
					}
					if ($i == $e) {
						break;
					}
				}
				$istr = $istr."<td><a href=\"".get_script_name()."?id=$id&search=$search&p=$pages&$g\"><font class=texthighlighted>Last</font></a></td>";
				$istr = $istr."</tr></table></td></tr></table></div>";
			}
			if (strlen($istr) > 0) {
				$str = $str."".$istr;
			}
		}
	}
	if ($dbfilters == true) {
		$sidemenu = $sidemenu.filters($currenttable, $dbfilters_excluded, $asql); 
	}
	return $str;
}

function view($tb, $cid) {
	global $dba;
	if (strlen(trim($tb)) > 0 && strlen(trim($cid)) > 0) {
		$sql = "select * from `$tb` where id='".$cid."' limit 1;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<font size=+1 color=#000000>".$tb."<tt>-></tt>".$rs->row(1)."</font><br/>";
			if (trim($rs->row("statusid")) == "7") {
				$str = $str."<table border=3 cellpadding=5 bordercolor=#cc3300 cellspacing=\"0\" width='100%'><tr><td>";
			}
			$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='100%'><tr><td bgcolor=#f0f0f0>";
			$str = $str."<table border=\"0\" cellpadding=5 cellspacing=1 width='100%'>";
			$mcount = 0;
			$count = 3; //round((sizeof($rs->fields) + 1) / 2);
			for ($i = 1; $i < sizeof($rs->fields); $i++) {
				$f = $rs->fields[$i];
				$v = $rs->row($i);
				if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
					$f = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
					$v = getmyfield($f, $v);
				}
				$f = ucwords(hashtable($f));
				if (strlen(trim($v)) == 0) {
					$v = "-";
				}
				if ($mcount == 0) {
					$str = $str."<tr bgcolor=\"#ffffff\">";
				}
				$str = $str."<td bgcolor=#fcfcfc><font class=texthighlighted>".$f."</font></td><td>".$v."</td>";
				$mcount++;
				if ($mcount == $count) {
					$mcount = 0;
					$str = $str."</tr>";
				}
			}
			if ($mcount > 0) {
				$str = $str."<td colspan=".(($count - $mcount)*2).">&nbsp;</td>";
				$str = $str."</tr>";
			}
			$str = $str."</table>";
			$str = $str."</td></tr></table><br/>";
		}
		else {
			$str = $str.messagebox("Sorry, Requested Information could not be displayed", false);
		}
	}
	else {
		$str = $str.messagebox("Sorry, Information could not be displayed", false);
	}
	return $str;
}


function view_page($id) {
	global $dba;
	$sql = "select id, title, content from `page` where id=".$id.";";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<center><table border=\"0\" width='98%' cellpadding=\"0\" cellspacing=\"0\"><tr valign=\"top\"><td><div align=\"justify\">";
		if (strlen(trim($rs->row(2))) > 0) {
			$content = $content.$rs->row(2);
		}
		$str = $str.$content;
		$str = $str."</div></td></tr></table>";
	}
	else {
		$str = $str.messagebox("Sorry, the requested content is not available", false);
	}
	return $str;
} 


function get_prev_status($id, $statusid) {
	global $dba;
	$sql = "select statusid from activity where firearmid = $id and statusid != $statusid order by date desc, id desc;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		return $rs->row(0);
	}
}

function title() {
	global $mtitle, $application;
	$str = $str.$application["title"];
	if (strlen(trim($mtitle)) > 0) {
		$str = $str." - ".$mtitle;
	}
	return $str;
}

function scripts() {
	global $mpage, $indicatorviewid;
	if (strtolower(trim($mpage)) == "indicators") {
		$str = $str."<script type=\"text/javascript\" src=\"js/jquery-1.7.2.min.js\"></script>\n";
		if (trim($indicatorviewid) == "2012050810463899807") {
			$str = $str."<script type=\"text/javascript\" src=\"http://maps.googleapis.com/maps/api/js?key=AIzaSyBeMQzyx95lS2q0ZHfd-TQ3XRbAENcKdY8&sensor=false\"></script>\n";
			$str = $str."<script type=\"text/javascript\" src=\"js/gmap3.min.js\"></script>\n";
			$str = $str."<script type=\"text/javascript\" src=\"js/mapscripts.js\"></script>\n";
		}
		else {
			$str = $str."<link type=\"text/css\" href=\"css/ui-lightness/jquery-ui-1.8.19.custom.css\" rel=\"stylesheet\" />\n";
			$str = $str."<script type=\"text/javascript\" src=\"js/jquery-ui-1.8.19.custom.min.js\"></script>\n";
			$str = $str."<script type=\"text/javascript\" src=\"js/scripts.js\"></script>\n";
		}
	}
	return $str;
}


function shorten_url($longurl) {
	/*
	$apiKey = 'AIzaSyBeMQzyx95lS2q0ZHfd-TQ3XRbAENcKdY8';
	$postData = array('longUrl' => $longurl, 'key' => $apiKey);
	$jsonData = json_encode($postData);

	$curlObj = curl_init();
	curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
	curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 1);
	curl_setopt($curlObj, CURLOPT_HEADER, 0);
	curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
	curl_setopt($curlObj, CURLOPT_POST, 1);
	curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
	 
	$response = curl_exec($curlObj);

	$json = json_decode($response);
	curl_close($curlObj);
	return $json->id;
	*/
	return $longurl;
}

?>
