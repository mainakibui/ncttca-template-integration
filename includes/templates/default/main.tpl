{{bsheaders}}

    <!--Header-->
    <div class="head-banner">
	    <div class="container nopadding">
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 logo-combination nopadding">
				<a href="index.php">
					<div class="logo-head-plain pull-left">
						<img src="../images/top_logo_en.png" alt="TOP LOGO">
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 nopadding">
				<div class="site-info">
				
					<div class="clear pull-right country-flags"> <img src="all-flags.png" alt="DRC, Burundi, Tanzania, South Sudan, Kenya, Uganda, Rwanda"> </div>
					
					<div class="pull-right language-btn"> 
						{{loginbuttons}}
						<a href="language.php?id=1" class="btn btn-sm btn-default language" role="button"> <i class="fa fa-language"></i> English </a>
						<a href="language.php?id=2" class="btn btn-sm btn-default language" role="button"> <i class="fa fa-language"></i> Fench </a> 
					</div>
					
					<div class="clear pull-right">
						<form class="mainnav-form pull-right search-form" role="search" action="search.php">
							<div class="form-group has-feedback">
							<label for="search" class="sr-only">Search</label>
							<input type="text" class="form-control" name="search" id="search" placeholder="search">
							<button type="submit" class="fa fa-search form-control-feedback"></button>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	    </div>
    </div>
    

    <!-- Fixed navbar -->
        
    {{menu(1, 0, "h")}}<!--/top menu-->
        

    <!-- Begin page content -->
    
    <div class="">
    
    	<!--container-->
    	<div class="main-page container nopadding">

		{{content}}
	
	</div><!--/container-->
	
	</div><!--/panel1-->

	
{{stakeholdersfooterslider}}


{{bsfooters}}

<script type="text/javascript">
$(document).ready(function() {

	$('.responsive').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		autoplay: true,
		autoplaySpeed: 5000,
		responsive: [
		
		{
		breakpoint: 1024,
		settings: {
		slidesToShow: 3,
		slidesToScroll: 3,
		infinite: true,
		dots: true
		}
		},
		
		{
		breakpoint: 600,
		settings: {
		slidesToShow: 2,
		slidesToScroll: 2
		}
		},
		
		{
		breakpoint: 480,
		settings: {
		slidesToShow: 1,
		slidesToScroll: 1
		}
		}
		]
	});		

});
</script>
