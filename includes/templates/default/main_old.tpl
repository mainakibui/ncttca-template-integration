<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{{title}}</title>
<meta name="keywords" content="Northern Corridor, Corridor, East Africa, Central Africa, Transport, Kenya, Uganda, Rwanda, Burundi, Democratic Republic of Congo, Port, Border, Observatory" />
<meta name="description" content="The Northern Corridor comprises of the transport infrastructure, facilities and services in East and Central Africa linked to the Maritime Port of Mombasa. These primary transport network and facilities link the Port of Mombasa in Kenya to the Great Lakes countries of Uganda, Rwanda, Burundi and the Democratic Republic of Congo." />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
{{stylesheet}}
{{scripts}}
</head>
<body>
<table width="1000" border="0" cellspacing="1" cellpadding="0" id="main" bgcolor="#ffffff">
<tr valign="top" align="left"><td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td height="20">&nbsp;&nbsp;{{language}}</td>
			<td valign="top" align="right">{{userinfo}}</td>
			</tr>
			<tr align="left" valign="top"><td colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td><img src="logo.png" width="697" height="154" alt="NCTTCA Logo"/></td>
				<td align="left" valign="top">
					<table border="0" align="right" cellpadding="0" cellspacing="4">
					<tr>
					<td><img src="burundi.png" width="24" height="24" alt="Burundi Flag"/></td>
					<td><img src="drc.png" width="24" height="24" alt="DRC Flag" /></td>
					<td><img src="kenya.png" width="24" height="24" alt="Kenya Flag"/></td>
					<td><img src="rwanda.png" width="24" height="24" alt="Rwanda Flag"/></td>
					<td><img src="uganda.png" width="24" height="24" alt="Uganda Flag"/></td>
					<td><img src="ssudan.jpg" width="24" height="16" alt="South Sudan Flag"/></td>                              
					</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr><td valign="bottom" align="right">{{searchbox}}</td></tr>
					</table>
				</td></tr>
				</table>
			</td></tr>
			</table>
		</td></tr>
		</table>
	</td></tr>
	<tr><td>{{menu(1, 0, "h")}}</td></tr>
	</table>
</td></tr>
<tr valign="top"><td width="100%" height="400">
	<table cellspacing="0" cellpadding="8" width="100%" border="0"><tr valign="top"><td>
	{{page_image()}} 
	{{pagetitle()}} 
	{{content}}
	</td></tr></table>

</td></tr>
<tr><td>
	<table cellspacing="0" cellpadding="0" width="100%" border="0">
	<tr><td><hr size="1" noshade color="#e0e0e0"></td></tr>
	<tr><td height="30">
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
		<tr><td width="100%">{{bottommenus}}</td>
		<td><div align="right">{{copyright}}</div></td></tr>
		</table>
	</td></tr>
	</table>
</td></tr>
</table>
</body>
</html>
