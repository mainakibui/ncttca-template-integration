{{bsheaders}}

    <!--Header-->
    <div class="head-banner">
	    <div class="container nopadding">
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 logo-combination nopadding">
				<a href="index.php">
					<div class="logo-head-plain pull-left">
						<img src="../images/top_logo_en.png" alt="TOP LOGO">
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 nopadding">
				<div class="site-info">
				
					<div class="clear pull-right country-flags"> <img src="all-flags.png" alt="DRC, Burundi, Tanzania, South Sudan, Kenya, Uganda, Rwanda"> </div>
					
					<div class="pull-right language-btn"> 
						{{loginbuttons}}
						<a href="language.php?id=1" class="btn btn-sm btn-default language" role="button"> <i class="fa fa-language"></i> English </a>
						<a href="language.php?id=2" class="btn btn-sm btn-default language" role="button"> <i class="fa fa-language"></i> Fench </a> 
					</div>
					
					<div class="clear pull-right">
						<form class="mainnav-form pull-right search-form" role="search" action="search.php">
							<div class="form-group has-feedback">
							<label for="search" class="sr-only">Search</label>
							<input type="text" class="form-control" name="search" id="search" placeholder="search">
							<button type="submit" class="fa fa-search form-control-feedback"></button>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	    </div>
    </div>
    

    <!-- Fixed navbar -->
        
    {{menu(1, 0, "h")}}<!--/top menu-->
        

    <!-- Begin page content -->
    
    <div class="">
    
    	<!--container-->
    	<div class="main-page container">

		<div class="row">
		
			<div class="col-sm-6 col-md-6 col-lg-6 nopadding dashboard">
		    
				<div class="row">
						
					<div id="content" class="col-sm-12 col-md-12 col-lg-12 nopadding">
					
						<!--menu item-->
						<a href="http://kandalakaskazini.go.ke/" target="_blank">
							<div class="flip-container feature data-masonry mason-item box" >
								<div id="f1_card" class="flipper bg-green dashboard-tile tile detail clean">
									<div class="content"> 
										<div class="front home-icon"> 
											<i class="icon-image "> <img class="dashboard_icon_image" src="images/dashboard_icon.png" alt="Dashboard"></i> 
											<div class="front-tile-text text-left"> <h3>Dashboard</h3> </div> 
										</div> 
									
										<div class="back back_dashboard description">
											<div class="back-tile-text text-left"> <h3>Dashboard</h3> </div>
											<div class="description-text">A weekly monitoring tool for selected performance indicators (nine) ranging from Maritime measures, Port indicators as well as Corridor of Hinterland.</div>
										</div>
									</div><!--/content-->

								</div>
							</div>
						</a>
						<!--/menu item-->
						
						<!--menu item-->
						<a href="indicators.php">
							<div class="flip-container feature data-masonry mason-item box" >
								<div class="flipper bg-maroon indicators-tile tile detail clean">
									<div class="content"> 
										<div class="front home-icon"> 
											<i class="icon-image "> <img class="menu_icon_image" src="images/indicators-icon.png" alt="Indicators"></i> 
											<div class="front-tile-text text-left"> <h3>Indicators</h3> </div> 
										</div> 
									
										<div class="back description">
											<div class="back-tile-text text-left"> <h3>Indicators</h3> </div>
											<div class="description-text">A section of the Observatory portal covering Corridor Performance Monitoring on longer time frequencies like monthly, quarterly and annually.</div>
										</div>
									</div><!--/content-->

								</div>
							</div>
						</a>
						<!--/menu item-->
						
						<!--menu item-->
						<a href="gis.php">
							<div class="flip-container feature data-masonry mason-item box" >
								<div class="flipper bg-blue gis-tile tile detail clean">
									<div class="content"> 
										<div class="front home-icon"> 
											<i class="icon-image "> <img class="menu_icon_image" src="images/gis-icon.png" alt="GIS"></i> 
											<div class="front-tile-text text-left"> <h3>GIS</h3> </div> 
										</div> 
									
										<div class="back description">
											<div class="back-tile-text text-left"> <h3>GIS</h3> </div>
											<div class="description-text">The Global Information System for the Northern corridor covering the various sections and nodes of the entire corridor based on the various modes of transport.</div>
										</div>
									</div><!--/content-->

								</div>
							</div>
						</a>
						<!--/menu item-->
						
						<!--menu item-->
						<a href="downloads.php">
							<div class="flip-container feature data-masonry mason-item box" >
								<div class="flipper bg-grey documents-tile tile detail clean">
									<div class="content"> 
										<div class="front home-icon"> 
											<i class="icon-image "> <img class="menu_icon_image" src="images/documents-icon.png" alt="Documents"></i> 
											<div class="front-tile-text text-left"> <h3>Documents</h3> </div> 
										</div> 
									
										<div class="back description">
											<div class="back-tile-text text-left"> <h3>Documents</h3> </div>
											<div class="description-text">The Transport Observatory Repository for reports, publications and other resourceful material relating to the initiatives.</div>
										</div>
									</div><!--/content-->

								</div>
							</div>
						</a>
						<!--/menu item-->
						
					</div>
				</div><!--/row-->
				
			</div><!--/dashboard collumn-->
			
			<div class="col-sm-6 col-md-6 col-lg-6 nopadding dashboard">
				<div class="col-sm-12 col-md-12 col-lg-12 nopadding  dashboard-featured text-center">
					{{content}}
				</div>
				
			</div><!--/dashboard collumn-->
			
		</div><!--/row-->
	
	</div><!--/container-->
	
	</div><!--/panel1-->

	
{{stakeholdersfooterslider}}


{{bsfooters}}

<script type="text/javascript">
$(document).ready(function() {

	$('.responsive').slick({
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		autoplay: true,
		autoplaySpeed: 5000,
		responsive: [
		
		{
		breakpoint: 1024,
		settings: {
		slidesToShow: 3,
		slidesToScroll: 3,
		infinite: true,
		dots: true
		}
		},
		
		{
		breakpoint: 600,
		settings: {
		slidesToShow: 2,
		slidesToScroll: 2
		}
		},
		
		{
		breakpoint: 480,
		settings: {
		slidesToShow: 1,
		slidesToScroll: 1
		}
		}
		]
	});		

});
</script>