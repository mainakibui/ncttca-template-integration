<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="/codebase/templates/bs-annie-template/css/bootstrap.min.css" rel="stylesheet">
	<link href="/codebase/templates/bs-annie-template/css/base.css" rel="stylesheet">

	<style rel="stylesheet">
		#map-canvas {
			height: 800px;
			width: 100%;
			margin: 0px;
			padding: 0px
		}
		#legend{
			height: 50px;
			width: 100%;
			margin: 0px;
			padding: 0px	
		}
		#legend ul{
			list-style-type: none;
		    margin: 0;
		    padding: 0;
		}
		#legend ul li{
			float:left;
			padding-right:5px;
		}
		.btn-group{
			padding: 5px;
		}

		.btn-primary {
			color: #fff;
			background-color: #00A8D6;
			border-color: #0494BB;
		}
		.input-indicator-2
		{
			width: 150px !important;
			border-radius: 0px;
		}
		.filter-button
		{
			/*width: 200px;*/
		}
		.dropdown-toggle{
			border-radius: 0px;
			padding: 6px 12px;
		}
		#filters{
			display: none;
		}
		body {
		    font-size: 11px;
		}

		.input-group-addon {
		  font-size: 11px;
		}

		.btn{
		  font-size: 11px;
		}
		.form-control {
			height: 25px;
			padding: 0px;
			font-size: 11px;
			border-radius: 0px;
		}
		.legend-icon{
			height: 20px;
		}
		.x-btn .icon-expand {
		    background-image: url(/codebase/templates/bs-annie-template/images/expand.png) !important;
		}

	</style>
</head>
<body>
<div id="map-canvas"></div>
<div id="legend"> 
<ul>
  <li><img src="/codebase/templates/bs-annie-template/images/makers/boardercross.png" class="legend-icon"> <b>Border Posts</b> </li>  
  <li><img src="/codebase/templates/bs-annie-template/images/makers/bridge_old.png" class="legend-icon"><b>Weigh Bridge</b> </li>
  <li><img src="/codebase/templates/bs-annie-template/images/makers/tramway.png" class="legend-icon"><b>Train Station</b> </li>
  <li><img src="/codebase/templates/bs-annie-template/images/makers/festival.png" class="legend-icon"><b>Town</b></li>
  <li><img src="/codebase/templates/bs-annie-template/images/makers/boatcrane.png" class="legend-icon"><b>Sea Port</b></li>

</ul>
</div>
<div id="filters">
	<form>
		<p style="color:#A82025;font-size:12px">Please select the filters below to and view changes on the map</p>
		<div class="btn-group">
		<select id="country" class="form-control">
			<option value="">-- Select Country --</option>
		</select>
		</div>
	
		<div class="btn-group">
		<select id="mode_of_transport" class="form-control">
			<option value="">-- Select Mode of Transport --</option>
		</select>
		</div>
	
		<div class="btn-group noncompare">
		<select id="route" class="form-control">
			<option value="">-- Select Route --</option>
		</select>
		</div>		
	
		<div class="btn-group noncompare">
		<select id="node" class="form-control">
			<option value="">-- Select Node --</option>
		</select>
		</div>
		
		<div class="btn-group noncompare">
		<select id="subnode" class="form-control">
			<option value="">-- Select Sub Node --</option>
		</select>
		</div>

	
		<div class="btn-group noncompare">
		<select id="indicator_category" class="form-control">
			<option value="">-- Select Indicator Category --</option>
		</select>
		</div>

	
		<div class="btn-group noncompare">
		<select id="indicator" class="form-control">
			<option value="">-- Select Indicator --</option>
		</select>
		</div>

		<div class="input-group date" id="filterdatepicker">
			<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
		    <input type="text" class="form-control" name="filterdate" />
	    </div>

<!--
	    <div class="input-daterange input-group" id="filterdatepicker">
	    	<span class="input-group-addon">Date</span>
	    	<input type="text" class="input-sm form-control" name="start" />
		    <span class="input-group-addon">To</span>
		    <input type="text" class="input-sm form-control" name="end" />
	    </div>
	    -->
	    <div id='dateError' style="color:red;"></div>
	    <hr/>
	    
		<!--<a href="javascript:submitForm();" class="btn btn-default bg-lightgrey pull-right" role="submit">Submit</a> -->
		<input type="reset" class="btn pull-left" value="Reset Fields" onclick="resettmpVectors();">
	</form>
</div>
<?php include_once('gisjs.html');?>
</body>
</html>
