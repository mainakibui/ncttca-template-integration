<?php
require_once("includes/config.php");
global $dba;

$offset = (int)(clean($_POST['offset']));
//$offset = (int)clean($_GET['offset']);

$sql = "select  id, title, summary, url, content, date, author from `news` where statusid = '1' ";
$sql = $sql." order by date desc, id desc limit 5 offset $offset";

$rs = $dba->execute($sql);
$count = $rs->rowCount();
	
if( !$rs->eof() )
{
	$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
	$str = $str."<hr>";
	$str = $str."</div>";
	
	while(!$rs->eof()) {
		$str = $str."<div class='row'>";
		
		$url = "news.php?newsid=".$rs->row("id");
		$title = removehtml($rs->row("title"));
		$author = removehtml($rs->row("author"));
		$summary = removehtml($rs->row("summary"));
		
		if ($id == 0 || strlen(trim($id)) == 0) {
			$title = removehtml($rs->row("title"));
		}
		else {
			$content = $rs->row("content");
			$ourl = removehtml($rs->row("url"));			
		}
		$str = $str."<div class='col-sm-2 col-md-2 col-lg-2' style='text-align: center; display: block;'>";
		$str = $str."<div class='date' style='margin: 20px 0 0;'>";
		$str = $str."<div class='day' style='font-size: 32px; font-weight: 700;'>".day($rs->row("date"))."</div>";
		$str = $str."<div class='month' style='font-family: Lato,sans-serif;text-transform: uppercase;font-size: 12px;font-weight: 400;'>".monthname($rs->row("date"))."&nbsp;".year($rs->row("date"))."</div>";
		$str = $str."</div>";
		$str = $str."</div>";
		
		$str = $str."<div class='col-sm-10 col-md-10 col-lg-10'>";
		if (strlen(trim($title)) > 0) {	
			$str = $str."<h4> <a href='".$url."' style='white-space: normal;'>".$title."</a> </h4>";
		}
		
		if (strlen(trim($author)) > 0) {
			$str = $str.$author;	
		}
		
		if (strlen(trim($summary)) > 0) {	
			if ($id == 0 || strlen(trim($id)) == 0) {
				$str = $str."<div class='1'>".$summary."</div>";
			}
			else {
				$str = $str."<div class='2'>".messagebox($summary)."</div>";
			}
		}
		if (strlen(trim($content)) > 0) {	
			$str = $str.$content."<div class='col-sm-12 col-md-12 col-lg-12'> <a class='btn btn-success pull-right' role='button' href='news.php'> View More News Articles </a> </div>";
		}
		if (strlen(trim($ourl)) > 0) {	
			$str = $str."<div> <b>Related URL</b>: <a href='".$ourl."' target=w".$rs->row("id").">".$ourl."</a> </div>";
		}
		if (strlen(trim($id)) > 0 && is_numeric($id) && $id != 0) {
			$str = $str.comments($rs->row("id"), $title);
		}
		$str = $str."</div>";
		
		$rs->movenext();
		if (!$rs->eof()){
			$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
			$str = $str."<hr>";
			$str = $str."</div>";
		}
		$str = $str."</div>";
	}

	$data = array(
		'success'=>'true',
		'message'=>'sucess: '.$count.' records found',
		'string'=>$str,
		);
}
else
{
		$data = array(
		'success'=>'false',
		'message'=>'error: No records found',
		'string'=>'',
		);
}

echo json_encode($data);
?>
