<?php
require_once("includes/config.php");

$mtitle = "Events Calendar";
$y = clean($_GET['y']);
$m = clean($_GET['m']);
if (!is_numeric($y) || strlen(trim($y)) == 0) {
	$y = year(getmydate());
}
if (!is_numeric($m) || strlen(trim($m)) == 0) {
	$m = month(getmydate());
}
if ($y + 0 < 2010 || $y + 0 > 2015) {
	$y = year(getmydate());
}
if ($m + 0 < 1 || $m + 0 > 12) {
	$m = month(getmydate());
}
$y = pad($y, 1);
$m = pad($m, 1);


$mystr = $mystr."<table width='100%' border=0 cellspacing=0 cellpadding=0>";
$mystr = $mystr."<tr valign=top align=left><td><div height=\"300\" width=\"100%\" name=\"calendar\" id=\"calendar\">";
	$mystr = $mystr."<table width='100%' border=0 cellspacing=0 cellpadding=6>";
	$mystr = $mystr."<tr valign=top align=left><td width=\"300\"><img src=images/margin.gif border=0 width=300 height=1>".bigcalendar("calendar", "date", $y, $m)."</td><td width=\"100%\"><br>".list_events($y, $m)."</td></tr>";
	$mystr = $mystr."</table>";
$mystr = $mystr."</div></td></tr>";
$mystr = $mystr."</table>";

display($mystr);



function bigcalendar($db, $f, $y, $m) {
	global $dba;
	$x = array(1=>'jan',2=>'feb',3=>'mar',4=>'apr',5=>'may',6=>'jun',7=>'jul',8=>'aug',9=>'sept',10=>'oct',11=>'nov',12=>'dec');
	$sql = "select year($f) as `year` from `$db` group by `year` order by `year` desc limit 1;";
	$rsy = $dba->execute($sql);
	if (!$rsy->eof()) {
		while (!$rsy->eof()) {
			$mn = array();
			$sql = "select month($f) as `month`, count(*) as total from `$db` where year(`$f`)='".$rsy->row("year")."' group by `month` order by `month`;";
			$rsm = $dba->execute($sql);
			while (!$rsm->eof()) {
				$mn[$rsm->row("month")] = $rsm->row("total");
				$rsm->movenext();
			}
			$w = "25%";
			$h = "47";
			$str = $str."<table border=0 width='100%' cellpadding=5 cellspacing=5>";
			$str = $str."<tr><td width='$w' height='$h' align=center valign=center bgcolor=#000000><h3><b><font color=#ffffff>".$rsy->row("year")."</font></b></h3></font></td><td colspan=3>&nbsp;</td></tr>";
			$mcount = 0;
			for ($i = 1; $i <= 12; $i++) {
				if ($mcount == 0) {
					$str = $str."<tr>";
				}
				$e = false;
				foreach ($mn as $q=>$r) {
					if (trim(strtolower($q)) == trim(strtolower($i))) {
						$e = true;
						break;
					}
				}
				$bg = "#f0f0f0";
				$fg = "#808080";
				$a1 = "";
				$a2 = "";
				if ($e == true) {
					$bg = "#FFCC99";
					if (trim($y) == trim($rsy->row("year")) && trim($m) == trim($i)) {
						$bg = "#cc3300";
					}
					$fg = "#ffffff";
					$a1 = "<a href=\"calendar.php?y=".$rsy->row("year")."&m=".$i."\">";
					$a2 = "</a>";
				} 	
				$str = $str."<td width='$w' height='$h' bgcolor='$bg' align=center valign=center>".$a1."<h3><b><font color='$fg'>".strtoupper($x[$i])."</font></b></h3>".$a2."</td>";
				$mcount++;
				if ($mcount == 4) {
					$str = $str."</tr>";
					$mcount = 0;
				}
			}
			$str = $str."</table>";
			$rsy->movenext();
		}
	}
	return $str;
}


function list_events($year, $month) {
	global $dba, $application;
	$x = array("", "January", "February", "March", "April", "May", "June", "July", "August", "Septmeber", "October", "November", "December");
	if (is_numeric($year) && is_numeric($month)) {
		$sql = "select id, title, date, venue, details from `calendar` where month(date) = $month and year(date) = $year and statusid = 1 order by date asc limit 30;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$str = $str."<h2><b>".titlecase(translate("Events Happening in ".$x[(int)$month]." ".$year.""))."</b></h2><hr size=1 noshade color=#e0e0e0>";
			while(!$rs->eof()){
				$str = $str."<table cellspacing='3' cellpadding='0' border='0' width='100%'><tr valign=top><td><img src=\"includes/templates/".$application["template"]."/images/calendar.jpg\" width=40 border=0></td><td width='100%'>";
				$str = $str."<table cellspacing='5' cellpadding='0' border='0' width='100%'>";
				$str = $str."<tr><td align=\"left\"><font color=#06C size=+0><b>".titlecase(translate($rs->row("title")))."</b></font></td></tr>";
				$str = $str."<tr><td align=\"left\"><font class=\"texthighlighted\">".titlecase(translate("When")).":</font>&nbsp;".formatmydate($rs->row("date"))."&nbsp;&nbsp;|&nbsp;&nbsp;<font class=\"texthighlighted\">".titlecase(translate("Where")).":</font>&nbsp;".translate($rs->row("venue"))."</td></tr>";
				$str = $str."<tr><td align=\"left\"><div>".translate($rs->row("details"))."</div></td></tr>";
				//$str = $str."<tr><td align=\"left\"><font class=\"textfaded\">Posted By:&nbsp;".getmyfield("account", $rs->row("accountid"))."</font></td></tr>";
				$rs->movenext();
				if (!$rs->eof()) {
					$str = $str."<tr><td align=\"left\"><hr noshade color=#e0e0e0 size=1></td></tr>";
				}
				$str = $str."</table>";
				$str = $str."</td></tr></table>";
			}
		}
		else {
			$str = messagebox("<br><b>There are no events for ".date("F")."</b>");
		}
	}
	return $str;
}


?>
