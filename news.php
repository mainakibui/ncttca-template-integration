<?php
require_once("includes/config.php");
$mtitle = "News & Information";
$newsid = get_default(clean($_GET['newsid']), "n", 0);


display(viewnews($newsid));

function viewnews($id) {
	global $dba;
	
	$str = $str."<div class='tab-content'>";
		$str = $str."<div class='row panel4 clearfix'>";
			$str = $str."<div class='col-sm-12 col-md-12 col-lg-12  nopadding main-inner-page-padding'>";
				$str = $str."<div class='col-sm-12 col-md-12 col-lg-12  article'>";
				
					$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'> <h3>News</h3> </div>";
				
					$sql = "select  id, title, summary, url, content, date, author from `news` where statusid = '1' ";
					if (strlen(trim($id)) > 0 && is_numeric($id) && $id != 0) {
						$sql = $sql." and id='$id' ";
					}
					$sql = $sql." order by date desc, id desc limit 5";
					$rs = $dba->execute($sql);
					if (!$rs->eof()){
						$str = $str."<div id='show-posts'>";
						while(!$rs->eof()) {
							$str = $str."<div class='row'>";
							
							$url = "news.php?newsid=".$rs->row("id");
							$title = removehtml($rs->row("title"));
							$author = removehtml($rs->row("author"));
							$summary = removehtml($rs->row("summary"));
							
							if($id != 0)
							{
								$content = $rs->row("content");
								$ourl = removehtml($rs->row("url"));
								
								$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
									$str = $str."<div class='col-sm-2 col-md-2 col-lg-2' style='text-align: center; display: block;'>";
										$str = $str."<div class='date' style='margin: 20px 0 0;'>";
											$str = $str."<div class='day' style='font-size: 32px; font-weight: 700;'>".day($rs->row("date"))."</div>";
											$str = $str."<div class='month' style='font-family: Lato,sans-serif;text-transform: uppercase;font-size: 12px;font-weight: 400;'>".monthname($rs->row("date"))."&nbsp;".year($rs->row("date"))."</div>";
										$str = $str."</div>";
									$str = $str."</div>";
									
									$str = $str."<div class='col-sm-10 col-md-10 col-lg-10' style='display: block;'>";
										$str = $str."<div class='date' style='margin: 20px 0 0;'>";
											if (strlen(trim($title)) > 0) {	
												$str = $str."<h4> <a href=\"".$url."\" style='white-space: normal;'>".$title."</a> </h4>";
											}
											else
											{
												$str = $str."&nbsp;";
											}
											
											if (strlen(trim($author)) > 0) {
												$str = $str."<div> Author: ".$author."</div>";	
											}
										$str = $str."</div>";
									$str = $str."</div>";
								$str = $str."</div>";
								
								
								$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
								$str = $str."<hr>";
								
								if (strlen(trim($summary)) > 0) {
									$str = $str."<div class='2'>".messagebox($summary)."</div>";
								}
								if (strlen(trim($content)) > 0) {	
									$str = $str.$content."<div class='col-sm-12 col-md-12 col-lg-12'> <a class='btn btn-success pull-right' role='button' href=\"news.php\"> View More News Articles </a> </div>";
								}
								if (strlen(trim($ourl)) > 0) {	
									$str = $str."<div> <b>Related URL</b>: <a href=\"".$ourl."\" target=w".$rs->row("id").">".$ourl."</a> </div>";
								}
								if (strlen(trim($id)) > 0 && is_numeric($id) && $id != 0) {
									$str = $str.comments($rs->row("id"), $title);
								}
								$str = $str."</div>";
								
								$rs->movenext();
								if (!$rs->eof()){
									$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
										$str = $str."<hr>";
									$str = $str."</div>";
								}
							}
							else
							{
								if ($id == 0 || strlen(trim($id)) == 0) {
									$title = removehtml($rs->row("title"));
								}
								else {
									$content = $rs->row("content");
									$ourl = removehtml($rs->row("url"));			
								}
								$str = $str."<div class='col-sm-2 col-md-2 col-lg-2' style='text-align: center; display: block;'>";
									$str = $str."<div class='date' style='margin: 20px 0 0;'>";
										$str = $str."<div class='day' style='font-size: 32px; font-weight: 700;'>".day($rs->row("date"))."</div>";
										$str = $str."<div class='month' style='font-family: Lato,sans-serif;text-transform: uppercase;font-size: 12px;font-weight: 400;'>".monthname($rs->row("date"))."&nbsp;".year($rs->row("date"))."</div>";
									$str = $str."</div>";
								$str = $str."</div>";
								
								$str = $str."<div class='col-sm-10 col-md-10 col-lg-10'>";
								if (strlen(trim($title)) > 0) {	
									$str = $str."<h4> <a href=\"".$url."\" style='white-space: normal;'>".$title."</a> </h4>";
								}
								
								if (strlen(trim($author)) > 0) {
									$str = $str.$author;	
								}
								
								if (strlen(trim($summary)) > 0) {	
									if ($id == 0 || strlen(trim($id)) == 0) {
										$str = $str."<div class='1'>".$summary."</div>";
									}
									else {
										$str = $str."<div class='2'>".messagebox($summary)."</div>";
									}
								}
								if (strlen(trim($content)) > 0) {	
									$str = $str.$content."<div class='col-sm-12 col-md-12 col-lg-12'> <a class='btn btn-success pull-right' role='button' href=\"news.php\"> View More News Articles </a> </div>";
								}
								if (strlen(trim($ourl)) > 0) {	
									$str = $str."<div> <b>Related URL</b>: <a href=\"".$ourl."\" target=w".$rs->row("id").">".$ourl."</a> </div>";
								}
								if (strlen(trim($id)) > 0 && is_numeric($id) && $id != 0) {
									$str = $str.comments($rs->row("id"), $title);
								}
								$str = $str."</div>";
								
								$rs->movenext();
								if (!$rs->eof()){
									$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
										$str = $str."<hr>";
									$str = $str."</div>";
								}
							}
							$str = $str."</div>";
						}
						$str = $str."</div>";
						
						if($id == 0 || $id='')
							{
								$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
								$str = $str."<hr>";
								$str = $str."</div>";
								
								$str = $str."<div class='col-sm-12 col-md-12 col-lg-12' style='margin-bottom: 10px;'>";
									$str = $str."<div class='col-sm-4 col-md-4 col-lg-4'>";
										$str = $str."&nbsp;";
									$str = $str."</div>";
									$str = $str."<div class='col-sm-4 col-md-4 col-lg-4'>";
									
										$str = $str."<form id='load-more-news'>";
											$str = $str."<input type='hidden' id='offset' name='offset' value='10' readonly='readonly'>";
											$str = $str."<button class='btn btn-success center'>Load More News Items</button>";
										$str = $str."</form>";
										
									$str = $str."</div>";
									$str = $str."<div class='col-sm-4 col-md-4 col-lg-4'>";
										$str = $str."&nbsp;";
									$str = $str."</div>";
								$str = $str."</div>";
							}
				    }
				    else{
				    	    $str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
				    	    	$str = $str.messagebox("Sorry, there are no news articles available at present.",false);
				    	    $str = $str."</div>";
				    } 
				    
			    $str = $str."</div>";
		    $str = $str."</div>";
	    $str = $str."</div>";
    $str = $str."</div>";
    
    return $str;
}

function comments($mmid, $title) {
	$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
		$str = $str."<div id=\"disqus_thread\"></div>";
		$str = $str."<script type=\"text/javascript\">\n";
		$str = $str."var disqus_shortname = 'ncttcatopnews';\n";
		$str = $str."var disqus_identifier = '".$mmid."';\n";
		$str = $str."var disqus_title = '".$title."';\n";
		$str = $str."(function() {\n";
			$str = $str."var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;\n";
			$str = $str."dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';\n";
			$str = $str."(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);\n";
		$str = $str."})();\n";
		$str = $str."</script>\n";
		$str = $str."<noscript>Please enable JavaScript to view the <a href=\"http://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>";
		$str = $str."<a href=\"http://disqus.com\" class=\"dsq-brlink\">comments powered by <span class=\"logo-disqus\">Disqus</span></a>";
	$str = $str."</div>";
	return $str;
}

?>

