<?php
require_once("includes/config.php");
$mtitle = "Search Results";
$keywords = clean($_GET['keywords']);
$keywords = cleanstring($keywords);

$mystr = $mystr."<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
if (strlen($keywords) > 2) {
	$mystr = $mystr."<tr><td>".searchdb("indicator", "title,summary", $keywords)."</td></tr>";
}
else {
	$mystr = $mystr."<tr><td align=\"center\"> <div class='alert alert-danger' role='alert'> The search query specified is too short. No results found. </div> </td></tr>";
}
$mystr = $mystr."</table>";

display($mystr);

function searchdb($mdb, $mfield, $marray) {
	global $dba;
	$marray = explode(" ", $marray);
	$sql="select id, $mfield from `$mdb` where statusid = 1 and ";
	$sqlany=" ( ";
	foreach (explode(",", $mfield) as $field)	{
		foreach ($marray as $i)	{
			if (strlen(trim($i)) > 2) {
				$sqlany = $sqlany."(".$field." like '%".trim($i)."%') or ";
			} 
		}
	}
	$sqlany = substr($sqlany,0,strlen($sqlany)-4);
	$sqlany = $sqlany." ) ";
	$sql = $sql.$sqlany." order by id asc;";
	$rssearch = $dba->execute($sql);
	if (!$rssearch->eof()) {
		$str = $str."<table border=\"0\" cellpadding=6 cellspacing=1 width='98%'>";
		while(!$rssearch->eof()) {
			$str = $str."<tr><td><a href=\"indicators.php?id=".$rssearch->row("id")."\"><b>".stripcslashes($rssearch->row("title"))."</b></a><br/>".substr(removehtml($rssearch->row("summary")),0,400)."<br/><br/></td></tr>";
			$rssearch->movenext();
		} 
		$str = $str."</table>";
	}
    else {
	    $str = $str."<br/><br/><br/><center>No results were found for the search query specified.</center><br/><br/>";
	} 
  return $str;
}


function cleanstring($searchwords) {
	$str = strtolower(trim($searchwords));
	$str = strip_tags($str);
	$searcharray = explode(" ", $str);
	foreach ($searcharray as $i) {
		if (strlen($i) > 2 && $i != "and" && $i != "the" && $i != "their" && $i != "will" && $i != "there" && $i != "these") {
			$cleanstuff = $cleanstuff.$i." ";
		}
	}
	$str = trim($cleanstuff);
	return $str;
} 

?>
