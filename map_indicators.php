<?php
require_once("includes/config.php");
$transitlocationid = get_default(clean($_GET['transitlocationid']), "n", 0);

if (is_numeric($transitlocationid) && $transitlocationid > 0) {
	print list_indicators($transitlocationid);
} 

function list_indicators($transitlocationid) {
	global $dba;
	$mcount = 0;
	$indicators = getmyfieldid("transitlocation", $transitlocationid, "indicator");
	$indicators = substr($indicators, 1, strlen($$indicators) - 1);
	if (strlen(trim($indicators)) > 0) {
		$sql = "select id, title from `indicator` where statusid = 1 and id in ($indicators) order by id asc;";
		$rs = $dba->execute($sql);
		if(!$rs->eof()) {
			$str = $str."<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";
			$str = $str."<tr align=left valign=top><td width=\"200\">";
				$str = $str."<img src=\"images/margin.gif\" height=1 width=200>";
				$str = $str."<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" class=\"box\">";
				$str = $str."<tr align=left valign=top><td><b>".translate("Available Indicators")."&nbsp;-&nbsp;".getmyfield("transitlocation", $transitlocationid)."</b><hr noshade color=#e0e0e0 size=1></td></tr>";
				$str = $str."<tr align=left valign=top><td><ol style=\"padding-left:15px;margin:0px;\">";
				while(!$rs->eof()) {
					$str = $str."<li style=\"margin:5px;\"><a href=\"indicators.php?id=".$rs->row("id")."\">".titlecase(translate($rs->row("title")))."</a></li>";
					$rs->movenext();
					if (!$rs->eof()) {
						$str = $str."<hr size=1 noshade color=#f0f0f0>";
					}
					$mcount++;
				}
				$str = $str."<ol></td></tr>";
				$str = $str."</table>";
			$str = $str."</td></tr>";
			$str = $str."</table>";
		}
		else {
			$str = messagebox("There are no Indicators for ".getmyfield("transitlocation", $transitlocationid).".", false);
		}
	}
	else {
		$str = messagebox("There are no Indicators for ".getmyfield("transitlocation", $transitlocationid).".", false);
	}
	return $str;
}

?>
