<?php
require_once("includes/config.php");

$id = clean(strtolower($_GET['id']));


if (!is_numeric($id)) {
	$mystr = $mystr."<div class='tab-content'>";
		$mystr = $mystr."<div class='row panel4 clearfix'>";
			$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12  nopadding main-inner-page-padding'>";
				$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12  article'>";
					$mystr = $mystr.messagebox("Sorry, the requested page is not available", false);
				$mystr = $mystr."</div>";
			$mystr = $mystr."</div>";
		$mystr = $mystr."</div>";
	$mystr = $mystr."</div>";
}
else {
	$mystr = $mystr."<div class='tab-content'>";
		$mystr = $mystr."<div class='row panel4 clearfix'>";
			$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12  nopadding main-inner-page-padding'>";
				$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12  article'>";	
					$page     = getpage($id);
					$mtitle   = $page['title'];
					$keywords = $page['keywords'];
					$summary  = $page['summary'];
					$content  = $page['content'];
					
					$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12'>";
						$mystr = $mystr."<h3>".$mtitle."</h3>";
					$mystr = $mystr."</div>";
					
					$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12'>";
						$mystr = $mystr;
						$mystr = $mystr.$content;
					$mystr = $mystr."</div>";
					
				$mystr = $mystr."</div>";
			$mystr = $mystr."</div>";
		$mystr = $mystr."</div>";
	$mystr = $mystr."</div>";	
} 
display($mystr);




function getpage($id) {
	global $dba;
	$sql = "select id, title, content from `page` where id=".$id.";";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<div class='col-sm-8 col-md-8 col-lg-8'>";
			if (strlen(trim($rs->row(2))) > 0) {
				$content = $content.translate($rs->row(2));
				$content = str_replace("../", "", $content);
			}
			$str = $str.$content;
		$str = $str."</div>";
			
			if (checksub($rs->row("id"))) {
				$str = $str.viewsubmenu($rs->row("id"));
			}
			
			$page[0] = $rs->row("id");
			$page['id'] = $rs->row("id");
			$page['title'] = removehtml($rs->row("title"));
			$page['keywords'] =extractkeywords($str);
			$page['content'] = $str;
	}
	else {
		$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
			$str = $str.messagebox("Sorry, the requested page is not available", false);
			$page['content'] = $str;
			$page['title'] = removehtml($str);
			$page['keywords'] = extractkeywords($str);
			$page['summary'] = removehtml($str);
		$str = $str."</div>";
	}
	return $page;
} 

function viewsubmenu($id) {
	global $dba;
	$parent = getmyfieldid("page", $id, "pageid");
	if (trim($parent) == "0") {
		$parent = $id;
	}
	$sql = "select id, title, url from `page` where pageid = $parent and statusid = 1 order by positionid, id;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<div class='col-sm-4 col-md-4 col-lg-4'>";
			$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
				$str = $str."<h4>".titlecase(translate("Related Information"))."</h4><hr>";
				$str = $str."<div> <i class='fa fa-hand-o-right'></i> <a href=\"page.php?id=$parent\">".titlecase(translate("Overview"))."</a></div>";
				while (!$rs->eof()) {
					if (strlen(trim($rs->row("url"))) > 0) {
						$url = $rs->row("url");
					}
					else {
						$url = "page.php?id=".$rs->row("id");
					}
					$str = $str."<div> <i class='fa fa-hand-o-right'></i> <a href=\"".$url."\">".titlecase(translate($rs->row("title")))."</a></div>";
					$rs->movenext();
				}
			$str = $str."</div>";
		$str = $str."</div>";
	}	
	return $str;
}

function checksub($id) {
	global $dba;
	$parent = getmyfieldid("page", $id, "pageid");
	if (trim($parent) == "0") {
		$parent = $id;
	}
	$sql = "select id, title, url from `page` where pageid = $parent and statusid = 1 and (menuid = 1 or menuid = 3) order by positionid, id;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		return true;
	}
}

?>
