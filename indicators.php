<?php
require_once("includes/config.php");
require_once("includes/analyticstracking.php");

$mpage = "indicators";

$id = get_default(clean($_GET['id']), "n", 0);
$indicatorgroupid = get_default(clean($_GET['indicatorgroupid']), "n", "1");
$indicatortypeid = get_default(clean($_GET['indicatortypeid']), "n", "1");
$indicatorviewid = get_default(clean($_GET['indicatorviewid']), "n", "2012042709383376241");
$p = get_default(clean($_GET['p']), "n", 1);
$export = clean($_GET['export']);

switch(strtolower($export)) {
	case "pdf":
	case "xls":
	case "xml":
	case "json":
		$export = $export;
	break;
	default:
		$export = "";
	break;
}

//$_SESSION["referrer"] = "indicators.php";
$_SESSION["referrer"] = $_SERVER[REQUEST_URI];

$mystr = $mystr."<div class='row'>";
$mystr = $mystr.indicators($indicatorviewid);
$mystr = $mystr."</div>";

display($mystr);

function indicators($indicatorviewid) {
	global $dba;
	$sql = "select id, title, field from `indicatorview` where statusid = 1 order by id asc;";
	$rs = $dba->execute($sql);
	if(!$rs->eof()) {
			while(!$rs->eof()) {
				if (trim($rs->row("id")) == trim($indicatorviewid)) {
					$field = $rs->row("field");
					$fieldid = get_first($field);
					foreach ($_GET as $i=>$j) {
						if (strtolower(trim($i)) == trim($rs->row("field"))) {
							$fieldid = $_GET[$i];
							break;		
						}
					}
				}
				$rs->movenext();
			}
		if (trim($indicatorviewid) != "2012050810463899807") {
			$str = $str."<div class='tab-header'>".show_tabs($indicatorviewid, $field, $fieldid)."</div>";
			
			$str = $str."<div class='tab-content'>".list_indicators($indicatorviewid, $field, $fieldid)."</div>";
		}
	}
	return $str;
}

function show_tabs($indicatorviewid, $field, $fieldid) {
	global $dba;
	if (strlen($field) > 0 && strlen($fieldid) > 0 &&  is_numeric($fieldid)) {
		$sql = "select distinct $field from `indicator` where statusid = 1 and $field is not null order by $field asc;";
		$rs = $dba->execute($sql);
		if(!$rs->eof()) {
			$str = $str."<ul class='nav nav-tabs' role='tablist'>";
			while(!$rs->eof()) {
				foreach ($_GET as $i=>$j) {
					if (strtolower(trim($i)) == $field) {
						$fieldid = $_GET[$i];
						break;		
					}
				}
				if ($rs->row(0) == $fieldid) {
					$str = $str."<li class='active'> <a href='javascript:void(0);'>".titlecase(translate(getmyfield(substr($field, 0, strlen($field) - 2), $rs->row(0))))."</a></li>";
				}
				else {
					$str = $str."<li><a href=\"indicators.php?indicatorviewid=$indicatorviewid&$field=".$rs->row(0)."\">".titlecase(translate(getmyfield(substr($field, 0, strlen($field) - 2), $rs->row(0))))."</a></li>";
				}
				$rs->movenext();
			}
			$str = $str."</ul>";
		}
	}
	return $str;
}

function list_indicators($indicatorviewid, $field, $fieldid) {
	global $dba, $id;
	if (strlen($field) > 0 && strlen($fieldid) > 0 && is_numeric($fieldid)) {
		$mcount = 0;
		$sql = "select id, title, statusid from `indicator` where statusid != 2 and $field = $fieldid order by statusid desc, id asc;";
		$rs = $dba->execute($sql);
		if(!$rs->eof()) {
			$str = $str."<div class='tab-pane fade in active panels' id='vc'>";
				$str = $str."<div class='panel-container'>";
					$str = $str."<div class='row'>";
						$str = $str."<div id='equalheight'>";
							$str = $str."<div id='main-panel' class='col-sm-4 col-md-4 col-lg-4 main-panel-indicators nopadding equal'>";
								$str = $str."<div class='indicators-title'> <h3>".titlecase(translate("Available Indicators : ".getmyfield(substr($field, 0, strlen($field) - 2), $fieldid)))."</h3> </div>";
								while(!$rs->eof()) {
									if ($mcount == 0 && $id == 0) {
										$id = $rs->row("id");
									}
									if ($rs->row("id") == $id) {
										$str = $str."<div class='arrow active-indicator'>".titlecase(translate($rs->row("title")))."</div>";
									}
									else {
										if ($rs->row("statusid") != 0) {
											$str = $str."<a href=\"indicators.php?indicatorviewid=$indicatorviewid&$field=$fieldid&id=".$rs->row("id")."\"> <div class='arrow'>".titlecase(translate($rs->row("title")))."</div> </a>";
										}
										else {
											$str = $str."<div class='arrow'>".titlecase(translate($rs->row("title")))."</div>";
										}
										
									}
									$rs->movenext();
									$mcount++;
								}
							
							$str = $str."</div>";
							
							$str = $str."<div id='main-panel' class='col-sm-8 col-md-8 col-lg-8 indicator-data nopadding equal'>";
							
							$str = $str."<!--data export--><div class='export-data col-sm-12 col-md-12 col-lg-12' style='background: #f1f1f1;'>";
							
							$str = $str."</div><!--/data export-->";
							$str = $str."<div class='plot-data col-sm-12 col-md-12 col-lg-12' style='background: #f1f1f1;'>".show_indicatorinfo($id, $indicatorviewid, $field, $fieldid)."</div>";
							$str = $str."</div>";
							
						$str = $str."</div>";
					$str = $str."</div>";
				$str = $str."</div>";
			$str = $str."</div>";
		}
		else {
			$str = messagebox("There are no Indicators at the moment.", false);
		}
	}
	else {
		$str = messagebox("The selected view was not found.", false);
	}
	return $str;
}

function show_indicatorinfo($cid, $indicatorviewid, $tfield, $tfieldid) {
	global $dba, $indicatortypeid, $export;
	$x = $y = $xt = $yt = $tt = "";
	$sql = "select id, title, summary, indicatorgroupid, disaggregationid, indicatorsourceid, provider from `indicator` where statusid = 1 and id = $cid limit 1;";
	$rs = $dba->execute($sql);
	//echo "<pre>";print_r($rs);echo "</pre>";
	if(!$rs->eof()) {
		$str = $str."<div class='indicator-info'>";
		
		if ($indicatortypeid == 1) {
			$str = $str.show_indicator($rs->row("id"), $indicatortypeid, $tfield, $tfieldid,$rs->row("title"),$rs->row("summary"),$rs->row("indicatorsourceid"),$rs->row("provider"));
		}
		else {	
			//if (loggedin()) {
				$str = $str.show_indicator($rs->row("id"), $indicatortypeid, $tfield, $tfieldid);
			/*}
			else {
				$str = $str."<div>".messagebox("<br><a href=\"login.php\">Login</a> is required to access this view.", false)."</div>";
			}*/
		}
		
		/*$str = $str."<div class=\"box-white\"><font size=\"+0\" color=\"#336699\"><b>".titlecase(translate($rs->row("title")))."</b></font><hr size=\"1\" noshade color=#f6f6f6>".translate($rs->row("summary"));
		$str = $str."<br/><b>Data Source:</b>&nbsp;".getmyfield("indicatorsource", $rs->row("indicatorsourceid")); 
		if (strlen(trim($rs->row("provider"))) > 0) {
			$str = $str."<br/><b>Data Provider:</b>&nbsp;".$rs->row("provider"); 
		}
		$str = $str."<br/></div>";*/
		
		/*if (strtolower(trim($export)) == "") {
			$str = $str."<tr align=\"left\" valign=\"top\"><td><br>".get_indicatorview($rs->row("id"), $tfield, $tfieldid)."</td></tr>";
		}*/
		/*if ($indicatortypeid == 1) {
			$str = $str.show_indicator($rs->row("id"), $indicatortypeid, $tfield, $tfieldid);
		}
		else {	
			if (loggedin()) {
				$str = $str.show_indicator($rs->row("id"), $indicatortypeid, $tfield, $tfieldid);
			}
			else {
				$str = $str."<div>".messagebox("<br><a href=\"login.php\">Login</a> is required to access this view.", false)."</div>";
			}
		}*/
		$str = $str."</div>";
		
		if (strtolower(trim($export)) == "pdf") {
			print_pdf($str, $rs->row("title"));
		}
		if (strtolower(trim($export)) == "xls") {
			export_file($rs->row("title").".xls", "application/vnd.ms-excel", $str);
		}
	}
	return $str;
}

function show_indicator($cid, $indicatortypeid, $tfield, $tfieldid, $title, $summary, $indicatorsourceid, $provider) {
	global $dba, $p, $application, $export, $indicatorviewid;
	$x = $y = $xt = $yt = $tt = "";
	$sql = "select id, indicatorid, db, `fields`, singlefilterfield, multifilterfield, filter, groupby, orderby, `limit`, graphtypeid, plotx, ploty from `indicatorsubtype` where statusid = 1 and indicatorid = $cid and indicatortypeid = $indicatortypeid limit 1;";
	$rs = $dba->execute($sql);
	
	if(!$rs->eof()) {
		$t = $rs->row("db");
		$fields = $rs->row("fields");
		$sfilters = $rs->row("singlefilterfield");
		$mfilters = $rs->row("multifilterfield");
		$filters = $sfilters.",".$mfilters;
		$filterby = $rs->row("filter");
		$groupby = $rs->row("groupby");
		$orderby = $rs->row("orderby");
		$limit = 0 + $rs->row("limit");
		$graphtype = $rs->row("graphtypeid");
		$plotx = $rs->row("plotx");
		$ploty = $rs->row("ploty");
		if ($limit == 0) {
			$limit = 20;
		}		
		$page = ($p - 1) * $limit;
		
		
		$str = $str."<div class='plot-data' style='background: #f1f1f1;'>";
		if (strtolower(trim($export)) != "pdf" && strtolower(trim($export)) != "xls") {
			//if (loggedin()) {
				$str = $str."<div class='O-filters'>".show_filters($t, $sfilters, $mfilters, $filterby, $cid, $indicatorviewid, $tfield, $tfieldid)."</div>";
			//}
		}
		
		$str = $str."<div class='well'>";
			$str = $str."<h4>".titlecase(translate($title))."</h4><hr/>";
			$str = $str."<div> <b>Summary</b> <br>".translate($summary)."</div>";
			$str = $str."<br><div><b>Data Source:</b>&nbsp;".getmyfield("indicatorsource", $indicatorsourceid)."</div>"; 
			if (strlen(trim($provider)) > 0) {
				$str = $str."<div><b>Data Provider:</b>&nbsp;".$provider."</div>"; 
			}
		$str = $str."</div>";
		
		if (strlen(trim($t)) > 0 && strlen(trim($fields)) > 0) {
			if (strtolower(trim($t)) == "bordercrossing") {
				$nn = 0;
				foreach ($_GET as $i=>$j) {
					if(strtolower(trim($i)) == "nodeid") {
						$nn = 1;
					}
				}
				if ($nn == 0) {
					$filterby = $filterby." nodeid = ".get_firstfield($t, "nodeid", $filterby);
					//echo "Field = ".get_firstfield($t, "nodeid", $filterby);
				}
			}
			$isql = "select $fields from `$t`";
			if (strlen(trim($filterby)) > 0) {
				$isql = $isql." where $filterby";
			}
			if (strlen(trim($filters)) > 0) {
				$filters = explode(",", $filters);
				foreach ($_GET as $i=>$j) {
					$ft = "";
					if (in_array(trim($i), $filters) || trim($i) == "reportingperiodid") {
						$a = $_GET[$i];
						if (!is_array($a)) {
							$a = explode("||||<|", $a);
						}
						foreach ($a as $j) {
							$j = clean($j);
							//echo $j;
							if (strlen(trim($j)) > 0) {
								if (strtolower(trim($i)) == "reportingperiodid") {
									if ($indicatortypeid == 1) {
										if ($j > 0 && $j <= 4) {
											$ft = $ft."`".$t."`.`monthid` in (".get_months($j).") or ";
										}
									}
									else {
										$ft = $ft."`".$t."`.`".strtolower(trim($i))."`='".$j."' or ";
									}
								}
								else {
									$ft = $ft."`".$t."`.`".strtolower(trim($i))."`='".$j."' or ";
								}
							}
						}		
					}
					if (strlen(trim($ft)) > 0) {
						$fsql = $fsql." (".substr($ft, 0, strlen($ft) - 4).") and ";
					}
				}
				if (strlen(trim($fsql)) > 0) {
					$fsql = substr($fsql, 0, strlen($fsql) - 4);
					if (strlen(trim($filterby)) > 0) {
						$isql = $isql." and $fsql";
					}
					else {
						$isql = $isql." where $fsql";
					}
				}
			}
			if (strlen(trim($groupby)) > 0) {
				$isql = $isql." group by $groupby";
			}
			if (strlen(trim($orderby)) > 0) {
				$isql = $isql." order by $orderby";
			}
			$gsql = $isql.";";
			if (strlen(trim($export)) > 0) {
				$isql = $gsql;
			}
			else {
				//$isql = $isql." limit $page, $limit;";
			}
			//echo $gsql;
			$rsi = $dba->execute($isql);
			$rsg = $dba->execute($gsql);
			$pageitems = $rsg->recordcount();
			$fcg = $rsg->fieldcount();
			
			//Export
			if (strtolower(trim($export)) != "pdf" && strtolower(trim($export)) != "xls") {
				if (strlen($istr) > 0) {
					$str = $str."".$istr;
				}
				/*f(user("accounttypeid") > 1) {*/
					if (strlen(trim($_SERVER['QUERY_STRING'])) > 0) {
						$murl = $_SERVER['REQUEST_URI']."&";
					}
					else {
						$murl = $_SERVER['REQUEST_URI']."?";
					}
					
					$str = $str."<div class='row'>";
						$str = $str."<div class='col-md-12'>";
						
							$str = $str."<div class='col-md-3'>";
							$str = $str."Change chart type
							<select id='redrawType' class='form-control' onchange='redrawType()' style='width: 100px;'>
								<option value='ColumnChart'>ColumnChart</option>	
								<option value='BarChart'>BarChart</option>
								<option value='PieChart'>PieChart</option>
								<option value='LineChart'>LineChart</option>
								<option value='AreaChart'>AreaChart</option>
							</select>";
							$str = $str."</div>";
							
							$str = $str."<div class='col-md-9'> <div class='pull-right'> Export: <a href=\"".$murl."export=pdf\" type='button' class='btn btn-primary btn-sm'> <i class='fa fa-file-pdf-o'> &nbsp; PDF</i> </a> <a href=\"".$murl."export=xls\" type='button' class='btn btn-success btn-sm'> <i class='fa fa-file-excel-o'> &nbsp; Excel</i> </a> <a href=\"".$murl."&export=xml\" type='button' class='btn btn-info btn-sm'> <i class='fa fa-file-text-o'> &nbsp; XML</i> </a> <a href=\"".$murl."export=json\" type='button' class='btn btn-warning btn-sm'> <i class='fa fa-file-code-o'> &nbsp; JSON</i> </a> <button type='button' class='btn btn-default btn-sm' data-toggle='modal' data-target='#embed-graph'> <i class='fa fa-code'> &nbsp; Embed Graph</i> </button> </div> </div>"; 
						$str = $str."</div>";
					$str = $str."</div> <br>";
				/*}*/
			}
			//Export
			
			if (!$rsi->eof()) {
				$str = $str."<div class='multi-data-display'>";
				if ($graphtype > 0 && strlen(trim($plotx)) > 0 && strlen(trim($ploty)) > 0) {
					$title = getmyfield("indicator", $rs->row("indicatorid"));
					if ($graphtype == 4) {
						$years = array();
						$months = array();
						$quarters = array();
						while (!$rsg->eof()) {
							for ($i = 0; $i < $fcg; $i++) {
								if (substr($rsg->fields[$i], (strlen($rsg->fields[$i])-2), 2) == "id" && strlen($rsg->fields[$i]) > 2) {
									$json[$rsg->fields[$i]][] = getmyfield(substr($rsg->fields[$i], 0, strlen($rsg->fields[$i])-2), $rsg->row($rsg->fields[$i]));
								}
								else {
									$json[$rsg->fields[$i]][] = $rsg->row($rsg->fields[$i]);
								}
							}
							array_push($years, $rsg->row("yearid"));
							if ($indicatortypeid == 1) {
								array_push($months, $rsg->row("monthid"));
							}
							if ($indicatortypeid == 2) {
								array_push($quarters, $rsg->row("reportingperiod"));
							}
							$rsg->movenext();
						}
						$years = array_unique($years);
						$months = array_unique($months);
						$quarters = array_unique($quarters);
						if (strtolower(trim($export)) == "json") {
							export_file($title.".json", "application/json", json_encode($json));
						}

						if (strtolower(trim($export)) == "xml") {
							export_file($title.".xml", "text/xml", arraytoxml($json));
						}
						$str = $str."<div class='graph'>".show_graph($title, $graphtype, $years, $months, $quarters, $gsql, $plotx, $ploty)."</div>";
					}
					else {
						$ploty = explode(",", $ploty);
						$xt = "Period";
						$yt = "Weight/Volume";
						$y = array();
						$json = array();
						while (!$rsg->eof()) {
							for ($i = 0; $i < $fcg; $i++) {
								if (substr($rsg->fields[$i], (strlen($rsg->fields[$i])-2), 2) == "id" && strlen($rsg->fields[$i]) > 2) {
									$json[$rsg->fields[$i]][] = getmyfield(substr($rsg->fields[$i], 0, strlen($rsg->fields[$i])-2), $rsg->row($rsg->fields[$i]));
								}
								else {
									$json[$rsg->fields[$i]][] = $rsg->row($rsg->fields[$i]);
								}
							}
							if (substr(strtolower($plotx), (strlen($plotx)-2), 2) == "id" && strlen($plotx) > 2) {
								if (strtolower(trim($plotx)) == "monthid") {
									$x[] = substr(getmyfield(substr($plotx, 0, strlen($plotx) - 2), $rsg->row($plotx)), 0, 3);
								}
								else {
									$x[] = getmyfield(substr($plotx, 0, strlen($plotx) - 2), $rsg->row($plotx));
								}
							}
							else {
								$x[] = $rsg->row($plotx);
							}
							foreach($ploty as $pt) {
								$y[$pt] = $y[$pt].$rsg->row($pt).",";
							}
							$rsg->movenext();
						}
						$x = implode("|", $x);
						$y = json_encode($y);
						//echo "Hi -> <br>";print_r($x);echo "<br>".$y;
						$width  = "750";
						$height = "320";
						//$graph_src = get_script_base()."/jgraph.php?x=".urlencode($x)."&y=".urlencode($y)."&xt=".urlencode($xt)."&yt=".urlencode($yt)."&t=".urlencode($title)."&c=".urlencode(getmyfieldid("graphtype", $graphtype, "code"))."&w=".urlencode($width)."&h=".urlencode($height);
						//echo get_script_base()."/jgraph.php?x=".urlencode($x)."&y=".urlencode($y)."&type=1";
						$graph_src = file_get_contents(get_script_base()."/jgraph.php?x=".urlencode($x)."&y=".urlencode($y)."&type=1");
						//$graph_src = "http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]."jgraph.php?x=".urlencode($x)."&y=".urlencode($y)."&xt=".urlencode($xt)."&yt=".urlencode($yt)."&t=".urlencode($title)."&c=".urlencode(getmyfield("graphtype", $graphtype))."&w=".urlencode($width)."&h=".urlencode($height);
						
						
						$str = $str."<div class='inner-graph'>";
						if (strlen(trim($export)) == 0) {
							//Prepare
							$str = $str."<div id='dual_x_div' style='min-height: 500px;'></div>";
							
							$str = $str."
							<script type=\"text/javascript\" src=\"http://www.google.com/jsapi\"></script>
							<script type=\"text/javascript\">
							google.load('visualization', '1',null); // No 'packages' section.
							
							google.setOnLoadCallback(drawVisualization);
							
							function drawVisualization() {
							
							// Draw a column chart
							wrapper = new google.visualization.ChartWrapper();
							wrapper.setDataTable(".$graph_src.");
							wrapper.setChartType('ColumnChart');
							wrapper.setContainerId('dual_x_div');
							wrapper.setOption('legend','bottom');
							
							wrapper.draw();
							}
							</script>
							
							<script type='text/javascript'>
							function redrawType()
							{
							chartType = document.getElementById('redrawType').value;
							wrapper.setChartType(chartType);
							wrapper.draw();
							}
							</script>
							
							";
							
							//embed dialog
							$str = $str."<div class='modal fade' id='embed-graph' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>";
							$str = $str."<div class='modal-dialog'>";
							$str = $str."<div class='modal-content'>";
							$str = $str."<div class='modal-header'>";
							$str = $str."<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
							$str = $str."<h4 class='modal-title' id='myModalLabel'>Embed code</h4>";
							$str = $str."</div>";
							$str = $str."<div class='modal-body'>";
							$str = $str."<textarea name=\"text\" cols=\"50\" rows=\"5\"><b>$title</b><br/><iframe width=\"750\" height=\"320\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"".shorten_url("http://".$_SERVER["SERVER_NAME"]."/ncttca/".$graph_src)."\"></iframe><br/><small><a href=\"".shorten_url("http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"])."\" target=\"_blank\" style=\"color:#0000FF;text-align:left\">View Dataset</a></small></textarea>";
							$str = $str."</div>";
							$str = $str."<div class='modal-footer'>";
							$str = $str."<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>";
							$str = $str."</div>";
							$str = $str."</div>";
							$str = $str."</div>";
							$str = $str."</div>";
							//embed dialog
							
						}
						$str = $str."</div>";
						
						
						/*if (strlen(trim($export)) != 0) {
						
							$graph_image = file_get_contents($graph_src);
						}*/
						if (strtolower(trim($export)) == "json") {
							export_file($title.".json", "application/json", json_encode($json));
						}

						if (strtolower(trim($export)) == "xml") {
							export_file($title.".xml", "text/xml", arraytoxml($json));
						}
						$str = $str.$graph_image;
					}
				}
				//if (loggedin()) {
						$fc = $rsi->fieldcount();
						
							$str = $str."<table data-toggle='table' data-search='true' data-pagination='true' data-show-columns='true' data-query-params='queryParams' data-pagination='true' >";
								$str = $str."<thead>";
									$str = $str."<tr>";
									for ($i = 0; $i < $fc; $i++) {
										if (substr(strtolower($rsi->fields[$i]), strlen($rsi->fields[$i])-2, 2) == "id") {
											$f = substr($rsi->fields[$i], 0, strlen($rsi->fields[$i])-2);
										}
										else {
											$f = $rsi->fields[$i];
										}
										$str = $str."<th height=40 align='center'>".putspace(strtoupper(hashtable($f)))."</th>";
									}
									$str = $str."</tr>";
								$str = $str."</thead>";
								
								$mcount = 0;
								$str = $str."<tbody>";
									while (!$rsi->eof()) {
										$mtemp = "";
										$str = $str."<tr valign=\"top\" class=\"rownormal\" bgcolor=\"#ffffff\">";
										for ($i = 0; $i < $fc; $i++) {
											if (substr(strtolower($rsi->fields[$i]), (strlen($rsi->fields[$i])-2), 2) == "id" && strlen($rsi->fields[$i]) > 2) {
												$mtemp = getmyfield(substr($rsi->fields[$i], 0, (strlen($rsi->fields[$i])-2)), $rsi->row($i));
											}
											else {
												if (strtolower(substr($rsi->fields[$i], strlen($rsi->fields[$i]) - 4, 4)) == "date") {
													$mtemp = "<div align=right>".formatmydate($rsi->row($i))."</div>";
												}
												else {
													if(is_numeric($rsi->row($i))) {
														$mtemp = number_format($rsi->row($i));
													}
													else {
														$mtemp = $rsi->row($i);
													}
												}
											}
											$mtemp = trim($mtemp);
											$mtemp = ucwords(strtolower($mtemp));
											$str = $str."<td class=\"rownormal\" align=right>".$mtemp."</td>";
											$mcount++;
										}
										$str = $str."</tr>";
										$rsi->movenext();
									}
								$str = $str."</tbody>";
							$str = $str."</table>";
						
						$str = $str."
						<script type='text/javascript'>
						function queryParams() {
							return {
								per_page: 100,
								page: 1
							};
						}
						</script>";
					
						/*$pages = round($pageitems / $limit);
						if (($pages * $limit) < $pageitems) {
							$pages++;
						}
						if ($pages > 0) {
							$g = "";
							foreach ($_GET as $i=>$j) {
								if (strtolower(trim($i)) != "id" && strtolower(trim($i)) != "p") {
									$a = $j;
									if (is_array($a)) {
										if (sizeof($a) > 0) {
											$a = implode(",", $a);
											$g = $g.$i."=".$a."&";
										}
									}
									else {
										$a = trim($a);
										if (strlen(trim($a)) > 0) {
											$g = $g.$i."=".$a."&";
										}
									}
								}
							}			
							$istr = $istr."<!--pagination--><div class='row'>";
								$istr = $istr."<div class='col-sm-12'>";
									$istr = $istr."<div class='col-sm-6'>Viewing&nbsp;Page&nbsp;$p&nbsp;of&nbsp;$pages&nbsp;&nbsp;&nbsp;</div>";
									
									$istr = $istr."<div class='col-sm-6'>";
										$istr = $istr."<ul class='pagination pull-right'>";
											$istr = $istr."<li class='paginate_button previous' aria-controls='editable' tabindex='0' id='editable_previous'><a href=\"".get_script_name()."?id=$cid&indicatorgroupid=$indicatorgroupid&p=1&$g\">First</a></li>";
											$s = $p - 5;
											if ($s < 1) {
												$s = 1;
											}
											$e = $s + 9;
											for ($i = $s; $i <= $pages; $i++) {
												if ($p == $i) {
													$istr = $istr."<li class='paginate_button active disabled' aria-controls='editable' tabindex='0'><a href='#'>$i</a></li>";
												}
												else {
													$istr = $istr."<li class='paginate_button active' aria-controls='editable' tabindex='0'><a href=\"".get_script_name()."?id=$cid&indicatorgroupid=$indicatorgroupid&p=$i&$g\">$i</a></li>";
												}
												if ($i == $e) {
													break;
												}
											}
											$istr = $istr."<li class='paginate_button next' aria-controls='editable' tabindex='0'><a href=\"".get_script_name()."?id=$cid&indicatorgroupid=$indicatorgroupid&p=$pages&$g\">Last</a></li>";
											
										$istr = $istr."</ul>";
									$istr = $istr."</div>";
								$istr = $istr."</div>";
							$istr = $istr."</div><!--/pagination-->";
						}
						
						if (strlen($istr) > 0) {
								$str = $str."".$istr;
							}*/
						
				/*}
				else {
					$str = $str.messagebox("<br><a href=\"login.php\">Login</a> is required to view datasets used to generate this indicator.", false);
				}*/
				$str = $str."</div>";	
			}
			else {
				$str = $str."<div class='alert alert-warning' role='alert'>".messagebox("No records found that matches your selection, <a href=\"".get_script_name()."?id=$cid&indicatorviewid=$indicatorviewid&$tfield=$tfieldid\">click here to reset</a>.</div>", false);
			}
		}
		else {
			$str = messagebox("Requested Indicator was not found.", false);
		}
		$str = $str."</div>";		
	}
	else {
		$str = messagebox("Requested indicator was not found.", false);
	}
	return $str;
}

function get_indicatorview($cid, $tfield, $tfieldid) {
	global $dba, $indicatortypeid, $indicatorviewid;
	$sql = "select distinct indicatortypeid from `indicatorsubtype` where indicatorid = $cid and statusid = 1 order by indicatortypeid limit 3;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";
		$str = $str."<tr align=\"left\" valing=\"top\"><td>";
			$str = $str."<table border=\"0\" cellspacing=\"0\" cellpadding=\"4\">";
			$str = $str."<tr align='left'>";
			while(!$rs->eof()) {
				if (trim($rs->row(0)) == trim($indicatortypeid)) {
					$str = $str."<td height=\"24\" bgcolor=\"#0380f4\">&nbsp;&nbsp;<b><font color=#ffffff>".titlecase(translate(getmyfield("indicatortype", $rs->row(0))))."</font></b>&nbsp;&nbsp;</td>";
				}
				else {
					$str = $str."<td height=\"24\" bgcolor=\"#f6f6f6\">&nbsp;&nbsp;<a href=\"indicators.php?id=$cid&indicatorviewid=$indicatorviewid&$tfield=$tfieldid&indicatortypeid=".$rs->row(0)."\"><font color=#808080><b>".titlecase(translate(getmyfield("indicatortype", $rs->row(0))))."</b></font></a>&nbsp;&nbsp;</td>";
					
				}
				$rs->movenext();
				if (!$rs->eof()) {
					$str = $str."<td bgcolor=\"#ffffff\"><img src=\"images/vmargin.gif\" width=\"2\" height=\"24\"></td>";
				}
			}
			$str = $str."</tr>";
			$str = $str."</table>";
		$str = $str."<tr><td align=\"left\" valign=\"top\" bgcolor=#0380f4><img src=\"images/margin.gif\" width=\"100%\" height=\"1\"></td></tr>";
		$str = $str."</td></tr>";
		$str = $str."</table>";
	}
	return $str;
}

function show_map() {
	$str = $str."<table border=\"0\" cellspacing=\"10\" cellpadding=\"0\" width=\"100%\">";
	$str = $str."<tr valign=\"top\" align=\"left\"><td><div id=\"mapcanvas\" style=\"width:680px; height:500px;\"></div></td>";
	$str = $str."<td style=\"width:250px;\">".get_transitpoints()."<br/><br/><div id=\"mapdata\">Select Transit Point to view markers on map, then click on the marker to view available indicators.</div></td></tr>";
	$str = $str."</table>";
	return $str;
}

function get_transitpoints() {
	global $dba;
	$sql = "select id, title, image from `transitpoint` where statusid = 1 and image <> '' order by id asc;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\" class=\"box-white\">";
		$str = $str."<tr valign=\"top\" align=\"left\"><td><h3>Transit Points</h3></td></tr>";
		$str = $str."<tr valign=\"top\" align=\"left\"><td><hr noshade color=\"#e0e0e0\" size=\"1\"/></td></tr>";
		$str = $str."<tr valign=\"top\" align=\"left\"><td>";
			$str = $str."<table border=\"0\" cellspacing=\"5\" cellpadding=\"0\" width=\"100%\">";
			while(!$rs->eof()) {
				$str = $str."<tr valign=\"middle\" align=\"left\"><td><input type=\"checkbox\" name=\"transitpoint\" id=\"transitpoint\" value=\"".$rs->row("id")."\"></td><td>".translate($rs->row("title"))."</td><td><img src=\"images/".$rs->row("image")."\"></td></tr>";
				$rs->movenext();
			}
			$str = $str."</table>";
		$str = $str."</td></tr>";
		$str = $str."</table>";
	}
	return $str;
}

function show_graph($title, $graphtype, $years, $months, $quarters, $sql, $plotx, $ploty) {
	global $dba, $export, $indicatortypeid;
	$data = array();
	$plotv = explode(",", $ploty);
	$i = 0;
	if(is_array($years)) {
		foreach($years as $year) {
			if (strpos($sql, "where") != false) {
				$gsql = str_replace("where", "where yearid = $year and ", $sql);
			}
			else {
				$gsql = str_replace("group", "where yearid = $year group ", $sql);
			}
			//echo $gsql;
			$rsy = $dba->execute($gsql);
			if (!$rsy->eof()) {
				foreach($plotv as $y) {
					switch ($indicatortypeid) {
						case 1:
							$data[$y][$year] = array_fill(0, 12, 0);
						break;
						case 2:
							$data[$y][$year] = array_fill(0, 4, 0);
						break;
						case 3:
							$data[$y][$year] = array_fill(0, count($years), 0);
						break;
						default:
						break;
					}
				}
				while (!$rsy->eof()) {
					foreach($plotv as $y) {
						if ($indicatortypeid != 3) {
							$data[$y][$year][$rsy->row($plotx) - 1] = $rsy->row($y);
						}
						else {
							$data[$y][$year][$i] = $rsy->row($y);
						}
						$x[] = $rsy->row($plotx);
					}
					$rsy->movenext();
					$i++;
				}
			}
		}
		//print_r($data);
		switch ($indicatortypeid) {
			case 1:
				$x = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
			break;
			case 2:
				$x = array("Q1", "Q2", "Q3", "Q4");
			break;
			case 3:
				$x = $years;
			break;
			default:
				$x = array_unique($x);
				sort($x);
				for($i = 0; $i < count($x); $i++) {
					$xx[] = getmyfield(substr($plotx, 0, strlen($plotx) - 2), $x[$i]);
				}
				$x = $xx;
			break;
		}
		$x = implode("|", $x);
		//print $x;
		$y = json_encode($data);
		$width  = "750";
		$height = "320";
		$graph_src = file_get_contents(get_script_base()."/jgraph.php?x=".urlencode($x)."&y=".urlencode($y)."&type=2");
		//echo get_script_base()."/jgraph.php?x=".urlencode($x)."&y=".urlencode($y)."&type=2";
		$str = $str."<div class='inner-graph'>";
		if (strlen(trim($export)) == 0) {
			//Prepare
			$str = $str."<div id='dual_x_div' style='min-height: 500px;'></div>";
			
			$str = $str."
			<script type=\"text/javascript\" src=\"http://www.google.com/jsapi\"></script>
			<script type=\"text/javascript\">
			google.load('visualization', '1',null); // No 'packages' section.
			
			google.setOnLoadCallback(drawVisualization);
			
			function drawVisualization() {
			
			// Draw a column chart
			wrapper = new google.visualization.ChartWrapper();
			wrapper.setDataTable(".$graph_src.");
			wrapper.setChartType('ColumnChart');
			wrapper.setContainerId('dual_x_div');
			wrapper.setOption('legend','bottom');
			
			wrapper.draw();
			}
			</script>
			
			<script type='text/javascript'>
			function redrawType()
			{
				chartType = document.getElementById('redrawType').value;
				wrapper.setChartType(chartType);
				wrapper.draw();
			}
			</script>
			
			";
			
			//embed dialog
			$str = $str."<div class='modal fade' id='embed-graph' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>";
			$str = $str."<div class='modal-dialog'>";
			$str = $str."<div class='modal-content'>";
			$str = $str."<div class='modal-header'>";
			$str = $str."<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
			$str = $str."<h4 class='modal-title' id='myModalLabel'>Embed code</h4>";
			$str = $str."</div>";
			$str = $str."<div class='modal-body'>";
			$str = $str."<textarea name=\"text\" cols=\"50\" rows=\"5\"><b>$title</b><br/><iframe width=\"750\" height=\"320\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"".shorten_url("http://".$_SERVER["SERVER_NAME"]."/ncttca/".$graph_src)."\"></iframe><br/><small><a href=\"".shorten_url("http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"])."\" target=\"_blank\" style=\"color:#0000FF;text-align:left\">View Dataset</a></small></textarea>";
			$str = $str."</div>";
			$str = $str."<div class='modal-footer'>";
			$str = $str."<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>";
			$str = $str."</div>";
			$str = $str."</div>";
			$str = $str."</div>";
			$str = $str."</div>";
			//embed dialog
			
		}
		/*if (strlen(trim($export)) == 0) {
			//$str = $str."<tr><td><img src=\"$graph_src\" border=\"0\"></td></tr>";
			$str = $str."<tr align=\"left\" valign=\"top\"><td><img src=\"$graph_src\" border=\"0\"></td></tr>";
		}
		else {
			$graph_image = file_get_contents($graph_src);
			$filename = "graph_".date("Ymdhis").".png";
			$save_graph = fopen("images/graph/$filename", 'w');
			fwrite($save_graph, $graph_image);
			fclose($save_graph);
			$str = $str."<tr align=\"left\" valign=\"top\"><td><img src=\"images/graph/$filename\" border=\"0\"></td></tr>";
		}*/
		$str = $str."</div>";
		
	}
	return $str;
}


function get_first($t) {
	global $dba;
	$t = substr($t, 0, strlen($t) - 2);
	$sql = "select id, title from `$t` order by id asc limit 1;";
	//echo $sql;
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $rs->row("id");
	}
	return $str;
}

function get_firstfield($t, $field, $filter) {
	global $dba;
	$sql = "select distinct $field from `$t`";
	if (strlen(trim($filter)) > 0) {
		$sql = $sql." where $filter";
	}
	$sql = $sql." order by $field asc limit 1;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $rs->row(0);
	}
	return $str;
}



function show_filters($tb, $sfields, $mfields, $filterby, $id, $indicatorviewid, $tfield, $tfieldid) {
	global $dba, $indicatortypeid;
	if (strlen(trim($sfields)) > 0 || strlen(trim($mfields)) > 0) {
		$sfields = explode(",", $sfields);
		$mfields = explode(",", $mfields);
		
		$str = $str."<div class='row'>";
		$str = $str."<div class='col-md-3'> <h5>Filter:</h5> </div>";
		$str = $str."</div>";
		
		$str = $str."<div class='row'>";
		$str = $str."<form class=\"niceform\" method=\"get\" action=\"".get_script_name()."\">";
		foreach($sfields as $fld) {
			if (strlen(trim($fld)) > 2) {
				
				if (strtolower(trim($fld)) == "monthid") {
					$str = $str."<div class='col-md-3'>";
					$str = $str."<div>".putspace("Filter By Quarter")."</div><div id='quarter'>".quarter_filter()."</div>";
					$str = $str."</div>";
				}
				
				$str = $str."<div class='col-md-3'>";
				$str = $str."<div>".putspace("Filter By ".hashtable($fld))."</div><div id='".strtolower(hashtable($fld))."'>".multi_filter($tb, $fld, $filterby, true)."</div>";
				$str = $str."</div>";
			}
		}

		foreach($mfields as $fld) {
			if (strlen(trim($fld)) > 2) {
				
				if (strtolower(trim($fld)) == "monthid") {
					$str = $str."<div class='col-md-3'>";
					$str = $str."<div>".putspace("Quarter")."</div><div id='quarter'>".quarter_filter()."</div>";
					$str = $str."</div>";
				}
				
				$str = $str."<div class='col-md-3'>";
				$str = $str."<div>".putspace(hashtable($fld))."</div><div id='".strtolower(hashtable($fld))."'>".multi_filter($tb, $fld, $filterby)."</div>";
				$str = $str."</div>";
			}
		}
		
		$str = $str."<div class='col-md-3'>";
			$str = $str."<div class='action-buttons'>";
				$str = $str."<input class='btn btn-default' type=\"submit\" value=\"Go\"> <a class='btn btn-default' role='button' href=\"".get_script_name()."?id=$id&indicatorviewid=$indicatorviewid&$tfield=$tfieldid&indicatortypeid=$indicatortypeid\">Clear</a>";
			$str = $str."</div>";
		$str = $str."</div>";
		
		$str = $str."<input type=\"hidden\" name=\"id\" value=\"".$id."\">";	
		$str = $str."<input type=\"hidden\" name=\"indicatorviewid\" value=\"".$indicatorviewid."\">";	
		$str = $str."<input type=\"hidden\" name=\"$tfield\" value=\"".$tfieldid."\">";	
		$str = $str."<input type=\"hidden\" name=\"indicatortypeid\" value=\"".$indicatortypeid."\">";
		
		$str = $str."</form>";
		$str = $str."</div> <hr/>";		
	}
	return $str;
}

function quarter_filter() {
	global $dba;
	$mcount = 0;
	$sql = "select id, title from `reportingperiod` order by id;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<select id='quarter' name='reportingperiodid[]' class='multiselect form-control quarter' name='browsers' multiple>";
		while (!$rs->eof()) {
			$checked = "";
			foreach ($_GET as $i=>$j) {
				if (strtolower(trim($i)) == "reportingperiodid") {
					$a = $_GET[$i];
					if (!is_array($a)) {
						$a = explode("||||<|", $a);
					}
					foreach ($a as $j) {
						if (trim($rs->row(0)) == trim($j)) {
							$checked = "selected";
							break;
						}
					}
					break;		
				}
			}
			
			$str = $str."<option value=\"".$rs->row(0)."\" $checked>&nbsp;".$rs->row(1)."</option>";//onclick=\"this.form.submit();\"
			$rs->movenext();
			$mcount++;
		}
		$str = $str."</select>";
	}
	
	return $str;
}



function multi_filter($t, $field, $filter, $singlefilter = false) {
	global $dba;
	$mcount = 0;
	$field = trim($field);
	$getcount = 0;
	switch(strtolower(trim($field))) {
		case "monthid":
			$limit = 12;
		break;
		case "nodeid":
		case "subnodeid":
		case "containertypeid":
		//case "triprouteid":
			$limit = 3;
		break;
		case "qualitativeid":
		case "countryid":
			$limit = 5;
		break;
		default:
			$limit = 4;
	}
	$sql = "select distinct $field from `$t`";
	if (strlen(trim($filter)) > 0) {
		$sql = $sql." where $filter";
	}
	$sql = $sql." order by $field asc;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<select id=\"".$field."\" name=\"".$field."[]\" class='multiselect form-control ".strtolower(hashtable($field))."' multiple>";
		while (!$rs->eof()) {
			$checked = "";
			foreach ($_GET as $i=>$j) {
				if (strtolower(trim($i)) == $field) {
					$a = $_GET[$i];
					if (!is_array($a)) {
						$a = explode("||||<|", $a);
					}
					foreach ($a as $j) {
						if (trim($rs->row(0)) == trim($j)) {
							$checked = "selected";
							break;
						}
					}
					break;		
				}
				if (strtolower(trim($i)) == "nodeid") {
					$getcount = 1;
				}
			}
			if ($getcount == 0) {
				if ($rs->row(0) == get_firstfield("bordercrossing", "nodeid", "")) {
					$checked = "selected";
				}
			}
			
			if (strtolower($field) == "monthid") {
				$textstr = substr(getmyfield(substr($field, 0, strlen($field) - 2), $rs->row(1)), 0, 3);
			}
			else {
				$textstr = getmyfield(substr($field, 0, strlen($field) - 2), $rs->row(1));
			}
			
			if ($singlefilter == true) {
				$str = $str."<option value=\"".$rs->row(0)."\" $checked>&nbsp;".$textstr."</option>";//onclick=\"this.form.submit();\"
			}
			else {
				$str = $str."<option value=\"".$rs->row(0)."\" $checked>&nbsp;".$textstr."</option>";//onclick=\"this.form.submit();\"
			}
			
			$rs->movenext();
			$mcount++;
			if ($mcount == $limit) {
				$mcount = 0;
			}
		}
		
		$str = $str."</select>";	
	}
	return $str;
}


function country_filter($t, $f) {
	global $dba;
	$country_fields = array("countryid", "orgcountryid", "destcountryid", "rcvcountryid");
	$sql = "select id, title from `country` order by regionid,title;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<div style=\"height:250px; overflow-x:hidden; overflow-y:scroll;\">";
		$str = $str."<table border=\"0\" cellspacing=\"3\" cellpadding=\"0\" width=\"100%\">";
		while (!$rs->eof()) {
			$checked = "";
			foreach ($_GET as $i=>$j) {
				if (in_array(trim($i), $country_fields)) {
					$a = $_GET[$i];
					if (!is_array($a)) {
						$a = explode("||||<|", $a);
					}
					foreach ($a as $j) {
						if (trim($rs->row(0)) == trim($j)) {
							$checked = "checked";
							break;
						}
					}		
				}
			}
			$str = $str."<tr align=\"left\"><td><input type=\"checkbox\" name=\"destcountryid[]\" id=\"destcountryid\" value=\"".$rs->row(0)."\" $checked onclick=\"this.form.submit();\"></td><td>".$rs->row(1)."</td></tr>";
			$rs->movenext();			
		}
		$str = $str."</table>";	
		$str = $str."</div>";	
	}
	return $str;
}

function get_months($rpid) {
	global $dba;
	if (is_numeric($rpid)) {
		$sql = "select id, title from `month` where reportingperiodid = $rpid;";
		$rs = $dba->execute($sql);
		if(!$rs->eof()) {
			while(!$rs->eof()) {
				$str = $str.$rs->row(0).",";
				$rs->movenext();
			}
			$str = substr($str, 0, strlen($str) - 1);
		}
	}
	return $str;
}

function print_pdf($document, $filename) {
	global $application;
	include("codebase/plugins/pdf/mpdf/mpdf.php");
	global $site_logo, $site_organization, $site_contact;
	//$document = strip_tags($document, "<br/>, <pagebreak>, <div>, <span>, <h1>, <h2>, <h3>");
	$document = mb_convert_encoding($document, 'UTF-8', 'HTML-ENTITIES');
	ob_end_clean();
	$mpdf = new mPDF('win-1252','A4','','',20,20,20,0,20,20); 
	$mpdf->useOnlyCoreFonts = false;
	$mpdf->setBasePath("/home/ubuntu/site/");
	//$mpdf->SetProtection(array('print', 'copy', 'modify'));
	$mpdf->SetTitle($filename);
	$mpdf->SetAuthor($application['title']);
	//$mpdf->SetWatermarkText("TTCANC");
	//$mpdf->showWatermarkText = true;
	//$mpdf->watermark_font = 'DejaVuSansCondensed';
	//$mpdf->watermarkTextAlpha = 0.1;
	$mpdf->SetDisplayMode('fullpage');
	$mpdf->SetCompression(true);
	$mpdf->shrink_tables_to_fit = 0;
	$mpdf->keep_table_proportions = true;
	$mpdf->SetHTMLFooter("&copy; 2012 Northern Corridor Transport Observatory Project. All Rights Reserved");
	$mpdf->defaultfooterfontsize = 4;
	$mpdf->defaultfooterfontstyle = 'N';
	$mpdf->defaultfooterline = 0;
	$mpdf->WriteHTML($document);
	$mpdf->Output($filename.".pdf", "D");
	exit;
}

function arraytoxml($data) {
	$rowcount = 0;
	$cols = array();
	foreach($data as $key=>$value) {
		$cols[] = $key;
		$rowcount = count($value);
	}
	$xml = $xml."<?xml version='1.0' encoding='utf-8'?>\r\n";
	$xml = $xml."<data rows='$rowcount'>\r\n";
	for($i=0; $i<$rowcount; $i++){
		$xml = $xml."<row>\r\n";
		foreach($cols as $col) {
			$xml = $xml."<$col>".$data[$col][$i]."</$col>\r\n";
		}
		$xml = $xml."</row>\r\n";
	}
	$xml = $xml."</data>";
	return $xml;
}

function export_file($filename, $mime, $data) {
	header('Pragma: public');
	header('Expires: 0');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Cache-Control: private',false);
	header('Content-Type: '.$mime);
	header('Content-Disposition: attachment; filename='.$filename);
	header('Content-Transfer-Encoding: binary');
	print_r($data);
	exit;
}

function get_graph($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, 0);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}

function get_script_base() {
	//$a = "http://".$_SERVER["SERVER_NAME"];
	$a = "http://".$_SERVER['SERVER_ADDR'];
	$s = trim($_SERVER["SCRIPT_NAME"]);
	if (strlen($s) > 0) {
		$s = explode("/", $s);
		for($i = 0; $i < sizeof($s) - 1; $i++) {
			if (strlen($s[$i]) > 0) {
				$a = $a."/".$s[$i];
			}
		}
	}
	return $a;
}

?>

