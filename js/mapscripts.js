$(function(){
	var $map = $('#mapcanvas');
	var center = [-0.439449, 33.991699];
		
	$('#mapcanvas').gmap3({ 
		action : 'init', 
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		//center:{lat: -0.439449,lng: 33.991699},
		center:center,
		zoom: 6
	});
    
    $("input:checkbox[name=transitpoint]").change(function() {
		var transitpoint = $(this).val();
		var checked = $(this).is(':checked');
		$("#mapdata").html("Select Transit Point to view markers on map, then click on the marker to view available indicators.");
		if ($(this).is(':checked')) {
			$.ajax({
				type: 'get',
				url: "transitpoint.php",
				data: {transitpointid:transitpoint},
				success: function(data) {
					var json = jQuery.parseJSON(data);
					if (json != null) {
						var id = new String(json.id);
						var title = new String(json.title);
						var lat = new String(json.latitude);
						var lng = new String(json.longitude);
						var icon = new String(json.icon);

						id = id.split(",");
						title = title.split(",");
						lat = lat.split(",");
						lng = lng.split(",");
						icon = icon.split(",");
						
						for(var i = 0; i < id.length; i++) {
							addmarker(id[i], title[i], lat[i], lng[i], icon[i], transitpoint);
						}
					}
				},
				error: function(msg){
					alert("The following error was detected:" + msg);
				}
			});
		}
		else {
			var markers = $('#mapcanvas').gmap3({
                action:'get',
                name:'marker',
                all: true,
                tag: transitpoint
            });
            $.each(markers, function(i, marker){
				marker.setMap(null);
            });
		}
		//$map.gmap3('get').setCenter(center);
	});
	
	
	function addmarker(id, title, lat, lng, icon, tag) {
		$('#mapcanvas').gmap3({
			action:'addMarker',
			latLng:[lat,lng],
			marker:{
				tag: tag,
				options: {
					suppressInfoWindows: true,
					title: title,
					icon: new google.maps.MarkerImage('http://top.demo.co.ke/ncttca/images/' + icon +''),
					draggable: false
				},
				events:{
					click: function(marker, event, data){
						$.ajax({
							type: 'get',
							url: "map_indicators.php",
							data: {transitlocationid:id},
							success: function(data) {
								$("#mapdata").html(data);
							},
							error: function(msg){
								alert("The following error was detected:" + msg);
							}
						});
					}
				}
			}
			
		});		
	}
});
