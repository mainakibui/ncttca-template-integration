<?php
require_once("includes/config.php");

$mtitle = "Change Account Password";

$a = trim($_POST["a"]);
$currentpassword = clean(strtolower($_POST['currentpassword']));
$password = clean(strtolower($_POST['password']));
$password2 = clean(strtolower($_POST['password2']));

if (strlen($a) == 0 && !is_numeric($a)) {
    $a = 1;
}

if (!loggedin()) {
	header("Location: "."login.php");
}
else {
	$mystr = $mystr."<div class='tab-content'>";
		$mystr = $mystr."<div class='row panel4 clearfix'>";
			$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12  nopadding main-inner-page-padding'>";
				$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12  article'>";
				
					$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12'> <h3>Edit Account Password</h3> </div>";
	
					switch(trim($a)) {
						case "1":
							$mystr = $mystr.editaccount();
							break;
						case "2":
							$mystr = $mystr.updateaccount($currentpassword, $password, $password2);
							break;
						default:
							$_SESSION['callback_message'] = messagebox("Sorry, the requested action is not available",false);
							header("Location: "."account.php");
							break;
					}
				$mystr = $mystr."</div>";
			$mystr = $mystr."</div>";
		$mystr = $mystr."</div>";
	$mystr = $mystr."</div>";
	
	display($mystr);
}





function editaccount() {
	global $dba;
	$sql = "select name, email, username, password from `account` where id=".user("id").";";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
			$str = $str."<br>";
			$str = $str."<form method=\"post\" action=\"account_password_edit.php\" name=\"editaccount\">";
			
			$str = $str."<legend><b>*</b> Indicates Required fields.</legend>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>Username: </label>";
				$str = $str.$rs->row("username");
			$str = $str."</div>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>(*) Current Password</label>";
				$str = $str."<input value='' style='max-width: 30%;' id='currentpassword' name='currentpassword' placeholder='password' type='password' required class='form-control' />";
			$str = $str."</div>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>(*) New Password</label>";
				$str = $str."<input value='' style='max-width: 30%;' id='password' name='password' placeholder='password' type='password' required class='form-control' />";
			$str = $str."</div>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>(*) Repeat New Password</label>";
				$str = $str."<input value='' style='max-width: 30%;' id='password2' name='password2' placeholder='password' type='password' required class='form-control' />";
			$str = $str."</div>";
			
			$str = $str."<input type=hidden name=\"a\" value=2>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<input value='Update Password' type='submit' class='btn btn-success btn-login-submit' />";
			$str = $str."</div>";
			
			$str = $str."</form>";
		$str = $str."</div>";
	}
	return $str;
}

function updateaccount($currentpassword, $password, $password2) {
	global $dba;
	$cols = array();
	$values = array();
	$t = "account";
	if ($password == $password2) {
		if (strlen($password) < 6 ) {
			$m = $m."<li>The password entered is too short.</li>";
		}
	}
	else {
		$m = $m."<li>The password entered do not match.</li>";
	}
	if (strlen($m) > 0) {
		$str = $str."Some mandatory fields have not been filled in. Please complete the field(s) listed below:-";
		$str = $str."<ul type=square>".$m."</ul><a href=\"javascript:window.history.go(-1)\">Click here to go back and complete the missing fields</a>";
		$str = messagebox($str, false);
	}
	else {
		if (sha1(strtolower(trim($currentpassword))) != strtolower(trim(getmyfieldid("account", user("id"), "password")))) {
			$str = messagebox("The current password provided is incorrect", false)."<br><br>";
		}
		else {
			$sql = "update `account` set password = '".sha1(strtolower(trim($password)))."' where id = ".user("id")." and username='".user("username")."' and password = '".sha1(strtolower(trim($currentpassword)))."';";
			$rs = $dba->execute($sql);
			if ($dba->querystatus) {
				$mflag = true;
				getuserprefs(user("username"), $password, "account");
				$_SESSION['callback_message'] = messagebox("Your account password has been changed successfully", true);
				header("Location: "."account.php");			
			}
			else {
				$_SESSION['callback_message'] = messagebox("An error has occured while updating your account. Please try again later", false);
				header("Location: "."account.php");
			}
		}		
		$str = $str.editaccount();
		
	}
	return $str;
}

?>
