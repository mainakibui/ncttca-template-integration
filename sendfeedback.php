<?php
require_once("includes/config.php");
$mtitle = "Contacts Us";

$email = clean($_POST['email']);
$organization = clean($_POST['organization']);
$name = clean($_POST['name']);
$telephone = clean($_POST['telephone']);
$enquiry = clean($_POST['enquiry']);
$captcha = clean(strtolower($_POST['captcha']));
//$countryid = clean($_POST['countryid']);
$country = getmyfield("country", $countryid);

if (strlen($email) < 6 || (strpos($email,"@") ? strpos($email,"@") + 1 : 0) < 1 || (strpos($email,".") ? strpos($email,".") + 1 : 0) < 1) {
	$m = $m."<li>The email provided is not valid</li>";
} 
if (strlen(trim($name)) < 6) {
	$m = $m."<li>The name you have specified is too short. Please specify first & last name e.g. John Smith</li>";
}
if (strlen($enquiry) == 0) {
	$m = $m."<li>Missing enquiry.</li>";
}
if (strtolower(trim($captcha)) != strtolower(trim($_SESSION['captcha'])) || strlen(trim($captcha)) == 0) {
	$m = $m."<li>The security code you have provided is not correct.</li>";
}

$mystr = $mystr."<div class='tab-content'>";
	$mystr = $mystr."<div class='row panel4 clearfix'>";
		if (strlen(trim($m)) > 0) {
			$mystr = $mystr.messagebox("<b><font color=#cc3300>The following errors have been detected</font></b><ul type=square>".$m."</ul><a href=\"javascript:window.history.go(-1)\">Click here to go back and complete the missing fields</a>",false)."";
		}
		else {
			$mystr = $mystr.sendfeedback($email, $name, $organization, $telephone, $country, $enquiry);
		}
	$mystr = $mystr."</div>";
$mystr = $mystr."</div>";

display($mystr);

function sendfeedback($em, $nm, $org, $tel, $cn, $mmsg) {
	global $application;

	$msg = $msg."Hello Webmaster,\n\n";
	$msg = $msg."Find below an enquiry made from ".$application["title"]." website\n\n";
	$msg = $msg."Names: $nm\n";
	$msg = $msg."Organization: $org\n";
	$msg = $msg."Email Address: $em\n";
	$msg = $msg."Telephone: $tel\n";
	//$msg = $msg."Country: $cn\n";
	$msg = $msg."Enquiry: $mmsg";
	$msg = $msg."\n\nThanks,\n".$application["title"]." Administrator\n";
	sendmail($em, $application["email"], "$site_name - Online Enquiry.", $msg);
	$str = $str.messagebox("Thank you! your enquiry have been sent successfully.",true);
	return $str;
}

?>



