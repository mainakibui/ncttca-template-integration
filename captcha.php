<?php
require_once("includes/config.php");
$bg = "images/captcha.gif";
header("Content-type: image/jpeg");
$im = writetoimage($bg, getcaptcha());
imagejpeg($im);

function writetoimage($imagefile, $text) {
	if(file_exists($imagefile)) {
		$im = @imagecreatefromgif($imagefile);
		$text_color = imagecolorallocate($im, 100, 100, 100);
		imagestring($im, 20, 10, 8,  "$text", $text_color);
	}
	else {
		$im  = imagecreatetruecolor(150, 30);
		$bgc = imagecolorallocate($im, 255, 255, 255);
		$tc  = imagecolorallocate($im, 0, 0, 0);
		imagefilledrectangle($im, 0, 0, 150, 30, $bgc);
		imagestring($im, 1, 5, 5, "Error loading $imagefile", $tc);
	}
	return $im;
}

function getcaptcha() {
	$n = mt_rand(1000, 9999);
	$_SESSION['captcha'] = $n;
	return $_SESSION['captcha'];
}

?>
