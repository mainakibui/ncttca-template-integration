<?php 
require_once("includes/config.php");
ini_set('display_errors', 0); 
ini_set('display_startup_errors', 0); 

$mtitle = "Reset Password";
$email = clean(strtolower($_POST['email']));

if (strlen($email) > 0) {
	if (strlen($email) < 6 || (strpos($email,"@") ? strpos($email,"@") + 1 : 0) < 1 || (strpos($email,".") ? strpos($email,".") + 1 : 0) < 1) {
		$m = $m."<li>Email address provided is invalid</li>";
	}

	if (strlen($m) > 0) {
		$mystr = $mystr.messagebox("<ul>$m</ul>", false);
		$mystr = $mystr.login("",true,false,"login.php");
	}
	else {
		$mystr = $mystr.sendpassword();
	}
}
else {
	$mystr = $mystr.messagebox("Missing or invalid email address entered.", false);
	//$mystr = $mystr."<br><br>".login("", true, false, "login.php");
}

display($mystr);

function sendpassword() {
	global $dba, $email, $application;
	$subject = $application["title"]." Login Information";
	$sql = "select name, email, username, password from `account` where email = '".$email."';";
	$rscheck = $dba->execute($sql);
	if (!$rscheck->eof()) {
		$msg = $msg."Hi ".$rscheck->row("name").",\n\n";
		$msg = $msg."Below is your login information:-\n\n";
		$msg = $msg."\tUsername: ".$rscheck->row("username");
		$msg = $msg."\n\tPassword: ".$rscheck->row("password");
		$msg = $msg."\n\nPlease keep this information in a safe place. Thank you.";
		$msg = $msg."\n\n".$application["title"]." Administrator\n".$application["domain"];
		sendmail($application["email"], $email, $subject, $msg);
		$str = $str.messagebox("Login information has been sent to <b>[ <font class=texthighlighted>$email</font> ]</b>. You should receive this information shortly.",true);
	}
	else {
		$str = $str.messagebox("The email address specified was not found on our system. If you believe this is an error, Please contact us through our email address <a href=\"mailto:".$application["email"]."\">".$application["email"]."</a> for assistance.",false);
	}
	return $str;
}

?>
