<?php
require_once("includes/config.php");
$id = "1";
$a = trim($_POST["a"]);
$email = clean(strtolower($_POST['email']));

if (strlen($a) == 0 && !is_numeric($a)) {
    $a = 1;
}
else {
	$a = $a + 0;
}

if (!loggedin() && accesscontrol()) {
	header("Location: "."index.php");
}
else {
	$mystr = $mystr."<div class='tab-content'>";
		$mystr = $mystr."<div class='row panel4 clearfix'>";
			$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12  nopadding main-inner-page-padding'>";
				$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12  article'>";
				
					$mystr = $mystr."<div class='col-sm-12 col-md-12 col-lg-12'> <h3>Edit Account Details</h3> </div>";
				
					if (strlen($a) > 0 && is_numeric($a)) {
						switch($a) {
							case 1:
								$mtitle = "Edit Account Details";
								$mystr = $mystr.editaccount();
							break;
							case 2:
								$mtitle = "Edit Account Details";
								$mystr = $mystr.updateaccount();
							break;
							default:
								$mystr = $mystr.messagebox("Sorry, the requested action is not available",false);
							break;
						}
					}
					else {
						$mystr = $mystr.messagebox("Sorry, the requested action is not available",false);
					}
	
				$mystr = $mystr."</div>";
			$mystr = $mystr."</div>";
		$mystr = $mystr."</div>";
	$mystr = $mystr."</div>";
	
	display($mystr);
}


function editaccount() {
	global $dba;
	$sql = "select name, email, username, address, telephone, town, countryid from `account` where id=".user("id").";";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>";
			$str = $str."<br>";
			$str = $str."<form method=\"post\" action=\"account_edit.php\" name=\"editaccount\">";
			
			$str = $str."<legend><b>*</b> Indicates Required fields.</legend>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>Names: </label>";
				$str = $str.$rs->row("name");
			$str = $str."</div>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>User name: </label>";
				$str = $str.$rs->row("username");
			$str = $str."</div>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>(*) Email</label>";
				$str = $str."<input value='".$rs->row("email")."' style='max-width: 30%;' id='email' name='email' placeholder='another@email.com' type='text' required class='form-control' />";
			$str = $str."</div>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>(*) Telephone</label>";
				$str = $str."<input value='".$rs->row("telephone")."' style='max-width: 30%;' id='telephone' name='telephone' placeholder='another@email.com' type='text' required class='form-control' />";
			$str = $str."</div>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>(*) Address</label>";
				$str = $str."<textarea value='".$rs->row("address")."' style='max-width: 30%;' id='address' name='address' placeholder='another@email.com' type='text' required class='form-control'>".$rs->row("address")."</textarea>";
			$str = $str."</div>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>(*) Town</label>";
				$str = $str."<input value='".$rs->row("town")."' style='max-width: 30%;' id='town' name='town' placeholder='another@email.com' type='text' required class='form-control' />";
			$str = $str."</div>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<label for='edit-account'>(*) Country</label>";
				$str = $str.dropdown("countryid", $rs->row("countryid"), null, 'form-control', 'max-width: 30%;');
			$str = $str."</div>";
			
			$str = $str."<input type=hidden name=\"a\" value=2>";
			
			$str = $str."<div class='form-group'>";
				$str = $str."<input value='Update My Info' type='submit' class='btn btn-success btn-login-submit' />";
			$str = $str."</div>";
			
			$str = $str."</form>";
		$str = $str."</div>";
	}
	return $str;
}

function updateaccount() {
	global $dba, $email;
	$cols = array();
	$values = array();
	$t = "account";
	foreach ($_POST as $i=>$j) {
		if (strlen($j) == 0 && trim(strtolower($i)) != "a" && trim(strtolower($i)) != "email")  {
			$m = $m."<li>".ucfirst(hashtable($i))." is empty or invalid</li>";
		}
	}
	if (strlen($email) < 6 || (strpos($email,"@") ? strpos($email,"@") + 1 : 0) < 1 || (strpos($email,".") ? strpos($email,".") + 1 : 0) < 1) {
		$m = $m."<li>The email provided is invalid, use a valid email address.</li>";
	}
	if (strlen($m) > 0) {
		$str = $str."Some mandatory fields have not been filled in. Please complete the field(s) listed below:-";
		$str = $str."<ul type=square>".$m."</ul><a href=\"javascript:window.history.go(-1)\">Click here to go back and complete the missing fields</a>";
		$str = messagebox($str,false);
	}
	else {
		foreach ($_POST as $i=>$j) {
			if (trim(strtolower($i)) != "a") {
				array_push($cols, $i);
				array_push($values, $_POST[$i]);
			}
		}
		if (sizeof($cols) > 0) {
			$sql = $sql."update ".$t." set ";
			for ($i = 0; $i < sizeof($cols); $i++) {
				$sql = $sql."`".$cols[$i]."`"."='".mmysql_real_escape_string($values[$i])."'";
				if ($i + 1 < sizeof($cols)) {
					$sql = $sql.",";
				}
			}
			$sql = $sql." where id=".user("id").";";
			$rs = $dba->execute($sql);
			if ($dba->querystatus) {
				$mflag = true;
				$str = messagebox("Your account has been updated successfully",true);				
			}
			else {
				$str = messagebox("An error has occured while updating your account.",false);
			}
			$str = $str.editaccount();
		}
	}
	return $str;
}

?>
