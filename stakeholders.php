<?php
require_once("includes/config.php");
$mtitle = "Stakeholders";
$_SESSION["referrer"] = "stakeholders.php";

//if (loggedin()) {
	$mystr = list_stakeholders();
	display($mystr);
/*
}
else {
	header("Location: "."login.php");
}
*/

function list_stakeholders() {
	global $dba;
	$sql = "select distinct countryid from `stakeholder` order by countryid asc;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$str = $str."<div class='tab-content'>";
		$str = $str."<div class='row panel4 clearfix'>";
			$str = $str."<div class='col-sm-12 col-md-12 col-lg-12 align-article main-inner-page-padding'>";
			$str = $str."<div class='col-sm-12 col-md-12 col-lg-12 article'>";
				$str = $str."<div class='col-sm-12 col-md-12 col-lg-12 nopadding articlecontent'>";
				$str = $str."<br><h2>Stakeholders</h2><br>";
					$str = $str."<div class='col-sm-12 col-md-12 col-lg-12 nopadding'>";
					while (!$rs->eof()) {
						$str = $str."<h3 class='stakeholder-name'>".titlecase(translate(getmyfield("country", $rs->row("countryid"))))."</h3>";
						$str = $str."<div class='stakeholders-list'>";
						
						$ssql = "select id, title, stakeholdertypeid, description, email, website, logo from `stakeholder` where statusid = 1 and countryid = '".$rs->row("countryid")."' order by positionid;";
						$rss = $dba->execute($ssql);
						if (!$rss->eof()) {
							while (!$rss->eof()) {
								
								$str = $str."<div class='col-sm-12 col-md-12 col-lg-12 nopadding'>";
									$str = $str."<div class='col-sm-4 col-md-4 col-lg-4 nopadding' style='text-align: center;'>";
										$str = $str."<img src='".$rss->row('logo')."'/>";
									$str = $str."</div>";
									
									$str = $str."<div class='col-sm-8 col-md-8 col-lg-8 nopadding'>";
										$str = $str."<h4>".titlecase(translate($rss->row("title")))."</h4>";
									
										
										$str = $str."<p>Classification:&nbsp;&nbsp;".getmyfield("stakeholdertype", $rss->row("stakeholdertypeid"))."</p>";
										
										if (strlen(trim($rss->row("email")))) {
											$str = $str."<p>Email:&nbsp;&nbsp;<a href=\"mailto:".$rss->row("email")."\">".$rss->row("email")."</a> </p>";
										}
										if (strlen(trim($rss->row("website")))) {
											$str = $str."<p>Website:&nbsp;&nbsp;<a href=\"".$rss->row("website")."\" target=\"_blank\">".$rss->row("website")."</a> </p>";
										}
										if (strlen(trim($rss->row("description")))) {
											//$str = $str."<br><div style=\"padding-top:5px;padding-bottom:5px;\">".translate($rss->row("description"))."</div>";
										}
										$str = $str."</p>";
										$str = $str."<hr>";
									$str = $str."</div>";
								$str = $str."</div>";
								$rss->movenext();
							}
						}
						$str = $str."</div>";
						$rs->movenext();
						if (!$rs->eof()) {
							$str = $str."<tr><td valign=\"top\" align=\"left\" bgcolor=\"#ff6633\"><img src=\"images/margin.gif\" height=\"1\"/></td></tr>";
						}
					}
					$str = $str."</div>";
				$str = $str."</div>";
			$str = $str."</div>";
			$str = $str."</div>";
		$str = $str."</div>";
		$str = $str."</div>";
	}
	return $str; 
}

?>
