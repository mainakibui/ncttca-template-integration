<?php

$graph_src = "http://localhost/ncttca/jgraph.php?x=Jan%7CFeb%7CMar%7CApr%7CMay%7CJun%7CJul%7CAug%7CSep%7COct%7CNov%7CDec&y=%7B%22weight%22%3A%7B%222009%22%3A%5B%221256266%22%2C%221048569%22%2C%221456680%22%2C%221503141%22%2C%221535768%22%2C%221343540%22%2C%221472184%22%2C0%2C%221314544%22%2C%221668064%22%2C%221662792%22%2C%221694796%22%5D%2C%222010%22%3A%5B%221921420%22%2C0%2C%221786937%22%2C%221762821%22%2C%221565580%22%2C%221555976%22%2C%221529436%22%2C%221825609%22%2C%221652602%22%2C%221660705%22%2C%221913472%22%2C%221901366%22%5D%7D%2C%22transitWeight%22%3A%7B%222009%22%3A%5B%22387356%22%2C%22317010%22%2C%221234992%22%2C%22414077%22%2C%221309196%22%2C%22341764%22%2C%22402227%22%2C0%2C%22396628%22%2C%22507761%22%2C%22422898%22%2C%22549039%22%5D%2C%222010%22%3A%5B%22780369%22%2C0%2C%22724446%22%2C%22734309%22%2C%22653653%22%2C%22649689%22%2C%22659415%22%2C%22724958%22%2C%22662999%22%2C%22731330%22%2C%22822746%22%2C%22795999%22%5D%7D%7D&xt=&yt=&t=Total+cargo+throughput+of+the+port+of+Mombasa+%28TCPMsa%29+vs+transit+traffic+%28TTPMsa%29+in+tonnes&c=groupbar&w=750&h=320";
//$graph_src = "http://jpgraph.net/features/src/new_bar3.php";
//$graph_src = "images/watercraft.png";

if (_iscurlinstalled()) {
	echo "cURL is installed"; 
	$graph_image = get_graph($graph_src);
	$filename = "graph_".date("Ymdhis").".png";
	$save_graph = fopen("images/graph/$filename", 'w');
	fwrite($save_graph, $graph_image);
	fclose($save_graph);
}
else {
	echo "cURL is NOT installed";
}

function _iscurlinstalled() {
	if  (in_array  ('curl', get_loaded_extensions())) {
		return true;
	}
	else{
		return false;
	}
}

function get_graph($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, 0);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	print_r($result);
	curl_close($ch);
	return $result;
}

?>
