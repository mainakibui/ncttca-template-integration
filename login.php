<?php
require_once("includes/config.php");
$mtitle = "Account Login";

$username = clean(strtolower($_POST['username']));
$password = clean(strtolower($_POST['password']));

if (loggedin()) {
	header("Location: account.php");
}
else {
	if (strlen(trim($username)) > 0 || strlen(trim($password)) > 0) {
		if (!getuserprefs($username, $password, "account", "username", "password", "statusid=1"))	{
			$mystr = $mystr.messagebox("Authentication failure. Cannot login due to invalid username and/or password", false);
			$mystr = $mystr.login($username, false, "", true, false, "login.php", "Login to your Account");
		}
		else {
			if (strlen(trim($_SESSION['referrer'])) > 0) {
				$r = $_SESSION['referrer'];
				$_SESSION['referrer'] = "";
				header("Location: ".$r);
			}
			else {
				header("Location: "."account.php");
			}
		}
	}
	else {
		if (strlen(trim($username)) > 0 || strlen(trim($password)) > 0) {
			$mystr = $mystr.messagebox("Authentication failure. Cannot login due to invalid username and/or password", false);
		}
		$mystr = $mystr.login($username, false, "", true, false, "login.php", "Login to your Account");
	}
}
display($mystr);
?>
