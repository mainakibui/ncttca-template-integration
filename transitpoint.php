<?php
require_once("includes/config.php");
$transitpointid = get_default(clean($_GET['transitpointid']), "n", 0);

if (is_numeric($transitpointid) && $transitpointid > 0) {
	$data = array();
	$sql = "select id, title, latitude, longitude, (select image from `transitpoint` where id = $transitpointid limit 1) as icon from `transitlocation` where statusid = 1 and transitpointid = $transitpointid order by title asc;";
	$rs = $dba->execute($sql);
	$fields = $rs->fieldcount();
	if (!$rs->eof()) {
		while(!$rs->eof()) {
			for ($i = 0; $i < $fields; $i++) {
				$data[$rs->fields[$i]][] = $rs->row($rs->fields[$i]);
			}
			$rs->movenext();
		}
		echo json_encode($data);
	}
} 

?>
