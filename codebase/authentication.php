<?php



function authkey($t, $system_key) {
	global $dba;
	if (strlen(trim($t)) > 0 && strlen(trim($system_key)) > 12) {
		$result = false;
		$o = strtolower(substr(trim($system_key), 0, 12));
		$d = null;
		$hex = explode(",", "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f");
		$mod = explode(",", "c,b,d,e,f,g,h,i,j,k,l,n,r,t,u,v");
		$modhex = array();
		for ($i = 0; $i < sizeof($hex); $i++) {
			$modhex[$mod[$i]] = $hex[$i];
		}
		for ($i = 0; $i < strlen($o); $i++) {
			$d = $d.$modhex[substr($o, $i, 1)];
		}
		$code = hexdec($d);
		if (strlen(trim($code)) > 4) {
			$authsql = "select id from `auth` where lower(trim(code))='".strtolower(trim($code))."' and active='1';";
			$rsauth = $dba->execute($authsql);
			if (!$rsauth->eof()) {
				$result = true;
			}
		}	
	}
	return $result;
}


function user($value) {
	global $system_key;
	$j = $system_key."_".strtolower($value);
	return $_SESSION[$j];
}


function set_session($name, $value) {
	global $system_key;
	if (strlen(trim($name)) > 0) {
		$j = $system_key."_".strtolower(trim($name));
		$_SESSION[$j] = trim($value);
	}
	return $_SESSION[$j];
}


function loggedin() {
	global $dba, $system_key;
	$l = false;
	$j = $system_key."_id";
	if (strlen(trim($_SESSION[$j])) > 0 && is_numeric($_SESSION[$j])) {
		$usql = "select count(*) as total from `session` where lower(trim(username))='".strtolower(trim(user("username")))."' and lower(session)='".strtolower(session_id())."' and enddate >= now();";
		$rstest = $dba->execute($usql);
		if ($rstest->row("total") > 0) {
			$l = true;
		}
	}
	return $l;
}

function logout($u = "index.php") {
	global $dba, $system_key;
	$system_keys = &$_SESSION;
	foreach($system_keys as $x=>$y) {
		if(strtolower(substr(trim($x), 0, strlen($system_key) + 1)) == strtolower($system_key)."_") {
			unset($system_keys[$x]);
		}
	}
	session_unset();
	header("Location: $u");
	return;
}



function userinfo() {
	return $str;
}


function getuserprefs($u, $p, $t, $uf, $pf, $ft) {
	global $dba, $system_key, $application;
	$c = $application['concurrencyid'];
	$c = getmyfieldid("concurrency", $c, "sessions");
	if (!is_numeric($c) || strlen(trim($c)) == 0) {
		$c = 1;
	}
	$usql = "select * from `$t` where lower(trim($uf))='".strtolower(trim($u))."' and lower(trim($pf))='".strtolower(trim($p))."' ";
	if (strlen(trim($ft)) > 0) {
		$usql = $usql." and $ft ";
	}
	$usql = $usql." and (select count(*) from `session` where lower(trim(username))='".strtolower(trim($u))."' and enddate >= now()) <= $c ";
	$usql = $usql.";";
	print($usql);
	$rs = $dba->execute($usql);
	if (!$rs->eof()) {
		$ssql = "insert into `session` (id, username, db, accountid, session, logindate, enddate, ip, host, agent) values ('".getid("session")."', '".$rs->row("username")."', '$t', '".$rs->row("id")."', '".session_id()."', now(), date_add(now(), interval 20 minute), '".mmysql_real_escape_string($_SERVER['REMOTE_ADDR'])."', '".mmysql_real_escape_string(gethostbyaddr($ip))."', '".mmysql_real_escape_string($_SERVER['HTTP_USER_AGENT'])."');";
		$rsse = $dba->execute($ssql);
		if (strlen(trim($key)) > 0) {
			for ($i = 0; $i < $rs->fieldcount(); $i++) {
				$j = $system_key."_".strtolower($rs->fields[$i]);
				$_SESSION[$j] = $rs->row($i);
			}
			return true;
		}
	}
	else {
		return false;
	}
}



function keepalive() {
	global $dba;
	$ssql = "select id from `session` where lower(trim(session))='".strtolower(trim(session_id()))."' and enddate >= now() order by id desc limit 1;";
	$rs = $dba->execute($ssql);
	if (!$rs->eof()) {
		$usql = "update `session` set date=date_add(now(), interval 20 minute) where id='".$rs->row("id")."';";
		$rsu = $dba->execute($usql);
	}
}
		

function getip() {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	}
	else {
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
	}
	return $ip;
}


function getuseragent() {
	$ua = $_SERVER['HTTP_USER_AGENT'];
	if (strlen(trim($ua)) == 0) {
		$ua = "unknown user-agent";
	}
	return $ua;
}


?>
