<?php


function authkey($t, $key) {
	global $dba;
	$result = false;
	if (strlen(trim($t)) > 0 && strlen(trim($key)) > 12) {
		$o = strtolower(substr(trim($key), 0, 12));
		$d = null;
		$hex = explode(",", "0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f");
		$mod = explode(",", "c,b,d,e,f,g,h,i,j,k,l,n,r,t,u,v");
		$modhex = array();
		for ($i = 0; $i < sizeof($hex); $i++) {
			$modhex[$mod[$i]] = $hex[$i];
		}
		for ($i = 0; $i < strlen($o); $i++) {
			$d = $d.$modhex[substr($o, $i, 1)];
		}
		$code = hexdec($d);
		if (strlen(trim($code)) > 4) {
			$authsql = "select id from `auth` where lower(trim(code))='".strtolower(trim($code))."' and active='1';";
			$rsauth = $dba->execute($authsql);
			if (!$rsauth->eof()) {
				$result = true;
			}
		}	
	}
	return $result;
}


function user($value) {
	global $system_key;
	$j = $system_key."_".strtolower($value);
	return $_SESSION[$j];
}


function set_session($name, $value) {
	global $system_key;
	if (strlen(trim($name)) > 0) {
		$j = $system_key."_".strtolower(trim($name));
		$_SESSION[$j] = trim($value);
	}
	return $_SESSION[$j];
}


function loggedin() {
	global $dba, $system_key;
	$l = false;
	$j = $system_key."_id";
	if (strlen(trim($_SESSION[$j])) > 0 && is_numeric($_SESSION[$j])) {
		$l = true;
	}
	return $l;
}


function login($username, $register = false, $registerurl = "register.php", $lostpwd = false, $border = false, $url = "login.php", $title = null) {
	global $application;
	$bordercolor = "#ffffff";
	if ($border) {
		$bordercolor = "#e0e0e0";
	} 
	
	$str = $str."<div class='tab-content'>";
		$str = $str."<div class='row panel4 clearfix'>";
			$str = $str."<div class='col-sm-12 col-md-12 col-lg-12 nopadding'>";

				//login form			
				$str = $str."<div class='container'>";
				    $str = $str."<div class='row'>";
					$str = $str."<div class='col-md-12'>";
					    $str = $str."<div class='well login-box'>";
					    
					    if (strlen(trim($application["logo"])) > 0) {
						    //$str = $str."<center><img src=\"images/".$application["logo"]."\" border=\"0\"></center>";
					    }
					    
						$str = $str."<form method=\"post\" action=\"$url\">";
						    $str = $str."<legend>Login</legend>";
						    $str = $str."<div class='form-group'>";
							$str = $str."<label for='username-email'>Username</label>";
							$str = $str."<input value='' id='username' name='username' placeholder='Username' type='text' class='form-control' />";
						    $str = $str."</div>";
						    $str = $str."<div class='form-group'>";
							$str = $str."<label for='password'>Password</label>";
							$str = $str."<input id='password' name='password' value='' placeholder='Password' type='password' class='form-control' />";
						    $str = $str."</div>";
						    $str = $str."<div class='form-group text-center'>";
							$str = $str."<input type='submit' class='btn btn-success btn-login-submit' value='Login' />";
						    $str = $str."</div>";
						    
						    if ($register == true) {
							    $str = $str."<div><center>Don't have an Account? <a href=\"".$registerurl."\">Register Here</a></center></div>";
						    }
						    
						    if ($lostpwd) {
							    $str = $str."<form method=\"post\" action=\"getpassword.php\">
							    <hr size=\"1\" noshade color=#f0f0f0>
							    <h4>".titlecase(translate("Forgotten your Password?"))."</h4>
							    
							    	<div>".translate("If you have forgotten your password, please enter your email address in the box below and we will send it to you via this email address")."</div>
							    
							    	<div class='form-group'>
							    	<label for='password'>Email</label>
							    	<input id='email' name='email' value='' placeholder='Email' type='text' class='form-control'>
							    	</div>
							    	
							    	<div class='form-group text-center'>
							    	<input type='submit' value='Send' class='btn btn-success btn-login-submit'>
							    	</div>
							    
							    </form>";
						    }
						    
						$str = $str."</form>";
					    $str = $str."</div>";
					$str = $str."</div>";
				    $str = $str."</div>";
				$str = $str."</div>";
				//login form
			
			$str = $str."</div>";
		$str = $str."</div>";
	$str = $str."</div>";
	
	return $str;
}

function loginadmin($username, $register = false, $registerurl = "register.php", $lostpwd = false, $border = false, $url = "login.php", $title = null) {
	global $application;
	$bordercolor = "#ffffff";
	if ($border) {
		$bordercolor = "#e0e0e0";
	} 
	$str = $str."<center><table border=\"0\" cellpadding=1 cellspacing=\"0\" width=\"600\"><tr><td align=\"center\" bgcolor=".$bordercolor.">";
	$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#ffffff\" width='100%'>";
	$str = $str."<tr valign=top><td align=\"left\">";
	if (strlen(trim($application["logo"])) > 0) {
		//$str = $str."<img src=\"images/".$application["logo"]."\" border=\"0\">";
	}
	$str = $str."</td>";
	$str = $str."<td valign=\"middle\"><img src=\"images/sep.gif\" height=\"150\" width=\"1\" border=\"0\"/></td>";
	$str = $str."<td width='100%' align=\"left\">";
	if (strlen(trim($title)) > 0) {
		$str = $str."<br/><h2>".titlecase(translate($title))."</h2><hr size=\"1\" noshade color=\"#e0e0e0\"><br/>";
	}
	$str = $str."<form method=\"post\" action=\"$url\">";
	$str = $str."<center><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width='400'><tr><td><table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" width='100%'>";
	$str = $str."<tr><td>".titlecase(translate("Username")).": </td><td>".textfield("username", $username, 26)."</td></tr>";
	$str = $str."<tr><td>".titlecase(translate("Password")).": </td><td>".passwordfield("password", "", 20)."</td></tr>";
	$str = $str."<tr><td>&nbsp;</td><td align=\"left\" valign=bottom><input class=\"button\" type=\"submit\" value='".titlecase(translate("Login to your Account"))."'></td></tr>";
	if ($register == true) {
		$str = $str."<tr><td align=\"left\" colspan=2 valign=\"bottom\">Don't have an Account? <a href=\"".$registerurl."\">Register Here</a></td></tr>";
	}
	$str = $str."</table></td></tr></table></center>";
	$str = $str."</form>";
	if ($lostpwd) {
		$str = $str."<form method=\"post\" action=\"getpassword.php\"><hr size=\"1\" noshade color=#f0f0f0><h4>".titlecase(translate("Forgotten your Password?"))."</h4><br/>".translate("If you have forgotten your password, please enter your email address in the box below and we will send it to you via this email address")."<br/><table border=\"0\" cellpadding=\"3\"><tr><td><font class=\"texthighlighted\">".titlecase(translate("Email Address"))."</font>&nbsp;&nbsp;</td><td>".textfield("email", "", 20)."</td><td><input type=\"submit\" class=\"button\" value='Send'></td></tr></table></form>";
	}
	$str = $str."</td></tr>";
	$str = $str."</table>";
	$str = $str."</td></tr></table></center>";
	return $str;
}




function logout($u = "index.php") {
	global $dba, $system_key;
	$system_keys = &$_SESSION;
	foreach($system_keys as $x=>$y) {
		if(strtolower(substr(trim($x), 0, strlen($system_key) + 1)) == strtolower($system_key)."_") {
			unset($system_keys[$x]);
		}
	}
	session_unset();
	header("Location: $u");
	return;
}

function getuserprefs($u, $p, $t, $uf, $pf, $ft) {
	global $dba, $system_key, $application;
	$result = false;
	$c = $application['concurrencyid'];
	$c = getmyfieldid("concurrency", $c, "sessions");
	if (!is_numeric($c) || strlen(trim($c)) == 0) {
		$c = 1;
	}
	$usql = "select * from `$t` where lower(trim($uf))='".strtolower(trim($u))."' and lower(trim($pf))='".sha1(strtolower(trim($p)))."' ";
	if (strlen(trim($ft)) > 0) {
		$usql = $usql." and $ft ";
	}
	//$usql = $usql." and (select count(*) from `session` where lower(trim(username))='".strtolower(trim($u))."' and enddate >= now()) <= $c ";
	$usql = $usql.";";
	$rs = $dba->execute($usql);
	if (!$rs->eof()) {
		$ssql = "insert into `session` (id, username, db, accountid, session, logindate, enddate, ip, host, agent) values ('".getid("session")."', '".$rs->row("username")."', '$t', '".$rs->row("id")."', '".session_id()."', now(), now(), '".mmysql_real_escape_string($_SERVER['REMOTE_ADDR'])."', '".mmysql_real_escape_string(gethostbyaddr(getip()))."', '".mmysql_real_escape_string($_SERVER['HTTP_USER_AGENT'])."');";
		$rsse = $dba->execute($ssql);
		if (strlen(trim($system_key)) > 0) {
			for ($i = 0; $i < $rs->fieldcount(); $i++) {
				$j = $system_key."_".strtolower($rs->fields[$i]);
				$_SESSION[$j] = $rs->row($i);
			}
			$result = true;
		}
	}
	return $result;
}



function keepalive() {
	global $dba;
	if (loggedin()) {
		$ssql = "select id from `session` where session='".session_id()."' order by id desc limit 1;";
		$rs = $dba->execute($ssql);
		if (!$rs->eof()) {
			$usql = "update `session` set enddate=now() where id='".$rs->row("id")."';";
			$rsu = $dba->execute($usql);
		}
	}
}
		

function getip() {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	}
	else {
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
	}
	return $ip;
}


function getuseragent() {
	$ua = $_SERVER['HTTP_USER_AGENT'];
	if (strlen(trim($ua)) == 0) {
		$ua = "unknown user-agent";
	}
	return $ua;
}


?>
