<?php

class database {

	protected $dba;
	protected $connected = false;
	protected $stmt;
	public $querystatus = false;

	function __construct($dbengine, $dbserver, $database, $dbusername, $dbpassword) {
		if (strlen($dbengine) > 0 && strlen($dbserver) > 0 && strlen($database) > 0 && strlen($dbusername) > 0 && strlen($dbpassword) > 0) {
			try {
				$this->dba = new PDO("$dbengine:host=$dbserver;dbname=$database", $dbusername, $dbpassword);
				$this->dba->setattribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->connected = true;
			}
			catch(exception $e) {
				print messagebox($e->getmessage(), false);
				die("");
			}
		}
	}


	public function connected() {
		return $this->connected;
	}


	function execute($sql) {
		$success = false;
		$this->querystatus = false;
		if (strlen($sql) > 0) {
			try {
				$stmt = $this->dba->prepare($sql);
				if ($stmt->execute()) {
					$success = true;
					$this->querystatus = true;
				}
			}
			catch(exception $e) {
				print $e->getmessage();
			}
		}
		return (new recordset($stmt, $success));
	}


	function __destruct() {
		$this->dba = null;
		$this->connected = null;
	}



}



class recordset {
	protected $records = array();
	public $fields = array();
	public $types = array();
	protected $eof = true;
	protected $pos = -1;

	function __construct($resultset, $status) {
		$j = 0;
		for ($i = 0; $i < $resultset->columncount(); $i++) {
			$meta = $resultset->getColumnMeta($i);
			$this->fields[$j] = strtolower($meta["name"]);
			$this->types[$j] = strtolower($meta["native_type"]);
			$j++;
		}
		if ($status) {
			try {
				$temp = $resultset->fetchall(PDO::FETCH_BOTH);
			}
			catch(exception $e) {
				//print $e->getmessage();
			}
			if (sizeof($temp) > 0) {
				for ($i = 0; $i < (sizeof($temp)); $i++) {
					$ttemp = $temp[$i];
					$j = 0;
					foreach($ttemp as $name=>$value) {
						$this->records[$i][$j] = $value;
						$this->records[$i][trim(strtolower($name))] = $value;
						$j++;
					}
				}
				$this->eof = false;
				$this->movenext();
			}
		}
	}

	public function __call($name, $i) {
		if (is_numeric($i[0])) {
			return $this->records[$this->pos][$i[0]];
		}
		else {
			return $this->records[$this->pos][trim(strtolower($i[0]))];
		}
	}


	public function movenext() {
		$this->pos++;
		if ($this->pos >= sizeof($this->records)) {
			$this->eof = true;
		}
	}

	public function moveprevious() {
		$this->pos--;
		if ($this->pos < 0) {
			$this->eof = true;
		}
	}

	public function movefirst() {
		$this->pos = 0;
	}

	public function movelast() {
		$this->pos = (sizeof($this->records) - 1);
	}


	public function recordcount() {
		if (is_array($this->records)) {
			return sizeof($this->records);
		}
		else {
			return 0;
		}
	}

	public function fieldcount() {
		if (is_array($this->fields)) {
			return sizeof($this->fields);
		}
		else {
			return 0;
		}
	}

	public function fields() {
		return $this->fields;
	}


	public function eof() {
		return $this->eof;
	}
}














?>
