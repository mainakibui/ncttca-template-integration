<?php
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR);


ini_set("memory_limit","256M");
session_start();
cachecontrol();

require_once("databases.php");
$dba = new database($dbengine, $dbserver, $database, $dbusername, $dbpassword);
if (!$dba->connected()) {
	print messagebox("fatal error: ".$e->getmessage(), false);
	die;
}
else {
	require_once("authentication.php");
	if (file_exists("includes/custom.php")) {
		require_once("includes/custom.php");
	}

	$appsql = "select * from `application` order by id desc limit 1;";
	$rsapp = $dba->execute($appsql);
	if (!$rsapp->eof()) {
		for ($i = 0; $i < sizeof($rsapp->fields); $i++) {
			$application[strtolower(trim($rsapp->fields[$i]))] = trim($rsapp->row($i));
		}
		$lg = $rsapp->row("languageid");
		if (strlen(trim(user("languageid"))) > 0 && is_numeric(trim(user("languageid")))) {
			$lg = trim(user("languageid"));
		} 
		$application["language"] = getmyfieldid("language", $lg, "code");
		if (strlen(trim($application["language"])) == 0) {
			$application["language"] = "en";
		}
		if (strlen(trim($application["language"])) == 2 && strtolower(trim($application["language"])) != "en") {
			require_once("codebase/plugins/translate/translate.php");
		}
		putenv("LC_ALL=".$application['language']);
		setlocale(LC_ALL, $application['language']);
		bindtextdomain("messages", "../language");
		textdomain("messages");

		if (strlen(trim($application['template'])) == 0) {
			$application['template'] = "default";
		}
		$application["codebase"] = $codebase;
		$page     = get_page(get_script_name());
		$pageid   = $page[0];
		$mtitle   = $page[1];
		$parentid = $page[2];
		$parent   = $page[3];

	}
	else {
		print messagebox("fatal error: could not load application settings", false);
		die;
	}
}

function cachecontrol() {
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", FALSE);
	header("Pragma: no-cache");
 	header("Expires: ".gmdate('D, d M Y H:i:s', time() + 0)."GMT");
	header('Content-Type: text/html; charset=utf-8');
}

function loadfunctions() {
	if (file_exists("key.php")) {
		require_once("key.php");
		if (file_exists("custom.php")) {
			require_once("custom.php");
		}
	}
	else {
		print messagebox("fatal error: could not load application key", false);
		die;
	}
}

function accesscontrol($acl = null) {
	global $page;
	$a = false;
	$g = trim(user("accounttypeid"));
	$u = trim(user("id"));

	if (strlen(trim($acl)) == 0) {
		$acl = strtolower(trim($page[5]));
	}
	if (strlen(trim($acl)) == 0) {
		$a = true;
	}
	else {
		if (strlen(trim($g)) > 0 && strlen(trim($u)) > 0) {
			if (trim($g) == "1") {
				$a = true;
			}
			else {
				if (strlen(trim($acl)) > 0) {
					$acl = explode(",", $acl);
					if (in_array($g, $acl) || in_array($u, $acl)) {
						$a = true;
					}
				}	
			}
		}
	}
	return $a;
}




function headers($ah = null) {
	global $application, $title, $summary, $keywords;
	$str = $str."<html>";
	$str = $str."<head>";
	$str = $str."<title>".removehtml($application['organization']);
	if (strlen(trim($application['title'])) > 0) {
		$str = $str." - ".trim(removehtml($application['title']));
	}
	if (strlen(trim($title)) > 0) {
		$str = $str." - ".trim(removehtml($title));
	}
	$str = $str."</title>";
	$str = $str."<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
  	$str = $str."<meta name=\"title\" content=\"$title\">";
  	$str = $str."<meta name=\"description\" content=\"$summary\">";
	$str = $str."<meta name=\"keywords\" content=\"$keywords\">";
	$str = $str."\n<script type=\"text/javascript\">\n";
	$str = $str."var GB_ROOT_DIR = \"../codebase/js/greybox/\";\n";
	$str = $str."</script>\n";
	
	$str = $str."<script src='includes/templates/default/jquery-2.1.3.min.js' type=\"text/javascript\"></script>";
	$str = $str."<script src='includes/templates/default/custom.js' type=\"text/javascript\"></script>";
	
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/prototype/prototype.js\"></script>";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/scriptaculous/scriptaculous.js?load=builder,effects\"></script>";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/greybox/AJS.js\"></script>";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/greybox/AJS_fx.js\"></script>";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/greybox/gb_scripts.js\"></script>";
	$str = $str."<link href=\"../codebase/js/greybox/gb_styles.css\" rel=\"stylesheet\" type=\"text/css\" />";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/tabs/tabs.js\"></script>";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/popup/popup.js\"></script>";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/ui/ui.js\"></script>";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/calendar/calendar.js\"></script>";
	$str = $str."<link href=\"../codebase/js/calendar/calendar.css\" rel=stylesheet media=\"screen\">";
	$str = $str."<link href=\"../codebase/js/tabs/tabs.css\" rel=stylesheet media=\"screen\">";
	$str = $str."<script language=\"javascript\" type=\"text/javascript\" src=\"../codebase/js/mce/tiny_mce/tiny_mce_src.js\"></script>\n";
	$str = $str."<script language=\"javascript\" type=\"text/javascript\" src=\"../codebase/js/mce/tiny_mce/config.js\"></script>\n";
	
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/jquery/jquery-1.6.4.min.js\"></script>";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/clock/coolclock.js\"></script>";
	$str = $str."<!--[if IE]><script type=\"text/javascript\" src=\"../codebase/js/clock/excanvas.js\"></script><![endif]-->";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/js/clock/moreskins.js\"></script>";
	$str = $str."<script type=\"text/javascript\" src=\"../codebase/plugins/keyboard/keyboard.js\" charset=\"UTF-8\"></script>";
	$str = $str."<link rel=\"stylesheet\" type=\"text/css\" href=\"../codebase/plugins/keyboard/keyboard.css\">";
		
	$str = $str."<script language=javascript src='../codebase/js/ui/hiddendiv.js' type=\"text/javascript\"></script>";
	$str = $str."<script language=javascript src='../codebase/js/ui/ui.js' type=\"text/javascript\"></script>";
	$str = $str."<link href=\"includes/templates/default/stylesheet.css\" rel='stylesheet'>";
	if (strlen(trim($ah)) > 0) {
		$str = $str.$ah;
	}
	$str = $str."</head>";
	return $str;
}

function bsheaders() {
	global $application, $title, $summary, $keywords;
	$keywords = isset($keywords) ? $keywords : 'Northern Corridor, Corridor, East Africa, Central Africa, Transport, Kenya, Uganda, Rwanda, Burundi, Democratic Republic of Congo, Port, Border, Observatory';
	$summary = isset($summary) ? $summary : 'The Northern Corridor comprises of the transport infrastructure, facilities and services in East and Central Africa linked to the Maritime Port of Mombasa. These primary transport network and facilities link the Port of Mombasa in Kenya to the Great Lakes countries of Uganda, Rwanda, Burundi and the Democratic Republic of Congo.';
	$str = $str."<html>";
		$str = $str."<head>";
		
			$str = $str."<title>".removehtml($application['organization']);
			if (strlen(trim($application['title'])) > 0) {
				$str = $str." - ".trim(removehtml($application['title']));
			}
			if (strlen(trim($title)) > 0) {
				$str = $str." - ".trim(removehtml($title));
			}
			$str = $str."</title>";
		
		$str = $str."<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
		$str = $str."<meta name=\"title\" content=\"$title\">";
		$str = $str."<meta name=\"description\" content=\"$summary\">";
		$str = $str."<meta name=\"keywords\" content=\"$keywords\">";
		$str = $str."<meta name=\"author\" content=\"@mainakibui\">";
		
		$str = $str."<link rel=\"shortcut icon\" href=\"/favicon.ico\" type=\"image/x-icon\">";
		$str = $str."<link rel=\"icon\" href=\"/favicon.ico\" type=\"image/x-icon\">";
		
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/jquery-ui.css\" rel=stylesheet>";
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/bootstrap-multiselect.css\" rel=stylesheet>";
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/prettify.css\" rel=stylesheet>";
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/slick.css\" rel=stylesheet>";
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/bootstrap.min.css\" rel=stylesheet>";
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/base.css\" rel=stylesheet>";
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/sticky-footer-navbar.css\" rel=stylesheet>";
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/font-awesome.min.css\" rel=stylesheet>";
		
		$str = $str."<!--Bootstrap tables-->";
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/bootstrap-table.css\" rel=stylesheet>";
		
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/bootstrap-multiselect.css\" rel=stylesheet>";
		
		$str = $str."<link href=\"../codebase/templates/bs-annie-template/css/custom.css\" rel=stylesheet>";
		
		$str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/ie10-viewport-bug-workaround.js\"></script>";
		$str = $str."<!--[if lt IE 9]>";
		$str = $str."<script type=\"text/javascript\" src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>";
		$str = $str."<script type=\"text/javascript\" src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>";
		$str = $str."<![endif]-->";
		
		$str = $str."<script type='text/javascript' src='https://www.google.com/jsapi'></script>";
		$str = $str."</head>";
		$str = $str."<body>";
	return $str;
}

function loginbuttons()
{
	if (loggedin()) {
		$str = $str."<a href='logout.php' class='btn btn-sm btn-danger language' role='button'> <i class='fa fa-sign-out'></i> Logout </a>&nbsp;|&nbsp;";
	}
	else
	{
		$str = $str."<a href='login.php' class='btn btn-sm btn-success language' role='button'> <i class='fa fa-sign-in'></i> Login </a>&nbsp;|&nbsp";
	}
	return $str;
}

function bsfooters() {

	    $str = $str."<div class=\"footer\">";
	    
		/*$str = $str."<div class=\"panel2\">";
		
			$str = $str."<div class=\"row\">";
				$str = $str."<!--container-->";
				$str = $str."<div class=\"container nopadding\">";
					$str = $str."<div class=\"col-sm-12 col-md-12 col-lg-12 nopadding\">";
							$str = $str."<div class=\"links\">";
								$str = $str."<div class=\"row\">";
								
									$str = $str."<a target=\"_blank\" href=\"http://www.ttcanc.org/\">";
										$str = $str."<div class=\"footer-quick-links col-md-3\">";
											$str = $str."Main Site";
										$str = $str."</div>";
									$str = $str."</a>";
									
									$str = $str."<a href=\"faq.php\">";
										$str = $str."<div class=\"footer-quick-links col-md-3\">";
											$str = $str."Faq";
										$str = $str."</div>";
									$str = $str."</a>";
									
									$str = $str."<a href=\"contactus.php\">";
										$str = $str."<div class=\"footer-quick-links col-md-3\">";
											$str = $str."Contact Us";
										$str = $str."</div>";
									$str = $str."</a>";
									
									$str = $str."<a href=\"sitemap.php\">";
										$str = $str."<div class=\"footer-quick-links col-md-3\">";
											$str = $str."Sitemap";
										$str = $str."</div>";
									$str = $str."</a>";
									
								$str = $str."</div>";
							$str = $str."</div>";
					$str = $str."</div>";
				$str = $str."</div>";
				$str = $str."<!--/container-->";
			$str = $str."</div>";
			
		$str = $str."</div><!--/panel2-->"; */
	
		$str = $str."<div class=\"panel3\">";
		      $str = $str."<div class=\"container nopadding\">";
			$str = $str."<div class=\"row\">";
				$str = $str."<div  class=\"col-sm-4 col-md-4 col-lg-4  nopadding\">";
				
					$str = $str."<div class=\"social-links\">";
						$str = $str."<ul class=\"social-networks\">";
							$str = $str."<li> <a href=\"#\" target=\"_blank\"><i class=\"twitter icon fa fa-twitter\"></i></a> </li>";
							$str = $str."<li> <a href=\"#\" target=\"_blank\"><i class=\"facebook icon fa fa-facebook\"></i></a> </li>";
							$str = $str."<li> <a href=\"#\" target=\"_blank\"><i class=\"youtube icon fa fa-youtube\"></i></a> </li>";
							$str = $str."<li> <a href=\"#\" target=\"_blank\"><i class=\"linkedin icon fa fa-linkedin\"></i></a> </li>";
						$str = $str."</ul>";
					$str = $str."</div>";
					
					$str = $str."<div class=\"clear quick-contacts\">";
						$str = $str."<ul class=\"contact-details\">";
							$str = $str."<li> P O Box 34068, Post - Code 80118 Mombasa, Kenya </li>";
							$str = $str."<li> +254 (0)41 4470734, 254 (0)41 2000881 </li>";
							$str = $str."<li> +254 (0)41 4470735 </li>";
							$str = $str."<li> <a href=\"mailto:ttca@ttcanc.org?Subject=Hello\" target=\"_top\"> ttca@ttcanc.org </a> </li>";
						$str = $str."</ul>";
					$str = $str."</div>";
					
					/*$str = $str."<div class=\"copyright clear\">";
						$str = $str."<p class=\"copyright\">&copy; 2015 NCTTCA - Transport Observatory Project</p>";
					$str = $str."</div>";*/
				$str = $str."</div>";
	
				$str = $str."<div  class='col-sm-2 col-md-2 col-lg-2  nopadding'>";
					$str = $str."<div class='footer2-links'>"; 
						$str = $str."<ul class='footer-nav-links'>";
							//$str = $str."<li><a target='_blank' href='http://www.ttcanc.org/'>Main Site</a></li>";
							$str = $str.($pagename=='stakeholdersarticle' ? '' : "<li><a target='_blank' href='stakeholders.php'>Stakeholders</a></li>");
							$str = $str.($pagename=='faq' ? '' : "<li><a target='_blank' href='faqs.php'>FAQ</a></li>");
							$str = $str.($pagename=='contactus' ? '' : "<li><a target='_blank' href='contacts.php'>Contact Us</a></li>");
							$str = $str.($pagename=='sitemap' ? '' : "<li><a target='_blank' href='sitemap.php'>Sitemap</a></li>");
							$str = $str.($pagename=='news' ? '' : "<li><a target='_blank' href='news.php'>News</a></li>");
							//$str = $str.($pagename=='calendar' ? '' : "<li><a target='_blank' href='calendar.php'>Events</a></li>");
						$str = $str."</ul>";	
				
					$str = $str."</div>";
				$str = $str."</div>";
				
				$str = $str."<div  class=\"col-sm-3 col-md-3 col-lg-3  nopadding\">";
						$str = $str."<img src=\"../images/ntcca-logo-top.png\" alt=\"Northern Corridor\" style=\"width: 100px;\">";
						$str = $str."<img src=\"../images/trademark-logo-top.png\" alt=\"Trademark East Africa\" style=\"width: 100px;\">";
				$str = $str."</div>";
	
				$str = $str."<div  class=\"col-sm-3 col-md-3 col-lg-3  nopadding\">";
					$str = $str."<!--subscribe-->";
						$str = $str."<form id=\"banner-form\" method=\"post\" action=\"subscribe.php\">";
							$str = $str."<div class=\"banner-optin\">";
								$str = $str."<div class=\"row\">";
									$str = $str."<div class=\"form-group col-md-12 nopadding\">";
										$str = $str."<input name=\"banner-name\" id=\"banner-name\" type=\"text\" class=\"form-control\" required=\"\" placeholder=\"Your Name\">";
									$str = $str."</div>";
									$str = $str."<div class=\"form-group col-md-12 nopadding\">";
										$str = $str."<input name=\"banner-email\" id=\"banner-email\" type=\"text\" class=\"form-control\" required=\"\" placeholder=\"Your e-mail\">";
									$str = $str."</div>";
									$str = $str."<div class=\"form-group col-md-12 nopadding\">";
										$str = $str."<button type=\"submit\" class=\"btn btn-default btn-submit\">Subscribe</button>";
									$str = $str."</div>";
								$str = $str."</div>";
							$str = $str."</div>";
							$str = $str."<div class=\"row\">";
								$str = $str."<div class=\"col-md-12\">";
									$str = $str."<div class=\"form-process\"></div><!-- Displays status when submitting form -->";
								$str = $str."</div>";
							$str = $str."</div>";
						$str = $str."</form>";     		
					$str = $str."<!--/subscribe-->";
				$str = $str."</div>";
			$str = $str."</div>";
			
			$str = $str."<div class='row'>";
				$str = $str."<div class='col-sm-12 col-md-12 col-lg-12  nopadding copyright clear'>";
					$str = $str."<p class='copyright'>&copy; ".date('Y')." Northern Corridor Transit and Transport Coordination Authority</p>";
				$str = $str."</div>";
			$str = $str."</div>";
			
		      $str = $str."</div>";
	      $str = $str."</div><!--/panel3-->";
	    $str = $str."</div><!--/footer-->";
	    
	    $str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/jquery.min.js\"></script>";
	    $str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/bootstrap.min.js\"></script>";
	    $str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/bootstrap-multiselect.js\"></script>";
	    $str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/jquery.easing.min.js\"></script>";
	    $str = $str."<script type=\"text/javascript\" src=\"//code.jquery.com/ui/1.11.1/jquery-ui.js\"></script>";
	    $str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/slick.js\"></script>";

	    $str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/jquery.masonry.min.js\"></script>";
	    $str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/base.js\"></script>";
	    $str = $str."<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>";
	    
	    $str = $str."<!--Bootstrap tables-->";
	    $str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/bootstrap-table.js\"></script>";
	    
	    $str = $str."<script type=\"text/javascript\" src=\"../codebase/templates/bs-annie-template/js/custom.js\"></script>";
	    $str = $str."<script type='text/javascript' src='../codebase/templates/bs-annie-template/js/jquery.dataTables.js'></script>";
	    $str = $str."<script type='text/javascript' src='../codebase/templates/bs-annie-template/js/dataTables.bootstrap.js'></script>";
	    
	  $str = $str."</body>";
	$str = $str."</html>";
	return $str;
}


function stakeholdersfooterslider(){
	$str = $str."<div class='stakeholders'>";
	$str = $str."<div class='container wrapper' style='padding: 0px 50px 0px 50px;'>";
		$str = $str."<a class='stakeholders-link' href='stakeholders.php'> <h3 class='stakeholders-title pull-left'> Stakeholders </h3> <small class='pull-left' style='margin-left: 10px;'><i class='fa fa-link'>&nbsp;view all</i></small> </a>";
		$str = $str."<div class='col-sm-12 col-md-12 col-lg-12 responsive'>";
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://kenha.co.ke' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/kenha.co.ke-b.png'' onmouseout='this.src='images/stakeholders/kenha.co.ke.png'' src='images/stakeholders/kenha.co.ke.png' data-src='images/stakeholders/kenha.co.ke.png' alt='kenha.co.ke'> <span class='stakeholders-site'>kenha.co.ke</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://kpa.co.ke' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/kpa.co.ke-b.png'' onmouseout='this.src='images/stakeholders/kpa.co.ke.png'' src='images/stakeholders/kpa.co.ke.png' data-src='images/stakeholders/kpa.co.ke.png' alt='kpa.co.ke'> <span class='stakeholders-site'>kpa.co.ke</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://kpc.co.ke' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/kpc.co.ke-b.png'' onmouseout='this.src='images/stakeholders/kpc.co.ke.png'' src='images/stakeholders/kpc.co.ke.png' data-src='images/stakeholders/kpc.co.ke.png' alt='kpc.co.ke'> <span class='stakeholders-site'>kpc.co.ke</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://kra.go.ke' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/kra.go.ke-b.png'' onmouseout='this.src='images/stakeholders/kra.go.ke.png'' src='images/stakeholders/kra.go.ke.png' data-src='images/stakeholders/kra.go.ke.png' alt='kra.go.ke'> <span class='stakeholders-site'>kra.go.ke</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://krc.co.ke' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/krc.co.ke-b.png'' onmouseout='this.src='images/stakeholders/krc.co.ke.png'' src='images/stakeholders/krc.co.ke.png' data-src='images/stakeholders/krc.co.ke.png' alt='krc.co.ke'> <span class='stakeholders-site'>krc.co.ke</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://kta.co.ke' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/kta.co.ke-b.png'' onmouseout='this.src='images/stakeholders/kta.co.ke.png'' src='images/stakeholders/kta.co.ke.png' data-src='images/stakeholders/kta.co.ke.png' alt='kta.co.ke'> <span class='stakeholders-site'>kta.co.ke</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://minifra.gov.rw' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/minifra.gov.rw-b.png'' onmouseout='this.src='images/stakeholders/minifra.gov.rw.png'' src='images/stakeholders/minifra.gov.rw.png' data-src='images/stakeholders/minifra.gov.rw.png' alt='minifra.gov.rw'> <span class='stakeholders-site'>minifra.gov.rw</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://obr.bi' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/obr.bi-b.png'' onmouseout='this.src='images/stakeholders/obr.bi.png'' src='images/stakeholders/obr.bi.png' data-src='images/stakeholders/obr.bi.png' alt='obr.bi'> <span class='stakeholders-site'>obr.bi</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://ogefrem.net' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/ogefrem.net-b.png'' onmouseout='this.src='images/stakeholders/ogefrem.net.png'' src='images/stakeholders/ogefrem.net.png' data-src='images/stakeholders/ogefrem.net.png' alt='ogefrem.net'> <span class='stakeholders-site'>ogefrem.net</span> </a> </div></div>";
			
			//$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://protek.com' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/protek.com-b.png'' onmouseout='this.src='images/stakeholders/protek.com.png'' src='images/stakeholders/protek.com.png' data-src='images/stakeholders/protek.com.png' alt='protek.com'> <span class='stakeholders-site'>protek.com</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://riftvalleyrail.com' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/riftvalleyrail.com-b.png'' onmouseout='this.src='images/stakeholders/riftvalleyrail.com.png'' src='images/stakeholders/riftvalleyrail.com.png' data-src='images/stakeholders/riftvalleyrail.com.png' alt='riftvalleyrail.com'> <span class='stakeholders-site'>riftvalleyrail.com</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://rra.gov.rw' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/rra.gov-b.png'' onmouseout='this.src='images/stakeholders/rra.gov.png'' src='images/stakeholders/rra.gov.png' data-src='images/stakeholders/rra.gov.rw.png' alt='rra.gov.rw'> <span class='stakeholders-site'>rra.gov.rw</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://rtda.gov.rw' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/rtda.gov-b.png'' onmouseout='this.src='images/stakeholders/rtda.gov.png'' src='images/stakeholders/rtda.gov.png' data-src='images/stakeholders/rtda.gov.rw.png' alt='rtda.gov.rw'> <span class='stakeholders-site'>rtda.gov.rw</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://unra.go.ug' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/unra.go.ug-b.png'' onmouseout='this.src='images/stakeholders/unra.go.ug.png'' src='images/stakeholders/unra.go.ug.png' data-src='images/stakeholders/unra.go.ug.png' alt='unra.go.ug'> <span class='stakeholders-site'>unra.go.ug</span> </a> </div></div>";
			
			$str = $str."<div class='stakeholder-logo-item'><div class='stakeholder-logo-item-inner'> <a href='http://ura.go.ug' target='_blank'> <img class='stakeholders-logo' onmouseover='this.src='images/stakeholders/ura.go.ug-b.png'' onmouseout='this.src='images/stakeholders/ura.go.ug.png'' src='images/stakeholders/ura.go.ug.png' data-src='images/stakeholders/ura.go.ug.png' alt='ura.go.ug'> <span class='stakeholders-site'>ura.go.ug</span> </a> </div></div>";
		$str = $str."</div>";
	$str = $str."</div>";
$str = $str."</div>";
return $str;

}

function headerize($m) {
	global $application, $mtitle;
	$str = $str."<html>";
	$str = $str.headers($hdr);
	$str = $str."<body topmargin=0 bgcolor=\"#ffffff\" bottommargin=0 leftmargin=0 rightmargin=0 marginheight=0 marginwidth=0>";
	$str = $str."<center><table border=\"0\" cellpadding=3 cellspacing=\"0\" width='100%' bgcolor=\"#ffffff\">";
	$str = $str."<tr valign=\"top\"><td background=\"includes/templates/".$application["template"]."/images/barbkg.gif\" height=35><h3>&nbsp;".$mtitle."</h3></td></tr>";
	$str = $str."<tr valign=\"top\"><td>";
	$str = $str.$m;
	$str = $str."</td></tr>";
	$str = $str."</table></center>";
	return $str;
}


function pagetitle() {
	global $mtitle, $page;
	if (strlen(trim($mtitle)) > 0) {
		$parentid = $page[2];
		$parent   = $page[3];
		if (strlen(trim($parent)) > 0) {
			if (strtolower(trim($parent)) != "root") {
				$mtitle = "<font class=textfaded>".translate($parent)."</font> > ".$mtitle;
			}	
			$mtitle = hashtable($mtitle);
		}
		$mtitle = "<br/><p><h1>".titlecase(translate($mtitle))."</h1></p></br>";
	}
	return $mtitle;
}
	


function copyright() {
	global $application;
	$str = $str."&copy;&nbsp;".year(getmydate())."&nbsp;".putspace(translate($application["title"].". All Rights Reserved"));
	return $str;
}


function language() {
	global $application, $dba;
	$sql = "select id, title, code from `language` where statusid = 1;";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		if (strlen(trim($_SESSION["language"])) == 0) {
			$_SESSION["language"] = $rs->row("code");
		}
		if ($rs->recordcount() > 1) {
			while (!$rs->eof()) {
				$str = $str."<a href=\"language.php?id=".$rs->row("id")."\"><b>".$rs->row("title")."</b></a>";
				$rs->movenext();
				if (!$rs->eof()) {
					$str = $str."&nbsp;<font class=\"textfaded\">&nbsp;|&nbsp;</font>&nbsp;";
				}
			}
		}
	}
	return $str;
}

function displayhome($m, $template = "default") {
	if (strlen(trim($template)) > 0) {
		if (file_exists("includes/templates/".$template."/main-home.tpl")) {
			$tpl = file_get_contents("includes/templates/".$template."/main-home.tpl");
			if (strlen(trim($tpl)) > 0) {
				$a = array();
				preg_match_all("{{.*}}", $tpl, $a);
				$tpl = callfn($tpl, $a);

				$tpl = str_replace("<img src=", "<img src={{template}}", $tpl);
				$tpl = str_replace("src={{template}}\"", "src=\"{{template}}", $tpl);
				$tpl = str_replace("src={{template}}'", "src='{{template}}", $tpl);
				$tpl = str_replace("{{template}}", "includes/templates/".$template."/images/", $tpl);
				$tpl = str_replace("{{stylesheet}}", "<link href=\"includes/templates/".$template."/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\" />", $tpl);
				$tpl = str_replace("{{content}}", $m, $tpl);
				
				$tpl = preg_replace("{{.*}}", "", $tpl);
				$m = $tpl;


			}
		}
	}
	
	print($m);	
}

function display($m, $template = "default") {
	if (strlen(trim($template)) > 0) {
		if (file_exists("includes/templates/".$template."/main.tpl")) {
			$tpl = file_get_contents("includes/templates/".$template."/main.tpl");
			if (strlen(trim($tpl)) > 0) {
				$a = array();
				preg_match_all("{{.*}}", $tpl, $a);
				$tpl = callfn($tpl, $a);

				$tpl = str_replace("<img src=", "<img src={{template}}", $tpl);
				$tpl = str_replace("src={{template}}\"", "src=\"{{template}}", $tpl);
				$tpl = str_replace("src={{template}}'", "src='{{template}}", $tpl);
				$tpl = str_replace("{{template}}", "includes/templates/".$template."/images/", $tpl);
				$tpl = str_replace("{{stylesheet}}", "<link href=\"includes/templates/".$template."/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\" />", $tpl);
				$tpl = str_replace("{{content}}", $m, $tpl);
				
				$tpl = preg_replace("{{.*}}", "", $tpl);
				$m = $tpl;


			}
		}
	}
	
	print($m);	
}



function callfn($tpl, $a) {
	foreach ($a as $i => $j) {
		$result = false;
		if (is_array($j)) {
			$tpl = callfn($tpl, $j);
		}
		else {
			$f = strtolower(trim($j));
			$f = str_replace("}", "", str_replace("{", "", $f));
			$f = trim($f);
			$v = null;
			if (strlen(trim($f)) > 0) {
				if (strpos($f, ")") != false && strpos($f, "(") != false) {
					$s = strpos($f, "(");
					$e = strrpos($f, ")");
					$v = trim(substr($f, $s + 1, $e - ($s + 1)));
					$f = trim(substr($f, 0, $s));
					if (strlen(trim($v)) > 0) {
						$v = explode(",", $v);
						for ($i = 0; $i < sizeof($v); $i++) {
							$v[$i] = str_replace("'", "", str_replace("\"", "", trim($v[$i])));
						}
					}
				}
				if (!is_array($v)) {
					$v = array();
				}
				if (is_callable($f)) {
					$result = call_user_func_array($f, $v);
				}
				else {
					if (strlen(trim($$f)) > 0) {
						$result = $$f;
					}
				}
				if ($result != false) {
					$tpl = str_replace($j, $result, $tpl);
				}
			}
		}
	}
	return $tpl;
}


function reconstruct_get() {
	$g = "";
	foreach ($_GET as $i=>$j) {
		$a = $j;
		if (is_array($a)) {
			if (sizeof($a) > 0) {
				$a = implode(",", $a);
				$g = $g."<input type=hidden name=\"".$i."[]"."\" value=\"".$a."\">";
			}
		}
		else {
			$a = trim($a);
			if (strlen(trim($a)) > 0) {
				$g = $g."<input type=hidden name=\"".$i."\" value=\"".$a."\">";
			}
		}
	}			
	return $g;
}

function searchbox($keywords, $url = "search.php") {
	$a = $_SERVER["QUERY_STRING"];
	$str = $str."<form class=\"niceform\" method=\"get\" action=\"".$url."\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td><font class=\"textsmall\">".translate("Search")."</font></td><td>&nbsp;</td><td><input type=\"text\" name=\"keywords\" id=\"keywords\" class=\"small\" value=\"".$keywords."\" size=\"15\" /></td><td><input type=\"submit\" value=\"".translate("search")."\" /></td></tr></table></form>";//".reconstruct_get()."
	return $str;
}


function button($m) {
	if (strlen($m) > 0) {
		if (trim(strtolower($m)) == trim(strtolower(removehtml($m)))) {
			$m = putspace($m);
		}
		$m = "<span class=button>".$m."</span>";
	}
	return $m;
}

function showpopup($db, $id, $title, $mtype=0, $btitle=null) {
	return "&nbsp;<a href=\"record.php?a=".urlencode($mtype)."&t=".urlencode($db)."&cid=".urlencode($id)."&title=".urlencode($title)."\" onclick=\"return parent.GB_show('".str_replace("\"", "". str_replace("'", "", $title))."', this.href, 580, 780)\">$btitle</a>&nbsp;";
}


function showajax($div, $url, $evt) {
	return " $evt=\"new Ajax.Updater('$div', '$url', { method: 'get' })\"";
}


function messagebox($m, $b = null, $heading = null) {
	global $application;
	$heading = translate($heading);
	$m = translate($m);
	if (is_bool($b)) {
		if ($b) {
			$sheet = "alert alert-success";
			$heading = "<img src=\"includes/templates/".$application["template"]."/images/tick.png\" width=30 border=\"0\">";
		}
		else {
			$sheet = "alert alert-danger";
			$heading = "<img src=\"includes/templates/".$application["template"]."/images/alert.png\" width=30 border=\"0\">";
		}
	}
	else {
		$sheet = "info";
		$heading = "<img src=\"includes/templates/".$application["template"]."/images/info.png\" width=30 border=\"0\">";
		$bg = "f9f9f9";
	}	
	$str = $str."<div class='".$sheet." col-sm-12 col-md-12 col-lg-12' role='alert'>";
		if (strlen(trim($heading)) > 0) {
			$str = $str."<div class='col-sm-2 col-md-2 col-lg-2'>".$heading."</div>";
		}	
		$str = $str."<div class='col-sm-10 col-md-10 col-lg-10'>".$m."</div>";
	$str = $str."</div>";
	return $str;
}


function get_default($a, $b, $c) {
	if (strlen(trim($a)) == 0) {
		return $c;
	}
	else {
		if (strtolower(trim($b)) == "n") {
			if (!is_numeric($a)) {
				return $c;
			}
			else {
				return trim($a);
			}
		}
		else {
			return $a;
		}
	}
}


function get_script_name() {
	$s = $_SERVER["SCRIPT_NAME"];
	$sb = explode('/', $s);
	$s = $sb[count($sb) - 1];
	return $s;
} 


function get_page($u) {
	global $dba;
	$page[0] = "";
	$page[1] = "";
	$page[2] = "";
	$page[3] = "";
	$page[4] = "";
	$page[5] = "";
	if (strlen(trim($u)) > 0) {
		$sql = "select id, title, pageid, (select title from page where id=pageid limit 1) as parent, content, accesscontrol from page where lower(trim(url)) like '".strtolower(trim($u))."%' order by id desc limit 1;";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			$page[0] = $rs->row(0);
			$page[1] = $rs->row(1);
			$page[2] = $rs->row(2);
			$page[3] = $rs->row(3);
			$page[4] = $rs->row(4);
			$page[5] = $rs->row(5);
		}
	}
	return $page;
}


function geticon($a) {
	$a = strrev($a);
	$a = substr($a, 0, strpos($a, "."));
	$a = trim(strtolower(strrev($a)));
	$icon = "icon_unknown.gif";
	switch(trim($a)) {
		case "doc":
		$icon = "icon_doc.gif";
		break;
		case "docx":
		$icon = "icon_doc.gif";
		break;
		case "pdf":
		$icon = "icon_pdf.gif";
		break;
		case "zip":
		$icon = "icon_zip.gif";
		break;
		case "xls":
		$icon = "icon_xls.gif";
		break;
		case "jpg":
		$icon = "icon_jpg.gif";
		break;
		case "jpeg":
		$icon = "icon_jpg.gif";
		break;
		case "png":
		$icon = "icon_jpg.gif";
		break;
		case "gif":
		$icon = "icon_jpg.gif";
		break;
		case "bmp":
		$icon = "icon_jpg.gif";
		break;
		case "tiff":
		$icon = "icon_jpg.gif";
		break;
		case "tif":
		$icon = "icon_jpg.gif";
		break;
		case "ppt":
		$icon = "icon_ppt.gif";
		break;
	}
	return $icon;
}


function roundedbox($m) {
	$str = $str."<div><b class=\"spiffy\"><b class=\"spiffy1\"><b></b></b><b class=\"spiffy2\"><b></b></b><b class=\"spiffy3\"></b><b class=\"spiffy4\"></b><b class=\"spiffy5\"></b></b> <div class=\"spiffyfg\">";
	$str = $str.$m;
 	$str = $str."</div><b class=\"spiffy\"><b class=\"spiffy5\"></b><b class=\"spiffy4\"></b><b class=\"spiffy3\"></b><b class=\"spiffy2\"><b></b></b><b class=\"spiffy1\"><b></b></b></b></div>";
	return $str;
}


function gettables() {
	global $database, $dba;
	$tb = array();
	$excludedtables = "*";
	$sql = "show full tables from $database where lower(trim(table_type))='base table';";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		while (!$rs->eof()) {
			if (trim($excludedtables) == "*") {
				array_push($tb, $rs->row(0));
			}
			else {
				if (substr_count(",".strtolower($excludedtables).",", ",".strtolower($rs->row(0)).",") > 0) {
					array_push($tb, $rs->row(0));
				}
			}
			$rs->movenext();
		}
		return $tb;
	}
	else {
		return null;
	}
}


function gettablestats($tb) {
	global $database, $dba;
	$f = "name,engine,rows,data_length,index_length,create_time,update_time";
	$f = explode(",", $f);
	if (strlen(trim($tb)) > 0) {
		$sql = "show table status from `$database` like '$tb' ";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			while (!$rs->eof()) {
				for ($i = 0; $i < sizeof($rs->fields); $i++) {
					if (array_search(trim(strtolower($rs->fields[$i])), $f) != false && strlen(trim($rs->row($i))) > 0) {
						$str = $str."<tr valign=\"top\"><td><span class=neatbox-field>".putspace(strtoupper(str_replace("_", " ", $rs->fields[$i])))."</span></td><td width='100%'><span class=neatbox-text>".putspace(formatmynumber($rs->row($i)))."</span></td></tr>";
					}
				}
				$rs->movenext();
			}
		}
	}
	if (strlen(trim($str)) > 0) {
		$str = "<table cellpadding=2 cellspacing=2 class=neatbox width='100%'><tr><td colspan=2><span class=neatbox-title>".translate("Database Statistics")."</span><hr size=\"1\" class=neatbox-hr noshade></td></tr>".$str."</table>";
	}
	return $str;
}


function getarrayid($a, $b) {
	$id = 0;
	if (is_array($a)) {
		for ($i = 0; $i < sizeof($a); $i++) {
			if (strtolower(trim($a[$i])) == strtolower(trim($b))) {
				$id = $i;
				break;
			}
		}
	}
	return $id;
}
	


function getfields($t) {
	global $dba;
	$fd = array(); //0 = dropdown, 1 = varchar, 2 = blob+others
	$sql = "select * from `$t` limit 1";
	$rs = $dba->execute($sql);
	for ($i = 0;$i<sizeof($rs->fields);$i++) {
		if (strlen(trim($rs->fields[$i])) > 0) {
			$d3 = $d3.",".strtolower($rs->fields[$i]);
			if (strlen($rs->fields[$i]) > 2 && substr(strtolower($rs->fields[$i]), strlen($rs->fields[$i]) - 2, 2) == "id") {
				$d0 = $d0.",".strtolower($rs->fields[$i]);
			}
			else {
				if (strtolower($rs->types[$i]) == "blob") {
					$d2 = $d2.",".strtolower($rs->fields[$i]);
				}
				else {
					$d1 = $d1.",".strtolower($rs->fields[$i]);
				}
			}
		}
	}
	$d0 = trim($d0);
	$d1 = trim($d1);
	$d2 = trim($d2);
	$d3 = trim($d3);
	if (substr($d0, 0, 1) == ",") {
		$d0 = substr($d0, 1, strlen($d0));
	}
	if (substr($d1, 0, 1) == ",") {
		$d1 = substr($d1, 1, strlen($d1));
	}
	if (substr($d2, 0, 1) == ",") {
		$d2 = substr($d2, 1, strlen($d2));
	}
	if (substr($d3, 0, 1) == ",") {
		$d3 = substr($d3, 1, strlen($d3));
	}
	array_push($fd, $d0);
	array_push($fd, $d1);
	array_push($fd, $d2);
	array_push($fd, $d3);
	return $fd;
}

function getid($t) {
	$tsp = time();
	$mct = microtime(true);
	$mct = explode(".", $mct);
	$mct = substr($mct[1], 0, 4);
	$mct = pad($mct, 3);
	$smt = getrandom(1000, 9999);
	$id = $tsp.$mct.$smt;
	return $id;	
}

function getfirstid($t) {
	global $database, $dba;
	$result = 1;
	$sql = "select id from `$t` order by id limit 1";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		if (is_numeric($rs->row(0))) {
			$result = (int)$rs->row(0);
		}
	}
	return $result;
}

function getcount($tb, $ft=null) {
	global $dba;
	if (strlen($tb) > 0) {
		$sql = "select count(*) from `$tb`";
		if (strlen(trim($ft)) > 0) {
			$sql = $sql." where $ft ";
		}
		$sql = $sql.";";
		$rs = $dba->execute($sql);
		if (!$rs->eof()) {
			return $rs->row(0);
		}
		else {
			return 0;
		}
	}
	else {
		return 0;
	}
}


function runquery($sql) {
	global $dba;
	$result = false;
	if (strlen(trim($sql)) > 0) {
		$rs = $dba->execute($sql);
		if ($dba->querystatus) {
			$result = true;
		}
	}
	return $result;
}


function getmyfield($tb, $v) {
	global $dba;
	$fields = getfields($tb);
	$fields = $fields[3];
	$fields = explode(",", $fields);
	$msql = "select `".$fields[1]."` from `".$tb."` where `id`='".$v."';";
	$rs = $dba->execute($msql);
	if (!$rs->eof()) {
		return $rs->row(0);
	}
}

function getmyfieldid($tb, $v, $fd) {
	global $dba;
	$msql = "select `".$fd."` from `".$tb."` where `id`='".$v."';";
	$rs = $dba->execute($msql);
	if (!$rs->eof()) {
		return $rs->row(0);
	}
}


function geteventlog($tb, $rc) {
	global $dba;
	$result = "-";
	if (strlen($tb) > 0 && strlen($rc) > 0 && is_numeric($rc)) {
		$msql = "select `username`, `action`, `date` from `eventlog` where `table`='$tb' and `record`='$rc';";
		$rs = $dba->execute($msql);
		if (!$rs->eof()) {
			$result = $rs->row(2);
		}
	}
	return $result;
}




function record($t, $cid, $s, $title, $picture = null) {
	global $dba;
	$t = clean($t);
	$cid = clean($cid);
	$s = clean($s);
	$str = $str."<div align=right><font class=textfaded><b>".translate($title)."</b>&nbsp;&nbsp;&nbsp;".putspace(translate("Database: ".$t))."&nbsp;&nbsp;&nbsp;".translate("Record").":&nbsp;".$cid."</font></div><br/>";
	if (loggedin()) {
		$skip = "joindate,logindate,expirydate,ip";
		if (trim($s) == "2") {
			//$display = "accounttypeid,username,statusid,accountid";
			$display = "username,accountid";
		}
		$skip = explode(",", $skip);
		$display = explode(",", $display);
		
		if (is_numeric($cid) && is_numeric($s) && strlen($t) > 0) {
			$s = $s + 0;
			if ($s == 1 || $s == 2) {
				if ($s == 1) {
					$sql = "select * from `$t` limit 1;";
					$button = "Save New $t Item";
				}
				if ($s == 2) {
					$sql = "select * from `$t` where id='$cid' limit 1;";
					$button = "Update Item";
				}
				$rs = $dba->execute($sql);
				if ($s == 2 && $rs->eof()) {
					$str = $str.messagebox("Requested record is not available for editing", false);
				}
				else {
					$str = $str."<center><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='98%'><form method=post class=\"niceform\" name=record id=record action=recordupdate.php><tr><td height=400 valign=\"top\">";
					$str = $str."<center><table border=\"0\" cellpadding=2 cellspacing=\"0\" width='98%'>";
					$mcount = 0;
					for ($i = 1; $i < $rs->fieldcount(); $i++) {
						$name = strtolower(trim($rs->fields[$i]));
						$type = strtolower(trim($rs->types[$i]));
						$value = null;
						if ($s + 0 == 2) {
							$value = $rs->row($i);
						}
						
						if (in_array(strtolower(trim($name)), $skip)) {
							//do nothing
						}
						else {
							if (in_array(strtolower(trim($name)), $display)) {
								$value = $rs->row($i);
								if (strtolower(substr($rs->fields[$i], strlen($rs->fields[$i]) - 2, 2)) == "id" && strlen(trim($rs->fields[$i])) > 2) {
									$name = substr($rs->fields[$i], 0, strlen($rs->fields[$i]) - 2);
									$value = getmyfield($name, $value);
								}
							}
							else {
						
								if (strlen($name) > 2 && substr($name, strlen($name)-2, 2) == "id") {
									/*
									if (!is_numeric($value)) {
										$value = 0;
									}
									else {
										$value = (int) $value;
									}
									*/ 
									$name = ucwords(substr($name, 0, strlen($name)-2));
									$value = dropdown(strtolower($name."id"), $value);
								}
								else {
									$name = strtolower(ucwords($name));
									if (strtolower($type) == "blob") {
										if (strtolower(trim($name)) == "indicator") {
											$value = multiselect($name, $value);
										}
										else {
											$value = textarea($name, $value);
										}
									}
									else {
										if (strtolower($type) == "datetime") {
											$value = datefield($name, $value);
										}
										else {
											if (strtolower($type) == "long" && strtolower($name) == "active") {
												$value = truefalse($name, $value);
											}
											else {
												if (strtolower(substr($name, strlen($name) - 8, strlen($name))) == "password") {
													$value = passwordfield($name, $value);
												}
												else {
													if (strtolower(trim($name)) == "accesscontrol") {
														$value = multiselect($name, $value);
													}
													else {
														if (strtolower(trim($name)) == "image") {
															$value = filedropdownfield($name, $value, "../ncttca/images");
														}
														else {
															if (strtolower(trim($name)) == "documentfile") {
																$value = filedropdownfield($name, $value, "../ncttca/documents");
															}
															else {
																$value = textfield($name, $value);
															}
														}
													}
												}		
											}
										}
									}
								}

							}
						
							$str = $str."<tr valign=\"top\">";
							$str = $str."<td><font class=fieldname>".putspace(titlecase(translate(hashtable($name))))."</font></td>";
							$str = $str."<td><font class=texthighlighted>".$value."</font></td>";
							$str = $str."</tr>";
						}
					}
					$str = $str."</table></center>";
					$str = $str."</td>";
					if ($picture == true && trim($s) == "2") {
						$str = $str."<td valign=\"top\">".picturefield($t, $cid)."</td>";
						set_session("picturefield", trim(strtolower($t))."_".trim($cid).".jpg");
					}
					$str = $str."</tr></table></center>";
					$str = $str."<div align=right><table border=\"0\"><tr valign=\"top\"><td height='100%' colspan=3 align=right>".button("<a onmouseover=\"this.style.cursor='pointer';\" onclick=\"document.record.submit();\"><b><font class=textbright>".translate($button)."</font></b></a>")."</td></tr><input type=hidden name=id value=\"".$cid."\"><input type=hidden name=t value=\"".$t."\"><input type=hidden name=a value=\"".$s."\"></form></table></div>"; //parent.parent.GB_hide()

				}
			}
			else {
				$str = $str.messagebox("The requested directive is not available", false);
			}
		}
		else {
			$str = $str.messagebox("invalid parameters specified", false);
		}
	}
	else {
		$str = $str.messagebox("not logged in", false);
	}
	return $str;
}

function is_sha1($s) {
	return (bool) preg_match('/^[0-9a-f]{40}$/i', $s);
}

function updaterecord($t, $cid, $a) {
	global $database, $dba;
	$cols = array();
	$values = array();
	$mflag = false;
	$action = "";
	foreach ($_POST as $i=>$j) {
		if (trim(strtolower($i)) != "cid" && trim(strtolower($i)) != "x" && trim(strtolower($i)) != "y" && trim(strtolower($i)) != "t" && trim(strtolower($i)) != "id" && trim(strtolower($i)) != "a") {
			if (is_array($_POST[$i])) {
				$tvalue = ",";
				foreach ($_POST[$i] as $q) {
					$tvalue = $tvalue.trim($q).",";
				}
				$_POST[$i] = $tvalue;
			}
			$v = $_POST[$i];
			if (strtolower(trim($i)) == "password") {
				if (!is_sha1($v)) {
					$v = sha1(strtolower(trim($v)));
				}
			}
			array_push($cols, $i);
			array_push($values, $v);
		}
	}
	if (sizeof($cols) > 0) {
		if (trim($a) == "1") {
			$sql = $sql."insert into `".$t."` (";
			$val = "";
			for ($i = 0; $i < sizeof($cols); $i++) {
				$sql = $sql."`".$cols[$i]."`";
				$val = $val."'".mmysql_real_escape_string($values[$i])."'";
				if ($i + 1 < sizeof($cols)) {
					$sql = $sql.",";
					$val = $val.",";
				}
			}
			$cid = getid($t);
			$sql = $sql.", id) values (".$val.",'".$cid."')";
			$action = "Added";
		}
		else {
			$sql = $sql."update `".$t."` set ";
			for ($i = 0; $i < sizeof($cols); $i++) {
				$sql = $sql."`".$cols[$i]."`"."='".mmysql_real_escape_string($values[$i])."'";
				if ($i + 1 < sizeof($cols)) {
					$sql = $sql.",";
				}
			}
			$sql = $sql." where id=".$cid.";";
			$action = "Edited";
		}
		//print $sql;
		$rs = $dba->execute($sql);
		if ($dba->querystatus) {
			$mflag = true;
		}

		$sql = "delete from `eventlog` where `table`='$t' and `record`='$cid';";
		$rsevtd = $dba->execute($sql);
		$evtsql = "insert into `eventlog` (`id`, `table`, `record`, `action`, `accountid`, `date`, `ip`) values ('".getid("eventlog")."', '$t', '$cid', '$action','".user("id")."',now(),'".getip()."');";
		$rs = $dba->execute($evtsql);
	}
	return $mflag;
}


function mmysql_real_escape_string($m) {
	if (strlen($m) > 0) {
		//$m = mysql_real_escape_string($m);
		$m = stripslashes($m);
		$m = str_replace("'", "\'",$m);
	}
	return $m;
}

function deleterecord($t, $cid, $a) {
	global $database, $dba;
	$mflag = false;
	$action = "Deleted";
	if (loggedin()) {
		if (trim($a) == "3" && is_numeric($cid)) {
			$sql = "delete from `".$t."` where id=".$cid.";";
			$rs = $dba->execute($sql);
			if ($dba->querystatus) {
				$mflag = true;
			}
			$sql = "delete from `eventlog` where `table`='$t' and `record`='$cid';";
			$rsevtd = $dba->execute($sql);
			$evtsql = "insert into `eventlog` (`id`, `table`, `record`, `action`, `username`, `date`, `status`) values ('".getid("eventlog")."', '$t', '$cid', '$action','".user("username")."',now(),'".str_replace(false,"0", str_replace(true,"1",$mflag))."');";
			$rs = $dba->execute($evtsql);
		}
	}
	return $mflag;
}




function form($t, $cid, $s, $title, $style, $excludedfields) {
	global $dba;
	$t = clean($t);
	$cid = clean($cid);
	$s = clean($s);
	$excluded = explode(",", $excludedfields);
	if (loggedin()) {
		if (is_numeric($cid) && is_numeric($s) && strlen($t) > 0) {
			$s = $s + 0;
			if ($s == 0 || $s == 1) {
				if ($s == 0) {
					$sql = "select * from $t;";
					$button = "Save New $t Item";
				}
				if ($s == 1) {
					$sql = "select * from $t where id=$cid ;";
					$button = "Update Item";
				}
				$rs = $dba->execute($sql);
				if ($s == 1 && $rs->eof()) {
					$str = $str.messagebox("Requested record is not available for editing", false);
				}
				else {
					//$str = $str."<center><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width='100%'><form method=post name=record id=record action=recordupdate.php><tr><td valign=\"top\">";
					//$str = $str."<center><table border=\"0\" cellpadding=2 cellspacing=\"0\" width='100%'>";
					$mcount = 0;
					for ($i = 1; $i < $rs->fieldcount(); $i++) {
						$name = trim(strtolower($rs->fields[$i]));
						$type = trim(strtolower($rs->types[$i]));
						$value = null;
						$xflag = false;

						if (sizeof($excluded) > 0) {
							for ($q = 0; $q < sizeof($excluded); $q++) {
								if (strlen(trim($excluded[$q])) > 0) {
									if (trim(strtolower($name)) == trim(strtolower($excluded[$q]))) {
										$xflag = true;
										break;
									}
								}
							}
						}
						if ($xflag == false) {
							if ($s + 0 == 1) {
								$value = $rs->row($i);
							}
							if (strlen($name) > 2 && substr($name, strlen($name)-2, 2) == "id") {
								/*
								if (!is_numeric($value)) {
									$value = 0;
								}
								else {
									$value = (int) $value;
								}
								*/ 
								echo $name;
								$name = substr($name, 0, strlen($name)-2);
								$value = dropdown(strtolower($name."id"), $value);
							}
							else {
								if (strtolower($type) == "blob" || strtolower($name) == "response" || strtolower($name) == "usage" ) {
									$value = textarea($name, $value);
								}
								else {
									if (strtolower($type) == "datetime") {
										$value = datefield($name, $value);
									}
									else {
										if (strtolower($type) == "") {
											$value = truefalse($name, $value);
										}
										else {
											$value = textfield($name, $value);
										}
									}
								}
							}
							$displayfield = "<label for=\"".$name."\">".translate(hashtable($name))."</label>";
							if ($mcount == 0) {
								$str = $str."<tr valign=\"top\">";
							}
							if (strtolower($type) == "blob") {
								if ($mcount > 0) {
									$str = $str."<td colspan=".(2 - $mcount).">&nbsp;</td></tr>";
									$str = $str."<tr valign=\"top\">";
									$mcount = 0;
								}
								$str = $str."<td colspan=3>".$displayfield."<br/>".$value."</td>";
								$mcount = $mcount + 3;
							}
							else {
								if (trim($style) == "1") {
									$str = $str."<td>".$displayfield."<br/>".$value."</td>";
								}
								else {
									$str = $str."<td>".$displayfield."</td><td width='100%'>".$value."</td>";
								}
								$mcount = $mcount + 1;
							}
							if (trim($style) == "1") {
								if ($mcount == 3) {
									$str = $str."</tr>";
									$mcount = 0;
								}
							}
							else {
								$str = $str."</tr>";
								$mcount = 0;
							}
						}
					}



					if ($mcount > 0) {
						$str = $str."<td colspan=".(2 - $mcount).">&nbsp;</td></tr>";
						$mcount = 0;
					}
					//$str = $str."</table></center>";
					//$str = $str."</td></tr></table></center>";
				}
			}
			else {
				$str = $str.messagebox("The requested directive is not available", false);
			}
		}
		else {
			$str = $str.messagebox("invalid parameters specified", false);
		}
	}
	else {
		$str = $str.messagebox("not logged in", false);
	}
	return $str;
}









function textfield($name, $value, $size = 50) {
    return "<input type=\"text\" name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" size=\"".$size."\" />";
}

function filefield($name, $value, $size = 22) {
	$str = $str."<input type=\"text\" name=\"file_tmp_".$name."\" id=\"file_tmp_".$name."\" class=\"filefield_textbox\" readonly=\"readonly\" />";
	$str = $str."<div class=\"filefield_div\">";
	$str = $str."<input type=\"button\" value=\"".translate("Select File")."\" class=\"filefield_button\" />";
	$str = $str."<input type=\"file\" class=\"filefield_hidden\" name=\"".$name."\" onchange=\"javascript:document.getElementById('file_tmp_".$name."').value = this.value\" />";
	$str = $str."</div>";
	return $str;
}

function filedropdownfield($name, $value, $dir) {
	if (strlen(trim($dir)) > 0) {
		if (is_dir($dir)) {
			$files = scandir($dir);
			foreach ($files as $file) {
				if (!is_dir($file) && $file != '.' && $file != '..') {
					$str = $str."<option value=\"".$file."\"";
					if (trim($value) == trim($file)) {
						$str = $str." selected";
					}
					//$fn = explode(".", $file);
					$str = $str.">".ucwords($file)."</option>";
				}
			}
			if (strlen(trim($str)) > 0) {
				$str = "<select name=\"".$name."\" id=\"".$name."\"><option value=\"\">--- Select ".hashtable($name)." ---</option>".$str."</select>";
			}
		}
	}
	return $str;
}

function passwordfield($name, $value, $size = 30, $confirm = false) {
	$str = $str."<input type=password name=\"".$name."\" id=\"".$name."\" value=\"".$value."\" size=\"".$size."\" maxlength=50 />";
	if ($confirm == true) {
		$str = $str."<br/><small><font class=textalert>".putspace(translate("Confirm Password"))."</font></small><br/><input type=password name=\"".$name."2\" id=\"".$name."2\" value=\"".$value."\" size=\"".$size."\" maxlength=50 />";
	}
	return $str;
}


function textarea($name, $value, $cols='50', $rows='1') {
    if (strtolower(trim($name)) == "content") {
        return "<textarea  name=\"".$name."\" id=\"".$name."\" class=\"mceEditor\" rows=\"10\" cols=\"75\">".$value."</textarea>";
    }
    else {
        return "<textarea name=\"".$name."\" id=\"".$name."\" cols=\"$cols\" rows=\"$rows\">".$value."</textarea>";
    }
}


function smstextarea($name, $value) {
	return "<textarea name=\"".$name."\" id=\"".$name."\" cols=60 onkeyup=\"gencount('".$name."')\" rows=7 wrap=soft>".$value."</textarea><br/><input value=".$msize." size=2 name=msgcount disabled>";
}


function phonefield($countryid, $prefix, $phone) {
	return "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>".dropdown_extended("countryid", $countryid, false, "id, code","id > 0 order by code")."</td><td>-</td><td>"."<input type=\"text\" name=prefix id=\"prefix\" size=3 value=\"".$prefix."\" maxlength=3 /></td><td>-</td><td><input type=\"text\" name=phone id=\"phone\" size=7 value=\"".$phone."\" maxlength=7 /></td></tr></table>";
}

function picturefield($t, $cid) {
	$str = $str."<table border=\"0\" cellpadding=3>";
	if (file_exists("images/snapshots/".strtolower(trim($t))."_".trim($cid).".jpg")) {
		$str = $str."<tr><td align=\"center\"><table border=\"0\" cellpadding=3 class=neatbox><tr><td><img src=\"images/snapshots/".strtolower(trim($t))."_".trim($cid).".jpg\" border=\"0\" width=200></td></tr></table></td></tr>";
	}
	$str = $str."<tr><td>";
	$str = $str."<center><h3><b>".translate("Capture Photograph")."</b></h3>".messagebox("Click on the \"Allow\" button to activate the camera. Aim your camera at the individual and click the \"Take Snapshot\" button to capture the photo")."</center><OBJECT classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\" WIDTH=\"250\" wmode=window HEIGHT=\"200\" id=\"../codebase/plugins/camera/croflash.swf\" ALIGN=\"\">";
	$str = $str."<PARAM NAME=\"".$name."\" VALUE=\"../codebase/plugins/camera/croflash.swf\"><param name=wmode value=window><PARAM NAME=quality VALUE=high> <PARAM NAME=bgcolor VALUE=#f0f0f0> <EMBED src=\"../codebase/plugins/camera/croflash.swf\" quality=high bgcolor=#f0f0f0 WIDTH=\"250\" HEIGHT=\"200\" NAME=\"../codebase/plugins/camera/croflash.swf\" ALIGN=\"\" TYPE=\"application/x-shockwave-flash\" PLUGINSPAGE=\"http://www.macromedia.com/go/getflashplayer\"></EMBED> </OBJECT>";
	$str = $str."</td>";
	$str = $str."</tr></table>";
	
	return $str;
}		


function multiselect($name, $value, $t = null, $b = 2, $onchange = false, $o = "", $default = false, $ct = null, $ft = null) {
	global $dba;
	$tb = $name;
	if (substr(strtolower($tb), strlen($tb) - 2, 2) == "id") {
		$tb = substr(strtolower($tb), 0, strlen($tb) - 2);
	}

	$fields = getfields($tb);
	$fields = $fields[3];
	$fields = explode(",", $fields);
	
	if (strlen(trim($o)) == 0) {
		$o = $fields[1];
	}
	
	if (strlen(trim($ct)) > 0) {
		$sql = "select sql_no_cache distinct `".$tb."`.".$fields[0].", `".$tb."`.".$fields[1].", count(*) as totalc from `".$tb."` inner join `".$ct."` on `".$ct."`.".$tb."id=`".$tb."`.id ";
		if (strlen(trim($ft)) > 0) {
			$sql = $sql." where ".substr($ft, 5, strlen($ft))." ";
		}
		$sql = $sql." ";
		//$sql = "select `".$tb."`.* from `".$tb."` ";
		//if (strlen(trim($ft)) > 0) {
		//	$sql = $sql." where id in (select ".$tb."id from `".$ct."` where id > 0 ".$ft." group by ".$tb."id) ";
		//}

		$sql = $sql." group by  `".$tb."`.".$o." ";
		$sql = $sql." order by `".$tb."`.".$o.";";
	}
	else {
		$sql = "select `".$tb."`.".$fields[0].", `".$tb."`.".$fields[1]." from `".$tb."` order by $o;";
	}
	//print $sql;
	$rsm = $dba->execute($sql);
	if (!$rsm->eof()) {
		if (!is_array($value)) {
			$value = explode(",", $value);
		}
		if (strlen(trim($t)) > 0) {
			$str = $str."<hr size=\"1\" noshade color=\"#e0e0e0\"><b>".putspace(translate("Filter by"))." ".titlecase(translate(hashtable($t)))."</b><hr size=\"1\" noshade color=#ffffff>";
		}
		$str = $str."<table border=\"0\" width='100%' cellspacing=\"0\" cellpadding=\"0\">";
		while (!$rsm->eof()) {
			if ($mcount == 0) {
				$str = $str."<tr valign=\"top\">";
			}
			$checked = "";
			foreach ($value as $i) {
				if (trim($rsm->row(0)) == trim($i)) {
					$checked = " checked";
					break;
				}
			}
			$str = $str."<td width=1><input type=checkbox ";
			if ($onchange == true) {
				$str = $str." onchange=this.form.submit() ";
			}
			$count = "";
			if (strlen(trim($rsm->row("totalc"))) > 0) {
				$count = "&nbsp;<font class=textfaded>(&nbsp;".$rsm->row("totalc")."&nbsp;)</font>";
			}
			$str = $str." name=\"".$name."[]\" value=\"".$rsm->row(0)."\" $checked></td><td>".trim($rsm->row(1)).$count."</td>";
			$mcount++;
			if ($mcount == $b) {
				$str = $str."</tr>";
				$mcount = 0;
			}
			$rsm->movenext();
		}
		$str = $str."</table>";
	}
	return $str;
}



function getmultiselectfield($name, $value) {
	global $dba;
	if (strlen(trim($name)) > 0 && strlen(trim($value)) > 0) {
		$tb = $name;
		if (substr(strtolower($tb), strlen($tb) - 2, 2) == "id") {
			$tb = substr(strtolower($tb), 0, strlen($tb) - 2);
		}
		$fields = getfields($tb);
		$fields = $fields[3];
		$fields = explode(",", $fields);
		$values = explode(",", $value);
		$sql = "select ".$fields[0].", ".$fields[1]." from `".$tb."` where id = '-1' ";
		foreach ($values as $i) {
			if (strlen(trim($i)) > 0) {
				$sql = $sql." or id='".trim($i)."' ";
			}
		}
		$sql = $sql.";";
		$rsm = $dba->execute($sql);
		if (!$rsm->eof()) {
			while (!$rsm->eof()) {
				$str = $str.trim($rsm->row(1));
				$rsm->movenext();
				if (!$rsm->eof()) {
					$str = $str.", ";
				}
			}
		}
	}
	return $str;
}



function dropdown($name, $value, $o = null, $class=null, $style=null) {
	global $dba;
	$tb = substr($name, 0, strlen($name)-2);

	$fields = getfields($tb);
	$fields = $fields[3];
	$fields = explode(",", $fields);
	$sql = "select `".$fields[0]."`, `".$fields[1]."` from `".$tb."` ";
	if (strlen(trim($o)) == 0) {
		$o = $fields[1];
	}
	$sql = $sql." order by ".$o.";";
	$rs = $dba->execute($sql);
	$str = $str."<select name=\"".$name."\" id=\"".$name."\" class=\"".$class."\" style=\"".$style."\" >";
	if (!$rs->eof()) {
		while (!$rs->eof()) {
			$str = $str."<option value=\"".$rs->row(0)."\"";
			if (strtolower(trim($rs->row(0))) == strtolower(trim($value))) {
				$str = $str." selected";
			}
			$str = $str.">".$rs->row(1)."</option>";
			$rs->movenext();
		}
	}
	$str = $str."</select>";
	return $str;
}

function dropdown_extended($name, $value, $all, $fields, $filter, $onchange=false, $o = null, $radio = false) {
	global $dba;
	$tb = str_replace("]", "", str_replace("[", "", $name));
	$tb = substr($tb, 0, strlen($tb)-2);
	$fields = getfields($tb);
	$fields = $fields[3];
	$fields = explode(",", $fields);
	$sql = "select ";
	if (strlen(trim($fields)) > 0) {
		$sql = $sql.$fields;
	}
	else {
		$sql = $sql." ".$fields[0].", ".$fields[1]." ";
	}
	$sql = $sql." from `".$tb."` ";
	if (strlen(trim($filter)) > 0) {
		$sql = $sql." where ".$filter;
	}
	if (strlen(trim($o)) == 0) {
		$o = $fields[1];
	}
	$sql = $sql." order by ".$o." ";
	$sql = $sql.";";
	//print($sql);
	$rs = $dba->execute($sql);
	if (!$radio) {
		$str = $str."<select ";
		if ($onchange == true) {
			$str = $str." onchange=this.form.submit() ";
		}
		$str = $str." name=\"".$name."\" id=\"".$name."\">";
	}
	if ($all) {
		if (!$radio) {
			$str = $str."<option value=''>- ".translate("Please Select")." ".hashtable(ucwords($tb))." -</option>";
		}
		else {
			$str = $str."<input type=\"radio\" id=\"".$name."\" name=\"".$name."\" value=''";
			if ($onchange == true) {
				$str = $str." onchange=this.form.submit() ";
			}
			$str = $str.">".translate("All")." ".hashtable(ucwords($tb));
		}
	}
	if (!$rs->eof()) {
		while (!$rs->eof()) {
			if (!$radio) {
				$str = $str."<option ";
			}
			else {
				$str = $str."<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr valign=\"top\"><td><input type=\"radio\" id=\"".$name."\" name=\"".$name."\" ";
				if ($onchange == true) {
					$str = $str." onchange=this.form.submit() ";
				}
			}
			$str = $str." value=\"".$rs->row(0)."\"";
			if (strtolower(trim($rs->row(0))) == strtolower(trim($value))) {
				if (!$radio) {
					$str = $str." selected";
				}
				else {
					$str = $str." checked";
				}
			}
			$str = $str."/>";
			if ($radio) {
				$str = $str."</td><td>";
			}
			$str = $str.truncate($rs->row(1));
			if (!$radio) {
				$str = $str."</option>";
			}
			else {
				$str = $str."</td></tr></table>";
			}
			$rs->movenext();
		}
	}
	if (!$radio) {
		$str = $str."</select>";
	}
	return $str;
}



function truefalse($name, $value) {
	if (trim($value) == "1")  {
		$str = $str."<select name=\"".$name."\" id=\"".$name."\"><option selected value=1>".translate("Yes")."</option><option value=0>".translate("No")."</option></select>";
	}
	else {
		$str = $str."<select name=\"".$name."\" id=\"".$name."\"><option value=0>".translate("No")."</option><option value=1>".translate("Yes")."</option></select>";
	}
	return $str;
}


function datefield($name, $value) {
	if (!isdate($value) || strlen(trim($value)) == 0) {
		$value = date('Y-m-d');
	}
	$value = getmydate($value);
	$dd = year($value)."-".pad(month($value), 1)."-".pad(day($value), 1);
	$str = $str."<input type=\"text\"  size=\"11\" name=\"".$name."\" value=\"".$dd."\" onclick=\"displayDatePicker('".$name."', false, 'ymd', '-');\">"; //onblur=\"if(this.disabled==true)this.disabled=false;\"
	return $str;
}


function formatmynumber($n) {
	if (is_numeric($n)) {
		$n = number_format($n);
	}
	return $n;
}



function year($d) {
	$d = trim($d);
	if (!date_parse($d)) {
		return $d;
	}
	else {
		$cdate = getdate(strtotime($d));
		return $cdate['year'];
	}
}

function month($d) {
	$d = trim($d);
	if (!date_parse($d)) {
		return $d;
	}
	else {
		$cdate = getdate(strtotime($d));
		return pad($cdate['mon'], 1);
	}
}

function monthname($d) {
	$d = trim($d);
	if (!date_parse($d)) {
		return $d;
	}
	else {
		$cdate = getdate(strtotime($d));
		return translate($cdate['month']);
	}
}


function day($d) {
	$d = trim($d);
	if (!date_parse($d)) {
		return $d;
	}
	else {
		$cdate = getdate(strtotime($d));
		return pad($cdate['mday'], 1);
	}
}

function weekday($d) {
	$d = trim($d);
	if (!date_parse($d)) {
		return $d;
	}
	else {
		$cdate = getdate(strtotime($d));
		return $cdate['wday'];
	}
}


function weekdayname($d) {
	$d = trim($d);
	if (!date_parse($d)) {
		return $d;
	}
	else {
		$cdate = getdate(strtotime($d));
		return translate($cdate['weekday']);
	}
}


function formatmydate($d) {
	$d = trim($d);
	if (!date_parse($d)) {
		return $d;
	}
	else {
		$cdate = getdate(strtotime($d));
		return translate($cdate['mday']."&nbsp;".ucwords(substr($cdate['month'], 0, 3))."&nbsp;".$cdate['year']);
	}
}


function formatmytime($d) {
	if (!date_parse($d) || strlen($d) == 0) {
		return $d;
	}
	else {
		$dd = getdate(strtotime(getmydate("")." ".$d));
		return translate(pad($dd['hours'], 1).":".pad($dd['minutes'], 1).":".pad($dd['seconds'], 1));
	}
}

function getmydate($d = "") {
	$d = trim($d);
	if (!date_parse($d) || strlen($d) == 0) {
		$d = date('Y-m-d');
	}
	$dd = getdate(strtotime($d));
	return $dd['year']."-".pad($dd['mon'], 1)."-".pad($dd['mday'], 1);
}


function getmytime($d = "") {
	$d = trim($d);
	if (!date_parse($d) || strlen($d) == 0) {
		$d = date('Y-m-d-H-i-s');
	}
	$dd = getdate($d);
	return pad($dd['hours'], 1).pad($dd['minutes'], 1).pad($dd['seconds'], 1);
}

function isdate($d) {
	if (!date_parse($d) || strlen($d) == 0) {
		return false;
	}
	else {
		return true;
	}
}


function dateadd($od, $t, $i) {
	if (isdate($od) && strlen(trim($t)) == 1 && is_numeric($i)) {
		$s = 0;
		$d = 0;
		$m = 0;
		$y = 0;
		switch (trim($t)) {
			case "s":
			$s = $i; break;
			case "n":
			$s = ($i * 60); break;
			case "h":
			$s = ($i * 60 * 60); break;
			
			case "d":
			$d = $i; break;
			case "m":
			$m = $i; break;
			case "y":
			$y = $i; break;
		}
		$cd = strtotime($od);
		$cd = $cd + $s;
		$fd = date('Y-m-d H:i:s', mktime(date('H',$cd), date('i',$cd), date('s',$cd), date('m', $cd) + $m, date('d', $cd) + $d, date('Y', $cd) + $y));
	}
	else {
		$fd = $od;
	}	
	return $fd;
}



function titlecase($s) {
	return ucwords($s);
}

function clean($n) {
	return str_replace("'", "", $n);
}

function truncate($m) {
	$size = 40;
	$m = trim($m);
	if (strlen($m) > $size) {
		$pos = strpos($m, " ", $size - 5);
		if (!$pos) {
			$m = substr($m, 0, $size)."...";
		}
		else {
			$m = trim(substr($m, 0, $pos));
		}
	}
	return $m;
}


function validate_email($email) {
	$result = false;
	if(preg_match( "/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {
		list($username, $domain) = split('@', $email);
		$mxhosts = array();
		if(!getmxrr($domain, $mxhosts)) {
			if (fsockopen($domain, 25, $errno, $errstr, 30)) {
				$result = false;
			}
		}
		else {
			foreach ($mxhosts as $host) {
				if (fsockopen($host, 25, $errno, $errstr, 30)) {
					$result = true;
				}
			}
		}
	}
	return $result;
}


function putspace($m) {
	if (strlen($m) > 0) {
		$m = str_ireplace(" ", "&nbsp;", $m);
	}
	return $m;
}

function extractsummary($t, $l = 300) {
	$t = trim($t);
	if (strlen(trim($t)) > $l) {
		$t = removehtml($t);
		$pos = strpos($t, ".", $l - round($l / 5, 0));
		if ($pos == false) {
			$pos = strpos($t, " ", $l - round($l / 5, 0));
			if ($pos == false) {
				$pos = $l;
			}
		}
		if (strlen($t) + round($l / 5, 0) > $pos) {
			$pos = $l;
		}
		$t = substr($t, 0, $pos)."...";
	}
	return $t;
}

function extractkeywords($c) {
	$c = str_replace("<br/>", " ", $c);
	$c = str_replace("<br />", " ", $c);
	$c = str_replace("\n", " ", $c);
	$c = removehtml($c);
	$c = iconv($charset, "utf-8", $c);
	$c = html_entity_decode($c, ENT_QUOTES, "utf-8");
	$c = mb_strtolower($c, "utf-8");
	$ct = $c;
	$c = "";
	for ($i = 0; $i < strlen($ct); $i++) {
		if ( (ord(substr($ct, $i, 1)) > 96 && ord(substr($ct, $i, 1)) < 123) || substr($ct, $i, 1) == " ") {
			$c = $c.substr($ct, $i, 1);
		}
		else {
			$c = $c." ";
		}
	} 

	$words = explode(" ", $c);
	$stop  = strtolower(file_get_contents("includes/resources/stopwords.txt"));
	$stopwords = explode("\n", $stop);
	$words = array_diff($words, $stopwords);
	$words = array_count_values($words);

	$words = array_keys($words);
	$c = "";
	foreach ($words as $i) {
		if (!is_numeric(trim($i))) {
			$c = $c.trim($i).", ";
		}
	}
	return $c;
}



function pad($m, $n) {
	$result = trim($m);
	if (is_numeric($m) && is_numeric($n)) {
		$m = (int)$m;		
		for ($i = 0; $i < $n; $i++) {
			if (strlen(pow(10, $n)) > strlen($result)) {
				$result = "0".$result;
			}
		}
	}
	return $result;
}


function removehtml($s) {
	$s = strip_tags($s);
	return $s;
}

function getrandom($s = 0000, $e = 9999) {
	return mt_rand($s, $e);
}

function get_url($url, $method = "GET", $headers = null) {
	$ch = curl_init();
	if (!$ch || strlen($url) == 0) {
		$data = false;
	}
	else {
		$pos = strpos($url, "?");
		if (!$pos) {
			$vars = "";
		}
		else {
			$vars = substr($url, $pos+1, strlen($url));
			$url = substr($url, 0, $pos);
		}
		//if (is_array($headers) && sizeof($headers) > 0) {
		//	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		//}
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION    ,1);
		if (strtolower($method) == "post") {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		}
		else {
			curl_setopt($ch, CURLOPT_GET, 1);
			curl_setopt($ch, CURLOPT_URL, $url."?".$vars);
		}
		$data = curl_exec($ch);
	}
	curl_close($ch);
	return $data;
}



function hashtable($m) {
	global $dba;
	$sql = "select `key`, `value` from `hashtable` where lower(`key`)='".strtolower(trim($m))."';";
	$rs = $dba->execute($sql);
	if (!$rs->eof()) {
		$m = $rs->row(1);
	}
	//$m = translate($m);
	return $m;
}


function translate($s) {
	global $dba, $application;
	$o = strtolower(trim($application["language"]));
	$p = $s;
	if (strlen(trim($s)) > 0 && strlen(trim($o)) == 2 && strtolower(trim($o)) != "en") {
		$add = false;
		$upd = false;
		$sql = "select `$o` from `translate` where lower(trim(`key`))='".strtolower(trim(mmysql_real_escape_string($s)))."' order by id desc limit 1;";
		$rs = $dba->execute($sql);
		if ($rs->eof()) {
			$add = true;
		}
		else {
			if (strlen(trim($rs->row(0))) > 0) {
				$p = $rs->row(0);
			}
			else {
				$upd = true;
			}
		}
		if ($add == true || $upd == true) {
			$gt = new BingTranslateWrapper("1E2B4E05B0CEA009845899854F383F350CE97048");
			$p = $gt->translate($p, "en", $o);
			if (strlen(trim($p)) > 0) {
				if ($add == true) {
					$asql = "insert into `translate` (`id`, `key`, `$o`) values ('".getid("translate")."', '". mmysql_real_escape_string(strtolower(trim($s)))."', '". mmysql_real_escape_string(strtolower(trim($p)))."');";
					$rsa = $dba->execute($asql); 
				}
				if ($upd == true) {
					$asql = "update `translate` set `$o`='".mmysql_real_escape_string(strtolower(trim($p)))."' where `key`='". mmysql_real_escape_string(strtolower(trim($s)))."';";
					$rsa = $dba->execute($asql); 
				}
			}
		}
		if (strlen(trim($p)) == 0) {
			$p = $s;
		}
	}
	return $p;
}



function set($o, $d) {
	if (strlen($o) > 0) {
		if (is_numeric($d)) {
			if (!is_numeric($o)) {
				return $d;
			}
			else {
				return $o;
			}
		}
		else {
			return $o;
		}
	}
	else {
		return $d;
	}
}

function sendmail($sender, $recipient, $subject, $message) {
	global $application;
	include("Mail.php");
	$recipients = $recipient;
	$headers["From"] = $sender;
	$headers["To"] = $recipient;
	$headers["Subject"] = $subject;
	$smtpinfo["host"] = $application["smtpserver"];
	$smtpinfo["port"] = "587";
	$smtpinfo["auth"] = true;
	$smtpinfo["username"] = $application["smtpusername"];
	$smtpinfo["password"] = $application["smtppassword"];
	$mail_object =& Mail::factory("smtp", $smtpinfo);
	$mail_object->send($recipients, $headers, $message);
}

function sendhtmlmail($sender, $recipient, $subject, $message, $html=false) {
	global $mailserver, $mailusername, $mailpassword;
	$message = wordwrap($message, 70);
	include("Mail.php");
	include("Mail/mime.php");
	$recipients = $recipient;
	$headers["From"] = $sender;
	$headers["To"] = $recipient;
	$headers["Subject"] = $subject;
	//$headers["Content-Type"] = "Content-type: text/html; charset=iso-8859-1"; 
	$msg = $message;
	$crlf = "\n";
	$mime = new Mail_mime($crlf);

	$mime->setHTMLBody($msg);
	$body = $mime->get();
	$headers = $mime->headers($headers);

	$smtpinfo["host"] = $mailserver;
	$smtpinfo["port"] = "25";
	$smtpinfo["auth"] = true;
	$smtpinfo["username"] = $mailusername;
	$smtpinfo["password"] = $mailpassword;
	
	$mail_object =& Mail::factory("smtp", $smtpinfo);
	$mail_object->send($recipients, $headers, $body);
}


function errortrap($m) {
	print $m;
}

