<?php

include '../GoogleTranslate.php'; 
$t = new GoogleTranslate; 

//set input and output language: 

$t->langIn = 'en'; 
$t->langOut = 'de'; 

//translate 
echo $t->translate('Hello World'); 

//translate again, this time it will be loaded from cache! 
echo $t->translate('Hello World');