var activeTab = 1;
function openTab(tabId) {
	document.getElementById("tabLink"+activeTab).className = "tabLink";
	document.getElementById("tabContent"+activeTab).className = "tabContent";
	document.getElementById("tabLink"+tabId).className = "tabLinkActive";
	document.getElementById("tabContent"+tabId).className = "tabContentActive";
	activeTab = tabId;
}

