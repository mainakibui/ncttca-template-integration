Ext.Loader.setConfig({
    enabled: true,
    disableCaching: false,
    paths: {
    	gxp: "js/lib/gxp",
        GeoExt: "js/lib/GeoExt",
        Ext: "http://cdn.sencha.com/ext/gpl/4.2.1/src"
    }
});
