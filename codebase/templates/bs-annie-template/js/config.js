/*
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Ext.namespace("Heron");
Ext.namespace("Heron.options");
Ext.namespace("Heron.options.layertree");
/**
 * Defines the entire layout of a Heron webapp using ExtJS-style.
 *
 * The layout specifies a hierarchy of ExtJS (Panel) components.
 * Each component is either a container of components (xtype: 'panel', i.e. an ExtJS Panel)
 * or a specific leaf component like a map panel (xtype: 'hr_mappanel') or simple HTML
 * panel (xtype: 'hr_htmlpanel'). Each component has a 'xtype' string and component-specific options.
 * The 'xtype' defines the component widget class .
 * For a container-type (xtype: 'panel') the options should include a 'layout' (like 'border' or 'card',
 * and an array of 'items' with each element being a component (another container or a leaf widget component).
 *
 * In order to distinguish ExtJS-specific config options from those that are Heron-specific,
 * the later are prefixed with "hr". These are defined outside this file to allow quick custimization.
 *
 * Specific config options for ExtJS components can be found in the API docs:
 * http://docs.sencha.com/ext-js/3-4/#!/api
 *
 * This is the core config, mainly the layout of a Heron browser application for all examples.
 * Many of the options refer to Javascript variables that are defined within
 * the DefaultOptions*.js. In particular Layers and specific widgets. This has been done
 * to create a reusable config for all examples. Each example may also add a 3rd refinement
 * using a local Config.js file. The names of the config files and variables like Heron.options.bookmarks
 * don't matter. They are just a convenience as to break up a large configuration into
 * the more stable common parts and the more variable parts. As it is all JSON/JavaScript, we
 * can use variables, in our case namespaced, like "Heron.options.bookmarks" as to avoid conflicts in
 * the global JS namespace. (If we would have XML configs we would have to resort to xlinks).
 *
 **/


var treeTheme = [
    {
        text:'BaseMaps', expanded: true, children:
            [
                {nodeType: "gx_layer", layer: "Google Terrain" },
                {nodeType: "gx_layer", layer: "Google Streets" },
                {nodeType: "gx_layer", layer: "Google Hybrid" },
                {nodeType: "gx_layer", layer: "Google Satellite"}
            ]
    },
    {
        text:'Member States', expanded: true, children:
            [
                {
                    text:'Burundi', nodeType: 'gx_layer', layer:"Burundi",children:
                        [
                            {
                                text:'Roads', nodeType: 'gx_layer', layer:"Burundi Roads",children:
                                    [
                                        {text:'Weighbridge', nodeType: "gx_layer", layer: "Burundi Weighbridge"},
                                        {text:'Border Post', nodeType: "gx_layer", layer: "Burundi Border Post"}
                                    ]
                            },
                            {
                                text:'Railway', nodeType: 'gx_layer', layer:"Burundi Railway", children:
                                    [
                                        {text:'Station', nodeType: "gx_layer", layer: "Burundi Railway Station"}
                                    ]
                            },
                            {text:'Sea Ports', nodeType: "gx_layer", layer: "Burundi Seaways"},
                            {text:'Transit Towns', nodeType: "gx_layer", layer: "Burundi Towns"}
                        ]
                },{
                    text:'DR Congo', nodeType: 'gx_layer', layer:"DR Congo", children:
                        [
                            {
                                text:'Roads', nodeType: 'gx_layer', layer:"DR Congo Roads",children:
                                    [
                                        {text:'Weighbridge', nodeType: "gx_layer", layer: "DR Congo Weighbridge"},
                                        {text:'Border Post', nodeType: "gx_layer", layer: "DR Congo Border Post"}
                                    ]
                            },
                            {
                                text:'Railway', nodeType: 'gx_layer', layer:"DR Congo Railway", children:
                                    [
                                        {text:'Station', nodeType: "gx_layer", layer: "DR Congo Railway Station"}
                                    ]
                            },
                            {text:'Sea Ports', nodeType: "gx_layer", layer: "DR Congo Seaways"},
                            {text:'Transit Towns', nodeType: "gx_layer", layer: "DR Congo Towns"}
                        ]
                },{
                    text:'Kenya', nodeType: 'gx_layer', layer:"Kenya", children:
                        [
                            {
                                text:'Roads', nodeType: 'gx_layer', layer:"Kenya Roads",children:
                                    [
                                        {text:'Weighbridge', nodeType: "gx_layer", layer: "Kenya Weighbridge"},
                                        {text:'Border Post', nodeType: "gx_layer", layer: "Kenya Border Posts"}
                                    ]
                            },
                            {
                                text:'Railway', nodeType: 'gx_layer', layer:"Kenya Railway", children:
                                    [
                                        {text:'Station', nodeType: "gx_layer", layer: "Kenya Railway Stations"}
                                    ]
                            },
                            {text:'Sea Port', nodeType: "gx_layer", layer: "Kenya Seaways"},
                            {text:'Transit Towns', nodeType: "gx_layer", layer: "Kenya Towns"}
                        ],    
                        listeners: {
                            radiochange: function(n) {
                                Ext.Msg.alert('Navigation Tree Click', 'You clicked: "' + n.attributes.text + '"');
                            }
                        },
                },{
                    text:'Rwanda', nodeType: 'gx_layer', layer:"Rwanda", children:
                        [
                            {
                                text:'Roads', nodeType: 'gx_layer', layer:"Rwanda Roads",children:
                                   [
                                        {text:'Weighbridge', nodeType: "gx_layer", layer: "Rwanda Weighbridge"},
                                        {text:'Border Post', nodeType: "gx_layer", layer: "Rwanda Border Post"}
                                    ]
                            },
                            {
                                text:'Railway', nodeType: 'gx_layer', layer:"Rwanda Railway", children:
                                    [
                                        {text:'Station', nodeType: "gx_layer", layer: "Rwanda Railway Station"}
                                    ]
                            },
                            {text:'Sea Ports', nodeType: "gx_layer", layer: "Rwanda Seaways"},
                            {text:'Transit Towns', nodeType: "gx_layer", layer: "Rwanda Towns"}
                        ]
                },{
                    text:'South Sudan', nodeType: 'gx_layer', layer:"South Sudan", children:
                        [
                            {
                                text:'Roads', nodeType: 'gx_layer', layer:"South Sudan Roads",children:
                                   [
                                        {text:'Weighbridge', nodeType: "gx_layer", layer: "South Sudan Weighbridge"},
                                        {text:'Border Post', nodeType: "gx_layer", layer: "South Sudan Border Post"}
                                    ]
                            },
                            {
                                text:'Railway', nodeType: 'gx_layer', layer:"South Sudan Railway", children:
                                    [
                                        {text:'Station', nodeType: "gx_layer", layer: "South Sudan Railway Station"}
                                    ]
                            },
                            {text:'Sea Ports', nodeType: "gx_layer", layer: "South Sudan Seaways"},
                            {text:'Transit Towns', nodeType: "gx_layer", layer: "South Sudan Towns"}
                        ]
                },{
                    text:'Uganda', nodeType: 'gx_layer', layer:"Uganda", children:
                        [
                            {
                                text:'Roads', nodeType: 'gx_layer', layer:"Uganda Roads",children:
                                    [
                                        {text:'Weighbridge', nodeType: "gx_layer", layer: "Uganda Weighbridge"},
                                        {text:'Border Post', nodeType: "gx_layer", layer: "Uganda Border Post"}
                                    ]
                            },
                            {
                                text:'Railway', nodeType: 'gx_layer', layer:"Uganda Railway", children:
                                    [
                                        {text:'Station', nodeType: "gx_layer", layer: "Uganda Railway Station"}
                                    ]
                            },
                            {text:'Sea Ports', nodeType: "gx_layer", layer: "Uganda Seaways"},
                            {text:'Transit Towns', nodeType: "gx_layer", layer: "Uganda Towns"}
                        ]
                }
            ]
    }
];

Heron.options.layertree.tree = treeTheme;

Heron.layout = {
    xtype: 'panel',
    renderTo: 'map-canvas',
    height: 700,
    //width: 1000,

    

    /* Optional ExtJS Panel properties here, like "border", see ExtJS API docs. */
    id: 'hr-container-main',
    layout: 'border',
    border: false,

    /** Any classes in "items" and nested items are automatically instantiated (via "xtype") and added by ExtJS. */
    items: [
        {
            xtype: 'panel',
            id: 'hr-menu-east-container',
            region: "east",
            collapsible: true,
            collapsed: false,
            border: true,
            layout: 'fit',
            split: true,
            width: 250,
            //autoScroll: true,
            items: [
                {
                    xtype: 'tabpanel',
                    border: false,
                    //layout: 'fit',
                    resizeTabs:true,        
                    enableTabScroll  :true,
                    monitorResize    :true,
                    deferredRender   :false,
                    layoutOnTabChange:true,
                    autoDestroy      :true, 
                    activeTab: 0,
                    autoScroll: true,
                    defaults:{autoHeight: true},
                    items: [
                        {
                            xtype: 'hr_layertreepanel',
                            border: false,
                            title:'Filters',

                            // The LayerTree tree nodes appearance: default is ugly ExtJS document icons
                            // Other values are 'none' (no icons). May be overridden in specific 'gx_layer' type config.
                            layerIcons : 'bylayertype',

                            // Allow moving layers
                            enableDD: true,
                            plugins: [{
                                ptype: "gx_treenodeactions",
                                listeners: {
                                    action: function (node, action, evt) {
                                        console.log(action);
                                    }
                                }
                            }],

                            // Right-mouse popoup menu
                            contextMenu: [
                                {
                                    xtype: 'hr_layernodemenulayerinfo'
                                },
                                {
                                    xtype: 'hr_layernodemenuzoomextent'
                                },
                                /*{
                                    xtype: 'hr_layernodemenustyle'
                                },*/
                                {
                                    xtype: 'hr_layernodemenuopacityslider'
                                }
                            ],
                            // Optional, use internal default if not set
                            hropts: Heron.options.layertree
                        },
                        {
                            xtype: 'panel',
                            contentEl: 'filters',                            
                            bodyStyle: {
                                'padding': '5px'
                            },
                            //collapseMode: "mini",
                            autoScroll: true,
                            split: true,
                            title: 'Indicators'
                        }
                        /*{
                            xtype: 'hr_htmlpanel',
                            id: 'hr-info-west',
                            border: true,
                            //html: Heron.options.info.html,
                            preventBodyReset: true,
                            title: 'Info'
                        },
                        {
                            xtype: 'hr_bookmarkspanel',
                            id: 'hr-bookmarks',
                            border: true,
                            //The map contexts to show links for in the BookmarksPanel.
                            hropts: Heron.options.bookmarks
                        }
                        */
                    ]
                }
            ]
        },
        {
            xtype: 'panel',
            id: 'hr-map-and-info-container',
            layout: 'border',
            region: 'center',
            width: '100%',
            collapsible: false,
            split: false,
            border: false,
            items: [
                {
                    xtype: 'hr_mappanel',
                    id: 'hr-map',
                    //title: '&nbsp;',
                    title: 'Northern Corridor Map',
                    region: 'center',
                    collapsible: false,
                    border: false,
                    hropts: Heron.options.map
                }
            ]
        },
	{
	  xtype:'panel',
	  contentEl:'legend',
	  region:'south',
	  height:100,
	  border:false,
	}
	  
        /*{
            xtype: 'panel',

            //id: 'hr-menu-right-container',
            //layout: 'accordion',
            renderTo:'legend',
            //region: "east",
            //width: 250,
            //collapsible: true,
            //split: false,
            border: false,
            items: [
                {
                    xtype: 'hr_layerlegendpanel',
                    id: 'hr-layerlegend-panel',
                    border: false,
                    defaults: {
                        useScaleParameter: true,
                        style:"float:left; padding:10px;",
                        baseParams: {
                            FORMAT: 'image/png'
                        }
                    },

                    // Should Legend Image URL be fetched from WMS Capabilities? 
                    legendFromCapabilities: false,
                     /// Should Legend Image URL be fetched from WMS Capabilities for these URL patterns? 
                    legendFromCapabilitiesPatterns: ['dino', 'arcgis'],
                    hropts: {
                        // Preload Legends on initial startup
                        // Will fire WMS GetLegendGraphic's for WMS Legends
                        // Otherwise Legends will be loaded only when Layer
                        // becomes visible. Default: false
                        prefetchLegends: false
                    }
                }
            ]
        }*/
    ]
};

//display Filters
$('#filters').css('display', 'block');
setTimeout(function() {
mapPanel = Heron.App.getMapPanel();
var p = Ext.getCmp('popiframe');
if (window==window.top) {
 p.destroy();
 //console.log('Iframe')
}

/*
vectorCoutries = Heron.App.getMap().getLayersByName('Northern Corridor Member States')[0];
vectorNodes = Heron.App.getMap().getLayersByName('Nodes')[0];
vectorRoad = Heron.App.getMap().getLayersByName('Roads')[0];

Heron.App.getMap().events.register("zoomend", Heron.App.getMap(), function(){
    if (Heron.App.getMap().getZoom() <= 5){
        var z = 2+Heron.App.getMap().getZoom();
    }else{
        var z = 5+Heron.App.getMap().getZoom();
    }
    var lyr = Heron.App.getMap().getLayersByName('Nodes')[0];
    for (i in lyr.styleMap.styles.default.rules) {
        if (lyr.styleMap.styles.default.rules[i].symbolizer){
            lyr.styleMap.styles.default.rules[i].symbolizer.pointRadius = z;
        }
    }
    lyr.redraw();
});
*/

}, 1000);
            

// functions for resizing the map panel
function mapSizeUp() {
    var size = mapPanel.getSize();
    size.width += 40;
    size.height += 40;
    mapPanel.setSize(size);
}

function mapSizeDown() {
    var size = mapPanel.getSize();
    size.width -= 40;
    size.height -= 40;
    mapPanel.setSize(size);
}

function zoomtoCountryfid(country) {
    var lyr = Heron.App.getMap().getLayersByName(country)[0]
    if (lyr) {
        lyr.setVisibility(true);
        Heron.App.getMap().zoomToExtent(lyr.getDataExtent());
    }
}

function zoomtoRoadfid(route) {
    var at = gLyr.getFeaturesByAttribute('Name', route)[0];
    if (at) {
        //vectorCoutries.setVisibility(false);
        //refreshLayers();
        at.renderIntent = 'select';
        at.layer.drawFeature(at);
        Heron.App.getMap().zoomToExtent(at.geometry.bounds, true);
        var z = Heron.App.getMap().zoom - 1;
        Heron.App.getMap().zoomTo(z);
        //gFID = at[0].fid;
    }
}

function zoomtoNodefid(node) {
    var prefix =[ 
        gCountry+' Weighbridge',
        gCountry+' Border Posts',
        gCountry+' Seaways',
        gCountry+' Railway Stations'
    ];
    for (i in prefix){
        var lyr = Heron.App.getMap().getLayersByName(prefix[i])[0];
        if(lyr){
            var at = lyr.getFeaturesByAttribute('Name', node)[0];
            if (at) {
                //refreshLayers();
                at.renderIntent = 'select';
                at.layer.drawFeature(at);
                Heron.App.getMap().zoomToExtent(at.geometry.bounds, true);
                gFID = at.fid;
            }else{
                lyr.setVisibility(false);
            }
            
        }
    }
}

function refreshLayers() {
    vectorCoutries.refresh();
    vectorRoad.refresh();
    vectorNodes.refresh();
}

function drawcircle(feature) {
    //vectorNodes.refresh();
    var rad;
    if(feature.attributes.Indicator >= 100){
        rad = [100,(100 *(50/100))]
    }else{
        rad = [feature.attributes.Indicator,feature.attributes.Indicator]
    }

    var sel = [
          new OpenLayers.Rule({
            title: feature.attributes.HTML ,
            //minScaleDenominator: 3000000,
            symbolizer: {
              graphicName: 'circle',
              pointRadius: rad[0],
              fillColor: getRandomColor(),
              fillOpacity: 0.8,
              strokeColor: '#000000',
              strokeWidth: 2,
              strokeOpacity: 0.8,
              label: '' + feature.attributes.Indicator,
              fontColor: 'white',
              fontFamily: 'Arial',
              fontSize: rad[1]
            }
          })
        ];
/*
    var sel = {
        graphicName: 'circle',
        title: feature.attributes.Name +' ('+feature.attributes.Indicator +')',
        pointRadius: feature.attributes.Indicator,
        //fillColor: '#006633',
        fillColor: getRandomColor(),
        fillOpacity: 0.8,
        strokeColor: '#000000',
        strokeWidth: 2,
        strokeOpacity: 0.8,
        label: '' + feature.attributes.Indicator,
        fontColor: 'white',
        fontFamily: 'Arial',
        fontSize: feature.attributes.Indicator
    };
*/
    //console.log(vectorNodes.setVisibility(false));
    //vectorNodes.setVisibility(false);
    gentempLayer(feature,sel);
    //feature.style = sel;
    //feature.layer.drawFeature(feature);
}

var tmpVector;

function gentempLayer(feature,style){
  if (tmpVector){
    var at = tmpVector.getFeaturesByAttribute('Name', feature.attributes.Name)[0];
    if (at) {
      tmpVector.destroy();
    }
  }


  tmpVector = new OpenLayers.Layer.Vector('Indicator ('+feature.attributes.Name +')',{
    styleMap: new OpenLayers.StyleMap({
      'default': new OpenLayers.Style({
      }, {
        rules: style
      })
    })
  });
  //tmpVector.addFeatures([new OpenLayers.Feature.Vector(feature.geometry,feature.attributes,style)]);
  tmpVector.addFeatures([new OpenLayers.Feature.Vector(feature.geometry,feature.attributes)]);
  Heron.App.getMap().addLayers([tmpVector]);
}

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function toggleRoadQuality(n) {
    var prefix =[ 
        {'Burundi':'Burundi Roads'},
        {'DR Congo':'DR Congo Roads'},
        {'Kenya':'Kenya Roads'},
        {'Rwanda':'Rwanda Roads'},
        {'South Sudan':'South Sudan Roads'},
        {'Uganda':'Uganda Roads'}
    ];
    if (n == 0) {
        for (i in prefix){
            var lyr = Heron.App.getMap().getLayersByName(prefix[i][gCountry])[0];
            if (lyr){
                lyr.styleMap.styles.default.rules = RoadQualityrules;
                lyr.redraw();
            }
        }     
    }
    if (n == 1) {
        for (i in prefix){
            var lyr = Heron.App.getMap().getLayersByName(prefix[i][gCountry])[0];
            if (lyr){
                lyr.styleMap.styles.default.rules = Roadrules;
                lyr.redraw();
            }
        }
      //vectorNodes.setVisibility(true);
      //vectorRoad.styleMap.styles.default.rules = Roadrules;
    }
    //vectorRoad.redraw();
    //reload legend 
    /*
    for (i in legendPanel.items.items) {
      if (legendPanel.items.items[i].layer.name == 'Roads') {
        legendPanel.items.items[i].setRules();
        legendPanel.items.items[i].update();
      }
    }*/
}

function colorRoad(feature) {
    //var rad = Math.floor((Math.random() * 100) + 1);
    var col = getRandomColor();
    var sel = {
        fillColor: col,
        fillOpacity: 0.8,
        strokeColor: col,
        strokeWidth: 5,
        strokeOpacity: 0.8,
    };
    feature.style = sel;
    feature.attributes.HTML = 'Quality of Infrastructure :' + feature.attributes.Indicator + '%';
    feature.layer.drawFeature(feature);
}

function findinicators(c, m, ic) {
    var items = [];
    items.push('<option value="" selected="selected" >-- Select Indicator --</option>');
    $.each(gFilters, function(key, obj) {
        if (obj.Country == c) {
            $.each(obj['Mode of Transport'], function(key, obj) {
                if (key == m) {
                    $.each(obj['Route Section'], function(key, obj) {
                        $.each(obj, function(key, obj) {
                            $.each(obj, function(key, obj) {
                                if (key == ic) {
                                    $.each(obj, function(key, obj) {
                                        //items.push('<li><a href="javascript:void(0)">' + obj.indicator + '</a></li>');
                                        items.push('<option value="'+ obj.indicator +'">' + obj.indicator + '</option>');
                                        //console.log(obj.indicator);
                                    });
                                }
                            });
                        });
                    });
                }
            });
        }
    });
    $('#cindicator.form-control').html($.unique(items.sort()).join(''));
}

function findnodes(c, m, ic, indicator) {
    var nitems = [];
    var ritems = [];
    var iitems = [];
    $.each(gFilters, function(key, obj) {
        if (obj.Country == c) {
            $.each(obj['Mode of Transport'], function(key, obj) {
                if (key == m) {
                    $.each(obj['Route Section'], function(rkey, robj) {
                        $.each(robj, function(nkey, nobj) {
                            $.each(nobj, function(key, obj) {
                                if (key == ic) {
                                    $.each(obj, function(key, obj) {
                                        if (obj.indicator == indicator) {
                                            if (nkey == '[Entire Section]') {
                                                ritems.push(rkey);
                                            } else {
                                                nitems.push(nkey);
                                            }
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
        }
    });
    items_ = [];
    var prefix =[ 
        gCountry+' Weighbridge',
        gCountry+' Border Posts',
        gCountry+' Seaways',
        gCountry+' Railway Stations'
    ];
    for (i in ritems) {
        for (i in prefix){
            var lyr = Heron.App.getMap().getLayersByName(prefix[i])[0];
            if(lyr){
                var at = lyr.getFeaturesByAttribute('Name', ritems[i])[0];
                if (at) {
                    var rad = Math.floor((Math.random() * 100) + 1);
                    at.attributes.Indicator = rad;
                    at.attributes.HTML = 'Quality Of Infrastructure: ' + rad + '%';
                    items_.push('<li><a href="javascript:zoomtoRoadfid(\'' + ritems[i] + '\');">' + ritems[i] + ' (' + rad + ')</a></li>');
                }
            }
        }
        toggleRoadQuality(0);
    }
    for (i in nitems) {
        for (i in prefix){
            var lyr = Heron.App.getMap().getLayersByName(prefix[i])[0];
            if(lyr){
                var at = lyr.getFeaturesByAttribute('Name', nitems[i])[0];
                if (at) {
                    var rad = Math.floor((Math.random() * 100) + 1);
                    at.attributes.Indicator = rad;
                    at.attributes.HTML = indicator +' = '+rad;
                    drawcircle(at);
                    items_.push('<li><a href="javascript:zoomtoNodefid(\'' + nitems[i] + '\');">' + nitems[i] + ' (' + rad + ')</a></li>');
                }
            }
        }
    }
    $('#cnodes.list-group').html($.unique(items_.sort()).join(''));
    //at[0].fid = gFID;
    //$('#indicator.form-control').html(items.join(''));
}
function resettmpVectors(){
  var v = Heron.App.getMap().layers;
  var cls = 0;
  while (cls == 0){

    if(v.length <= 8){
      cls = 1;
    }else{
      for (i in v){
        if(i >= 8){
          v[i].destroy();
        }
      }
    }
  }
  //vectorNodes.setVisibility(true);
  tmpVector = '';
}

function resetfilters() {
        vectorNodes.refresh();
        vectorCoutries.refresh();
        toggleRoadQuality(1);
        vectorRoad.refresh();
        resettmpVectors();
        $('#country.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#country.form-control').parents('.btn-group').find('.dropdown-toggle').html('<span class="caret"></span>');
        loadCountryfilter();
        $('#mode_of_transport.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#mode_of_transport.form-control').parents('.btn-group').find('.dropdown-toggle').html('<span class="caret"></span>');
        $('#route_section.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#route_section.form-control').parents('.btn-group').find('.dropdown-toggle').html('<span class="caret"></span>');
        $('#node.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#node.form-control').parents('.btn-group').find('.dropdown-toggle').html('<span class="caret"></span>');
        $('#indicator_category.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator_category.form-control').parents('.btn-group').find('.dropdown-toggle').html('<span class="caret"></span>');
        $('#indicator.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator.form-control').parents('.btn-group').find('.dropdown-toggle').html('<span class="caret"></span>');
        //$('#cindicator_category.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#cindicator_category.form-control').parents('.btn-group').find('.dropdown-toggle').html('<span class="caret"></span>');
        $('#cindicator.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#cindicator.form-control').parents('.btn-group').find('.dropdown-toggle').html('<span class="caret"></span>');
        $('#cnodes').html('');
        $('#filterdate.fa').html('');
	$('input:input[name=start]').val("");
	$('input:input[name=end]').val("");
    }
    //Global Variable for Filters
    //Indicator Filters

function loadCountryfilter() {
    var items = [];
    items.push('<option value="" selected="selected" >-- Select Country --</option>');
    for (i in gFilters){
        if(gFilters[i].country){
            items.push('<option value="'+ gFilters[i].country +'">' + gFilters[i].country + '</option>');
        }
    }
    $('#country.form-control').html($.unique(items.sort()).join(''));
}

function loadModeofTransportfilter(c) {
    var items = [];
    items.push('<option value="" selected="selected" >-- Select Mode of Transport --</option>');
    for (i in gFilters){
        if(gFilters[i].country && gFilters[i].country==c){
            items.push('<option value="'+ gFilters[i].transportmode +'">' + gFilters[i].transportmode + '</option>');
        }
    }
    $('#mode_of_transport.form-control').html($.unique(items.sort()).join(''));
    togglenodes();            

}

function loadRoutesfilter(c, m) {
    var items = [];
    items.push('<option value="" selected="selected" >-- Select Route --</option>');
    for (i in gFilters){
        if(gFilters[i].country && gFilters[i].country==c && gFilters[i].transportmode==m){
            items.push('<option value="'+ gFilters[i].route +'">' + gFilters[i].route + '</option>');
        }
    }
    $('#route.form-control').html($.unique(items.sort()).join(''));
    togglenodes();            

}
/*
function loadRouteSectionfilter(c, m, r) {
    var items = [];
    items.push('<option value="" selected="selected" >-- Select Route Section --</option>');
    for (i in gFilters){
        if(gFilters[i].country && gFilters[i].country==c && gFilters[i].transportmode==m && gFilters[i].route==r){
            items.push('<option value="'+ gFilters[i].routesection +'">' + gFilters[i].routesection + '</option>');
        }
    }
    $('#route_section.form-control').html($.unique(items.sort()).join(''));
    togglenodes();
}
*/
function loadNodefilter(c, m, r) {
    var items = [];
    items.push('<option value="" selected="selected" >-- Select Node --</option>');
    for (i in gFilters){
        if(gFilters[i].country && gFilters[i].country==c && gFilters[i].transportmode==m && gFilters[i].route==r){
            items.push('<option value="'+ gFilters[i].nodename +'">' + gFilters[i].nodename + '</option>');
        }
    }
    $('#node.form-control').html($.unique(items.sort()).join(''));
}

function loadSubNodefilter(c, m, r, n) {
    var items = [];
    items.push('<option value="" selected="selected" >-- Select SubNode --</option>');
    for (i in gFilters){
        if(gFilters[i].country && gFilters[i].country==c && gFilters[i].transportmode==m && gFilters[i].route==r && gFilters[i].nodename==n){
            items.push('<option value="'+ gFilters[i].subnodeid +'">' + gFilters[i].subnodename + '</option>');
        }
    }
    $('#subnode.form-control').html($.unique(items.sort()).join(''));
}

function loadIndicatorCategoryfilter(c, m, r, n, sn) {
    var items = [];
    items.push('<option value="" selected="selected" >-- Select Indicator Category --</option>');
    for (i in gFilters){
        if(gFilters[i].country && gFilters[i].country==c && gFilters[i].transportmode==m && gFilters[i].route==r && gFilters[i].nodename==n && gFilters[i].subnodeid==sn){
            items.push('<option value="'+ gFilters[i].indicatorgroupname +'">' + gFilters[i].indicatorgroupname + '</option>');
        }
    }
    $('#indicator_category.form-control').html($.unique(items.sort()).join(''));
}

function loadIndicatorfilter(c, m, r, n, sn, ic) {
    var items = [];
    items.push('<option value="" selected="selected" >-- Select Indicator --</option>');
    for (i in gFilters){
        if(gFilters[i].country && gFilters[i].country==c && gFilters[i].transportmode==m && gFilters[i].route==r && gFilters[i].nodename==n && gFilters[i].subnodeid==sn && gFilters[i].indicatorgroupname==ic){
            items.push('<option value="'+ gFilters[i].indicatorview +'">' + gFilters[i].indicatorname + '</option>');
        }
    }
    $('#indicator.form-control').html($.unique(items.sort()).join(''));
}
function togglenodes(){
    var prefix =[ 
        gCountry+' Weighbridge',
        gCountry+' Border Posts',
        gCountry+' Seaways',
        gCountry+' Railway Stations'
    ];
    for (i in prefix){
        var lyr = Heron.App.getMap().getLayersByName(prefix[i])[0];
        if(lyr){
            lyr.setVisibility(true);
        }
    }
  
}

function getindicatorvalue(subnodeid,filterDate,indicatorView){
    $('#dateError').html('<span style="color:#15428B">loading... Please Wait</span>');
    $.post( "/api/getindicatorvalue.php", { 'subnodeid': $.trim(subnodeid) , 'filterDate': filterDate,'indicatorview': $.trim(indicatorView) })
        .done(function( data ) {
            if (data == "Sorry, no data found."){
                alert(data);
            }else{
                var obj = $.parseJSON(data);
                console.log(obj);
                if (gIndicator && gNode) {

                        var prefix =[ 
                            gCountry+' Weighbridge',
                            gCountry+' Border Posts',
                            gCountry+' Seaways',
                            gCountry+' Railway Stations'
                        ];
                        for (i in prefix){
                            var lyr = Heron.App.getMap().getLayersByName(prefix[i])[0];
                            if(lyr){
                                lyr.setVisibility(true);
                                var feat = lyr.getFeaturesByAttribute('Name', gNode)[0];
                                if (feat){
                                    //var rad = Math.floor((Math.random() * 100) + 1);
                                    feat.attributes.Indicator = obj[0].stdhours;
                                    feat.attributes.HTML = '' + gIndicator + ': ' + obj[0].stdhours + '%';
                                    drawcircle(feat);
                                }
                            }
                        }                    
                }
            }
        })
        .always(function() {
            $('#dateError').html('');
    });
}

$(document).ready(function() {
    //getvalues fromdb
    $.getJSON( "/api/getnodeinicators.php", function( data ) {
        gFilters = data;
        //console.log(data);

    }
    );
    //initial load Countries
    setTimeout(function() {
        loadCountryfilter();
    },2000);
    $('#country.form-control').on('change', function() {
        gCountry = $(this).val();
        //clear others
        $('#mode_of_transport.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#route.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#route_section.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#node.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#subnode.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator_category.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        //load
        loadModeofTransportfilter(gCountry);
        //Zoom to Country
        zoomtoCountryfid(gCountry);
    });
    $('#mode_of_transport.form-control').on('change', function() {
        gModeofTransport = $(this).val();
        //clear others
        $('#route.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#route_section.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#node.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#subnode.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator_category.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        //load
        loadRoutesfilter(gCountry, gModeofTransport);
        if (gModeofTransport == 'Road') {
            var prefix = gCountry+' Roads';
        } else if (gModeofTransport == 'Rail') {
            var prefix = gCountry+' Railway';
        } else if (gModeofTransport == 'Sea') {
            var prefix = gCountry+' Seaways';
        }
        var lyr = Heron.App.getMap().getLayersByName(prefix)[0];
        if(lyr){
            gLyr =lyr
            lyr.setVisibility(true);
            setTimeout(function() {
                Heron.App.getMap().zoomToExtent(lyr.getDataExtent());
            },2000);
        }


    });
    $('#route.form-control').on('change', function() {
        gRoute = $(this).val();
        //clear others
        $('#route_section.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#node.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#subnode.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator_category.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
       //load
        //loadRouteSectionfilter(gCountry, gModeofTransport, gRoute);
        loadNodefilter(gCountry, gModeofTransport, gRoute);
        //Zoom to Route Section
        //zoomtoRoadfid(gRouteSection);
    });
    /*
    $('#route_section.form-control').on('change', function() {
        gRouteSection = $(this).val();
        //clear others
        $('#node.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#subnode.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator_category.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
       //load
        loadNodefilter(gCountry, gModeofTransport, gRoute);
        //Zoom to Route Section
        zoomtoRoadfid(gRouteSection);
    });
    */
    $('#node.form-control').on('change', function() {
        gNode = $(this).val();
        //clear others
        $('#subnode.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator_category.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        //load
        loadSubNodefilter(gCountry, gModeofTransport, gRoute, gNode);
        //zoom to Node
        if (gNode == '[Entire Section]') {
            zoomtoRoadfid(gRouteSection);
        } else {
            zoomtoNodefid(gNode);
        }
    });

    $('#subnode.form-control').on('change', function() {
        gSubNode = $(this).val();
        //clear others
        $('#indicator_category.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        $('#indicator.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        //load
        loadIndicatorCategoryfilter(gCountry, gModeofTransport, gRoute, gNode, gSubNode);
        //zoom to Node
        zoomtoNodefid(gNode);
    });

    $('#indicator_category.form-control').on('change', function() {
        gIndicatorCategory = $(this).val();
        //clear others
        $('#indicator.form-control').html('<option value="" selected="selected" >-- Nothing Loaded yet --</option>');
        //load
        loadIndicatorfilter(gCountry, gModeofTransport, gRoute, gNode, gSubNode, gIndicatorCategory);
    });
    $('#indicator.form-control').on('change', function() {
        gIndicator = $(this).val();
    });
    //Datepicker 
    $('#filterdatepicker.date').datepicker({
        format: 'yyyy-m',
        endDate: new Date(),
        minViewMode: 1      
    }).on('changeDate', function(e) {
        var filterDate = $('input:input[name=filterdate]').val();
        if (gSubNode && filterDate && gIndicator) {
            $('#dateError').html('');
            getindicatorvalue(gSubNode,filterDate,gIndicator);
        }
    });
    //Comparion Mode
    $('.compare').hide()
    $('input[type="checkbox"][name="nodecomparison"]').change(function() {
        if (this.checked) {
            //findnodes("Kenya","Road","Weighed Compliance")
            $('.compare').show();
            $('.noncompare').hide();
        } else {
            $('.compare').hide();
            $('.noncompare').show();
        }
    });
});


