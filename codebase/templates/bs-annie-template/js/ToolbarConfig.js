/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */




/** api: example[searchbydraw]
 *  Search features by drawing on Map
 *  ---------------------------------
 *  Search features by drawing geometries on the map.
 */


/** This config assumes the DefaultOptionsWorld.js to be included first!! */
Ext.namespace("Heron.examples");

/** Create a config for the search panel. This panel may be embedded into the accordion
 * or bound to the "find" button in the toolbar. Here we use the toolbar button.
 */
Heron.examples.searchPanelConfig = {
	xtype: 'hr_searchcenterpanel',
	hropts: {
		searchPanel: {
		    xtype: 'hr_searchbydrawpanel',
			id: 'hr-searchbydrawpanel',
			header: false,
            border: false,
			style: {
				fontFamily: 'Verdana, Arial, Helvetica, sans-serif',
				fontSize: '12px'
			}
		},
		resultPanel: {
			xtype: 'hr_featuregridpanel',
			id: 'hr-featuregridpanel',
			header: false,
            border: false,
			autoConfig: true,
            exportFormats: ['CSV', 'XLS', 'GMLv2', 'GeoJSON', 'WellKnownText', 'Shapefile'],
			hropts: {
				zoomOnRowDoubleClick: true,
				zoomOnFeatureSelect: false,
				zoomLevelPointSelect: 8,
				zoomToDataExtent: false
			}
		}
	}
};


// See ToolbarBuilder.js : each string item points to a definition
// in Heron.ToolbarBuilder.defs. Extra options and even an item create function
// can be passed here as well. By providing a "create" function your own toolbar
// item can be added.
// For menu's and other standard ExtJS Toolbar items, the "any" type can be
// used. There you need only pass the options, similar as in the function
// ExtJS Toolbar.add().
Heron.options.map.toolbar = [{
        type: "scale",
        options: {
            width: 110
        }
    }, {
        type: "-"
    }, {
        type: "featureinfo",
        options: {
            pressed: true,
            popupWindow: {
                width: 360,
                height: 200,
                featureInfoPanel: {
                    showTopToolbar: true,

                    // Should column-names be capitalized? Default true.
                    columnCapitalize: true,

                    // displayPanels option values are 'Table' and 'Detail', default is 'Table'
                    // displayPanels: ['Table', 'Detail']
                    // Export to download file. Option values are 'CSV', 'XLS', default is no export (results in no export menu).
                    // 'GeoPackage' needs heron.cgi with GDAL 1.1+ !!
                    exportFormats: ['CSV', 'XLS', 'GMLv2', 'Shapefile', 'GeoPackage', 'GeoJSON', 'WellKnownText'],
                    maxFeatures: 10
                }
            }
        }
    }, {
        type: "-"
    }, {
        type: "pan"
    }, {
        type: "zoomin"
    }, {
        type: "zoomout"
    }, {
        type: "zoomvisible"
    },
    //{type: "coordinatesearch", options: {onSearchCompleteZoom: 8, fieldLabelX: 'lon', fieldLabelY: 'lat'}},
    {
        type: "-"
    }, {
        type: "zoomprevious"
    }, {
        type: "zoomnext"
    }, {
        type: "-"
    }, {
        type: "measurelength",
        options: {
            geodesic: true
        }
    }, {
        type: "measurearea",
        options: {
            geodesic: true
        }
    }, {
        type: "-"
    }, {
        type: "printdialog",
        options: {
            url: 'http://127.0.0.1/geoserver/pdf'
                /*, showTitle: true
                , mapTitle: 'Northern Corridor Transit and Transport Coordination Authority'
                , mapTitleYAML: "mapTitle"       // MapFish - field name in config.yaml - default is: 'mapTitle'
                , showComment: true
                , mapComment: 'My Comment - Print Dialog'
                , mapCommentYAML: "mapComment"   // MapFish - field name in config.yaml - default is: 'mapComment'
                , showFooter: true
                , mapFooter: 'My Footer - Print Dialog'
                , mapFooterYAML: "mapFooter"     // MapFish - field name in config.yaml - default is: 'mapFooter'
                , printAttribution: true         // Flag for printing the attribution
                , mapAttribution: null           // Attribution text or null = visible layer attributions
                , mapAttributionYAML: "mapAttribution" // MapFish - field name in config.yaml - default is: 'mapAttribution'
                , showOutputFormats: true
                , showRotation: true
                , showLegend: true
                , showLegendChecked: true
                , mapLimitScales: false
                , mapPreviewAutoHeight: true
                , mapPreviewHeight: 400 */
        }
    }, {
        type: "-"
   },/*  {
        type: "searchcenter",
        // Options for SearchPanel window
        options: {
            //show: true,

            searchWindow: {
                title: 'Search',
                x: 100,
                y: undefined,
                width: 360,
                height: 440,
                items: [
                    Heron.examples.searchPanelConfig
                ]
            }
        }
    },*/
    //{type: "addbookmark"},
    //{type: "help", options: {tooltip: 'Help and info for this example'}},
    {type: "any",
        options: {
          //showText: false,
          id: "popiframe",
          iconCls: "icon-expand",
          tooltip: "Pop Out Map",
          text: "",
          allowDepress: true,
          handler: function() {
            window.open(document.URL, '_blank', 'location=yes,height=700,width=1300,scrollbars=yes,status=yes');
        }
    }
    }

];;
