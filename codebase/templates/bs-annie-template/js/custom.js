$(document).ready(function()
{

//Tooltips
/*$.widget.bridge('uibutton', $.ui.button);
$.widget.bridge('uitooltip', $.ui.tooltip);*/

$("[data-toggle=tooltip]").tooltip({placement: 'right'});
$("[data-toggle=popover]").popover({placement: 'right',trigger: 'hover'});
//Tooltips

//Document datepicker
$( "#from" ).datepicker({
		dateFormat: "yy-mm-dd",
		defaultDate: "+1w",
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate );
		}
});
$( "#to" ).datepicker({
		dateFormat: "yy-mm-dd",
		defaultDate: "+1w",
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
});
//Document datepicker

//Bootstrap multiselect
/*$('.selectpicker').selectpicker({
      style: 'btn-info',
      size: 4
  });*/
  //$('.multiselect').multiselect();
  $('.multiselect').multiselect({
  
        onChange: function(option, checked, select) {
        	if( $(option).closest(".month").length )
        	{
        		// Get selected options.
        		selectedOptions = $('.month option:selected').length;
        		if(selectedOptions > 0) {
				$('#quarter.multiselect').multiselect('deselectAll', true);
				$('#quarter.multiselect').multiselect('disable');
			}
			else
			{
				$('#quarter.multiselect').multiselect('enable');
			}
            	}
            	
            	if( $(option).closest(".quarter").length )
        	{
        		// Get selected options.
        		selectedOptions = $('.quarter option:selected').length;
        		if(selectedOptions > 0) {
				$('#monthid.multiselect').multiselect('deselectAll', true);
				$('#monthid.multiselect').multiselect('disable');
			}
			else
			{
				$('#monthid.multiselect').multiselect('enable');
			}
            	}
        }
    });
//Bootstrap multiselect

//fetch more news
$("form#load-more-news").submit(function(event){
  event.preventDefault();
 
  //grab all form data
  data = new FormData();
  data.append( 'offset', $( '#offset' ).val() );
 
  $.ajax({
    url: 'ajax_news.php',
    type: 'POST',
    data: data,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    error: function (xhr, status) {
    	    if(status='error'){
    	    	    data=JSON.parse(result);
    	    	    
    	    	    console.log(data['message']);
    	    	    
    	    }
    },
    success: function (result) {
    	    data=JSON.parse(result);console.log(data['success']);
    	    if(data['success'] == 'true')
    	    {
    	    	  $('#show-posts').append(data['string']);  
    	    	  inputoffset = $('#offset').val();
    	    	  $('#offset').val(parseInt(inputoffset) + 5);
    	    }
    	    else
    	    {
    	    	    $('#load-more-news').html('<center> <h4> No more items to show </h4> </center>');
    	    }
    }
  });
 
  return false;
});
//fetch more news


});//End document.ready
