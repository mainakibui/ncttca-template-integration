/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Defines settings for the Heron App layout wihtin Layout.js.
 *
 * The layout specifies a hierarchy of ExtJS (Panel) and GeoExt and Heron MC components.
 * For convenience specific settings within this layout are defined here
 * for structuring and reuse purposes.
 *
 **/

OpenLayers.Util.onImageLoadErrorColor = "transparent";
OpenLayers.ProxyHost = "/cgi-bin/proxy.cgi?url=";
Ext.BLANK_IMAGE_URL = 'http://cdnjs.cloudflare.com/ajax/libs/extjs/3.4.1-1/resources/images/default/s.gif';
OpenLayers.DOTS_PER_INCH = 25.4 / 0.28

var mapPanel, legendPanel, layerTree, vectorNodes, vectorRoad, vectorCoutries, mapPopup, printProvider, gFilters;
var gCountry, gModeofTransport, gRoute, gRouteSection, gNode, gSubNode, gSubNodeid, gIndicatorCategory, gIndicator, gSubIndicator, gFID, gLoc, gLyr, Noderules, Roadrules, RoadQualityrules, Countryrules;

//Style Rules
Townrules = [
    new OpenLayers.Rule({
        title: 'Town',
        symbolizer: {
            'externalGraphic': 'images/makers/festival.png',
            pointRadius: 10
        }
    })
]
Borderpostrules = [
    new OpenLayers.Rule({
        title: 'Border Post',
        symbolizer: {
            'externalGraphic': 'images/makers/boardercross.png',
            pointRadius: 10
        }
    })
]
Weighbridgerules = [
    new OpenLayers.Rule({
        title: 'Weighbridge',
        symbolizer: {
            'externalGraphic': 'images/makers/bridge_old.png',
            pointRadius: 10
        }
    })
]
Seaportrules = [
    new OpenLayers.Rule({
        title: 'Sea Port',
        symbolizer: {
            'externalGraphic': 'images/makers/boatcrane.png',
            pointRadius: 10
        }
    })
]
Railwaystationrules = [
    new OpenLayers.Rule({
        title: 'Railway Station',
        symbolizer: {
            'externalGraphic': 'images/makers/train.png',
            pointRadius: 10,
        }
    })
]

Noderules = [
    new OpenLayers.Rule({
        title: 'Town',
        filter: new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.EQUAL_TO,
            property: 'nodetype',
            value: 'Town'
        }),
        symbolizer: {
            'externalGraphic': 'images/makers/bigcity.png',
            pointRadius: 10
        }
    }),
    new OpenLayers.Rule({
        title: 'Weighbridge',
        filter: new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.EQUAL_TO,
            property: 'nodetype',
            value: 'Weighbridge'
        }),
        symbolizer: {
            'externalGraphic': 'images/makers/truck3.png',
            pointRadius: 10
        }
    }),
    new OpenLayers.Rule({
        title: 'Sea Port',
        filter: new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.EQUAL_TO,
            property: 'nodetype',
            value: 'Sea Port'
        }),
        symbolizer: {
            'externalGraphic': 'images/makers/boatcrane.png',
            pointRadius: 10
        }
    }),
    new OpenLayers.Rule({
        title: 'Border Post',
        filter: new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.EQUAL_TO,
            property: 'nodetype',
            value: 'Border Post'
        }),
        symbolizer: {
            'externalGraphic': 'images/makers/boardercross.png',
            pointRadius: 10
        }
    })

];
Railwayrules = [
    new OpenLayers.Rule({
        title: 'Railway',
        symbolizer: {
            fillColor: '#99ccff',
            fillOpacity: 1,
            strokeColor: '#172C00',
            strokeWidth: 2,
            strokeOpacity: 1
            //strokeDashstyle: 'dot'
        }
    })
];
Roadrules = [
    new OpenLayers.Rule({
        title: 'Roads',
        symbolizer: {
            fillColor: '#99ccff',
            fillOpacity: 0.2,
            strokeColor: '#A82025',
            strokeWidth: 2,
            strokeOpacity: 0.8,
            strokeDashstyle: 'solid'
        }
    })
];
RoadQualityrules = [
    new OpenLayers.Rule({
        title: '100 - 81%',
        //minScaleDenominator: 3000000,
        filter: new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.BETWEEN,
            property: 'Indicator',
            upperBoundary: 100,
            lowerBoundary: 81
        }),
        symbolizer: {
            fillColor: '#99ccff',
            strokeColor: '#845151',
            strokeWidth: 2
        }
    }),
    new OpenLayers.Rule({
        title: '80 - 61%',
        //minScaleDenominator: 3000000,
        filter: new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.BETWEEN,
            property: 'Indicator',
            upperBoundary: 80,
            lowerBoundary: 61
        }),
        symbolizer: {
            fillColor: '#6699cc',
            strokeColor: '#A33D3D',
            strokeWidth: 2
        }
    }),
    new OpenLayers.Rule({
        title: '60 - 41%',
        //minScaleDenominator: 3000000,
        filter: new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.BETWEEN,
            property: 'Indicator',
            upperBoundary: 60,
            lowerBoundary: 41
        }),
        symbolizer: {
            fillColor: '#6699cc',
            strokeColor: '#C12828',
            strokeWidth: 2
        }
    }),
    new OpenLayers.Rule({
        title: '40 - 21%',
        //minScaleDenominator: 3000000,
        filter: new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.BETWEEN,
            property: 'Indicator',
            upperBoundary: 40,
            lowerBoundary: 21
        }),
        symbolizer: {
            fillColor: '#6699cc',
            strokeColor: '#E01414',
            strokeWidth: 2
        }
    }),
    new OpenLayers.Rule({
        title: '20 - 1%',
        //minScaleDenominator: 3000000,
        filter: new OpenLayers.Filter.Comparison({
            type: OpenLayers.Filter.Comparison.BETWEEN,
            property: 'Indicator',
            upperBoundary: 20,
            lowerBoundary: 1
        }),
        symbolizer: {
            fillColor: '#6699cc',
            strokeColor: '#FF0000',
            strokeWidth: 3
        }
    })
];
Countryrules = [
    new OpenLayers.Rule({
        title: 'Country',
        symbolizer: {
            fillColor: '#ffcc66',
            fillOpacity: 0.2,
            strokeColor: '#cc6633',
            strokeWidth: 2,
            strokeOpacity: 0.1
        }
    })
];

/*
 * Common settings for MapPanel
 * These will be assigned as "hropts" within the MapPanel config
 */
Ext.namespace("Heron.options.map");
Heron.options.map.settings = {
    maxResolution: 456543.0399,
    numZoomLevels: 13,
    units: 'm',
    projection: new OpenLayers.Projection("EPSG:900913"),
    displayProjection: new OpenLayers.Projection("EPSG:4326"),
    resolutions: [860.160, 430.080, 215.040, 107.520, 53.760, 26.880, 13.440, 6.720, 3.360, 1.680, 0.840, 0.420, 0.210, 0.105, 0.0525],
    resolutions: [5445.9849047851562, 1222.9924523925781, 611.4962261962891,
        305.74811309814453, 152.87405654907226, 76.43702827453613,
        38.218514137268066, 19.109257068634033, 0.154628534317017
    ],
    maxExtent: '-180.0, -90.0, 180.0, 90.0',
    // center: '4.92, 52.35',
    //center: [3007709.0386724, -417632.25921929],
    center: new OpenLayers.LonLat(29.40,0.30).transform( new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913")),
    zoom: 5,
    xy_precision: 3,
    max_features: 10,
    //zoom: 1,
    theme: null,

    /**
     * Useful to always have permalinks enabled. default is enabled with these settings.
     * MapPanel.getPermalink() returns current permalink
     *
     **/
    permalinks: {
        /** The prefix to be used for parameters, e.g. map_x, default is 'map' */
        paramPrefix: 'map',

        /** Encodes values of permalink parameters ? default false*/
        encodeType: false,
        /** Use Layer names i.s.o. OpenLayers-generated Layer Id's in Permalinks */
        prettyLayerNames: true
    }

    /** You can always control which controls are to be added to the map. */
    /* controls : [
     new OpenLayers.Control.Attribution(),
     new OpenLayers.Control.ZoomBox(),
     new OpenLayers.Control.Navigation({dragPanOptions: {enableKinetic: true}}),
     new OpenLayers.Control.LoadingPanel(),
     new OpenLayers.Control.PanPanel(),
     new OpenLayers.Control.ZoomPanel(),
     new OpenLayers.Control.OverviewMap(),
     new OpenLayers.Control.ScaleLine({geodesic: true, maxWidth: 200})
     ] */
};

// TODO see how we can set/override Map OpenLayers Controls
//Heron.options.map.controls = [new OpenLayers.Control.ZoomBox(),
//          new OpenLayers.Control.ScaleLine({geodesic: true, maxWidth: 200})];
Ext.namespace("Heron.options.wfs");
Heron.options.wfs.downloadFormats = [{
        name: 'CSV',
        outputFormat: 'csv',
        fileExt: '.csv'
    }, {
        name: 'GML (version 2.1.2)',
        outputFormat: 'text/xml; subtype=gml/2.1.2',
        fileExt: '.gml'
    },
    //    {
    //        name: 'ESRI Shapefile (zipped)',
    //        outputFormat: 'SHAPE-ZIP',
    //        fileExt: '.zip'
    //    },
    {
        name: 'GeoJSON',
        outputFormat: 'json',
        fileExt: '.json'
    }
];

/*
 * Layers to be added to the map.
 * Syntax is defined in OpenLayers Layer API.
 * ("isBaseLayer: true" means the layer will be added as base/background layer).
 */
Heron.options.map.layers = [

    /*
     * ==================================
     *            BaseLayers
     * ==================================
     */
    //  May use new NASA WMTS : http://onearth.jpl.nasa.gov/wms.cgi?request=GetCapabilities
    new OpenLayers.Layer.Google('Google Terrain', {
        type: google.maps.MapTypeId.TERRAIN,
        'sphericalMercator': true,
        isBaseLayer: true,
        //attribution: '<a href=\'http://upande.com\'>powered by Upande</a>',
        transitionEffect: 'resize'
    }),
    new OpenLayers.Layer.Google('Google Streets', {
        'sphericalMercator': true,
        isBaseLayer: true,
        //attribution: '<a href=\'http://upande.com\'>powered by Upande</a>',
        transitionEffect: 'resize',
        visibility: false,
    }),
    new OpenLayers.Layer.Google('Google Hybrid', {
        type: google.maps.MapTypeId.HYBRID,
        'sphericalMercator': true,
        isBaseLayer: true,
        //attribution: '<a href=\'http://upande.com\'>powered by Upande</a>',
        transitionEffect: 'resize',
        visibility: false
    }),
    new OpenLayers.Layer.Google('Google Satellite', {
        type: google.maps.MapTypeId.SATELLITE,
        'sphericalMercator': true,
        isBaseLayer: true,
        //attribution: '<a href=\'http://upande.com\'>powered by Upande</a>',
        transitionEffect: 'resize',
        visibility: false,
    }),

    /*
     * ==================================
     *            Overlays
     * ==================================
     */
    new OpenLayers.Layer.Vector('Burundi', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#ffcc66',
                fillOpacity: 0.2,
                strokeColor: '#cc6633',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Countryrules
            }),
            'select': new OpenLayers.Style({
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            })
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:burundi&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('DR Congo', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#ffcc66',
                fillOpacity: 0.2,
                strokeColor: '#cc6633',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Countryrules
            }),
            'select': new OpenLayers.Style({
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            })
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:dr_congo&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Kenya', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#ffcc66',
                fillOpacity: 0.1,
                strokeColor: '#cc6633',
                strokeWidth: 2,
                strokeOpacity: 0.1
            }, {
                //rules: Countryrules
            }),
            'select': new OpenLayers.Style({
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            })
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Rwanda', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#ffcc66',
                fillOpacity: 0.2,
                strokeColor: '#cc6633',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Countryrules
            }),
            'select': new OpenLayers.Style({
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            })
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:rwanda&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('South Sudan', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#ffcc66',
                fillOpacity: 0.2,
                strokeColor: '#cc6633',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Countryrules
            }),
            'select': new OpenLayers.Style({
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            })
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:south_sudan&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Uganda', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#ffcc66',
                fillOpacity: 0.2,
                strokeColor: '#cc6633',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Countryrules
            }),
            'select': new OpenLayers.Style({
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            })
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:uganda&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),

    /* Roads */
    new OpenLayers.Layer.Vector('Kenya Roads', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Roadrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
                //labelYOffset: 20,
                //fontColor: 'black',
                //fontFamily: 'Arial',
                //fontSize: 15,
                //label: '${Name}' //Text entspricht feature.attributes.name
            },
            //'quality': new OpenLayers.Style({},{rules: Roadrules}),
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_road&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),    
    new OpenLayers.Layer.Vector('Uganda Roads', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Roadrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
                //labelYOffset: 20,
                //fontColor: 'black',
                //fontFamily: 'Arial',
                //fontSize: 15,
                //label: '${Name}' //Text entspricht feature.attributes.name
            },
            //'quality': new OpenLayers.Style({},{rules: Roadrules}),
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:Uganda_roads&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Burundi Roads', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Roadrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
                //labelYOffset: 20,
                //fontColor: 'black',
                //fontFamily: 'Arial',
                //fontSize: 15,
                //label: '${Name}' //Text entspricht feature.attributes.name
            },
            //'quality': new OpenLayers.Style({},{rules: Roadrules}),
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:burundi_road&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('South Sudan Roads', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Roadrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
                //labelYOffset: 20,
                //fontColor: 'black',
                //fontFamily: 'Arial',
                //fontSize: 15,
                //label: '${Name}' //Text entspricht feature.attributes.name
            },
            //'quality': new OpenLayers.Style({},{rules: Roadrules}),
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:south_sudan_roads&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('DR Congo Roads', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Roadrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
                //labelYOffset: 20,
                //fontColor: 'black',
                //fontFamily: 'Arial',
                //fontSize: 15,
                //label: '${Name}' //Text entspricht feature.attributes.name
            },
            //'quality': new OpenLayers.Style({},{rules: Roadrules}),
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:drc_roads&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Rwanda Roads', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Roadrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
                //labelYOffset: 20,
                //fontColor: 'black',
                //fontFamily: 'Arial',
                //fontSize: 15,
                //label: '${Name}' //Text entspricht feature.attributes.name
            },
            //'quality': new OpenLayers.Style({},{rules: Roadrules}),
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:rwanda_roads&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),

    /* Weighbridge */
    new OpenLayers.Layer.Vector('Kenya Weighbridge', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Weighbridgerules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_weighbrige&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Uganda Weighbridge', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Weighbridgerules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_weighbrige&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('South Sudan Weighbridge', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Weighbridgerules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_weighbrige&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Rwanda Weighbridge', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Weighbridgerules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_weighbrige&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('DR Congo Weighbridge', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Weighbridgerules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_weighbrige&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Burundi Weighbridge', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Weighbridgerules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_weighbrige&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),

    /* Boder Posts */
    new OpenLayers.Layer.Vector('Kenya Border Posts', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Borderpostrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_border_post&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Uganda Border Posts', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Borderpostrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_border_post&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('South Sudan Border Posts', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Borderpostrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_border_post&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Rwanda Border Posts', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Borderpostrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_border_post&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Burundi Border Posts', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Borderpostrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_border_post&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('DR Congo Border Posts', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Borderpostrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_border_post&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),

    /* Railway */
    new OpenLayers.Layer.Vector('Kenya Railway', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Railwayrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
            },
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_railway&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Uganda Railway', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Railwayrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
            },
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:Uganda_railway&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Burundi Railway', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Railwayrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
            },
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'gis_data/kenya_railway.json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('South Sudan Railway', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Railwayrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
            },
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'gis_data/kenya_railway.json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Rwanda Railway', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Railwayrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
            },
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'gis_data/kenya_railway.json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
new OpenLayers.Layer.Vector('DR Congo Railway', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fillColor: '#99ccff',
                fillOpacity: 0.2,
                strokeColor: '#666666',
                strokeWidth: 2,
                strokeOpacity: 0.8
            }, {
                rules: Railwayrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
                strokeWidth: 3,
            },
        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:DRC_Railway&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),

    /*Railway Station */
    new OpenLayers.Layer.Vector('Kenya Railway Stations', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Railwaystationrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_railway_station&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Uganda Railway Stations', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Railwaystationrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_railway_station&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('DR Congo Railway Stations', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Railwaystationrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_railway_station&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Burundi Railway Stations', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Railwaystationrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_railway_station&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('South Sudan Railway Stations', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Railwaystationrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_railway_station&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Rwanda Railway Stations', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Railwaystationrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_railway_station&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),

    /* Seaways */ 
    new OpenLayers.Layer.Vector('Kenya Seaways', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Seaportrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_seaways&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Uganda Seaways', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Seaportrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_seaways&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('South Sudan Seaways', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Seaportrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_seaways&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Burundi Seaways', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Seaportrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_seaways&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Rwanda Seaways', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Seaportrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_seaways&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('DR Congo Seaways', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Seaportrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_seaways&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),

    /* Towns */
    new OpenLayers.Layer.Vector('Kenya Towns', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Townrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:kenya_towns&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Uganda Towns', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Townrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'gis_data/kenya_towns.json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Burundi Towns', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Townrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:burundi_towns&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('Rwanda Towns', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Townrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'gis_data/kenya_towns.json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('South Sudan Towns', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Townrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'gis_data/kenya_towns.json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),
    new OpenLayers.Layer.Vector('DR Congo Towns', {
        projection: new OpenLayers.Projection('EPSG:4326'),
        //visibility: false,
        strategies: [
            new OpenLayers.Strategy.Fixed()
        ],
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({}, {
                rules: Townrules
            }),
            'select': {
                fillColor: '#8aeeef',
                strokeColor: '#32a8a9',
            }
            //Text entspricht feature.attributes.name

        }),
        protocol: new OpenLayers.Protocol.HTTP({
            url: 'http://127.0.0.1/geoserver/ncttca/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ncttca:DRC_towns&maxFeatures=50&outputFormat=application/json',
            headers: {
                'Accept': 'application/json'
            },
            format: new OpenLayers.Format.GeoJSON()
        })
    }),

    /*
        new OpenLayers.Layer.WMS(
                "World Soil Resources (FAO)",
                'http://data.fao.org/geoserver/ows?',
                {layers: "GEONETWORK:wsres25_1111", transparent: true, format: 'image/png'},
                {singleTile: true, opacity: 0.9, isBaseLayer: false, visibility: false, noLegend: false, transitionEffect: 'resize', metadata: {
                    wfs: {
                        protocol: 'fromWMSLayer'
                    }
                }}
        ),
        new OpenLayers.Layer.WMS(
                "Global Ecological Zones (FAO)",
                'http://data.fao.org/geoserver/ows?',
                {layers: "GEONETWORK:eco_zone_1255", transparent: true, format: 'image/png'},
                {singleTile: true, opacity: 0.9, isBaseLayer: false, visibility: false, noLegend: false, transitionEffect: 'resize'}
        ),
        new OpenLayers.Layer.WMS(
                "World Cities (FAO)",
                'http://data.fao.org/geoserver/ows?',
                {layers: "GEONETWORK:esri_cities_12764", transparent: true, format: 'image/png'},
                {singleTile: true, opacity: 0.9, isBaseLayer: false, visibility: true, noLegend: false, featureInfoFormat: 'application/vnd.ogc.gml', transitionEffect: 'resize', metadata: {
                    wfs: {
                        protocol: 'fromWMSLayer',
                        downloadFormats: Heron.options.wfs.downloadFormats
                    }
                }}
        ),
        new OpenLayers.Layer.WMS(
                "World Cities (OpenGeo)",
                'http://suite.opengeo.org/geoserver/ows?',
                {layers: "cities", transparent: true, format: 'image/png'},
                {singleTile: true, opacity: 0.9, isBaseLayer: false, visibility: false, noLegend: false, featureInfoFormat: 'application/vnd.ogc.gml', transitionEffect: 'resize',
                    metadata: {
                        wfs: {
                            protocol: 'fromWMSLayer',
                            featurePrefix: 'world',
                            featureNS: 'http://opengeo.org',
                            downloadFormats: Heron.options.wfs.downloadFormats
                        }
                    }}
        ),
        new OpenLayers.Layer.Vector("USA States (OpenGeo, WFS)", {
            minScale: 15000000,
            strategies: [new OpenLayers.Strategy.BBOX()],
            styleMap: new OpenLayers.StyleMap(
                    {'strokeColor': '#222222', 'fillColor': '#eeeeee', graphicZIndex: 1, fillOpacity: 0.8}),
            visibility: true,
            protocol: new OpenLayers.Protocol.WFS({
                url: 'http://suite.opengeo.org/geoserver/ows?',
                featurePrefix: 'usa',
                featureType: "states",
                featureNS: 'http://census.gov'
            })
        }),
        new OpenLayers.Layer.WMS(
                "USA States (OpenGeo)",
                'http://suite.opengeo.org/geoserver/ows?',
                {layers: "states", transparent: true, format: 'image/png'},
                {singleTile: true, opacity: 0.9, isBaseLayer: false, visibility: false, noLegend: false, featureInfoFormat: 'application/vnd.ogc.gml', transitionEffect: 'resize', metadata: {
                    wfs: {
                        protocol: 'fromWMSLayer',
                        featurePrefix: 'usa',
                        featureNS: 'http://census.gov',
                        downloadFormats: Heron.options.wfs.downloadFormats
                    }
                }
                }
        ),
    */
    /* No feature info, strange GML response from KNMI...ESRI? */
    /*
    new OpenLayers.Layer.WMS(
            "Meteosat Precipitation",
            'http://msgcpp-ogc-realtime.knmi.nl/msgrt.cgi?',
            {layers: "lwe_precipitation_rate", transparent: true, format: 'image/png'},
            {singleTile: true, opacity: 0.6, isBaseLayer: false, visibility: false, noLegend: false, transitionEffect: 'resize'}
    )
    */
    /* FOR DEBUGGING ESRI GFI !
     new OpenLayers.Layer.WMS(
     "Coastal Conditions",
     'http://arcserve.lawr.ucdavis.edu/arcgis/services/CSMW/Coastal_Conditions/MapServer/WMSServer?',
     {layers: "Coastal Conditions", transparent: true, format: 'image/png'},
     {singleTile: true, opacity: 0.9, isBaseLayer: false, visibility: false, noLegend: false, featureInfoFormat: 'application/vnd.esri.wms_featureinfo_xml', transitionEffect: 'resize'}
     ) */
];

