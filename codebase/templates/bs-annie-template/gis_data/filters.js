var filters = [
  {
    'Country': 'Burundi',
    'Mode of Transport': {
      'Road': {
        'Route Section': {
        },
      },
      'Rail': {
        'Route Section': {
        },
      },
      'Sea': {
        'Route Section': {
        },
      },
    }
  },
  {
    'Country': 'DR Congo',
    'Mode of Transport': {
      'Road': {
        'Route Section': {
        },
      },
      'Rail': {
        'Route Section': {
        },
      },
      'Sea': {
        'Route Section': {
        },
      },
    }
  },
  {
    'Country': 'Kenya',
    'Mode of Transport': {
      'Road': {
        'Route Section': {
          'Mombasa-Mariakani': {
            'Port of Mombasa': {
              'Time and Delays': [
                {
                  'indicator': 'Vessel waiting time before Birth'
                },
                {
                  'indicator': 'Vessel turn around time'
                },
                {
                  'indicator': 'DPC time'
                },
                {
                  'indicator': 'One Stop Center time'
                },
                {
                  'indicator': 'Time Taken after Customs Release'
                },
                {
                  'indicator': 'Mombasa port cargo dwell time'
                },
              ],
              'Volume and Capacity': [
                {
                  'indicator': 'Mombasa port cargo throughput',
                },
              ],
              'Efficiency and Productivity': [
                {
                  'indicator': 'Gross Moves Per Crane per Hour'
                },
              ],
            },
            'Mariakani Weighbridge': {
              'Time and Delays': [
                {
                  'indicator': 'Weigh-bridge Crossing time'
                },
              ],
              'Volume and Capacity': [
                {
                  'indicator': 'Weighed Traffic'
                },
              ],
              'Efficiency and Productivity': [
                {
                  'indicator': 'Weighed Compliance'
                },
              ],
            },
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Mariakani-Mtito Andei': {
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Mtito Andei-Athi River': {
            'Athi River Weighbridge': {
              'Time and Delays': [
                {
                  'indicator': 'Weigh-bridge Crossing time'
                },
              ],
              'Volume and Capacity': [
                {
                  'indicator': 'Weighed Traffic'
                },
              ],
              'Efficiency and Productivity': [
                {
                  'indicator': 'Weighed Compliance'
                },
              ],
            },
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Athi River-Nairobi': {
            'Nairobi ICD': {
              'Time and Delays': [
                {
                  'indicator': 'Termimal Dwell Time'
                },
              ],
              'Volume and Capacity': [
                {
                  'indicator': 'Termimal Throughput'
                },
              ],
            },
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Nairobi-Naivasha': {
            'Gilgil Weighbridge': {
              'Time and Delays': [
                {
                  'indicator': 'Weigh-bridge Crossing time'
                },
              ],
              'Volume and Capacity': [
                {
                  'indicator': 'Weighed Traffic'
                },
              ],
              'Efficiency and Productivity': [
                {
                  'indicator': 'Weighed Compliance'
                },
              ],
            },
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Naivasha-Nakuru': {
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Nakuru-Eldoret': {
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Eldoret-Webuye': {
            'Webuye Weighbridge': {
              'Time and Delays': [
                {
                  'indicator': 'Weigh-bridge Crossing time'
                },
              ],
              'Volume and Capacity': [
                {
                  'indicator': 'Weighed Traffic'
                },
              ],
              'Efficiency and Productivity': [
                {
                  'indicator': 'Weighed Compliance'
                },
              ],
            },
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Webuye-Malaba': {
            'Malaba Border Post': {
              'Time and Delays': [
                {
                  'indicator': 'Border Crossing Time (KE)'
                },
              ],
            },
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Nakuru-Kericho': {
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Kericho-Kisumu': {
            'Kisumu ICD': {
              'Time and Delays': [
                {
                  'indicator': 'Termimal Dwell Time'
                },
              ],
              'Volume and Capacity': [
                {
                  'indicator': 'Termimal Throughput'
                },
              ],
            },
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Kisumu-Maseno': {
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Maseno-Busia': {
            'Busia Border Post': {
              'Time and Delays': [
                {
                  'indicator': 'Border Crossing Time (KE)'
                },
              ],
            },
            '[Entire Section]': {
              'Efficiency and Productivity': [
                {
                  'indicator': 'Quality of Infrastructure'
                },
              ],
            },
          },
          'Mombasa-Malaba': {
            '[Entire Section]': {
              'Time and Delays': [
                {
                  'indicator': 'Transit time in Kenya (through Malaba)'
                },
              ],
              'Efficiency and Productivity': [
                {
                  'indicator': 'Reasons for Delay'
                },
              ],
            },
          },
          'Mombasa-Busia': {
            '[Entire Section]': {
              'Time and Delays': [
                {
                  'indicator': 'Transit time in Kenya (through Busia)'
                },
              ],
              'Efficiency and Productivity': [
                {
                  'indicator': 'Reasons for Delay'
                },
              ],
            },
          },
        }
      },
      'Rail': {
        'Route Section': {
        },
      },
      'Sea': {
        'Route Section': {
        },
      },
    }
  },
  {
    'Country': 'Rwanda',
    'Mode of Transport': {
      'Road': {
        'Route Section': {
        },
      },
      'Rail': {
        'Route Section': {
        },
      },
      'Sea': {
        'Route Section': {
        },
      },
    }
  },
  {
    'Country': 'South Sudan',
    'Mode of Transport': {
      'Road': {
        'Route Section': {
        },
      },
      'Rail': {
        'Route Section': {
        },
      },
      'Sea': {
        'Route Section': {
        },
      },
    }
  },
  {
    'Country': 'Uganda',
    'Mode of Transport': {
      'Road': {
        'Route Section': {
        },
      },
      'Rail': {
        'Route Section': {
        },
      },
      'Sea': {
        'Route Section': {
        },
      },
    }
  },
  
]
