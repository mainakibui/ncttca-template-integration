<?php include_once('header.php');?>  
    
    <!-- Begin page content -->
    
    <div class="panel1">

	<div class="row">
	
		<div class="col-sm-6 col-md-6 col-lg-6 dashboard">
		
			<div class="col-sm-12 col-md-12 col-lg-12 text-center panel-headers">
				<h3>Quick Navigation</h3>
			</div>
	    
			<div class="row">
					
				<div id="content" class="col-sm-12 col-md-12 col-lg-12">
		
					<!--menu item-->
					<a href="#">
						<div class="feature data-masonry mason-item box" >
							<div class="dashboard-item dashboard-quick-link text-center">
								<div class="dashboard-quick-link-image">
									<i class="fa fa-th fa-3x"></i>
								</div>
								<div class="dashboard-quick-link-text">
									<h3>Dashboard</h3>
									<p>some text here</p>
								</div>
							</div>
						</div>
					</a>
					<!--/menu item-->
					
					<!--menu item-->
					<a href="indicators.php">
						<div class="feature data-masonry mason-item box" >
							<div class="dashboard-item dashboard-quick-link text-center">
								<div class="dashboard-quick-link-image">
									<i class="fa fa-tasks fa-3x"></i>
								</div>
								<div class="dashboard-quick-link-text">
									<h3>Indicators</h3>
									<p>some text here</p>
								</div>
							</div>
						</div>
					</a>
					<!--/menu item-->
					
					<!--menu item-->
					<a href="gis.php">
						<div class="feature data-masonry mason-item box" >
							<div class="dashboard-item dashboard-quick-link text-center">
								<div class="dashboard-quick-link-image">
									<i class="fa fa-map-marker fa-3x"></i>
								</div>
								<div class="dashboard-quick-link-text">
									<h3>GIS</h3>
									<p>some text here</p>
								</div>
							</div>
						</div>
					</a>
					<!--/menu item-->
					
					<!--menu item-->
					<a href="#">
						<div class="feature data-masonry mason-item box" >
							<div class="dashboard-item dashboard-quick-link text-center">
								<div class="dashboard-quick-link-image">
									<i class="fa fa-file-word-o fa-3x"></i>
								</div>
								<div class="dashboard-quick-link-text">
									<h3>Documents</h3>
									<p>some text here</p>
								</div>
							</div>
						</div>
					</a>
					<!--/menu item-->
					
				</div>
			</div><!--/row-->
			
		</div><!--/dashboard collumn-->
		
		<div class="col-sm-6 col-md-6 col-lg-6 dashboard">
			<div class="col-sm-12 col-md-12 col-lg-12 text-center">
				
				<!--slider-->
				<div class="row">
				
					<div class="row">
						<div class="col-md-9 hidden-xs  panel-headers">
							<h3>Featured</h3>
						</div>
						<div class="col-md-3">
							<!-- Controls -->
							<div class="controls pull-right hidden-xs">
							    <a class="carouselbtn left fa fa-chevron-left btn btn-primary" href="#carousel-example-generic" data-slide="prev"></a>
								<a class="carouselbtn right fa fa-chevron-right btn btn-primary" href="#carousel-example-generic" data-slide="next"></a>
							</div>
						</div>
					</div>
				
					<div id="carousel-example-generic" class="carousel slide hidden-xs" data-ride="carousel">
					
						<div class="carousel-inner"><!-- carousel-inner -->
						
							<div class="item active">
								<div class="row">
								
									<div class=" featured">
										<div class="col-sm-12 col-md-12">
											<div class="thumbnail">
												<img src="images/rail.jpg" data-src="images/rail.jpg" alt="featured image">
											</div>
										</div>
										
										<div class="col-sm-12 col-md-12">
											<div class="caption">
												<h3>Weekly Monitoring Of The Corridor Performance(28th May - 06th June)</h3>
												<p>Average time for custom clearance at the Document Processing slightly... <a href="#" class="btn btn-default" role="button">Readmore</a></p>
											</div>
										</div>
										<div style="clear:both;"></div>
									</div>
								
								</div><!--/item-->
							</div><!--/row-->
								
							<div class="item">
								<div class="row">
								
									<div class=" featured">
										<div class="col-sm-12 col-md-12">
											<div class="thumbnail">
												<img src="images/sea.jpg" data-src="images/sea.jpg" alt="featured image">
											</div>
										</div>
										
										<div class="col-sm-12 col-md-12">
											<div class="caption">
												<h3>Weekly Monitoring Of The Corridor Performance(28th May - 06th June)</h3>
												<p>Average time for custom clearance at the Document Processing slightly... <a href="#" class="btn btn-default" role="button">Readmore</a></p>
											</div>
										</div>
										<div style="clear:both;"></div>
									</div>
								
								</div><!--/item-->
							</div><!--/row-->
						
						</div><!-- /carousel-inner -->
					</div><!-- /carousel-example-generic -->
				</div><!-- /row -->
				<!--/slider-->
				
			</div>
			
		</div><!--/dashboard collumn-->
		
	</div><!--/row-->
	
	
	</div><!--/panel1-->
	
	<div class="panel2">

		<div class="row">
		
			<div class="col-sm-12 col-md-12 col-lg-12">
				<form id="banner-form" method="post" action="banner-form.php">
					<div class="banner-optin">
						<div class="row">
							<div class="form-group col-md-4">
								<input name="banner-name" id="banner-name" type="text" class="form-control" required="" placeholder="Your Name">
							</div>
							<div class="form-group col-md-4">
								<input name="banner-email" id="banner-email" type="text" class="form-control" required="" placeholder="Your e-mail">
							</div>
							<div class="form-group col-md-4">
								<button type="submit" class="btn btn-default btn-submit">Subscribe</button>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-process"></div><!-- Displays status when submitting form -->
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div><!--/panel2-->

<?php include_once('indexjs.html');?>     
<?php include_once('footer.php');?>    

