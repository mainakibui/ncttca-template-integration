<?php
require_once("includes/config.php");
$mtitle = "Frequently Asked Questions";

display(faqs());


function faqs() {
	$str = $str."<div class='tab-content'>";
		$str = $str."<div class='row panel4 clearfix'>";
			$str = $str."<div class='col-sm-12 col-md-12 col-lg-12  nopadding main-inner-page-padding'>";
				$str = $str."<div class='col-sm-12 col-md-12 col-lg-12  article'>";
				$str = $str."<h3>FAQ</h3>";
					global $dba, $application;
					$mcount = 1;
					$sql = "select id, question, answer from `faqs` where statusid = 1 order by positionid asc;";
					$rs = $dba->execute($sql);
					
					if (!$rs->eof()) {
						$str = $str."<div class='col-sm-12 col-md-12 col-lg-12  nopadding'>";
						$str = $str."<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>";
						
						while(!$rs->eof()) {
				
						  //
						  $str = $str."<div class='panel panel-default'>";
							  $str = $str."<div class='panel-heading' role='tab' id='headingOne'>";
								  $str = $str."<h4 class='panel-title'>";
									  $str = $str."<a data-toggle='collapse' data-parent='#accordion' href='#collapse-".$rs->row(0)."' aria-expanded='true' aria-controls='collapseOne' class='collapsed'>";
										$str = $str.translate($rs->row(1));
									  $str = $str."</a>";
								  $str = $str."</h4>";
							  $str = $str."</div>";
						  
							  $str = $str."<div id='collapse-".$rs->row(0)."' class='panel-collapse collapse' role='tabpanel' aria-labelledby='headingOne' style='height: 0px;'>";
								  $str = $str."<div class='panel-body'>";
									$str = $str.translate($rs->row(2));
								  $str = $str."</div>";
							  $str = $str."</div>";
						  $str = $str."</div>";
						  //
						  
						  $mcount = $mcount + 1;
						  $rs->movenext();
						}
						
						$str = $str."</div>";
						$str = $str."<!--/Accordion-->";
						$str = $str."</div>";
					}
					else {
						$str = $str."<div class='col-sm-12 col-md-12 col-lg-12'>sorry, there are no frequently asked questions available at present.</div>";
					}
		
				$str = $str."</div>";
			$str = $str."</div>";
		$str = $str."</div>";
	$str = $str."</div>";	
	
	return $str;
} 

?>

