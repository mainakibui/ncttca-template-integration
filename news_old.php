<?php
require_once("includes/config.php");
$newsid = get_default(clean($_GET['newsid']), "n", 0);

$mtitle = "News Update";

display(viewnews($newsid));

function viewnews($newsid) {
    global $dba;
    if ($newsid == 0) {
		$sql = "select  id, title, summary, url, content, date, author from `news` where statusid = 1 order by date desc limit 30";
    }
    else{
		$sql = "select id, title, summary, url, content, date, author from `news` where id = $newsid order by date desc";
    } 
    $rs = $dba->execute($sql);
    if (!$rs->eof()){
		$str = $str."<table border=0 cellspacing=10 cellpadding=0 width='100%'>";
		while(!$rs->eof()) {
			$murl = "news.php?newsid=".$rs->row("id");
			if (strlen(trim($rs->row("url"))) > 0) {
				$murl = $rs->row("url");
			}
			if ($newsid == 0){
				$str = $str."<tr valign=top><td width='100%'>";
				$str = $str."<b><a href=\"$murl\"><font size=+1>".titlecase(translate($rs->row("title")))."</font></a></b>";
				if (strlen(trim($rs->row("author"))) > 0) {
					$str = $str."<br><b>Author:&nbsp;".translate($rs->row("author"))."</b>&nbsp;|&nbsp;";
				}
				$str = $str."<b>Date:&nbsp;".formatmydate($rs->row("date"))."</b>";
				$str = $str."<br><div>".translate($rs->row("summary"))."<a href=\"$murl\">...Read More</a></div></td></tr>";
				
			}
			else {
				$str = $str."<tr valign=top><td width='100%'><h2><font size=+1>".titlecase(translate($rs->row("title")))."</font></h2>";
				if (strlen(trim($rs->row("author"))) > 0) {
					$str = $str."<br><b>Author:&nbsp;".translate($rs->row("author"))."</b>&nbsp;|&nbsp;";
				}
				$str = $str."<b>Date:&nbsp;".formatmydate($rs->row("date"))."</b>";
				
				$str = $str."<br><br><div>".translate($rs->row("content"))."</div>";
				$str = $str."</td></tr>";
				$str = $str."<tr><td><div align=right style=\"padding-top:5px;\"><a href=\"news.php\"><b><font>".titlecase(translate("Back to News Page"))."</font></b></a></div></td></tr>";
				$str = $str."<tr><td>".disqus($rs->row("id"))."</td></tr>";
			}
			$rs->movenext();
			if (!$rs->eof()) {
				$str = $str."<tr valign=top><td width='100%' bgcolor=#e0e0e0><img src=\"images/margin.gif\" height=1></td></tr>";
			}
		} 
		$str = $str."</table>";
    }
    else{
    	$str = $str."<br>".messagebox("Sorry, there are no news articles available at present.",false);
    } 
    return $str;
}

function disqus($nid) {
	$str = $str."<div id=\"disqus_thread\"></div>\n";
	$str = $str."<script type=\"text/javascript\">\n";
	$str = $str."var disqus_shortname = 'ncttcatopnews';\n";
	$str = $str."var disqus_identifier = '$nid';\n";
	$str = $str."(function() {\n";
		$str = $str."var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;\n";
		$str = $str."dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';\n";
		$str = $str."(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);\n";
	$str = $str."})();\n";
	$str = $str."</script>\n";
	$str = $str."<noscript>Please enable JavaScript to view the <a href=\"http://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>";
	$str = $str."<a href=\"http://disqus.com\" class=\"dsq-brlink\">News comments powered by <span class=\"logo-disqus\">Disqus</span></a>";
	return $str;
}

?>
