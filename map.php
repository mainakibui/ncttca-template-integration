<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
    </style>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBeMQzyx95lS2q0ZHfd-TQ3XRbAENcKdY8&sensor=false">
    </script>
    <script type="text/javascript">
    var map;
	function initialize() {
		var myOptions = {
		  center: new google.maps.LatLng(-0.439449,33.991699),
		  zoom: 6,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	}
	function placeMarker(lat, lng) {
			alert(lat + ' = ' +lng);
			var location = new google.maps.LatLng(lat, lng);
			var marker = new google.maps.Marker({
			  position: location,
			  map: map
			});
			map.setCenter(location);
		}
    </script>
  </head>
  <body onload="initialize()">
    <div id="map_canvas" style="width:720px; height:500px;"></div>
	<div><a href="#" onclick="placeMarker('-4.05102156', '39.68919514');">Place Marker</a></div>
  </body>
</html>
